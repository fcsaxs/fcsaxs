#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Aug 13 15:21:22 2023

@author: kinanti_a
"""

from manim import *

import numpy as np

        

class AzimuthalIntegration(Scene):
    def construct(self):
        self.camera.background_color = WHITE

        corona= ImageMobject("/mnt/nas_Uwrite/AK54/KinanSAXSfiles/Kinantifffromporespy/Paperwithcodes/fcsaxs/455_1_2dsaxspattern.png")
        corona.scale(0.7)
        corona.to_edge(LEFT, buff=0)
        self.add(corona)      
        
        title=Tex("Small Angle Scattering - Data Reduction",color=BLACK)
        title.scale(0.6)
        title.to_edge(LEFT, buff=0.3)
        title.shift(3.4*UP)
        self.add(title)
        copyright_kaliyah=Tex("Aliyah, K. 2023 with Manim v0.17.3",color=BLACK)
        copyright_kaliyah.scale(0.4)
        copyright_kaliyah.to_edge(RIGHT, buff=0.2)
        copyright_kaliyah.shift(3.4*DOWN)
        self.add(copyright_kaliyah)
        
        ax = Axes(
            x_range=[0, 3, 1],
            y_range=[-3, 10, 1],
            x_length=6,
            y_length=6,
            tips=False,
            axis_config={"include_numbers": True,"color":BLACK},
            y_axis_config={"include_numbers": True,"scaling": LogBase(custom_labels=True)},

        )
        l1=ax.get_x_axis()
        l1.numbers[:].set_color(BLACK)
        
        l=ax.get_y_axis()
        l.set_color(BLACK)
        
        def func(x):
            return 100*x**-4
        ax.scale(0.8)
        ax.to_edge(RIGHT,buff=1.1)
        l1,l2=ax.get_axis_labels("q","Intensity (q)").set_color(BLACK)
        self.play(Create(ax))
        self.play(Write(l1),Write(l2))
        


        circle = Circle(color=BLUE,radius=0.2)  # create a circle
        circle.shift(1.92*DOWN+3.312*LEFT)
        twopiq=Tex(r'$q_1$',font_size=40,color=BLUE).next_to(circle).shift(RIGHT*0.2)
        self.play(Create(circle))
        self.play(Write(twopiq))
        
        # a dot with respect to the axes
        q=0.05
        dot_axes = Dot(ax.coords_to_point(q, func(q)), color=BLUE)
        lines = ax.get_lines_to_point(ax.c2p(q,func(q))).set_color(BLACK)
        twopiq=Tex(r'$q_1$',font_size=40,color=BLUE).next_to(dot_axes)
        self.add(dot_axes,lines,twopiq)
        self.wait()
        
        
        circle = Circle(color=PINK,radius=0.4)  # create a circle
        circle.shift(1.92*DOWN+3.312*LEFT)
        twopiq=Tex(r'$q_2$',font_size=40,color=PINK).next_to(circle).next_to(circle).shift(0.4*RIGHT)
        self.play(Create(circle))
        self.play(Write(twopiq))
        # a dot with respect to the axes
        q=0.1
        dot_axes = Dot(ax.coords_to_point(q, func(q)), color=PINK)
        lines = ax.get_lines_to_point(ax.c2p(q,func(q))).set_color(BLACK)
        twopiq=Tex(r'$q_2$',font_size=40,color=PINK).next_to(dot_axes)
        self.add(dot_axes,lines,twopiq)
        self.wait()
        
        
        circle = Circle(color=RED,radius=0.8)  # create a circle
        circle.shift(1.92*DOWN+3.312*LEFT)
        twopiq=Tex(r'$q_3$',font_size=40,color=RED).next_to(circle).next_to(circle).shift(0.5*RIGHT)
        self.play(Create(circle))
        self.play(Write(twopiq))
        # a dot with respect to the axes
        q=0.3
        dot_axes = Dot(ax.coords_to_point(q, func(q)), color=RED)
        lines = ax.get_lines_to_point(ax.c2p(q,func(q))).set_color(BLACK)
        twopiq=Tex(r'$q_3$',font_size=40,color=RED).next_to(dot_axes)
        self.add(dot_axes,lines,twopiq)
        self.wait()
        
        circle = Circle(color=GREEN,radius=1.6)  # create a circle
        circle.shift(1.92*DOWN+3.312*LEFT)
        twopiq=Tex(r'$q_4$',font_size=40,color=GREEN).next_to(circle).next_to(circle).shift(0.2*RIGHT)
        self.play(Create(circle))
        self.play(Write(twopiq))
        # a dot with respect to the axes
        q=1
        dot_axes = Dot(ax.coords_to_point(q, func(q)), color=GREEN)
        lines = ax.get_lines_to_point(ax.c2p(q,func(q))).set_color(BLACK)
        twopiq=Tex(r'$q_4$',font_size=40,color=GREEN).next_to(dot_axes)
        self.add(dot_axes,lines,twopiq)
        self.wait()
        
        circle = Circle(color=PINK,radius=3.2)  # create a circle
        circle.shift(1.92*DOWN+3.312*LEFT)
        twopiq=Tex(r'$q_5$',font_size=40,color=PINK).next_to(circle).next_to(circle).shift(0.1*LEFT)
        self.play(Create(circle))
        self.play(Write(twopiq))
        # a dot with respect to the axes
        q=2.5
        dot_axes = Dot(ax.coords_to_point(q, func(q)), color=PINK)
        lines = ax.get_lines_to_point(ax.c2p(q,func(q))).set_color(BLACK)
        twopiq=Tex(r'$q_5$',font_size=40,color=PINK).next_to(dot_axes)
        self.add(dot_axes,lines,twopiq)
        self.wait()
        
        
  
 
        x = np.arange(0.05,3,0.05)
        y= func(x)
        # x_min must be > 0 because log is undefined at 0.
        graph = ax.plot(func,x_range=[0.05,3,0.05], use_smoothing=False,color=RED)

        
        # # Display graph
        # self.add(ax,graph)
        self.play(Create(graph))
        self.wait()
        dimensiontoq=MathTex("Dimension = 2\pi/q", font_size=35,color=RED).next_to(ax).shift(LEFT*3.8+1.3*UP)
        self.play(Write(dimensiontoq))
        self.wait(5)
        
        
class FilledAngle(Scene):
    def construct(self):
        self.camera.background_color = WHITE
        title=Tex("Small-/Wide- Angle X-ray Scattering - Experimental",color=BLACK)
        title.scale(0.6)
        title.to_edge(LEFT, buff=0.3)
        title.shift(3.4*UP)
        self.add(title)
        copyright_kaliyah=Tex("Aliyah, K. 2023 with Manim v0.17.3",color=BLACK)
        copyright_kaliyah.scale(0.4)
        copyright_kaliyah.to_edge(RIGHT, buff=0.2)
        copyright_kaliyah.shift(3.4*DOWN)
        self.add(copyright_kaliyah)
        
        sample=Rectangle(color=BLACK,fill_color=BLACK,height = 1, width=0.2,fill_opacity=1)
        sample_label=Tex("Sample",font_size=30).next_to(sample,direction=UP).set_color(BLACK)

        xray = (
            Line(LEFT, ORIGIN )
            .set_color(YELLOW)
            
        )
        xray_label=Tex("X-ray",font_size=30).next_to(xray,direction=LEFT).set_color(BLACK)
        
        detector=Rectangle(color=BLUE,fill_color=BLUE,height = 3, width=0.1,fill_opacity=1).shift(RIGHT)
        detector_label=Tex("Detector (1)",font_size=30).next_to(detector,direction=UP).set_color(BLUE)     
        
        l1 = DashedLine(ORIGIN, UP + RIGHT).set_color(BLACK)
        l2 = (
            Line(ORIGIN, DOWN + RIGHT)
            .set_color(YELLOW)
            
        )
        l5 = (
            DashedLine(ORIGIN, RIGHT)
            .set_color(BLACK)
            
        )
        norm = l1.get_length()
        a1 = Angle(l1, l2, other_angle=True, radius=0,).set_color(YELLOW)
        a2 = Angle(l1, l2, other_angle=True, radius=norm).set_color(YELLOW)
        q1 = a1.points #  save all coordinates of points of angle a1
        q2 = a2.reverse_direction().points  #  save all coordinates of points of angle a1 (in reversed direction)
        pnts = np.concatenate([q1, q2, q1[0].reshape(1, 3)])  # adds points and ensures that path starts and ends at same point
        mfill = VMobject().set_color(YELLOW)
        mfill.set_points_as_corners(pnts).set_fill(YELLOW, opacity=0.3).set_stroke(width=0)
        angle1= Angle(
                  l1, l5, other_angle=True,radius=0.5).set_color(BLACK)
        tex = MathTex(r"2\theta_1",color=BLACK,font_size=25).next_to(angle1,direction=0.4*RIGHT).shift(0.1*UP)
        
        detector2=Rectangle(color=GREEN,fill_color=GREEN,height = 3, width=0.1,fill_opacity=1).shift(6*RIGHT)
        detector2_label=Tex("Detector (2)",font_size=30).next_to(detector2,direction=UP).set_color(GREEN)     
        
        l3 = DashedLine(5*LEFT+ORIGIN, UP + 6*RIGHT).set_color(BLACK)
        l4 = (
            Line(5*LEFT+ORIGIN, DOWN + 6*RIGHT)
            .set_color(YELLOW)
            
        )
        l6 = (
            DashedLine(5*LEFT+ORIGIN, 6*RIGHT)
            .set_color(BLACK)
            
        )
        norm = l3.get_length()
        a1 = Angle(l3, l4, other_angle=True, radius=0).set_color(ORANGE)
        a2 = Angle(l3, l4, other_angle=True, radius=norm).set_color(ORANGE)
        q1 = a1.points #  save all coordinates of points of angle a1
        q2 = a2.reverse_direction().points  #  save all coordinates of points of angle a1 (in reversed direction)
        pnts = np.concatenate([q1, q2, q1[0].reshape(1, 3)])  # adds points and ensures that path starts and ends at same point
        mfill2 = VMobject().set_color(ORANGE)
        mfill2.set_points_as_corners(pnts).set_fill(ORANGE, opacity=0.3).set_stroke(width=0)
        angle2= Angle(
                  l3, l6, other_angle=True,radius=4).set_color(BLACK)
        tex2 = MathTex(r"2\theta_2",color=BLACK,font_size=25).next_to(angle2,direction=4*RIGHT).shift(0.05*UP)
        
        
        WAXS_label = Tex(r"WAXS",color=BLUE,font_size=30).next_to(mfill,direction=3*DOWN).shift(5*LEFT)
        SAXS_label = Tex(r"SAXS",color=GREEN,font_size=30).next_to(mfill2,direction=3*DOWN)
        
        WAXS_realspace = Tex(r"0.1-1 nm",color=BLUE,font_size=30).next_to(WAXS_label,direction=DOWN)
        SAXS_realspace = Tex(r"1-1000 nm (ultraSAXS: few $\mu$m)",color=GREEN,font_size=30).next_to(SAXS_label,direction=DOWN)
        timeres = Tex(r"Time Res. : ms",color=BLACK,font_size=30).next_to(WAXS_label,direction=3*DOWN).shift(1.5*RIGHT)
        
        l7_q=Arrow(6*RIGHT,6*RIGHT+UP, buff=0).set_color(RED)
        l7_q_label = MathTex(r"q",color=RED,font_size=40).next_to(l7_q,RIGHT)
        
        equation1=MathTex(r'q = \frac{4{\pi}sin{\theta}}{\lambda}',substrings_to_isolate="q",color=BLACK,font_size=40).shift(1*LEFT+1.5*UP)
        equation1.set_color_by_tex("q", RED)
        equation2=MathTex(r'Dimension = \frac{2{\pi}}{q} ',substrings_to_isolate="q",color=BLACK,font_size=40).next_to(equation1, RIGHT).shift(0.5*RIGHT)
        equation2.set_color_by_tex("q", RED)
        
        self.play(Create(sample),Create(sample_label))
        self.wait(2)
        self.play(Create(xray_label),Create(xray))
        self.wait(2)
        self.play(Create(detector),Create(detector_label))
        self.wait(2)
        self.play(Create(mfill),Create(l1),Create(l5),Create(angle1),Create(tex))
        self.wait(2)

        objects=VGroup(detector,detector_label,xray,xray_label,sample,sample_label,l1,l5,angle1,mfill,tex)
        self.play(objects.animate.shift(5*LEFT))
        self.wait(2)
        self.play(Create(detector2),Create(detector2_label))
        self.wait(2)
        self.play(Create(mfill2),Create(l3),Create(l6),Create(angle2),Create(tex2))
        self.wait(2)
        self.play(Create(WAXS_label),Create(SAXS_label))
        self.wait(2)
        self.play(Create(l7_q),Create(l7_q_label))
        self.wait(2)
        
        self.play(Create(equation1))
        self.wait(5)
        self.play(Create(equation2))
        self.wait(3)
        self.play(Create(WAXS_realspace),Create(SAXS_realspace))
        self.wait(2)
        self.play(Create(timeres))
        self.wait(2)



