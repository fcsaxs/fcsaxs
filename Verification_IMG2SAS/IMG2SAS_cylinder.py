#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 22 10:21:29 2021

@author: kinanti_a
"""

import numpy as np
import scipy
from scipy import ndimage
import matplotlib.pyplot as plt
import matplotlib.pyplot as plt
import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import matplotlib.pyplot as plt
import time
import tifffile as tif
import math
import raster_geometry as ras
    
a=300 #boxsize, edge length of an image
boxsize=a
onevoxel = 2 #how much in nm is one voxel (can be any unit but your q will depend on it)
length = 600
radius = 50
#im = ras.sphere(a, radius/onevoxel, 0.5)
im = ras.cylinder(a,
        length/onevoxel,
        radius/onevoxel,
        axis=-1,
        position=0.5,
        smoothing=False)
im = im.astype('float32')
eta=100
im[im==0]=0
im[im==1]=eta
plt.imshow(im[:,150,:])

from scipy.signal import general_gaussian, tukey



def window3d(w):
    # Convert a 1D filtering kernel to 3D
    # eg, window3D(numpy.hanning(5))
    L=w.shape[0]
    m1=np.outer(np.ravel(w), np.ravel(w))
    win1=np.tile(m1,np.hstack([L,1,1]))
    m2=np.outer(np.ravel(w),np.ones([1,L]))
    win2=np.tile(m2,np.hstack([L,1,1]))
    win2=np.transpose(win2,np.hstack([1,2,0]))
    win=np.multiply(win1,win2)
    return win


# window = window3d(tukey(a, alpha=0.1))

# im_win = im*window

#fourier transform the 3d spectra
spectrum_3d = np.fft.fftn(im)
    
#shift the zero frequency to the center
spectrum_3d_sh = np.fft.fftshift(spectrum_3d) 

#fourier space intensity 
intensity_3d = (spectrum_3d_sh.real)**2 + (spectrum_3d_sh.imag)**2

plt.imshow(intensity_3d[:,:,2])

#taking into account the shape of intensity 
sx, sy, sz = intensity_3d.shape

#creating a grid based on that
X, Y, Z = np.ogrid[0:sx, 0:sy, 0:sz]

#converting X Y Z to r in polar coordinates
r = (np.sqrt(((X - sx/2)**2)+ ((Y - sy/2)**2)+ ((Z - sz/2)**2)))
#binningnormal is s=a/2, trial is s=a
s=a/2
b=1
#binning the r to dimension of real space perlin noise divided by 2 
#rbin = (s*(r)/(r.max())).astype(int)
rbin = np.around(s*np.sqrt(3)*(r)/(r.max()))

#perform an average of intensity inside one bin and do it for all the bins
radial_mean = ndimage.mean(intensity_3d, labels=rbin, index=np.arange(1, s+1))
#making x axis corresponding to the reciprocal of real space
x=np.arange(2*np.pi/(onevoxel*a),(np.pi/onevoxel)+(2*np.pi/(onevoxel*a)),2*np.pi/(onevoxel*a))

radial_mean = radial_mean * ((np.sin(x*onevoxel/2)/(x*onevoxel/2))**2)
#make a vector of q-range
Q = np.arange(2*np.pi/(onevoxel*a),(np.pi/onevoxel)+(2*np.pi/(onevoxel*a)),2*np.pi/(onevoxel*a))

import jscatter as js
import matplotlib.pylab as pylab
#I = js.ff.cylinder(x, length, radius, SLD = eta, solventSLD=0, alpha=None,nalpha=90, h=None)
I = js.ff.cylinder(x, length, radius, SLD = eta, solventSLD=0)
def realspace(q):
    return 2*np.pi/q


def reciprocal(q):
    return 2*np.pi/q

# Edit the font, font size, and axes width
mpl.rcParams['font.family'] = 'Times New Roman'
plt.rcParams['font.size'] = 60
plt.rcParams['axes.linewidth'] = 2
params = {'legend.fontsize': 'x-large',
          'figure.figsize': (15, 5),
          'axes.labelsize': 'x-large',
          'axes.titlesize': 'x-large',
          'xtick.labelsize': 'xx-large',
          'ytick.labelsize': 'xx-large'}
pylab.rcParams.update(params)



fig = plt.figure(figsize=(50,50))


axa = fig.add_subplot(111)
axa.set_box_aspect(1)
# Create new axes object by cloning the y-axis of the first plot
ax2a = axa.secondary_xaxis(location='top', functions=(reciprocal, realspace))
ax2a.set_xscale('linear')
# Edit the major and minor ticks of the x and y axes
axa.xaxis.set_tick_params(which='major', size=27, width=10, direction='in')
axa.xaxis.set_tick_params(which='minor', size=17, width=10, direction='in')
axa.yaxis.set_tick_params(which='major', size=27,
                          width=10, direction='in', right='on')
axa.yaxis.set_tick_params(which='minor', size=17,
                          width=10, direction='in', right='on')

# Edit the tick parameters of the energy x-axis
ax2a.xaxis.set_tick_params(which='major', size=27, width=10, direction='in')
ax2a.xaxis.set_tick_params(which='minor', size=17, width=10, direction='in')

#plt.loglog(Q,I)
axa.loglog(x,radial_mean*(onevoxel**6),'o',label='IMG2SAS',markersize=50)
axa.loglog(I.X,I.Y,label='Analytical',marker='x',linewidth=10)

# Add the x and y-axis labels
axa.set_xlabel(r'q [nm$^{-1}$]', fontsize='xx-large', labelpad=20)
axa.set_ylabel('Intensity [a.u.]', fontsize='xx-large', labelpad=20)


ax2a.set_xlabel('Dimension [nm]', fontsize='xx-large',  labelpad=20)


# Add legend to plot
axa.legend(bbox_to_anchor=(1, 1), loc=1, frameon=True, fontsize='x-large')

# axa.text(-0.1, 1.1, 'a)', transform=axa.transAxes,
#          size=120, weight='bold')

im.shape = 1, boxsize, 1, boxsize,boxsize
import tifffile as tiff 
tiff.imwrite("CylinderFF_IMG2SASKAliyah.tif",im, imagej=True, resolution=(1/onevoxel, 1/onevoxel), metadata={'spacing': onevoxel, 'unit': 'nm'})



plt.savefig("cylinderIMG2SAS.svg", transparent=False, bbox_inches='tight')










