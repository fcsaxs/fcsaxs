#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 22 10:21:29 2021

@author: kinanti_a
"""

import numpy as np
import scipy
from scipy import ndimage
import matplotlib.pyplot as plt
import matplotlib.pyplot as plt
import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import matplotlib.pyplot as plt
import time
import tifffile as tif
import math
import raster_geometry as ras
    
a=512#boxsize, edge length of an image

onevoxel =1 #how much in nm is one voxel (can be any unit but your q will depend on it)
Ra = 40
Rb = 15
#im = ras.sphere(a, radius/onevoxel, 0.5)
im = ras.ellipsoid(a, (Rb/onevoxel, Ra/onevoxel, Rb/onevoxel))
im = im.astype('float32')
eta=100
im[im==0]=0
im[im==1]=eta
plt.imshow(im[:,:,100])

from scipy.signal import general_gaussian, tukey


def window3d(w):
    # Convert a 1D filtering kernel to 3D
    # eg, window3D(numpy.hanning(5))
    L=w.shape[0]
    m1=np.outer(np.ravel(w), np.ravel(w))
    win1=np.tile(m1,np.hstack([L,1,1]))
    m2=np.outer(np.ravel(w),np.ones([1,L]))
    win2=np.tile(m2,np.hstack([L,1,1]))
    win2=np.transpose(win2,np.hstack([1,2,0]))
    win=np.multiply(win1,win2)
    return win


window = window3d(tukey(a, alpha=0.1))

im_win = im*window


import functions
functions.cpu_or_not(0)
radial_mean , Q = functions.img2sas(im,a,onevoxel,paddefault=0)
x=Q
import jscatter as js

x=np.ndarray.flatten(x)
I = js.ff.ellipsoid(x, Ra, Rb, SLD=eta)


plt.figure()
#plt.loglog(Q,I)
plt.loglog(x,radial_mean,label='IMG2SAS ({}, {}, {}, {}, {})'.format(a,onevoxel,Ra,Rb,eta))
plt.loglog(I.X,I.Y,label='Analytical ({}, {}, {}, {}, {})'.format(a,onevoxel,Ra,Rb,eta))
plt.legend()

