#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 19 12:54:01 2021

@author: kinanti_a
"""

import numpy as np
import porespy as ps
import scipy
import matplotlib.pyplot as plt
import scipy.ndimage as spim
ps.visualization.set_mpl_style()
np.random.seed(10)
from scipy.interpolate import UnivariateSpline
from scipy import ndimage 
import time
import pandas as pd
import itertools
df = pd.read_csv('/mnt/nas_Uwrite/XL54/NNMC/NNMC-decane_SAXS.dat', sep=' ',index_col=False)
data = df.to_numpy()
data = data[~np.isnan(data).any(axis=1)]
boxsize = 432
onevoxel=1
radiusB = [2,3,4,5,6,30,35,40,45,50]
#amount to one volfracB
volfracB = [0.05,0.1,0.20,0.1,0.05,0.05,0.1,0.20,0.1,0.05]
scalingfactor =1
params =radiusB, volfracB, scalingfactor 

fixedarg = [10,0.7,data]

def union_model_SAXSint_alldata(params, fixedarg):
    """
    Construct a united Boolean model with given parameters.

    Parameters
    ----------
    params : list
        The radius and volume fraction for each class of spheres, and the scalingfactor between the analytical solution and the experimental data
    fixedarg : list
        The fixed parameters, including number of classes of spheres, target porosity, and the data file (SAXS curve to fit)

    Returns
    -------
    Q: array-like
        The scattering vector array 
    Int_q: array-like
        The analytical scattering intensity array (after scaling)
    Phi_0: float32
        The theoretical porosity reached with the united Boolean model 
    """
    radiusB, volfracB, scalingfactor= params
    classB, porosity, data= fixedarg

    #select the full range of I and q of experimental data for modelling
    Iexp=data[:,0]
    Q= data[:,1]

    #define the length of r-wave and phi-wave
    n = len(Q)
    print('Number of data points: '+str(len(Q)))
    m = 80000
    phi = 1-porosity
    num = 1000

    #initialization
    C00_r = np.zeros(m)
    K_r = np.zeros(m)
    Gamma_r = np.zeros(m)
    Int_q = np.zeros(n)
    r_wave = np.zeros(m)
    R_grain = np.zeros(classB)
    Dens_grain = np.zeros(classB)
    Distribution_grain = np.zeros(classB)
    Phi_0 = np.zeros(classB)
    Distribution_volfrac = np.zeros(classB)
    error = np.zeros(num)
    PhiWave = np.zeros(num)
    PhiAmpl = np.zeros(num)
    
    for i in range(classB):
        Distribution_volfrac[i]=volfracB[i]
        R_grain[i]=radiusB[i]
        V_i = (4/3)*np.pi*(R_grain[i]**3)
        Distribution_grain[i]=Distribution_volfrac[i]/V_i
    
    #find the minimum error with given porosity in the phi-wave and take the corresponding phi-value as total Phi0
    for j in range(num):
        PhiAmpl[j]=0.01+j/100 #check this if the error is not converging for phiwave
        phi0 = 1
        for i in range(classB):
            phi_i = PhiAmpl[j]*Distribution_grain[i]
            V_i = 4/3*np.pi*(R_grain[i]**3)
            phi0*= np.exp(-phi_i*V_i)
        PhiWave[j] = phi0
        error[j] = abs(PhiWave[j]-porosity)
        
    index_errormin = np.argmin(error)
    Phi0_tot = PhiWave[index_errormin]
    Phi1_tot = 1 - Phi0_tot
    
    
    for i in range(classB):
        Dens_grain[i]=PhiAmpl[index_errormin]*Distribution_grain[i]
        Phi_0[i]=np.exp(-Dens_grain[i]*(4/3*np.pi*R_grain[i]**3))
        
    for i in range(m):
    	r_wave[i]=0.0001+i*(10/n)
        
    C00_r[C00_r==0]=1
    Gamma_r[C00_r==0]=1

    #Calculating K_r, C00_r, and gamma_r-values for each q-value  
    for i,j in itertools.product(*map(range, (classB, m))):
        if (2*R_grain[i] - r_wave[j]>=0):
            Heaviside = 1
        else:
            Heaviside = 0
        K_r[j] = (4/3) * np.pi *(R_grain[i]**3)* ((1-r_wave[j]/(2*R_grain[i]))**2)*(1+r_wave[j]/(4*R_grain[i]))*Heaviside
        C00_r[j]*=(Phi_0[i]**2)*(np.exp(Dens_grain[i]*K_r[j]))
        Gamma_r[j] = (C00_r[j]-((Phi0_tot)**2))/(Phi1_tot*(1-Phi1_tot))
    
    #Calculating the numerical scattering intensity of each q-value by approximated integration
    for j, i in itertools.product(*map(range,(n,m-1))):
        Int_q[j]+=(Phi1_tot*(1-Phi1_tot))*(r_wave[i+1]-r_wave[i])*(1e-7)*Gamma_r[i]*4*np.pi*((1e-7)*r_wave[i])**2*np.sin(Q[j]*r_wave[i])/(Q[j]*r_wave[i])
    
    Int_q = Int_q * scalingfactor
    
 
        
    #return Q,Int_q,Phi1_tot,residual
    return Q, Int_q,Phi_0
    
Q_overlap_anal,Int_overlap_anal,Phi_0_1 = union_model_SAXSint_alldata(params,fixedarg)
   
def union_model_porespy(params, fixedarg,tes):
    boxsize,onevoxel = tes
    radiusB, volfracB, scalingfactor= params
    classB, porosity, data = fixedarg
    phi = 1-porosity
    num = 1000
    R_grain = np.zeros(classB)
    Dens_grain = np.zeros(classB)
    Distribution_grain = np.zeros(classB)
    Phi_0 = np.zeros(classB)
    Distribution_volfrac = np.zeros(classB)
    error = np.zeros(num)
    PhiWave = np.zeros(num)
    PhiAmpl = np.zeros(num)
    boxlength = boxsize*onevoxel-onevoxel
    
    for i in range(classB):
        Distribution_volfrac[i]=volfracB[i]
        R_grain[i]=radiusB[i]
        V_i = (4/3)*np.pi*(R_grain[i]**3)
        Distribution_grain[i]=Distribution_volfrac[i]/V_i
    
    for j in range(num):
        PhiAmpl[j]=0.01+j/100 #check this if the error is not converging for phiwave
        phi0 = 1
        for i in range(classB):
            phi_i = PhiAmpl[j]*Distribution_grain[i]
            V_i = 4/3*np.pi*(R_grain[i]**3)
            phi0*= np.exp(-phi_i*V_i)
        PhiWave[j] = phi0
        error[j] = abs(PhiWave[j]-porosity)
        
    index_errormin = np.argmin(error)
    Phi0_tot = PhiWave[index_errormin]
    Phi1_tot = 1 - Phi0_tot
    print('Objective solid fraction = '+str(Phi1_tot))
    
    for i in range(classB):
        Dens_grain[i]=PhiAmpl[index_errormin]*Distribution_grain[i]
        Phi_0[i]=np.exp(-Dens_grain[i]*(4/3*np.pi*R_grain[i]**3))
    
#    R_grain_sorted = np.sort(R_grain)
#    GrainRadiusMax=np.around(R_grain_sorted[classB-1]/onevoxel)+1
#    if (GrainRadiusMax >= boxsize/2):
#        GrainRadiusMax=boxsize/2
#        
    BooleanField = []
#    x_= np.linspace(0,boxsize-1,boxsize)
#    y_ = np.linspace(0,boxsize-1,boxsize)
#    z_ = np.linspace(0,boxsize-1,boxsize)
#    X,Y,Z = np.meshgrid(x_,y_,z_,indexing='ij')
#    grid_size = boxsize
    for i in range(classB):
#        GrainNum = Dens_grain[i]*(boxlength**3)
        GrainRadiusMax=np.around(R_grain[i]/onevoxel);
        if (GrainRadiusMax >= boxsize/2):
            GrainRadiusMax=boxsize/2
        GrainRadius=R_grain[i]
#        GrainRadiusMax = int(GrainRadiusMax)
        BooleanField.append(np.zeros([boxsize,boxsize,boxsize]))
        im = ps.generators.overlapping_spheres([boxsize,boxsize,boxsize], GrainRadiusMax, Phi_0[i],
                        iter_max=10, tol=0.001)
        BooleanField[i]=1-im.astype('float32')
        
#        for k in range(int(np.around(GrainNum))+1):
#            x_pos, y_pos, z_pos = np.random.randint(0,boxsize),np.random.randint(0,boxsize),np.random.randint(0,boxsize)
#            offsets = itertools.combinations_with_replacement([grid_size,0,-grid_size],r=3)
#            centers = [(x_pos+x_offset, y_pos+y_offset,z_pos+z_offset) for x_offset,y_offset, z_offset in offsets]
#            I=  np.logical_or.reduce([(X-c_x)**2 + (Y-c_y)**2 + (Z-c_z)**2 < GrainRadiusMax**2 for c_x, c_y, c_z in centers])
#            I = I.astype('float32')
        #BooleanField[i]+= I
        a=BooleanField[i]
    print(Phi_0)    
    BooleanField.append(sum(BooleanField))
    a = BooleanField[classB]
    a[a==0]=0
    a[a>0]=1.72522*(10**11)
    print('Simulated solid fraction: '+str(a.sum()/(a.size*1.72522*(10**11))))
    return a,Phi_0


boxsize = 432
onevoxel=1

tes = boxsize, onevoxel


im_overlap,Phi_0_2 =union_model_porespy(params, fixedarg,tes)




def img2sas(im_win,params):
    a,onevoxel = params
    
    #fourier transform the 3d spectra
    spectrum_3d = np.fft.fftn(im_win)

    #shift the zero frequency to the center
    spectrum_3d_sh = np.fft.fftshift(spectrum_3d) 

    #fourier space intensity 
    intensity_3d = (spectrum_3d_sh.real)**2 + (spectrum_3d_sh.imag)**2


    #taking into account the shape of intensity 
    sx, sy, sz = intensity_3d.shape

    #creating a grid based on that
    X, Y, Z = np.ogrid[0:sx, 0:sy, 0:sz]

    #converting X Y Z to r in polar coordinates
    r = (np.sqrt(((X - sx/2)**2)+ ((Y - sy/2)**2)+ ((Z - sz/2)**2)))

    s=a/2

    rbin = np.around(s*np.sqrt(3)*(r)/(r.max()))

    #perform an average of intensity inside one bin and do it for all the bins
    radial_mean = ndimage.mean(intensity_3d, labels=rbin, index=np.arange(1, s+1))

    #making x axis corresponding to the reciprocal of real space
    x=np.arange(2*np.pi/(onevoxel*a),(np.pi/onevoxel)+(2*np.pi/(onevoxel*a)),2*np.pi/(onevoxel*a))
    
    return [radial_mean*(onevoxel**6),x]

import copy

tes = boxsize, onevoxel

Int_img2sas, x = img2sas(im_overlap,tes)
Int_img2sas_smooth = copy.deepcopy(Int_img2sas)
Int_img2sas_smooth = Int_img2sas* ((np.sin(x*onevoxel/2)/(x*onevoxel/2))**2)

def find_scalingfactor(expq,expI,analq,analI, qval=0.1):
    """
    Proportionally scale a scattering curve (analytical) to another scattering curve (experimental)

    Parameters
    ----------
    expq : 1D-array
        The scattering vector array of the experimental SAXS curve
    expI : 1D-array
        The scattering intensity array of the experimental SAXS curve
    analq : 1D-array
        The scattering vector array of the analytical SAXS curve.
    analI : 1D-array
        The scattering intensity array of the analytical SAXS curve.
    qval : float32
        The q-value at which the 2 scattering curves are scaled. The default is 0.2.

    Returns
    -------
    K1/K2 : float32
        The scaling factor by which the analytical intensity should be multiplied to fit the experimental curve

    """
    #find the experimental scattering intensity at qval
    idx = (np.abs(expq - qval)).argmin()
    K1=expI[idx]

    #find the analytical scattering intensity at qval
    idx = (np.abs(analq - qval)).argmin()
    K2=analI[idx]
    return K1/K2

from matplotlib import pylab


params_plot = {'legend.fontsize': 30,
          'figure.figsize': (30, 30),
         'axes.labelsize': 60,
         'axes.titlesize':25,
         'xtick.labelsize':30,
         'ytick.labelsize':30, 
         'xtick.direction':'in',
         'ytick.direction':'in',
         'xtick.major.width':2,
         'ytick.major.width':2,
         'xtick.minor.width':2,
         'ytick.minor.width':2,
         'xtick.major.size':15,
         'ytick.major.size':15,
         'xtick.minor.size':15,
         'ytick.minor.size':15,
         'legend.loc':'lower left'}


pylab.rcParams.update(params_plot)


K =  find_scalingfactor(Q_overlap_anal[10:],Int_overlap_anal[10:],x,Int_img2sas_smooth, qval=1)

plt.figure()
plt.loglog(Q_overlap_anal[10:], Int_overlap_anal[10:])
plt.loglog(x[:-100],K*Int_img2sas_smooth[:-100],'x')

im_overlap_babinet = copy.deepcopy(im_overlap)
im_overlap_babinet[im_overlap_babinet>1]=5
im_overlap_babinet[im_overlap_babinet==0]=10
im_overlap_babinet[im_overlap_babinet==10] = 1.72522*(10**11)
im_overlap_babinet[im_overlap_babinet==5] = 0

Int_img2sas_babinet, x = img2sas(im_overlap_babinet,tes)
Int_img2sas_smooth_babinet = copy.deepcopy(Int_img2sas_babinet)
Int_img2sas_smooth_babinet = Int_img2sas_babinet* ((np.sin(x*onevoxel/2)/(x*onevoxel/2))**2)

K2 =  find_scalingfactor(Q_overlap_anal[10:],Int_overlap_anal[10:],x,Int_img2sas_smooth_babinet, qval=1)

def realspace(q):
    return 2*np.pi/q

def reciprocal(q):
    return 2*np.pi/q

fig,ax=plt.subplots()
plt.loglog(Q_overlap_anal[10:], Int_overlap_anal[10:],label='Analytical')
plt.loglog(x[:-100],K*Int_img2sas_smooth[:-100],'x',label=('Simulated by IMG2SAS'))
secaxs= ax.secondary_xaxis(location='top',functions= (reciprocal,realspace))
secaxs.set_xticks(np.asarray([100,50,20,5]))
plt.xlim([10**-2, 10**0])
plt.ylim([10**-21,10**-16])
plt.legend()

plt.show()

#plt.loglog(x[:-100],K*Int_img2sas_smooth_babinet[:-100],'o')

import tifffile as tiff
import time
timestr = time.strftime("%Y%m%d-%H%M%S")

a = boxsize
im_overlap_babinet = im_overlap_babinet.astype('float32')
im_overlap_babinet.shape = 1,a,1,a,a,1

tiff.imwrite('overlapping_Figure1' +str(timestr)+'_'+str(boxsize)+'_'+str(onevoxel)+'.tif',im_overlap_babinet, imagej=True, resolution=(1, 1),
    metadata={'spacing': 1, 'unit': 'nm'})
