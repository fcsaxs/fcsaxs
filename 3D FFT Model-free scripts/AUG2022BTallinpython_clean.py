#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 21 16:24:38 2023

@author: kinanti_a
"""



listss = [[1691,1828,0,['cJ @650 mV 1.7 bar Unmodif1']],[1715,1852,1,['steady @650 mV 1.7 bar Unmodif1']],[1720,1857,1,['steady @650 mV 1.7 bar Unmodif1']],[1725,1862,0,['cJ @300 mV 1.7 bar Unmodif1']],[1749,1886,1,['steady @300 mV 1.7 bar Unmodif1']],[1754,1891,1,['steady @300 mV 1.7 bar Unmodif1']],[1760,1896,0,['cJ @650 mV 3 bar Unmodif1']],[1784	,1920,1,['steady @650 mV 3 bar Unmodif1']],[1789	,1925,1,['steady @650 mV 3 bar Unmodif1']],[1794	,1930,0,['cJ @300 mV 3 bar Unmodif1']],[1818	,1954,1,['steady @300 mV 3 bar Unmodif1']],[1823,	1959,1,['steady @300 mV 3 bar Unmodif1']],
[2170,2336,0,['cJ @650 mV 1.7 bar Toray']],[2194,2360,1,['steady @650 mV 1.7 bar Toray']],[2199,2365,1,['steady @650 mV 1.7 bar Toray']],[2204,2370,0,['cJ @300 mV 1.7 bar Toray']],[2228,2394,1,['steady @300 mV 1.7 bar Toray']],[2233,2399,1,['steady @300 mV 1.7 bar Toray']],[2292-24,2467+5,0,['cJ @650 mV 3 bar Toray']],[2292,2428,1,['steady @650 mV 3 bar Toray']],[2297,2433,1,['steady @650 mV 3 bar Toray']],[2302,2438,0,['cJ @300 mV 3 bar Toray']],[2326,2462,1,['steady @300 mV 3 bar Toray']],[2331,2467,1,['steady @300 mV 3 bar Toray']],
[2716,2886,0,['cJ @650 mV 1.7 bar Modif']],[2740,2910,1,['steady @650 mV 1.7 bar Modif']],[2745,2915,1,['steady @650 mV 1.7 bar Modif']],[2750,2920,0,['cJ @300 mV 1.7 bar Modif']],[2774,2944,1,['steady @300 mV 1.7 bar Modif']],[2779,2949,1,['steady @300 mV 1.7 bar Modif']],[2784,2954,0,['cJ @650 mV 1.7 bar Modif RH70%']],[2808,2978,1,['steady @650 mV 1.7 bar Modif RH70%']],[2813,2983,1,['steady @650 mV 1.7 bar Modif RH70%']],[2818,2988,0,['cJ @300 mV 1.7 bar Modif RH70%']],[2842,3012,1,['steady @300 mV 1.7 bar Modif RH70%']],[2847,3017,1,['steady @300 mV 1.7 bar Modif RH70%']],[2852,3022,0,['cJ @500 mV 1.7 bar Modif']],[2876,3046,1,['steady @500 mV 1.7 bar Modif']],[2881,3051,1,['steady @500 mV 1.7 bar Modif']],
[3266,3402,0,['cJ @650 mV 1.7 bar Pore']],[3290,3426,1,['steady @650 mV 1.7 bar Pore']],[3295,3431,1,['steady @650 mV 1.7 bar Pore']],[3300,3436,0,['cJ 300 mV 1.7 bar Pore']],[3324,3460,1,['steady @300 mV 1.7 bar Pore']],[3329,3465,1,['steady @300 mV 1.7 bar Pore']],[3334,3470,0,['cJ @650 mV 3 bar Pore']],[3358,3494,1,['steady @650 mV 3 bar Pore']],[3363,3499,1,['steady @650 mV 3 bar Pore']],[3368,3504,0,['cJ @300 mV 3 bar Pore']],[3392,3528,1,['steady @300 mV 3 bar Pore']],[3397,3533,1,['steady @300 mV 3 bar Pore']],
[3731,3901,0,['cJ @650 mV 1.7 bar Nonmodif']],[3755,3925,1,['steady @650 mV 1.7 bar Nonmodif']],[3760,3930,1,['steady @650 mV 1.7 bar Nonmodif']],[3765,3935,0,['cJ @300 mV 1.7 bar Nonmodif']],[3789,3959,1,['steady @300 mV 1.7 bar Nonmodif']],[3794,3964,1,['steady @300 mV 1.7 bar Nonmodif']],[3799,3969,0,['cJ @650 mV 1.7 bar Nonmodif RH70%']],[3823,3993,1,['steady @650 mV 1.7 bar Nonmodif RH70%']],[3828,3998,1,['steady @650 mV 1.7 bar Nonmodif RH70%']],[3833,4003,0,['cJ @300 mV 1.7 bar Nonmodif RH70%']],[3857,4027,1,['steady @300 mV 1.7 bar Nonmodif RH70%']],[3862,4032,1,['steady @300 mV 1.7 bar Nonmodif RH70%']],[3867,4037,0,['cJ @500 mV 1.7 bar Nonmodif RH100%']],[3891,4061,1,['steady @500 mV 1.7 bar Nonmodif RH100%']],[3896,4066,1,['steady @500 mV 1.7 bar Nonmodif RH100%']],
[4300,4436,0,['cJ @650 mV 1.7 bar PTFE']],[4324,4460,1,['steady @650 mV 1.7 bar PTFE']],[4329,4465,1,['steady @650 mV 1.7 bar PTFE']],[4334,4470,0,['cJ @300 mV 1.7 bar PTFE']],[4358,4494,1,['steady @300 mV 1.7 bar PTFE']],[4363,4499,1,['steady @300 mV 1.7 bar PTFE']],[4368,4504,0,['cJ @650 mV 3 bar PTFE']],[4392,4528,1,['steady @650 mV 3 bar PTFE']],[4397,4533,1,['steady @650 mV 3 bar PTFE']],[4402,4538,0,['cJ @300 mV 3 bar PTFE']],[4426,4562,1,['steady @300 mV 3 bar PTFE']],[4431,4567,1,['steady @300 mV 3 bar PTFE']]]

# /das/work/p20/p20138/analysis/radial_integration/e20138_1_05016_00000_00000_integ.h5

import h5py
import matplotlib.pyplot as plt
import numpy as np

Pt_peak_coll_operando = []
PTFE_peak_coll_operando = []
largepores_peak_coll_operando = []
water_peak_coll_operando = []
Pt_peak_coll_dry = []
PTFE_peak_coll_dry = []
largepores_peak_coll_dry = []
water_peak_coll_dry = []
for item in range(len(listss)):
    hx=201
    if listss[item][2]==0:
        hy=24
    else:
        hy=5
    Pt_peak_img_dry = np.zeros([hx,hy])
    PTFE_peak_img_dry = np.zeros([hx,hy])
    largepores_peak_img_dry = np.zeros([hx,hy])
    water_peak_img_dry = np.zeros([hx,hy])
    Pt_peak_img_wet = np.zeros([hx,hy])
    PTFE_peak_img_wet = np.zeros([hx,hy])
    largepores_peak_img_wet = np.zeros([hx,hy])
    water_peak_img_wet = np.zeros([hx,hy])
    scanwet=listss[item][0]
    scandry=listss[item][1]
    steadystate=listss[item][2]
    for j in range(hy):
    
        path = r"/das/work/p20/p20138/analysis/radial_integration/e20138_1_0{}_00000_00000_integ.h5".format(scanwet+j)
        pathwaxs = r"/das/work/p20/p20138/analysis/radial_integration_waxs/e20138_1_0{}_00000_00000_integ.h5".format(scanwet+j) #path on ra: /das/work/p20/p20138/analysis/radial_integration/e20138_1_05016_00000_00000_integ.h5
        # File = h5py.File(path, 'r')
        
        with h5py.File(path, 'r') as File:
        
            Q = File['q']
            I = File['I_all']
            Ierr = File['I_std']
            for i in range(I.shape[0]): #i are the points of the line scan
                qexp=Q[:,0]*10
                Iexp=I[i,0,:]
                Ptpeak = max(Iexp[(qexp<1.05)&(qexp>0.95)])
                Pt_peak_img_wet [i,j]=Ptpeak
                largeporespeak = max(Iexp[(qexp<0.06)&(qexp>0.05)])
                largepores_peak_img_wet [i,j]=largeporespeak 
                
        with h5py.File(pathwaxs, 'r') as File:
        
            Qw = File['q']
            Iw = File['I_all']
            Iwerr = File['I_std']
            for i in range(I.shape[0]): #i are the points of the line scan
                qexp=Qw[:,0]*10
                Iexp=Iw[i,0,:]
                PTFEpeak = max(Iexp[(qexp<12.86)&(qexp>12.80)] )
                PTFE_peak_img_wet [i,j]=PTFEpeak
                waterpeak = max(Iexp[(qexp>22.9)&(qexp<23.1)] )
                water_peak_img_wet [i,j]=waterpeak


            
        path = r"/das/work/p20/p20138/analysis/radial_integration/e20138_1_0{}_00000_00000_integ.h5".format(scandry+j)
        pathwaxs = r"/das/work/p20/p20138/analysis/radial_integration_waxs/e20138_1_0{}_00000_00000_integ.h5".format(scandry+j) #path on ra: /das/work/p20/p20138/analysis/radial_integration/e20138_1_05016_00000_00000_integ.h5
        # File = h5py.File(path, 'r')
        
        with h5py.File(path, 'r') as File:
        
            Q = File['q']
            I = File['I_all']
            Ierr = File['I_std']
            for i in range(I.shape[0]): #i are the points of the line scan
                qexp=Q[:,0]*10
                Iexp=I[i,0,:]
                Ptpeak = max(Iexp[(qexp<1.05)&(qexp>0.95)])
                Pt_peak_img_dry [i,j]=Ptpeak
                largeporespeak = max(Iexp[(qexp<0.06)&(qexp>0.05)])
                largepores_peak_img_dry [i,j]=largeporespeak 
                
        with h5py.File(pathwaxs, 'r') as File:
        
            Qw = File['q']
            Iw = File['I_all']
            Iwerr = File['I_std']
            for i in range(I.shape[0]): #i are the points of the line scan
                qexp=Qw[:,0]*10
                Iexp=Iw[i,0,:]
                PTFEpeak = max(Iexp[(qexp<12.87)&(qexp>12.83)] )
                PTFE_peak_img_dry [i,j]=PTFEpeak
                waterpeak = max(Iexp[(qexp>22.9)&(qexp<23.1)] )
                water_peak_img_dry [i,j]=waterpeak
                
                
                
                
    Pt_peak_coll_operando.append(Pt_peak_img_wet)
    PTFE_peak_coll_operando.append(PTFE_peak_img_wet) 
    largepores_peak_coll_operando.append(largepores_peak_img_wet) 
    water_peak_coll_operando.append(water_peak_img_wet) 
    # plt.figure()
    # plt.imshow(PTFE_peak_img_wet,aspect='auto')
    
    Pt_peak_coll_dry.append(Pt_peak_img_dry)
    PTFE_peak_coll_dry.append(PTFE_peak_img_dry) 
    largepores_peak_coll_dry.append(largepores_peak_img_dry)
    water_peak_coll_dry.append(water_peak_img_dry) 
    # plt.figure()
    # plt.imshow(PTFE_peak_img_dry,aspect='auto')
                
# plt.figure()
# plt.imshow(PTFE_peak_coll_dry[0],extent=[0,hy*30,hx*0.7,0],aspect='auto',vmin=0,vmax=10,cmap="Reds") 

# plt.figure()
# plt.imshow(PTFE_peak_coll_operando[0],extent=[0,hy*30,hx*0.7,0],aspect='auto',vmin=0,vmax=10,cmap="Reds")              
       
# for i in range(len(listss)):
#     plt.figure()
    
#     plt.plot(Pt_peak_coll_dry[i][:,0],label='dry')
#     plt.plot(Pt_peak_coll_operando[i][:,0],label='wet')
    
#     plt.legend()
    
#     plt.figure()
    
#     plt.plot(PTFE_peak_coll_dry[i][:,0],label='dry')
#     plt.plot(PTFE_peak_coll_operando[i][:,0],label='wet')
    
#     plt.legend()
    

#%%

from scipy.stats import sem
registrationshift_coll =[]
registrationscaling_coll =[]
shiftedrange = np.arange(-10,11)
for i in range(len(listss)):
#for i in [1]:
    hy=len(PTFE_peak_coll_dry[i][0,:])
    registrationshift= np.zeros(hy)
    registrationscaling= np.zeros(hy)
    for line in range(hy):
        error_coll=[]
        for shift in range(-10,11):
            shifteddry= np.roll(PTFE_peak_coll_dry[i][:,line],shift)
            wet=PTFE_peak_coll_operando[i][:,line]
            
            
            if shift<0:
                shifteddry=shifteddry[:shift]
                wet = wet[:shift]

            else:
                shifteddry=shifteddry[shift:]
                wet = wet[shift:]

            if i <=17:
                
                K=sum(wet[:50])/sum(shifteddry[:50])
            else:
                K=1
            sumerror= np.sum((shifteddry-(wet/K))**2)/len(shifteddry)
            error_coll.append(sumerror)
        error_coll = np.array(error_coll)
        idx=np.argwhere(error_coll==min(error_coll))
        idx=int(idx)
        
        
        
        registrationshift[line]=shiftedrange[idx]
        trueshift=shiftedrange[idx]

        _PTFEunshifteddry= np.roll(PTFE_peak_coll_dry[i][:,line],0)
        _PTFEshifteddry= np.roll(PTFE_peak_coll_dry[i][:,line],trueshift)
        _PTFEwet=PTFE_peak_coll_operando[i][:,line]
        
        
        _Ptunshifteddry= np.roll(Pt_peak_coll_dry[i][:,line],0)
        _Ptshifteddry= np.roll(Pt_peak_coll_dry[i][:,line],trueshift)
        _Ptwet=Pt_peak_coll_operando[i][:,line]
        
        
        _largeporesunshifteddry= np.roll(largepores_peak_coll_dry[i][:,line],0)
        _largeporesshifteddry= np.roll(largepores_peak_coll_dry[i][:,line],trueshift)
        _largeporeswet=largepores_peak_coll_operando[i][:,line]
        
        
        _waterunshifteddry= np.roll(water_peak_coll_dry[i][:,line],0)
        _watershifteddry= np.roll(water_peak_coll_dry[i][:,line],trueshift)
        _waterwet=water_peak_coll_operando[i][:,line]
        if i <=17:
            K=sum(_PTFEwet[:50])/sum(_PTFEshifteddry[:50])
        else:
            K=1
        registrationscaling[line]=K
        if trueshift<0:
            _PTFEshifteddry=_PTFEshifteddry[:trueshift]
            _PTFEwet = _PTFEwet[:trueshift]/K
            _Ptshifteddry=_Ptshifteddry[:trueshift]
            _Ptwet = _Ptwet[:trueshift]/K
            _largeporesshifteddry=_largeporesshifteddry[:trueshift]
            _largeporeswet = _largeporeswet[:trueshift]/K
            _watershifteddry=_watershifteddry[:trueshift]
            _waterwet = _waterwet[:trueshift]/K
        else:
            _PTFEshifteddry=_PTFEshifteddry[trueshift:]
            _PTFEwet = _PTFEwet[trueshift:]/K
            _Ptshifteddry=_Ptshifteddry[trueshift:]
            _Ptwet = _Ptwet[trueshift:]/K
            _largeporesshifteddry=_largeporesshifteddry[trueshift:]
            _largeporeswet = _largeporeswet[trueshift:]/K
            _watershifteddry=_watershifteddry[trueshift:]
            _waterwet = _waterwet[trueshift:]/K
            
            
        # fig,ax =plt.subplots(nrows=4,figsize=[5,5])
        # plt.suptitle('{},{},{}'.format(line,i,listss[i][3][0]))
        # x=range(len(_PTFEshifteddry))   
        # ax[0].plot(x,_PTFEshifteddry,label='dry')
        # ax[0].plot(x,_PTFEwet,label='operando')
        # ax[0].legend()
        
        # x=range(len(_Ptshifteddry))   
        # ax[1].plot(x,_Ptshifteddry,label='dry')
        # ax[1].plot(x,_Ptwet,label='operando')
        # ax[1].legend()
        
        # x=range(len(_largeporesshifteddry))   
        # ax[2].plot(x,_largeporesshifteddry,label='dry')
        # ax[2].plot(x,_largeporeswet,label='operando')
        # ax[2].legend()
        
        # x=range(len(_watershifteddry))   
        # ax[3].plot(x,_waterwet/_watershifteddry,label='operando/dry')
        # ax[3].legend()
 
 
        # ax[0].set_ylim([0,max(_PTFEshifteddry)+1])
        # ax[1].set_ylim([0,max(_Ptshifteddry)+1])
        # ax[2].set_ylim([0,max(_largeporesshifteddry)+1])
        # ax[3].set_ylim([0,2])


    registrationshift_coll.append(registrationshift)
    registrationscaling_coll.append(registrationscaling)

#%%

from scipy.stats import binned_statistic
from scipy.sparse.linalg import spsolve
from sklearn.metrics import auc
import copy
from lmfit.models import SkewedGaussianModel, LinearModel, Model, PowerLawModel
import matplotlib as mpl
from scipy.sparse import csc_matrix, eye, diags
import jscatter as js
from scipy.stats import binned_statistic
import scipy.io as sio
import matplotlib
from scipy.optimize import curve_fit
import scipy
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from matplotlib import pylab
from lmfit.models import ExponentialModel, GaussianModel, PolynomialModel

def binninginitial(q, I):
    qexp = q
    Iexp = I
    qexp = qexp[3:1000]
    Iexp = Iexp[3:1000]
    s = qexp
    ys = Iexp
    so, edges, _ = binned_statistic(s, ys, statistic='mean', bins=np.logspace(
        np.log10(np.min(qexp)), np.log10(np.max(qexp)), 500))
    d = edges[:-1]+np.diff(edges)/2
    qexp = d
    Iexp = so
    qexp = qexp[~np.isnan(Iexp)]
    Iexp = Iexp[~np.isnan(Iexp)]
    d = qexp
    so = Iexp
    #qexp,Iexp = minusbackground(qexp, Iexp)
    return qexp, Iexp


def binninginitial2(q, I):
    qexp = q
    Iexp = I
    qexp = qexp[3:]
    Iexp = Iexp[3:]
    s = qexp
    ys = Iexp
    so, edges, _ = binned_statistic(s, ys, statistic='mean', bins=np.logspace(
        np.log10(np.min(qexp)), np.log10(np.max(qexp)), 50))
    d = edges[:-1]+np.diff(edges)/2
    qexp = d
    Iexp = so
    qexp = qexp[~np.isnan(Iexp)]
    Iexp = Iexp[~np.isnan(Iexp)]
    d = qexp
    so = Iexp
    #qexp,Iexp = minusbackground(qexp, Iexp)
    return qexp, Iexp


def outMEMCLspot(i,line) :
    scanwet=listss[i][0]
    scandry=listss[i][1]
    howmuchshift=registrationshift_coll[i][line]
    howmuchshift=int(howmuchshift) 
    howmuchscaling=registrationscaling_coll[i][line]
    Ptline = Pt_peak_coll_dry[i][:,line]
    spot = np.argwhere(Ptline==max(Ptline))
    spot = int(spot[0,0])
    spotmembrane = np.argwhere(Ptline==min(Ptline))
    spotmembrane = int(spotmembrane[0,0])
    if spotmembrane <75:
        spotmembrane = np.argwhere(Ptline[75:]==min(Ptline[75:]))
        spotmembrane = int(spotmembrane[0,0])+75
        
    weirdornot =0
    if ((spot - spotmembrane) < 0)|((spot - spotmembrane) > 43):
        spot = spotmembrane+25
        weirdornot = 1
        
   
    # spotdry= int(spot-howmuchshift)
    
    
    anodespot = np.argwhere(Ptline[:spotmembrane]==max(Ptline[:spotmembrane]))
    anodespot = int(anodespot)
    
    weirdornot =0
    if ((spotmembrane-anodespot) < 0)|((spotmembrane-anodespot) > 43):
        anodespot = spotmembrane-22
        weirdornot = 1
    
    print(spot,spotmembrane, anodespot,weirdornot,listss[i][3][0],howmuchshift)
    # plt.figure()
    # plt.scatter(spot,Ptline[spot])
    # plt.scatter(spotmembrane,Ptline[spotmembrane])
    # plt.scatter(anodespot,Ptline[anodespot])
    # plt.plot(Ptline)
    return spot,spotmembrane,anodespot,howmuchshift
    
    
    
    
def outintensityq(i,line,spot,howmuchshift): 
    scanwet=listss[i][0]
    scandry=listss[i][1]
    howmuchscaling=registrationscaling_coll[i][line]
    path = r"/das/work/p20/p20138/analysis/radial_integration/e20138_1_0{}_00000_00000_integ.h5".format(scanwet+line)
    pathwaxs = r"/das/work/p20/p20138/analysis/radial_integration_waxs/e20138_1_0{}_00000_00000_integ.h5".format(scanwet+line) #path on ra: /das/work/p20/p20138/analysis/radial_integration/e20138_1_05016_00000_00000_integ.h5

    with h5py.File(path, 'r') as File:
    
        Q = File['q']
        I = File['I_all']
        Ierr = File['I_std']
        qexp=Q[:,0]*10
        Iexp=I[spot+howmuchshift,0,:]
        qexp,Iexp=binninginitial(qexp, Iexp)
            
    with h5py.File(pathwaxs, 'r') as File:
    
        Qw = File['q']
        Iw = File['I_all']
        Iwerr = File['I_std']
        qwexp=Qw[:,0]*10
        Iwexp=Iw[spot+howmuchshift,0,:]
        qwexp,Iwexp=binninginitial2(qwexp, Iwexp)
        
        
    qqwexpoperando = np.concatenate([qexp,qwexp])
    IIwexpoperando = np.concatenate([Iexp,Iwexp])
    IIwexpoperando /= howmuchscaling
    
    path = r"/das/work/p20/p20138/analysis/radial_integration/e20138_1_0{}_00000_00000_integ.h5".format(scandry+line)
    pathwaxs = r"/das/work/p20/p20138/analysis/radial_integration_waxs/e20138_1_0{}_00000_00000_integ.h5".format(scandry+line) #path on ra: /das/work/p20/p20138/analysis/radial_integration/e20138_1_05016_00000_00000_integ.h5
    
    with h5py.File(path, 'r') as File:
    
        Q = File['q']
        I = File['I_all']
        Ierr = File['I_std']
        qexp=Q[:,0]*10
        Iexp=I[spot,0,:]
        qexp,Iexp=binninginitial(qexp, Iexp)
            
    with h5py.File(pathwaxs, 'r') as File:
    
        Qw = File['q']
        Iw = File['I_all']
        Iwerr = File['I_std']
        qwexp=Qw[:,0]*10
        Iwexp=Iw[spot,0,:]
        qwexp,Iwexp=binninginitial2(qwexp, Iwexp)
        
        
    qqwexpdry = np.concatenate([qexp,qwexp])
    IIwexpdry = np.concatenate([Iexp,Iwexp])
    
    return qqwexpdry,IIwexpdry, IIwexpoperando


def scalingfactor(SLD_A, volfrac_A, dryinv):
    shiftingfactor = (dryinv)/(2*(np.pi**2)) / \
        ((SLD_A**2)*volfrac_A*(1-volfrac_A))
    return shiftingfactor


def volfraccalc(SLD_A, SLD_B, SLD_C, volfrac_A, wetinv, shiftingfactor):
    K = shiftingfactor
    Inv_Wet = wetinv
    c1 = (-Inv_Wet/(2*(np.pi**2)*K)) + (SLD_A-SLD_B) * \
        (SLD_A-SLD_C)*(volfrac_A-(volfrac_A**2))
    c2 = (SLD_B-SLD_A)*(SLD_B-SLD_C)*(1-volfrac_A-((1-volfrac_A)**2))
    b1 = (SLD_B-SLD_A)*(SLD_B-SLD_C)*2*(1-volfrac_A)
    b2 = (SLD_B-SLD_A)*(SLD_B-SLD_C)*-1
    b3 = (SLD_C-SLD_A)*(SLD_C-SLD_B)
    a1 = (SLD_B-SLD_A)*(SLD_B-SLD_C)*-1
    a2 = (SLD_C-SLD_A)*(SLD_C-SLD_B)*-1
    c = c1+c2
    b = b1+b2+b3
    a = a1+a2

    Volfrac1 = ((-b-(((b**2)-(4*a*c))**(1/2)))/(2*a))
    Volfrac2 = ((-b+(((b**2)-(4*a*c))**(1/2)))/(2*a))
    Volfrac3 = 1-volfrac_A
    return Volfrac1, Volfrac2, Volfrac3


def lognormradius(q, A, sig, radius, bgr):
    # def lognormradius(q, A, radius, bgr):
    """
    The model may use all needed parameters for smearing.
    """
    # ff=js.ff.sphere(q=q,radius=radius)
    ff = js.formel.pDA(js.ff.sphere, sig, 'radius',
                       type='lognormal', q=q, radius=radius)  # calc model
    ff.Y = ff.Y * A + bgr  # multiply amplitude factor and add bgr
    return ff


def WhittakerSmooth(x, w, lambda_, differences=1):
    '''
    Penalized least squares algorithm for background fitting

    input
        x: input data (i.e. chromatogram of spectrum)
        w: binary masks (value of the mask is zero if a point belongs to peaks and one otherwise)
        lambda_: parameter that can be adjusted by user. The larger lambda is,  the smoother the resulting background
        differences: integer indicating the order of the difference of penalties

    output
        the fitted background vector
    '''
    X = np.matrix(x)
    m = X.size
    i = np.arange(0, m)
    E = eye(m, format='csc')
    # numpy.diff() does not work with sparse matrix. This is a workaround.
    D = E[1:]-E[:-1]
    W = diags(w, 0, shape=(m, m))
    A = csc_matrix(W+(lambda_*D.T*D))
    B = csc_matrix(W*X.T)
    background = spsolve(A, B)
    return np.array(background)


def airPLS(x, lambda_=2.7, porder=1, itermax=1000):
    '''
    Adaptive iteratively reweighted penalized least squares for baseline fitting

    input
        x: input data (i.e. chromatogram of spectrum)
        lambda_: parameter that can be adjusted by user. The larger lambda is,  the smoother the resulting background, z
        porder: adaptive iteratively reweighted penalized least squares for baseline fitting

    output
        the fitted background vector
    '''
    m = x.shape[0]
    w = np.ones(m)
    for i in range(1, itermax+1):
        z = WhittakerSmooth(x, w, lambda_, porder)
        d = x-z
        dssn = np.abs(d[d < 0].sum())
        if(dssn < 0.001*(abs(x)).sum() or i == itermax):
            if(i == itermax):
                print('WARING max iteration reached!')
            break
        # d>0 means that this point is part of a peak, so its weight is set to 0 in order to ignore it
        w[d >= 0] = 0
        w[d < 0] = np.exp(i*np.abs(d[d < 0])/dssn)
        w[0] = np.exp(i*(d[d < 0]).max()/dssn)
        w[-1] = w[0]
    return z


def ionomercorr(q, Idry, Iwet):

    qtarget = copy.deepcopy(q)
    Itarget = copy.deepcopy(Iwet)

    qdry_exp = copy.deepcopy(q)
    Idry_exp = copy.deepcopy(Idry)

    y2 = Itarget[(qdry_exp > 0.6) & (qdry_exp < 2.5)] / \
        Idry_exp[(qdry_exp > 0.6) & (qdry_exp < 2.5)]
    # corrected values
    c2 = airPLS(y2)
    b2 = y2-c2

    corr = c2*Idry_exp[(qdry_exp > 0.6) & (qdry_exp < 2.5)]
    Itarget[(qdry_exp > 0.6) & (qdry_exp < 2.5)] = corr

    I_ionomer_CL = b2*Idry_exp[(qdry_exp > 0.6) & (qdry_exp < 2.5)]
    q_ionomer = qtarget[(qdry_exp > 0.6) & (qdry_exp < 2.5)]
    ionomer_peak = q_ionomer[I_ionomer_CL == max(I_ionomer_CL)]

    return qtarget, Itarget, ionomer_peak


def satlevel_calc(qqw, IIwdry, IIwwet):
    q_calc = qqw[(qqw>0.05)&(qqw<4)]

    I_dry_calc = IIwdry[(qqw>0.05)&(qqw<4)]
    I_wet_calc = IIwwet[(qqw>0.05)&(qqw<4)]

    # plt.figure(figsize=[5, 5])
    # plt.loglog(q_calc, I_dry_calc)
    # plt.loglog(q_calc, I_wet_calc)

    a, b, c = ionomercorr(q_calc, I_dry_calc, I_wet_calc)
    # plt.figure()
    # plt.loglog(q_calc,b)
    # plt.loglog(q_calc,I_dry_calc)
    # plt.loglog(a,I_wet_calc-b)

    # create dataArray from numpy array\
    # a= q_calc
    # b=I_wet_calc
    i5 = js.dA(np.c_[a, b, np.zeros(len(q_calc))].T)

    # fit it
    # fit it
    i5.fit(lognormradius,  # the fit function
           freepar={'sig': 0.41, 'A': [0.8]},
           # freepar={'A':[1*10**-6]},# freepar with start values; [..] indicate independent fit parameter
           # fixed parameters, single values indicates common fit parameter
           fixpar={'bgr':  I_dry_calc [q_calc == max(q_calc)], 'radius': 1.1},
           mapNames={'q': 'X'},
           # map names of the model to names of data attributes
           condition=lambda a: (a.X > 0.7) & (a.X < 4),
           maxfev=100000)  # a condition to include only specific values

    yy = lognormradius(q_calc, i5.lastfit.A-(0.15*i5.lastfit.A), i5.lastfit.sig,
                       i5.lastfit.radius, i5.lastfit.bgr)

    # plt.figure(figsize=[5,5])
    # plt.loglog(yy.X,b)
    # plt.loglog(yy.X,I_dry_calc)
    # # plt.loglog(yy.X,b-yy.Y)
    # # plt.loglog(yy.X,I_dry_calc-yy.Y)
    # # plt.loglog(yy.X,yy.Y)

    # #plt.ylim([10**-2,10**5])
    # plt.show()

    q_dry_calc_minus_Pt = yy.X
    q_wet_calc_minus_Pt = yy.X
    I_dry_calc_minus_Pt = I_dry_calc-yy.Y
    I_wet_calc_minus_Pt = b-yy.Y
    q_wet_calc_minus_Pt = q_wet_calc_minus_Pt[I_wet_calc_minus_Pt > 0]
    I_wet_calc_minus_Pt = I_wet_calc_minus_Pt[I_wet_calc_minus_Pt > 0]

    dryinv = auc(q_dry_calc_minus_Pt,
                 (I_dry_calc_minus_Pt*(q_dry_calc_minus_Pt**2)))
    wetinv = auc(q_wet_calc_minus_Pt,
                 (I_wet_calc_minus_Pt*(q_wet_calc_minus_Pt**2)))
    shiftingfactor = scalingfactor(17.6*(10**10), 0.3, dryinv)
    vf1, vf2, vf3 = volfraccalc(
        17.6*(10**10), 0, 9.447*(10**10), 0.3, wetinv, shiftingfactor)

    return vf1/vf3


def satlevel_calc_MPL(qqw, IIwdry, IIwwet):
    q_calc = qqw[(qqw>0.05)&(qqw<4)]

    I_dry_calc = IIwdry[(qqw>0.05)&(qqw<4)]
    I_wet_calc = IIwwet[(qqw>0.05)&(qqw<4)]

    q_dry_calc_minus_Pt = q_calc
    q_wet_calc_minus_Pt = q_calc
    I_dry_calc_minus_Pt = I_dry_calc
    I_wet_calc_minus_Pt = I_wet_calc
    q_wet_calc_minus_Pt = q_wet_calc_minus_Pt[I_wet_calc_minus_Pt > 0]
    I_wet_calc_minus_Pt = I_wet_calc_minus_Pt[I_wet_calc_minus_Pt > 0]

    dryinv = auc(q_dry_calc_minus_Pt,
                 (I_dry_calc_minus_Pt*(q_dry_calc_minus_Pt**2)))
    wetinv = auc(q_wet_calc_minus_Pt,
                 (I_wet_calc_minus_Pt*(q_wet_calc_minus_Pt**2)))
    shiftingfactor = scalingfactor(17.8*(10**10), 0.2, dryinv)
    vf1, vf2, vf3 = volfraccalc(
        17.8*(10**10), 0, 9.447*(10**10), 0.2, wetinv, shiftingfactor)

    return vf1/vf3
            















def memgaussfitdry(x, y, startingpoint=None):
   
    b = 2.1

    if startingpoint != None:
        b = startingpoint

    def super_gaussian(x, amplitude=1.0, center=0.0, sigma=1.0, expon=2.0):
        """super-Gaussian distribution
        super_gaussian(x, amplitude, center, sigma, expon) =
            (amplitude/(sqrt(2*pi)*sigma)) * exp(-abs(x-center)**expon / (2*sigma**expon))
        """
        sigma = max(1.e-15, sigma)
        return ((amplitude/(np.sqrt(2*np.pi)*sigma))
                * np.exp(-abs(x-center)**expon / 2*sigma**expon))
    gauss2 = Model(super_gaussian, prefix='g2_')

    gauss1 = GaussianModel(prefix='g1_')

    def porod(x, a, b, c):
        return (a*(x**-b)+c)

    def porod2(x, a):
        return (a*(x**-4))

    linear = Model(porod, prefix='porod_')
    linear2 = Model(porod2, prefix='porod2_')

    pars2 = gauss1.guess(y, x=x)
    pars5 = linear2.make_params()
    pars3 = linear.make_params()
    pars4 = gauss2.make_params()

    pars2['g1_amplitude'].set(min=0)
    pars2['g1_fwhm'].set(min=0)
    pars2['g1_center'].set(value=b, min=b-0.5, max=b+0.5)
    #pars2['g1_center'].set(value=b, min=1,max=2.7)
    pars2['g1_sigma'].set(min=0)

    pars3['porod_a'].set(0.1, min=0)
    pars3['porod_b'].set(4, min=0)
    pars3['porod_c'].set(-0.001, min=-0.01)

    pars5['porod2_a'].set(0.1, min=0)
    # pars5['porod2_b'].set(4,min)

    pars4['g2_amplitude'].set(min=0)
    pars4['g2_sigma'].set(min=0)
    pars4['g2_center'].set(value=b, min=b-0.3, max=b+0.3)
    pars4['g2_expon'].set(value=2, min=2)

    pars = pars3
    pars = pars3 + pars2+pars5
    #pars = pars3+pars4
    mod = linear
    mod = linear+gauss1+linear2
    #mod = gauss2 + linear

    out = mod.fit(y, pars, x=x)
    comps = out.eval_components(x=x)
    params_dict = out.params.valuesdict()
    y = out.best_fit

    # return x,y
    return x, y, params_dict['g1_amplitude'], params_dict['g1_center'], params_dict['g1_fwhm'], out.eval_uncertainty(x=x), params_dict['porod_c']
    # return x, y, params_dict['g2_amplitude'],params_dict['g2_center'], params_dict['g2_sigma'],out.eval_uncertainty(x=x)
   
    

def memgaussfitoperando(x, y, startingpoint=None):
   
    b = 1.6

    if startingpoint != None:
        b = startingpoint

    def super_gaussian(x, amplitude=1.0, center=0.0, sigma=1.0, expon=2.0):
        """super-Gaussian distribution
        super_gaussian(x, amplitude, center, sigma, expon) =
            (amplitude/(sqrt(2*pi)*sigma)) * exp(-abs(x-center)**expon / (2*sigma**expon))
        """
        sigma = max(1.e-15, sigma)
        return ((amplitude/(np.sqrt(2*np.pi)*sigma))
                * np.exp(-abs(x-center)**expon / 2*sigma**expon))
    gauss2 = Model(super_gaussian, prefix='g2_')

    gauss1 = GaussianModel(prefix='g1_')

    def porod(x, a, b, c):
        return (a*(x**-b)+c)

    def porod2(x, a):
        return (a*(x**-4))

    linear = Model(porod, prefix='porod_')
    linear2 = Model(porod2, prefix='porod2_')

    pars2 = gauss1.guess(y, x=x)
    pars5 = linear2.make_params()
    pars3 = linear.make_params()
    pars4 = gauss2.make_params()

    pars2['g1_amplitude'].set(min=0)
    pars2['g1_fwhm'].set(min=0)
    pars2['g1_center'].set(value=b, min=b-0.5, max=b+0.5)
    #pars2['g1_center'].set(value=b, min=1,max=2.7)
    pars2['g1_sigma'].set(min=0)

    pars3['porod_a'].set(0.1, min=0)
    pars3['porod_b'].set(4, min=0)
    pars3['porod_c'].set(-0.001, min=-0.01)

    pars5['porod2_a'].set(0.1, min=0)
    # pars5['porod2_b'].set(4,min)

    pars4['g2_amplitude'].set(min=0)
    pars4['g2_sigma'].set(min=0)
    pars4['g2_center'].set(value=b, min=b-0.3, max=b+0.3)
    pars4['g2_expon'].set(value=2, min=2)

    pars = pars3
    pars = pars3 + pars2+pars5
    #pars = pars3+pars4
    mod = linear
    mod = linear+gauss1+linear2
    #mod = gauss2 + linear

    out = mod.fit(y, pars, x=x)
    comps = out.eval_components(x=x)
    params_dict = out.params.valuesdict()
    y = out.best_fit

    # return x,y
    return x, y, params_dict['g1_amplitude'], params_dict['g1_center'], params_dict['g1_fwhm'], out.eval_uncertainty(x=x), params_dict['porod_c']
    # return x, y, params_dict['g2_amplitude'],params_dict['g2_center'], params_dict['g2_sigma'],out.eval_uncertainty(x=x)
   
 

# 0       12      39      66
# 1       13      40      67
# 2       14      41      68
# 3       15      42      69
# 4       16      43      70
# 5       17      44      71
# 6       18      45      72
# 7       19      46      73
# 8       20      47      74
# 9       21      48      75
# 10      22      49      76
# 11      23      50      77 ##

dspacing_coll_img=copy.deepcopy(Pt_peak_coll_dry)
CCLsatlevel_coll_img=copy.deepcopy(Pt_peak_coll_dry)
ACLsatlevel_coll_img=copy.deepcopy(Pt_peak_coll_dry)
CMPLsatlevel_coll_img=copy.deepcopy(Pt_peak_coll_dry)

for i in range(78):    
    dspacing_coll_img[i][dspacing_coll_img[i]>=0]=0.0
    CCLsatlevel_coll_img[i][CCLsatlevel_coll_img[i]>=0]=0.0
    ACLsatlevel_coll_img[i][ACLsatlevel_coll_img[i]>=0]=0.0
    CMPLsatlevel_coll_img[i][CMPLsatlevel_coll_img[i]>=0]=0.0
    
    for line in range(len(registrationshift_coll[i])): 
        spot,spotmembrane,anodespot,howmuchshift = outMEMCLspot(i, line)
        
        for aa in range(spotmembrane-10,spotmembrane+11):
            
            qqwexpdry,IIwexpdry, IIwexpoperando = outintensityq(i, line, aa, howmuchshift)
            qqwexpoperando = qqwexpdry
            x, y,a,dspacing,c,d,e=  memgaussfitoperando(qqwexpdry[(qqwexpdry>=1)&(qqwexpdry<=2.4)] ,IIwexpdry[(qqwexpdry>=1)&(qqwexpdry<=2.4)])
            x, y,a,dspacingoperando,c,d,e=  memgaussfitoperando(qqwexpdry[(qqwexpdry>=1)&(qqwexpdry<=2.4)] ,IIwexpoperando[(qqwexpdry>=1)&(qqwexpdry<=2.4)])
            dspacing = 2*np.pi/dspacing
            dspacingoperando = 2*np.pi/dspacingoperando
            dspacing_coll_img[i][aa,line]=dspacingoperando
                 
        for aa in range(spot-10,spot+6):
            qqwexpdry,IIwexpdry, IIwexpoperando = outintensityq(i, line, aa, howmuchshift)
            qqwexpoperando = qqwexpdry
            satlevel= satlevel_calc(qqwexpoperando, IIwexpdry, IIwexpoperando)
            satlevel*=100
            CCLsatlevel_coll_img[i][aa,line]=satlevel
 
        for aa in range(anodespot-5,anodespot+6):
            qqwexpdry,IIwexpdry, IIwexpoperando = outintensityq(i, line, aa, howmuchshift)
            qqwexpoperando = qqwexpdry
            satlevel= satlevel_calc(qqwexpoperando, IIwexpdry, IIwexpoperando)
            satlevel*=100
            ACLsatlevel_coll_img[i][aa,line]=satlevel
            
        for aa in range(spot-5,spot+6):
            if aa+20 >=201 :
                aa=200-20
            qqwexpdry,IIwexpdry, IIwexpoperando = outintensityq(i, line, aa+20, howmuchshift)
            qqwexpoperando = qqwexpdry
            satlevel= satlevel_calc_MPL(qqwexpoperando, IIwexpdry, IIwexpoperando)
            satlevel*=100
            CMPLsatlevel_coll_img[i][aa+20,line]=satlevel
            
#%%

for i in range(78)    :     

    fig,ax = plt.subplots(figsize=[6,5],dpi=300)  
    
    plt.title(listss[i][3][0])       
    palette = copy.copy(plt.get_cmap('gist_rainbow'))
    palette.set_under('white', 0)  # 1.0 represents not transparent
    
    p1=ax.imshow(CCLsatlevel_coll_img[i],extent=[0,CCLsatlevel_coll_img[i].shape[1]*30,202*120/201,0],aspect='auto',cmap=palette,vmin=10,vmax=100)
    ax.imshow(CMPLsatlevel_coll_img[i],extent=[0,CCLsatlevel_coll_img[i].shape[1]*30,202*120/201,0],aspect='auto',cmap=palette,vmin=10,vmax=100)
    ax.imshow(ACLsatlevel_coll_img[i],extent=[0,CCLsatlevel_coll_img[i].shape[1]*30,202*120/201,0],aspect='auto',cmap=palette,vmin=10,vmax=100)
    p2=ax.imshow(dspacing_coll_img[i],extent=[0,CCLsatlevel_coll_img[i].shape[1]*30,202*120/201,0],aspect='auto',cmap=palette,vmin=2.7,vmax=4)
    ax.set_xlabel('MEA through-plane [microns]')
    ax.set_ylabel('MEA in-plane [microns]')
    ax.imshow(Pt_peak_coll_dry[i],extent=[0,CCLsatlevel_coll_img[i].shape[1]*30,202*120/201,0],aspect='auto',cmap='Greys',alpha=0.5)
    cbar2=plt.colorbar(p1)
    cbar=plt.colorbar(p2)
    cbar.ax.set_ylabel('d-spacing [nm]',rotation=270, labelpad=10)
    cbar2.ax.set_ylabel('Sat. Level [%]',rotation=270)
    plt.savefig(listss[i][3][0]+'.svg')          























