**FCSAXS : Small Angle X-ray Scattering (SAXS) Data Interpretation Tools for Polymer Electrolyte Fuel Cell Application (PEFC)**

**WHY IS THIS PACKAGE NECESSARY?**
1) You can intuitively generate electron density 3D map (refered to 3D image) and get your simulated SAXS profile (Intensity versus scattering vector q) without complicated derivation of analytical solution. Approach could be extendable to small angle neutron scattering (SANS). 

See functions.py -> img2sas function, 512^3 3D image when run on CPU with 10 workers **1 iteration merely needs ~3.5 seconds** computing time! 

With GPU, even faster! **only 1.3 s!!!**

2) You can intuitively modify **multiphasic porous media** attributes in 3D image setting and see how the SAXS profiles will behave accordingly, and confirm with your experimental SAXS data.


3) More specifically, SAXS data can help investigation of **water management in nanoporous catalyst layer (CL) of PEFC** in real operating condition. Could also be extended to nanoporous microporous layers (MPL) fitting to the experimental q-range.

**This repository consists of functions and modules on:**

- How to get **numerical scattering from 3D images of basic elemental shapes** and compare to its analytical solution (basic shapes such as sphere, cube, cylinder, ellipsoid, porous structure with spherical pores)

To gain confidence of the tool img2sas, we tested on systems with known analytical solution. 

- How to **analytically fit a scattering profile of Pt/C catalyst layer & non-nobel metal catalyst layer** for PEFC application with Intersected Boolean model 

As an input to img2sas for baseline structure generation (in our case dry structure), we used stochastic modeling based on SAXS experimental data.
First, we fit SAXS data with analytical solution to get some parameters for image generation. 


<img src="https://gitlab.psi.ch/fcsaxs/fcsaxs/-/raw/master/VideosandPictures/IBM_PtCfitting.gif" width="70%" >

- How to get **3D representative structure from the analytical fit of Pt/C catalyst layer & non-nobel metal catalyst layer** with Intersected Boolean model

After getting the parameters from analytical fit, we generated the 3D image. See below for visualization purposes. 
One can also use other imaging techniques such as TEM and Ptychographic volumes as an input to img2sas. 

<img src="https://gitlab.psi.ch/fcsaxs/fcsaxs/-/raw/master/VideosandPictures/PtCfitting.gif" >



![Alt Text](https://gitlab.psi.ch/fcsaxs/fcsaxs/-/raw/d63144e4d5990b679243d9f7a1981340f5db73e5/VideosandPictures/PtC_tes1__2_.gif)


- How to **simulate 3 different wetting scenarios in 3D** (larger pores filled first, smaller pores filled first, thin film formation) and get the corresponding numerical scattering 

Once we acquire dry image, we can start experimenting _in silico_ how water will fill the catalyst layer. 

![Alt Text](https://gitlab.psi.ch/fcsaxs/fcsaxs/-/raw/master/VideosandPictures/newrainbowfig6.png)

- How to apply **three-phase Invariant calculation** in order to get liquid saturation level from a multiphasic structure (liquid-solid-void)

Regardless the mechanism of water filling and its scattering vector dependency, we can deduce saturation level by Invariant calculation of SAXS profile. 

![Alt Text](https://gitlab.psi.ch/fcsaxs/fcsaxs/-/raw/master/VideosandPictures/Invariant3rdphase.png)

- How the liquid saturation level from **Invariant calculation is verified with voxel counting** the liquid phase of 3D image.

The resulting saturation level derived from Invariant calculation is verified to the actual water content in the generated 3D image. 

Dependencies: raster-image, Porespy, Gradient-Free-optimizers, Hyperactive, Jscatter, among many others, please install the required packages before running
Recommended to run on Spyder environment

Please cite this paper when using any part of the codes :
https://doi.org/10.1021/acsami.3c00420
Some images here may be taken from the paper. 
