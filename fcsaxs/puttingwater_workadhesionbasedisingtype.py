#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 24 17:24:01 2021

@author: kinanti_a
"""
import numpy as np
import matplotlib.pyplot as plt
import copy
from scipy.ndimage import convolve, generate_binary_structure
import porespy as ps
import random
import math

carbonenergy = 1
voidenergy = 0
waterenergy = 2
mode = 'constant'
boxsize=500
voxelsize=1
surface_energy_water=72#69.6    #69.6 mN/m water to air    
surface_energy_carbon=95.67
deg = 180


surface_energy_water=72#69.6    #69.6 mN/m water to air    
surface_energy_carbon=63.93
workofadhesion_carbon=surface_energy_water*(1+(np.cos(math.radians(deg) )  )  ) 
#workofadhesion_Pt=surface_energy_water*(1+(np.cos(math.radians(degree2) )  )  )         #60-72.5*cos80     #150#83.5840868344        #95.67-69.6*cos(80deg) = 
surface_energy_water*=0.001
workofadhesion_carbon*=0.001
#workofadhesion_Pt*=0.001
#reference carbon contact angle and surface energyhttps://pubs.acs.org/doi/pdf/10.1021/la9621224

#%%
#create model of overlapping carbon particles
blob= ps.generators.overlapping_spheres([boxsize,boxsize,boxsize],20,0.4).astype(float)

#create model of cylinder
# import raster_geometry
# blob=raster_geometry.cylinder([50,50,50], 50,10)


#labeling the components
blob=blob.astype(float)
blob=1-blob
maxwaterfrac=np.count_nonzero(blob)/(blob.shape[0]**3)
maxwaterfrac=1-maxwaterfrac
blob[blob==0]=10
blob[blob==1]=5


blob[blob==10]=voidenergy
blob[blob==5]=carbonenergy


#set water volume fraction must be smaller than maxwaterfrac
waterfrac=0.2

#%%randomize water as initial state

voidinblob = np.argwhere(blob==voidenergy)
water_frac_ind = int(waterfrac/(voidinblob.shape[0]/(blob.shape[0]**3))*voidinblob.shape[0])



ind_shuffle = random.sample(range(0,len(voidinblob)),water_frac_ind)
 

for ind in ind_shuffle:
    ind_voidblob_swapx, ind_voidblob_swapy, ind_voidblob_swapz  = voidinblob[ind]
    blob[ind_voidblob_swapx, ind_voidblob_swapy, ind_voidblob_swapz ]=waterenergy
A=blob

boxsize,boxsize,boxsize=A.shape

plt.imshow(A[:,:,5])

#%% old calculate neighbor

# import scipy, scipy.ndimage

# def nb_vals(matrix, indices):
#     neighbor=[]
#     for ind in indices:
#         matrix = np.array(matrix)
        
 
#         a=ind[0]
#         b=ind[1]
#         c=ind[2]
#         a,b,c=a+1,b+1,c+1
#         firstneighbor=matrix[a+1,b,c]
#         secondneighbor=matrix[a-1,b,c]
#         thirdneighbor=matrix[a,b+1,c]
#         forthneighbor=matrix[a,b-1,c]
#         fifthneighbor=matrix[a,b,c-1]
#         sixthneighbor=matrix[a,b,c+1]
        
#         neighbor.append([firstneighbor,secondneighbor,thirdneighbor,forthneighbor,fifthneighbor,sixthneighbor])
         
#     return neighbor

# tes=nb_vals(blob,[[0,0,0]])
# a=np.argwhere(blob==waterenergy)

#%%count energy difference, energy new must be better than energy old

import time
import numpy as np

def energyoldnew(matrix, indicesnew, indicesold):
    start = time.time()

    # Define the relative positions of the 6 nearest neighbors
    neighbor_offsets = np.array([[+1+1, 1, 1], [-1+1, 1, 1], [1, +1+1, 1], [1, -1+1, 1], [1, 1, -1+1], [1, 1, 1+1]])

    # Create a padded version of the matrix
    matrix_padded = matrix

    # Calculate the coordinates of the neighbors for each index
    neighbor_coords_old = indicesold[:, np.newaxis] + neighbor_offsets
    neighbor_coords_new = indicesnew[:, np.newaxis] + neighbor_offsets
    # print(neighbor_coords_new[0],indicesnew[0])

    # Extract the values of the neighbors efficiently using advanced indexing
    neighbors_old = matrix_padded[neighbor_coords_old[:, :, 0], neighbor_coords_old[:, :, 1], neighbor_coords_old[:, :, 2]]
    neighbors_new = matrix_padded[neighbor_coords_new[:, :, 0], neighbor_coords_new[:, :, 1], neighbor_coords_new[:, :, 2]]
    

    # print(compare[0],neighbors_new[0])
    # Calculate the counts for each neighbor type
    count_waterneighbor_old = np.sum(neighbors_old == waterenergy, axis=1)
    count_carbonneighbor_old = np.sum(neighbors_old == carbonenergy, axis=1)
    count_voidneighbor_old = np.sum(neighbors_old == voidenergy, axis=1)

    count_waterneighbor_new = np.sum(neighbors_new == waterenergy, axis=1)
    count_carbonneighbor_new = np.sum(neighbors_new == carbonenergy, axis=1)
    count_voidneighbor_new = np.sum(neighbors_new == voidenergy, axis=1)

    voxelsize_sq = (voxelsize * (10**-9))**2

    energy_old = (
        -2 * count_waterneighbor_old * voxelsize_sq * surface_energy_water
        - count_carbonneighbor_old * voxelsize_sq * workofadhesion_carbon
    )

    energy_new = (
        -2 * count_waterneighbor_new * voxelsize_sq * surface_energy_water
        - count_carbonneighbor_new * voxelsize_sq * workofadhesion_carbon
    )

    energy_difference = energy_old - energy_new

    # print("energycount", time.time() - start)

    return energy_difference, energy_new, energy_old

#monte carlo step with acceptance prob. even if the energy is not favorable

def mcstep(step):
    start = time.time()
    temp = var_dict['temp']
    energy_time2 = var_dict['energy_time']
    temp = to_numpy_array(temp, A.shape)
    energy_time2 = to_numpy_array(energy_time2, [MC_step])

    temperature = 200

    # Get the indices of water and void elements
    list_water = np.argwhere(temp == waterenergy)
    list_void = np.argwhere(temp == voidenergy)

    # Generate random samples efficiently
    randint_water = np.random.choice(len(list_water), size=4000, replace=False)
    randint_void = np.random.choice(len(list_void), size=4000, replace=False)

    indexold = list_water[randint_water]
    indexnew = list_void[randint_void]

    tempwrap = np.pad(temp, ((1, 1), (1, 1), (1, 1)), 'reflect')
    energy_difference, energy_new, energy_old = energyoldnew(tempwrap, indexnew, indexold)

    t = temperature / float(step + 1)
    metropolis = np.exp(-0.3 / t)

    # Create masks for elements with positive energy differences
    positive_energy_mask = energy_difference > 0

    # Update temp based on masks
    temp[tuple(indexold[positive_energy_mask].T)] = voidenergy
    temp[tuple(indexnew[positive_energy_mask].T)] = waterenergy
    
    # Create masks for elements with negative energy differences
    negative_energy_mask = ~positive_energy_mask

    # Check if elements should be updated based on metropolis criterion
    random_values = np.random.rand(len(indexold))
    update_mask = random_values < metropolis

    # Update temp based on metropolis criterion
    temp[tuple(indexold[negative_energy_mask & update_mask].T)] = voidenergy
    temp[tuple(indexnew[negative_energy_mask & update_mask].T)] = waterenergy

  
    energy_time2[step] = energy_difference.sum() / (energy_difference).shape[0]
    step += 1

    my_list1 = energy_time2[step - 1500:step]
    if np.abs(np.std(my_list1)/np.abs(np.mean(my_list1)))<0.01:
        print(np.std(my_list1)/np.mean(my_list1))


    temp = to_shared_array(temp, False, "temp")
    energy_time2 = to_shared_array(energy_time2, False, "energy")
    print(step)


#%%

MC_step=100000
temp = copy.deepcopy(A)
energy_time2 = np.zeros(MC_step)
list_void2=np.argwhere(A==voidenergy)


import time
import numpy as np
import time
from multiprocessing import Pool, RawArray
import multiprocessing as mp
import ctypes as c
from multiprocessing import shared_memory

# A global dictionary storing the variables passed from the initializer.
var_dict = {}

def init_worker(temp,energy_time2):
    # Using a dictionary is not strictly necessary. You can also
    # use global variables.
    var_dict['temp'] = temp
    var_dict['energy_time'] = energy_time2

def to_shared_array(arr,create,name):

    d_size = np.dtype(float).itemsize * np.prod(arr.shape)
    if (create==True):
        shm = shared_memory.SharedMemory(create=False, size=d_size,name=name)
        shm.close()
        shm.unlink()
        

    shm = shared_memory.SharedMemory(create=create, size=d_size,name=name)
    # numpy array on shared memory buffer
    dst = np.ndarray(shape=arr.shape, dtype=float, buffer=shm.buf)
    dst[:] = arr[:]

    return shm


def to_shared_array_init(arr,create,name):

    d_size = np.dtype(float).itemsize * np.prod(arr.shape)


    shm = shared_memory.SharedMemory(create=create, size=d_size,name=name)
    # numpy array on shared memory buffer
    dst = np.ndarray(shape=arr.shape, dtype=float, buffer=shm.buf)
    dst[:] = arr[:]

    return shm

def to_numpy_array(shared_array, shape):
    '''Create a numpy array backed by a shared memory Array.'''
    arr = np.ndarray(shape, dtype=float, buffer=shared_array.buf)
    return arr

init=0
cont=1
name='npar'
if (init==0)&(cont==0):
    temp=to_shared_array(temp,True,"temp")
    energy_time2=to_shared_array(energy_time2,True,"energy")
elif cont==1:
    temp=to_shared_array(temp,False,"temp")
    energy_time2=to_shared_array(energy_time2,False,"energy")
    
else:
    temp=to_shared_array_init(temp,True,"temp")
    energy_time2=to_shared_array_init(energy_time2,True,"energy")
with Pool(processes=30, initializer=init_worker, initargs=(temp, energy_time2)) as pool:
    pool.map(mcstep, range(MC_step-1))
    pool.close()
    pool.join()
       # mapping.wait()
       
       
import multiprocessing
# get all active child processes
active = multiprocessing.active_children()
for child in active:
    child.terminate()
 
#%%
#conversion of arrays in shared memory to ordinary np array
temp=to_numpy_array(temp, A.shape)
energy_time2=to_numpy_array(energy_time2, [MC_step])

#see a slice
plt.figure()
plt.imshow(temp[:,25,:])
#see if convergence is achived
plt.figure()
plt.plot(energy_time2)
#see another slice
plt.figure()
plt.imshow(temp[:,:,5])
#compare to before
plt.figure()
plt.imshow(A[:,:,5])
#%%

import functions
functions.export_tiff("porespypores_180_init.tif",A)
functions.export_tiff("porespypores_180_end.tif",temp)

