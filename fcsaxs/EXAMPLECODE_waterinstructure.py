#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 16 14:00:41 2022

@author: kinanti_a
"""
import pickle
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
from pylab import cm
from matplotlib import colors
import porespy as ps

#load pkl files of Pt/C structure

directoryfittingPt= 'SAXSdataandpicklefiles/20230105goodanalyticalfit/fittingPt_nopadding_verygoodanalytical_periodic_nopad512.pkl'

with open(directoryfittingPt, 'rb') as f:
    Pt_struct= pickle.load(f)

im_Pt =  Pt_struct['im_Pt']

#state that there is no need for padding (repeat structure not to have boundary effect) or windowing (tapering structure)

import functions
functions.padding_or_not(0)
functions.windowing_or_not(0)
#img2sas Pt/C structure
I,q,im_Pt = functions.img2sas(im_Pt, 512, 1,returnim=1)


def realspace(q):
    return 2*np.pi/q

def reciprocal(q):
    return 2*np.pi/q
#,cmap=cmap, norm=

#visualize cross section of Pt/C
cmap = 'jet'
bounds=[0,16*(10**10),20*(10**10),180*(10**10)]
norm =colors.Normalize(vmin=0, vmax=np.max(im_Pt), clip=False)
plt.figure(figsize=[5,5],dpi=300)
plt.imshow(im_Pt[:,:,250],cmap=cmap,norm=norm)
plt.ylabel('[nm]')
plt.xlabel('[nm]')

#%%
import liquid_filling
import functions
import tifffile
from itertools import chain
import porespy as ps
#do local thickness of Pt/C structure 
watershed_im_Pt = functions.trim_localthickness(im_Pt)
functions.export_tiff('localthicknessmap_imPtdecanepaper1.tif',watershed_im_Pt)
watershed_im_Pt=tifffile.imread('localthicknessmap_imPtdecanepaper1.tif')

#%%

#calculate pore size distribution 
psd = ps.metrics.pore_size_distribution(im=watershed_im_Pt,log=False,bins=30)

fig, ax = plt.subplots(figsize=(5, 4),dpi=300)
ax.set_xlabel('Pore Radius [nm]')
ax.set_ylabel('Normalized Volume Fraction')
ax.bar(x=psd.R, height=psd.pdf, width=psd.bin_widths, edgecolor='k');

#%%
from liquid_filling import filling



#state padding/windowing

functions.padding_or_not(0)
functions.windowing_or_not(0)

#make filling of im_Pt 

liquidfillingIBMPt = filling(im_Pt, 512, 1)

#define pore map (called watershed but it is not!!! it is local thickness

liquidfillingIBMPt.watershed = watershed_im_Pt




#define pore radius step to fill / thin film thickness 

fillingradiilargepores = chain(np.arange(70,50,-10),np.arange(50,10,-5),np.linspace(10,0,21,endpoint=True))
liquidfillingIBMPt.largeporefirst_fill(fillingradiilargepores,exporttiff=False)


int_largepores=liquidfillingIBMPt.Int_collection_largeporesfirst
satlevel_largepores=liquidfillingIBMPt.saturationlevel

q_largepores = liquidfillingIBMPt.qdry


fillingradiismallpores = chain(np.linspace(0.5,10.5,21,endpoint=True),np.arange(10,50,5),np.arange(50,75,5))
liquidfillingIBMPt.smallporefirst_fill(fillingradiismallpores,exporttiff=False)

int_smallpores=liquidfillingIBMPt.Int_collection_smallporesfirst
satlevel_smallpores=liquidfillingIBMPt.saturationlevel_smallporesfirst


fillingthinfilm = chain(np.arange(1,10,1),np.arange(10,30,2),np.arange(30,150,10))
liquidfillingIBMPt.thinfilm_fill(fillingthinfilm, exporttiff=False)
int_thinfilm=liquidfillingIBMPt.Int_collection_thinfilm
satlevel_thinfilm=liquidfillingIBMPt.saturationlevelthinfilm


#store mechanistic result in pkl files 


K_img2sas= Pt_struct['K_img2sas']
metadata ={}
#change date 
metadata['date']='20230109'
metadata['im_Pt']=im_Pt
metadata['localthickness_map']=watershed_im_Pt
metadata['K_img2sas']=K_img2sas
metadata['radii_largepores']=list(fillingradiilargepores)
metadata['radii_smallpores']=list(fillingradiismallpores)
metadata['radii_thinfilm']=list(fillingthinfilm)
metadata['int_collection_largepores']=int_largepores
metadata['int_collection_smallpores']=int_smallpores
metadata['int_collection_thinfilm']=int_thinfilm
metadata['satlevel_collection_largepores']=satlevel_largepores
metadata['satlevel_collection_smallpores']=satlevel_smallpores
metadata['satlevel_collection_thinfilm']=satlevel_thinfilm
metadata['q']=q_largepores


with open('wetting3mechanism.pkl', 'wb+') as f:
    pickle.dump(metadata, f)


#%%
#open pkl of wetting mechanism
import pickle
import matplotlib.pyplot as plt
with open('wetting3mechanism.pkl', 'rb') as fe:
    wetting= pickle.load(fe)
    
K_img2sas = wetting['K_img2sas']


windowing_or_not=0
padding_or_not=0
functions.windowing_or_not(windowing_or_not)
functions.padding_or_not(padding_or_not)

import shelve 


def realspace(q):
    return 2*np.pi/q

def reciprocal(q):
    return 2*np.pi/q


from itertools import chain
import numpy as np


#state pore radius step to fill / thin film thickness again, sometimes it is not saved in .pkl
fillingradiilargepores = chain(np.arange(70,50,-10),np.arange(50,10,-5),np.linspace(10,0,21,endpoint=True))
fillingradiismallpores = chain(np.linspace(0.5,10.5,21,endpoint=True),np.arange(10,50,5),np.arange(50,75,5))
fillingthinfilm = chain(np.arange(1,10,1),np.arange(10,30,2),np.arange(30,150,10))
wetting['radii_smallpores'] = list(fillingradiismallpores)
wetting['radii_largepores'] = list(fillingradiilargepores)
wetting['radii_thinfilm'] = list(fillingthinfilm)


from scipy.interpolate import UnivariateSpline
from scipy.stats import binned_statistic
from scipy.signal import savgol_filter
import gc
from scipy.interpolate import make_interp_spline
from statsmodels.nonparametric.kernel_regression import KernelReg
from lmfit.models import SkewedGaussianModel , LinearModel, Model, PowerLawModel

from scipy.optimize import curve_fit

from lmfit.models import ExponentialModel, GaussianModel, PolynomialModel

def moving_average(x, w):
    return np.convolve(x, np.ones(w), 'valid') / w

# def splining(qexp,Iexp,q_img2sas,I_img2sas):
#     q_img2sas_ori = q_img2sas
#     I_img2sas_ori = I_img2sas
    
#     artificialq=np.logspace(np.log10(0.001),np.log10(np.max(qexp)),500)
    
#     #s = make_interp_spline(qexp, Iexp)
#     s = UnivariateSpline(qexp, Iexp,s=0)
#     Iexp = s(artificialq)
#     qexp = artificialq
    
#     # I_img2sas = moving_average(I_img2sas, 5)
#     # q_img2sas = moving_average(q_img2sas, 5)
    
    
    
#     q_img2sas = np.where((q_img2sas<qexp[-1])&(q_img2sas>qexp[0]),q_img2sas,-1)
#     I_img2sas = np.where(q_img2sas>0,I_img2sas,-1)
#     q_img2sas_filter = q_img2sas[(I_img2sas>0)&(q_img2sas>0)]
#     I_img2sas_filter = I_img2sas[(I_img2sas>0)&(q_img2sas>0)]

#     s=qexp
#     ys=Iexp
#     so, edges, _ = binned_statistic(s,ys, statistic='median', bins=np.logspace(np.log10(0.001),np.log10(np.max(q_img2sas_ori)),150))
#     d=edges[:-1]+np.diff(edges)/2

#     q_img2sas_filter 
#     ys = so

#     so, edges, _ = binned_statistic(q_img2sas_filter,I_img2sas_filter, statistic='median', bins=np.logspace(np.log10(0.001),np.log10(np.max(q_img2sas_ori)),150))
#     d=edges[:-1]+np.diff(edges)/2

#     q_img2sas_filter = d
#     I_img2sas_filter = so
    
    
#     mask = np.logical_and(~np.isnan(I_img2sas_filter), ~np.isnan(ys))
    
#     q_img2sas_filter_ = q_img2sas_filter[mask]
#     ys_ = ys[mask]
#     I_img2sas_filter_  = I_img2sas_filter[mask]
    
    
    
#     # #I_img2sas_filter_ = savgol_filter(I_img2sas_filter_, 61, 4)
#     # kr = KernelReg(I_img2sas_filter_,q_img2sas_filter_,'c')

#     # I_img2sas_filter_, y_std = kr.fit(q_img2sas_filter_)
    
#     gc.collect(generation=2)
#     #return artificialq,ys_,I_img2sas_filter_
#     return q_img2sas_filter_,ys_,I_img2sas_filter_

def extrapolatesaxs(qexp, Iexp):
    """
    A function to extrapolate a SAXS profile with Guinier extrapolation at low-q
    and Porod extrapolation at high-q

    Parameters
    ----------
    qexp : array
        q data of a SAXS profile to be extrapolated.
    Iexp : array
        Intensity data of a SAXS profile to be extrapolated.

    Returns
    -------
    qextra : array
        Extrapolated q, both high-q and low-q.
    Iextra : array
        Extrapolated intensity, both high-q and low-q.

    """
    setvaluea,setvalueb= np.polyfit((qexp[-10:-1]**-4),(Iexp[-10:-1]),1)
    # #porod function
    def porod(x,a,c):
        return a*(x**-4)+c
    #fitting the porod
    mod = Model(porod, prefix='porod_')
    pars= mod.make_params()
    pars['porod_a'].set(value=setvaluea, min=0)
    pars['porod_c'].set(value=setvalueb, min=0)
    out = mod.fit(Iexp[-10:-1], pars, x=qexp[-10:-1])
    params_dict=out.params.valuesdict()
    C = params_dict['porod_c']

    qextra =  qexp
    Iextra = Iexp-C
    return qextra, Iextra


data = np.loadtxt(
    "/mnt/nas_Uwrite/AK54/KinanSAXSfiles/Kinantifffromporespy/Paperwithcodes/Ex-situ_data1/PtCdecane_dry_2.txt"
)
data = data[~np.isnan(data).any(axis=1)]
datawet = np.loadtxt(
    "/mnt/nas_Uwrite/AK54/KinanSAXSfiles/Kinantifffromporespy/Paperwithcodes/Ex-situ_data1/PtCdecane_wet_2.txt"
)
datawet = datawet[~np.isnan(datawet).any(axis=1)]

# cleaning data of Pt/C and extrapolation
qexp = data[:-50, 0]
Iexp = data[:-50, 1]
Eexp = data[:-50, 2]
qexpwet = datawet[:-50, 0]
Iexpwet = datawet[:-50, 1]
Eexpwet = datawet[:-50, 2]
qexp = qexp[Iexp > 0.1]
qexpwet = qexpwet[Iexpwet > 0.1]

Eexp = Eexp[Iexp > 0.1]
Eexpwet = Eexpwet[Iexpwet > 0.1]
Iexp = Iexp[Iexp > 0.1]
Iexpwet = Iexpwet[Iexpwet > 0.1]

# qexpwet, Iexpwet = splining(qexpwet, Iexpwet)
# qexp, Iexp = splining(qexp, Iexp)
qexpwet,Iexpwet=extrapolatesaxs(qexpwet,Iexpwet)
qexp,Iexp=extrapolatesaxs(qexp,Iexp)

#checking if Int simulated stil == Int experimental 
import matplotlib
matplotlib.rcParams.update(matplotlib.rcParamsDefault)

K_img2sas = functions.find_scalingfactor(qexp,Iexp,wetting['q'],wetting['int_collection_smallpores'][0])

q_binned, I_exp_binned, I_img2sas_binned=functions.binningq(qexp, Iexp, wetting['q'],wetting['int_collection_smallpores'][0])
K_img2sas = (np.sum(I_exp_binned[:10])/len(I_exp_binned[:10]))/(np.sum( I_img2sas_binned[:10])/len( I_img2sas_binned[:10]))
plt.figure()
plt.loglog(q_binned, I_exp_binned)
plt.loglog(q_binned, I_img2sas_binned*K_img2sas)
plt.show()
    
#%%

#the cell contains plotting of the 3 mechanisms 
    
import functions


K_img2sas = functions.find_scalingfactor(qexp,Iexp,wetting['q'],wetting['int_collection_smallpores'][0])


import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
from pylab import cm

# Edit the font, font size, and axes width
mpl.rcParams['font.family'] = 'Times New Roman'
plt.rcParams['font.size'] = 60
plt.rcParams['axes.linewidth'] = 2
 

n=len(wetting['int_collection_smallpores'])


colors = plt.cm.magma(np.linspace(0,1,101,endpoint=True))


factor =1


fig = plt.figure(figsize=(190,140))

import matplotlib.pylab as pylab
params = {'legend.fontsize': 150,
          'figure.figsize': (5, 5),
         'axes.labelsize': 170,
         'axes.titlesize':170,
         'xtick.labelsize':170,
         'ytick.labelsize':170}
pylab.rcParams.update(params)
import matplotlib.ticker as ticker

axa= fig.add_subplot(231)
axa.set_box_aspect(1)

ax2a = axa.secondary_xaxis(location='top',functions= (reciprocal,realspace))
ax2a.set_xscale('linear')

axa.xaxis.set_tick_params(which='major', size=27, width=10, direction='in')
axa.xaxis.set_tick_params(which='minor', size=17, width=10, direction='in')
axa.yaxis.set_tick_params(which='major', size=27, width=10, direction='in', right='on')
axa.yaxis.set_tick_params(which='minor', size=17, width=10, direction='in', right='on')


ax2a.xaxis.set_tick_params(which='major', size=27, width=10, direction='in')
ax2a.xaxis.set_tick_params(which='minor', size=17, width=10, direction='in')

def find_saturation(satarray,sattarget):
     satarray = np.asarray(satarray)
     idx = (np.abs(satarray - sattarget)).argmin()
     return idx
 

n=len(wetting['int_collection_smallpores'])
axa.loglog(wetting['q'],K_img2sas*wetting['int_collection_smallpores'][0],'o-',color=cm.viridis(np.abs(wetting['satlevel_collection_smallpores'][0])), markersize=50,label='Simulated Dry',linewidth=15)

for i in [
          find_saturation(wetting['satlevel_collection_smallpores'],0.15),
          find_saturation(wetting['satlevel_collection_smallpores'],0.3),
          find_saturation(wetting['satlevel_collection_smallpores'],0.5),
          find_saturation(wetting['satlevel_collection_smallpores'],0.7),
          find_saturation(wetting['satlevel_collection_smallpores'],0.9),
          find_saturation(wetting['satlevel_collection_smallpores'],1)]:
    axa.loglog(wetting['q'],K_img2sas*wetting['int_collection_smallpores'][i],'o-',color=cm.viridis(np.abs(wetting['satlevel_collection_smallpores'][i])), markersize=50,linewidth=15,label ='Sat. '+str(int(round(wetting['satlevel_collection_smallpores'][i]*100,0)))+' %')
axa.set_xlim([0.0135909, 1.68472])
axa.set_ylim([10 ** -1, 10 ** 7])


axa.set_xlabel(r'q [nm$^{-1}$]',fontsize=170, labelpad=10)
axa.set_ylabel('Intensity [a.u.]', fontsize=170, labelpad=10)

ax2a.set_xlabel('Dimension [nm]',fontsize=170,  labelpad=10)


axa.legend(bbox_to_anchor=(1, 1), loc=1, frameon=True, fontsize=100)

axa.text(-0.1, 1.1,'a-I)', transform=axa.transAxes, 
            size=170, weight='bold')




axba= fig.add_subplot(234)
axba.set_box_aspect(1)

ax2ba = axba.secondary_xaxis(location='top',functions= (reciprocal,realspace))
ax2ba.set_xscale('linear')

axba.xaxis.set_tick_params(which='major', size=27, width=10, direction='in')
axba.xaxis.set_tick_params(which='minor', size=17, width=10, direction='in')
axba.yaxis.set_tick_params(which='major', size=27, width=10, direction='in', right='on')
axba.yaxis.set_tick_params(which='minor', size=17, width=10, direction='in', right='on')

ax2ba.xaxis.set_tick_params(which='major', size=27, width=10, direction='in')
ax2ba.xaxis.set_tick_params(which='minor', size=17, width=10, direction='in')


n=len(wetting['int_collection_smallpores'])
axba.loglog(wetting['q'],wetting['int_collection_smallpores'][0]/wetting['int_collection_smallpores'][0],'o-',color=cm.viridis(np.abs(wetting['satlevel_collection_smallpores'][0])), markersize=50,linewidth=15,label='Simulated Dry')

for i in [
          find_saturation(wetting['satlevel_collection_smallpores'],0.15),
          find_saturation(wetting['satlevel_collection_smallpores'],0.3),
          find_saturation(wetting['satlevel_collection_smallpores'],0.5),
          find_saturation(wetting['satlevel_collection_smallpores'],0.7),
          find_saturation(wetting['satlevel_collection_smallpores'],0.9),
          find_saturation(wetting['satlevel_collection_smallpores'],1)]:
    axba.loglog(wetting['q'],wetting['int_collection_smallpores'][i]/wetting['int_collection_smallpores'][0],'o-',color=cm.viridis(np.abs(wetting['satlevel_collection_smallpores'][i])), markersize=50,linewidth=15,label ='Sat. '+str(int(round(wetting['satlevel_collection_smallpores'][i]*100,0)))+' %')

axba.set_yscale('linear')
axba.set_ylim([-0.1,2])

axba.set_xlim([0.0135909, 1.68472])



axba.set_xlabel(r'q [nm$^{-1}$]',fontsize=170, labelpad=10)
axba.set_ylabel('Intensity Wet/Dry', fontsize=170, labelpad=10)


ax2ba.set_xlabel('Dimension [nm]',fontsize=170,  labelpad=10)


axba.legend(bbox_to_anchor=(1, 1), loc=1, frameon=True, fontsize=100)

axba.text(-0.1, 1.1,'b-I)', transform=axba.transAxes, 
            size=170, weight='bold')



axd = fig.add_subplot(232)
axd.set_box_aspect(1)

ax2d = axd.secondary_xaxis(location='top',functions= (reciprocal,realspace))
ax2d.set_xscale('linear')

axd.xaxis.set_tick_params(which='major', size=27, width=10, direction='in')
axd.xaxis.set_tick_params(which='minor', size=17, width=10, direction='in')
axd.yaxis.set_tick_params(which='major', size=27, width=10, direction='in', right='on')
axd.yaxis.set_tick_params(which='minor', size=17, width=10, direction='in', right='on')


ax2d.xaxis.set_tick_params(which='major', size=27, width=10, direction='in')
ax2d.xaxis.set_tick_params(which='minor', size=17, width=10, direction='in')
n=len(wetting['int_collection_largepores'])
axd.loglog(wetting['q'],K_img2sas*wetting['int_collection_largepores'][0],'o-',color=cm.viridis(np.abs(wetting['satlevel_collection_largepores'][0])), markersize=50,label='Simulated Dry',linewidth=15)

for i in [
          find_saturation(wetting['satlevel_collection_largepores'],0.15),
          find_saturation(wetting['satlevel_collection_largepores'],0.3),
          find_saturation(wetting['satlevel_collection_largepores'],0.5),
          find_saturation(wetting['satlevel_collection_largepores'],0.7),
          find_saturation(wetting['satlevel_collection_largepores'],0.9),
          find_saturation(wetting['satlevel_collection_largepores'],1)]:
    axd.loglog(wetting['q'],K_img2sas*wetting['int_collection_largepores'][i],'o-',color=cm.viridis(np.abs(wetting['satlevel_collection_largepores'][i])), markersize=50,linewidth=15,label ='Sat. '+str(int(round(wetting['satlevel_collection_largepores'][i]*100,0)))+' %')

axd.set_xlim([0.0135909, 1.68472])
axd.set_ylim([10 ** -1, 10 ** 7])


axd.set_xlabel(r'q [nm$^{-1}$]',fontsize=170, labelpad=10)
axd.set_ylabel('Intensity [a.u.]', fontsize=170, labelpad=10)


ax2d.set_xlabel('Dimension [nm]',fontsize=170,  labelpad=10)


axd.legend(bbox_to_anchor=(1, 1), loc=1, frameon=True, fontsize=100)

axd.text(-0.1, 1.1,'a-II)', transform=axd.transAxes, 
            size=170, weight='bold')


axea=fig.add_subplot(235)

axea.set_box_aspect(1)


ax2ea = axea.secondary_xaxis(location='top',functions= (reciprocal,realspace))
ax2ea.set_xscale('linear')

axea.xaxis.set_tick_params(which='major', size=27, width=10, direction='in')
axea.xaxis.set_tick_params(which='minor', size=17, width=10, direction='in')
axea.yaxis.set_tick_params(which='major', size=27, width=10, direction='in', right='on')
axea.yaxis.set_tick_params(which='minor', size=17, width=10, direction='in', right='on')


ax2ea.xaxis.set_tick_params(which='major', size=27, width=10, direction='in')
ax2ea.xaxis.set_tick_params(which='minor', size=17, width=10, direction='in')


n=len(wetting['int_collection_largepores'])
axea.loglog(wetting['q'],wetting['int_collection_smallpores'][0]/wetting['int_collection_largepores'][0],'o-',color=cm.viridis(np.abs(wetting['satlevel_collection_largepores'][0])), markersize=50,linewidth=15,label='Simulated Dry')

for i in [
          find_saturation(wetting['satlevel_collection_largepores'],0.15),
          find_saturation(wetting['satlevel_collection_largepores'],0.3),
          find_saturation(wetting['satlevel_collection_largepores'],0.5),
          find_saturation(wetting['satlevel_collection_largepores'],0.7),
          find_saturation(wetting['satlevel_collection_largepores'],0.9),
          find_saturation(wetting['satlevel_collection_largepores'],1)]:
    axea.loglog(wetting['q'],wetting['int_collection_largepores'][i]/wetting['int_collection_largepores'][0],'o-',color=cm.viridis(np.abs(wetting['satlevel_collection_largepores'][i])), markersize=50,linewidth=15,label ='Sat. '+str(int(round(wetting['satlevel_collection_largepores'][i]*100,0)))+' %')

axea.set_yscale('linear')
axea.set_ylim([-0.05,2])


axea.set_xlim([0.0135909, 1.68472])

axea.set_xlabel(r'q [nm$^{-1}$]',fontsize=170, labelpad=10)
axea.set_ylabel('Intensity Wet/Dry', fontsize=170, labelpad=10)

ax2ea.set_xlabel('Dimension [nm]',fontsize=170,  labelpad=10)


axea.legend(bbox_to_anchor=(1, 1), loc=1, frameon=True, fontsize=100)

axea.text(-0.1, 1.1,'b-II)', transform=axea.transAxes, 
            size=170, weight='bold')


axg=fig.add_subplot(233)
axg.set_box_aspect(1)

ax2g = axg.secondary_xaxis(location='top',functions= (reciprocal,realspace))
ax2g.set_xscale('linear')

axg.xaxis.set_tick_params(which='major', size=27, width=10, direction='in')
axg.xaxis.set_tick_params(which='minor', size=17, width=10, direction='in')
axg.yaxis.set_tick_params(which='major', size=27, width=10, direction='in', right='on')
axg.yaxis.set_tick_params(which='minor', size=17, width=10, direction='in', right='on')


ax2g.xaxis.set_tick_params(which='major', size=27, width=10, direction='in')
ax2g.xaxis.set_tick_params(which='minor', size=17, width=10, direction='in')

n=len(wetting['int_collection_thinfilm'])
axg.loglog(wetting['q'],K_img2sas*wetting['int_collection_thinfilm'][0],'o-',color=cm.viridis(np.abs(wetting['satlevel_collection_thinfilm'][0])), markersize=50,linewidth=15,label='Simulated Dry')

for i in [
          find_saturation(wetting['satlevel_collection_thinfilm'],0.15),
          find_saturation(wetting['satlevel_collection_thinfilm'],0.3),
          find_saturation(wetting['satlevel_collection_thinfilm'],0.5),
          find_saturation(wetting['satlevel_collection_thinfilm'],0.7),
          find_saturation(wetting['satlevel_collection_thinfilm'],0.9),
          find_saturation(wetting['satlevel_collection_thinfilm'],1)]:
    axg.loglog(wetting['q'],K_img2sas*wetting['int_collection_thinfilm'][i],'o-',color=cm.viridis(np.abs(wetting['satlevel_collection_thinfilm'][i])), markersize=50,linewidth=15,label ='Sat. '+str(int(round(wetting['satlevel_collection_thinfilm'][i]*100,0)))+' %')

axg.set_xlim([0.0135909, 1.68472])
axg.set_ylim([10 ** -1, 10 ** 7])
axg.set_xlabel(r'q [nm$^{-1}$]',fontsize=170, labelpad=10)
axg.set_ylabel('Intensity [a.u.]', fontsize=170, labelpad=10)


ax2g.set_xlabel('Dimension [nm]',fontsize=170,  labelpad=10)

axg.legend(bbox_to_anchor=(1, 1), loc=1, frameon=True, fontsize=100)

axg.text(-0.1, 1.1,'a-III)', transform=axg.transAxes, 
            size=170, weight='bold')


axha=fig.add_subplot(236)
axha.set_box_aspect(1)


ax2ha = axha.secondary_xaxis(location='top',functions= (reciprocal,realspace))
ax2ha.set_xscale('linear')

axha.xaxis.set_tick_params(which='major', size=27, width=10, direction='in')
axha.xaxis.set_tick_params(which='minor', size=17, width=10, direction='in')
axha.yaxis.set_tick_params(which='major', size=27, width=10, direction='in', right='on')
axha.yaxis.set_tick_params(which='minor', size=17, width=10, direction='in', right='on')

ax2ha.xaxis.set_tick_params(which='major', size=27, width=10, direction='in')
ax2ha.xaxis.set_tick_params(which='minor', size=17, width=10, direction='in')
intensityrelative =[]

n=len(wetting['int_collection_thinfilm'])
axha.loglog(wetting['q'],wetting['int_collection_thinfilm'][0]/wetting['int_collection_thinfilm'][0],'o-',color=cm.viridis(np.abs(wetting['satlevel_collection_thinfilm'][0])), markersize=50,linewidth=15,label='Simulated Dry')
for i in [
          find_saturation(wetting['satlevel_collection_thinfilm'],0.15),
          find_saturation(wetting['satlevel_collection_thinfilm'],0.3),
          find_saturation(wetting['satlevel_collection_thinfilm'],0.5),
          find_saturation(wetting['satlevel_collection_thinfilm'],0.7),
          find_saturation(wetting['satlevel_collection_thinfilm'],0.9),
          find_saturation(wetting['satlevel_collection_thinfilm'],1)]:
    axha.loglog(wetting['q'],wetting['int_collection_thinfilm'][i]/wetting['int_collection_thinfilm'][0],'o-',color=cm.viridis(np.abs(wetting['satlevel_collection_thinfilm'][i])), markersize=50,linewidth=15,label ='Sat. '+str(int(round(wetting['satlevel_collection_thinfilm'][i]*100,0)))+' %')

    intensityrelative.append(wetting['int_collection_thinfilm'][i]/wetting['int_collection_thinfilm'][0])

axha.set_yscale('linear')


axha.set_xlim([0.0135909, 1.68472])

axha.set_ylim(-0.05,2)

axha.set_xlabel(r'q [nm$^{-1}$]',fontsize=170, labelpad=10)
axha.set_ylabel('Intensity Wet/Dry', fontsize=170, labelpad=10)
ax2ha.set_xlabel('Dimension [nm]',fontsize=170,  labelpad=10)

axha.legend(bbox_to_anchor=(1, 1), loc=1, frameon=True, fontsize=100)

axha.text(-0.1, 1.1,'b-III)', transform=axha.transAxes, 
            size=170, weight='bold')




fig.savefig("Figure3mechwetting_fixed_"+str(padding_or_not)+str(windowing_or_not)+".svg", dpi=300, transparent=False, bbox_inches='tight')


#%%

#this cell contains subtraction of Pt contribution and the comparison between sat. level calculated by invariant versus voxel count

def scalingfactor(SLD_A,volfrac_A,dryinv):
    shiftingfactor = (dryinv)/(2*(np.pi**2))/((SLD_A**2)*volfrac_A*(1-volfrac_A))
    return shiftingfactor

def extrapolatesaxs(qexp,Iexp):
    # Iexp -= Iexp[-1]
    qlowq= np.linspace(0.00001,qexp[0],10000)
    qhighq= np.linspace(qexp[-1],100,10000)
    setvaluea,setvalueb= np.polyfit((qexp[0:3]**2),np.log(Iexp[0:3]),1)
    def guinier(x,a,b):
        return a*np.exp(-1*(b**2)*(x**2)/3)
    mod = Model(guinier, prefix='guinier_')
    pars= mod.make_params()
    pars['guinier_a'].set(value=Iexp[0], min=0)
    pars['guinier_b'].set(value=np.sqrt(-3*setvaluea), min=0)
    out = mod.fit(Iexp[0:3], pars, x=qexp[0:3])
    params_dict=out.params.valuesdict()
    alowq = params_dict['guinier_a']
    blowq = params_dict['guinier_b']
    ylowq= alowq*np.exp(-1*(blowq**2)*(qlowq**2)/3)
    setvaluea,setvalueb= np.polyfit((qexp[-2:-1]**-4),(Iexp[-2:-1]),1)
    def porod(x,a):
        return a*(x**-4)
    mod = Model(porod, prefix='porod_')
    pars= mod.make_params()
    pars['porod_a'].set(value=setvaluea, min=0)
    out = mod.fit(Iexp[-2:-1], pars, x=qexp[-2:-1])
    params_dict=out.params.valuesdict()
    mhighq = params_dict['porod_a']
    yhighq = mhighq*(qhighq**-4)
    qextra = np.concatenate((qlowq,qexp,qhighq))
    Iextra = np.concatenate((ylowq,Iexp,yhighq))
    return qextra, Iextra



def extrapolatesaxs(qexp,Iexp):
    Iextra=Iexp
    qextra=qexp
    return qextra, Iextra

def extrapolatesaxs2(qexp,Iexp):
    # Iexp -= Iexp[-1]
    if padding_or_not == 1:
        qlowq= np.linspace(0.00613592,qexp[0],10000)
    else: 
        qlowq= np.linspace(0.0122718,qexp[0],10000)
    #qlowq= np.linspace(0.00613592,qexp[0],10000)
    qhighq= np.linspace(qexp[-1],3.14159,10000)
    setvaluea,setvalueb= np.polyfit((qexp[0:10]**2),np.log(Iexp[0:10]),1)
    def guinier(x,a,b):
        return a*np.exp(-1*(b**2)*(x**2)/3)
    mod = Model(guinier, prefix='guinier_')
    pars= mod.make_params()
    pars['guinier_a'].set(value=Iexp[0], min=0)
    pars['guinier_b'].set(value=np.sqrt(-3*setvaluea), min=0)
    out = mod.fit(Iexp[0:10], pars, x=qexp[0:10])
    params_dict=out.params.valuesdict()
    alowq = params_dict['guinier_a']
    blowq = params_dict['guinier_b']
    ylowq= alowq*np.exp(-1*(blowq**2)*(qlowq**2)/3)
    qextra=np.concatenate([qlowq,qexp])
    Iextra=np.concatenate([ylowq,Iexp])
    return qextra, Iextra


def volfraccalc(SLD_A,SLD_B,SLD_C,volfrac_A,wetinv,shiftingfactor):
    K=shiftingfactor
    Inv_Wet = wetinv
    c1 = (-Inv_Wet/(2*(np.pi**2)*K)) + (SLD_A-SLD_B)*(SLD_A-SLD_C)*(volfrac_A-(volfrac_A**2))
    c2 = (SLD_B-SLD_A)*(SLD_B-SLD_C)*(1-volfrac_A-((1-volfrac_A)**2))
    b1 = (SLD_B-SLD_A)*(SLD_B-SLD_C)*2*(1-volfrac_A)
    b2 = (SLD_B-SLD_A)*(SLD_B-SLD_C)*-1
    b3 = (SLD_C-SLD_A)*(SLD_C-SLD_B)
    a1 = (SLD_B-SLD_A)*(SLD_B-SLD_C)*-1
    a2 = (SLD_C-SLD_A)*(SLD_C-SLD_B)*-1
    c = c1+c2
    b = b1+b2+b3
    a = a1+a2
    
    Volfrac1 = ((-b-(((b**2)-(4*a*c))**(1/2)))/(2*a))
    Volfrac2 = ((-b+(((b**2)-(4*a*c))**(1/2)))/(2*a))
    Volfrac3 = 1-volfrac_A
    return Volfrac1, Volfrac2, Volfrac3

import sklearn
from sklearn.metrics import auc
import copy
import functions

im_sld_nowater = wetting['im_Pt']
CL_carbon_Pt = im_sld_nowater
volfrac_void = np.count_nonzero(im_sld_nowater==0)/im_sld_nowater.size

volfrac_Pt = np.count_nonzero(im_sld_nowater>125*10**10)/CL_carbon_Pt.size


im_Pt_cons = copy.deepcopy(CL_carbon_Pt)
# im_Pt_cons[im_Pt_cons==0] = 9.447*(10**10)
# im_Pt_cons[(im_Pt_cons<125*10**10) & (im_Pt_cons>9.447*10**10)]= 9.447*(10**10)
im_Pt_cons[im_Pt_cons==0] = 0
im_Pt_cons[(im_Pt_cons<125*10**10) & (im_Pt_cons>9.447*10**10)]= 0

volfrac_carbon = 1-(volfrac_void+volfrac_Pt)
I_Pt_constant_, q_Pt_constant_ = functions.img2sas(im_Pt_cons,512,1)
q_Pt_constant, I_Pt_constant = extrapolatesaxs2(q_Pt_constant_[(q_Pt_constant_>=1)&(q_Pt_constant_<=3.15)], I_Pt_constant_[(q_Pt_constant_>=1)&(q_Pt_constant_<=3.15)])

Pt_constdry = auc(q_Pt_constant, I_Pt_constant*(q_Pt_constant**2))



im_Pt_cons = copy.deepcopy(CL_carbon_Pt)
im_Pt_cons[im_Pt_cons==0] = 9.447*(10**10)
im_Pt_cons[(im_Pt_cons<125*10**10) & (im_Pt_cons>9.447*10**10)]= 9.447*(10**10)
# im_Pt_cons[im_Pt_cons==0] = 0
# im_Pt_cons[(im_Pt_cons<125*10**10) & (im_Pt_cons>9.447*10**10)]= 0

volfrac_carbon = 1-(volfrac_void+volfrac_Pt)
I_Pt_constant_, q_Pt_constant_ = functions.img2sas(im_Pt_cons,512,1)
q_Pt_constant, I_Pt_constant = extrapolatesaxs2(q_Pt_constant_[(q_Pt_constant_>=1)&(q_Pt_constant_<=3.15)], I_Pt_constant_[(q_Pt_constant_>=1)&(q_Pt_constant_<=3.15)])
Pt_const = auc(q_Pt_constant, I_Pt_constant*(q_Pt_constant**2))


qdry = wetting['q']
Idry= wetting['int_collection_smallpores'][0]

qdryextra,Idryextra= extrapolatesaxs(qdry, Idry)
PtCdry=auc(qdryextra,Idryextra*(qdryextra)**2)


radialmeancollection_largepores = wetting["int_collection_largepores"]
radialmeancollection_smallpores = wetting["int_collection_smallpores"]
radialmeancollectionthinfilm = wetting["int_collection_thinfilm"]


thinfilm_satlevel_voxelcount = wetting['satlevel_collection_thinfilm']
smallpores_satlevel_voxelcount = wetting['satlevel_collection_smallpores']
largepores_satlevel_voxelcount = wetting['satlevel_collection_largepores']

qextra, Iextra = extrapolatesaxs(qdry, radialmeancollection_largepores[-1])
PtCwater=auc(qextra, Iextra*(qextra**2))
invfullywet=PtCwater
invfullydry=PtCdry
SLD_watercarbon= (1.72522*(10**11)-9.447*(10**10))
SLD_voidcarbon= 1.72522*(10**11)
# C= (((invfullydry*(SLD_watercarbon**2))-(invfullywet*(SLD_voidcarbon**2))))/((SLD_watercarbon**2)-((SLD_voidcarbon**2)))

# Pt_const = C

K_inv_PtC = scalingfactor(1.72522*(10**11),volfrac_carbon,PtCdry-Pt_const)

largepore_satlevel_invariant = [0]
for i in range(1,len(radialmeancollection_largepores)):
    qextra, Iextra = extrapolatesaxs(qdry, radialmeancollection_largepores[i])
    PtCwater=auc(qextra, Iextra*(qextra**2))
    vf1,vf2,vf3 = volfraccalc(1.72522*(10**11),0,9.447*(10**10),volfrac_carbon,PtCwater-Pt_const,K_inv_PtC)
    largepore_satlevel_invariant.append(vf1/(volfrac_void))



smallpores_satlevel_invariant = [0]


for i in range(1,len(radialmeancollection_smallpores)):
    qextra, Iextra = extrapolatesaxs(qdry, radialmeancollection_smallpores[i])
    PtCwater=auc(qextra, Iextra*(qextra**2))
    vf1,vf2,vf3 = volfraccalc(1.72522*(10**11),0,9.447*(10**10),volfrac_carbon,PtCwater-Pt_const,K_inv_PtC)
    smallpores_satlevel_invariant.append(vf1/(volfrac_void))


thinfilm_satlevel_invariant = [0]


for i in range(1,len(radialmeancollectionthinfilm)):
    qextra, Iextra = extrapolatesaxs(qdry, radialmeancollectionthinfilm[i])
    PtCwater=auc(qextra, Iextra*(qextra**2))
    vf1,vf2,vf3 = volfraccalc(1.72522*(10**11),0,9.447*(10**10),volfrac_carbon,PtCwater-Pt_const,K_inv_PtC)
    if vf1 >0:
        thinfilm_satlevel_invariant.append(vf1/(volfrac_void))
    else:
        thinfilm_satlevel_invariant.append(0)

thinfilm_satlevel_invariant_percentage = [i*100 for i in thinfilm_satlevel_invariant ]

thinfilm_satlevel_voxelcount_percentage = [i*100 for i in thinfilm_satlevel_voxelcount ]

largepore_satlevel_invariant_percentage = [i*100 for i in largepore_satlevel_invariant ]

largepores_satlevel_voxelcount_percentage = [i*100 for i in largepores_satlevel_voxelcount ]

smallpores_satlevel_invariant_percentage = [i*100 for i in smallpores_satlevel_invariant ]

smallpores_satlevel_voxelcount_percentage = [i*100 for i in smallpores_satlevel_voxelcount ]

import matplotlib
matplotlib.rcParams.update(matplotlib.rcParamsDefault)

from itertools import chain
import numpy as np


fillingradiilargepores = chain(np.arange(70,50,-10),np.arange(50,10,-5),np.linspace(10,0,21,endpoint=True))
fillingradiismallpores = chain(np.linspace(0.5,10.5,21,endpoint=True),np.arange(10,50,5),np.arange(50,75,5))
fillingthinfilm = chain(np.arange(1,10,1),np.arange(10,30,2),np.arange(30,150,10))


thinfilm_iter = list(fillingthinfilm)
smallpores_iter = list(fillingradiismallpores)
largepores_iter =list(fillingradiilargepores)

thinfilm_iter = [i for i in thinfilm_iter]
smallpores_iter = [i*2 for i in smallpores_iter]
largepores_iter = [i*2 for i in largepores_iter]

plt.figure(figsize=[5,5],dpi=300)
plt.loglog(qdryextra,Idryextra*K_img2sas,label='Dry Pt/C intensity profile from img2sas')
plt.loglog(qextra,Iextra*K_img2sas,label='Wet Pt/C intensity profile from img2sas')
plt.loglog(q_Pt_constant,I_Pt_constant*K_img2sas,label='Pt constant contribution')
plt.xlabel('q [1/nm]')
plt.ylabel('Intensity [a.u.]')
plt.legend()
plt.savefig("Ptcontributionsubtraction"+str(padding_or_not)+str(windowing_or_not)+".svg")

import matplotlib.pylab as pylab
params = {'legend.fontsize': 10,
          'figure.figsize': (5, 5),
         'axes.labelsize': 20,
         'axes.titlesize':20,
         'xtick.labelsize':18,
         'ytick.labelsize':18}

pylab.rcParams.update(params)

plt.figure()
plt.scatter(thinfilm_iter[0:], thinfilm_satlevel_invariant_percentage[1:],label='Invariant calculation from simulated SAXS profile')
plt.scatter(thinfilm_iter[0:],thinfilm_satlevel_voxelcount_percentage[1:],label='Voxel count calculation from 3D structure')
plt.ylim(-0.1,105)
plt.xlabel('Water thin film thickness [nm]')
plt.ylabel('Saturation Level [%]')
plt.xlim(-0.1,200)
plt.legend()

plt.savefig("Invariantversusvoxelcount1_withPtcont"+str(padding_or_not)+str(windowing_or_not)+".svg", dpi=300, transparent=False, bbox_inches='tight')


plt.figure()
plt.scatter(largepores_iter[0:], largepore_satlevel_invariant_percentage[1:],label='Invariant calculation from simulated SAXS profile')
plt.scatter(largepores_iter[0:],largepores_satlevel_voxelcount_percentage[1:],label='Voxel count calculation from 3D structure')
plt.ylim(-0.1,105)
plt.xlim(-0.1,200)
plt.xlabel('Pore size filled [nm]')
plt.ylabel('Saturation Level [%]')
plt.legend()

plt.savefig("Invariantversusvoxelcount2_withPtcont"+str(padding_or_not)+str(windowing_or_not)+".svg", dpi=300, transparent=False, bbox_inches='tight')

plt.figure()
plt.scatter(smallpores_iter[0:], smallpores_satlevel_invariant_percentage[1:],label='Invariant calculation from simulated SAXS profile')
plt.scatter(smallpores_iter[0:],smallpores_satlevel_voxelcount_percentage[1:],label='Voxel count calculation from 3D structure')
plt.ylim(-0.1,105)
plt.xlim(-0.1,200)
plt.xlabel('Pore size filled [nm]')
plt.ylabel('Saturation Level [%]')
plt.legend()


plt.savefig("Invariantversusvoxelcount3_withPtcont"+str(padding_or_not)+str(windowing_or_not)+".svg", dpi=300, transparent=False, bbox_inches='tight')


#%%

#this cell contains the comparison between sat. level calculated by invariant versus voxel count WITHOUT Pt subtraction! Not good agreement!!!


Pt_const = 0

K_inv_PtC = scalingfactor(1.72522*(10**11),volfrac_carbon,PtCdry-Pt_const)

largepore_satlevel_invariant = [0]

for i in range(1,len(radialmeancollection_largepores)):
    qextra, Iextra = extrapolatesaxs(qdry, radialmeancollection_largepores[i])
    PtCwater=auc(qextra, Iextra*(qextra**2))
    vf1,vf2,vf3 = volfraccalc(1.72522*(10**11),0,9.447*(10**10),volfrac_carbon,PtCwater-Pt_const,K_inv_PtC)
    largepore_satlevel_invariant.append(vf1/(volfrac_void))


smallpores_satlevel_invariant = [0]


for i in range(1,len(radialmeancollection_smallpores)):
    qextra, Iextra = extrapolatesaxs(qdry, radialmeancollection_smallpores[i])
    PtCwater=auc(qextra, Iextra*(qextra**2))
    vf1,vf2,vf3 = volfraccalc(1.72522*(10**11),0,9.447*(10**10),volfrac_carbon,PtCwater-Pt_const,K_inv_PtC)
    smallpores_satlevel_invariant.append(vf1/(volfrac_void))


thinfilm_satlevel_invariant = [0]


for i in range(1,len(radialmeancollectionthinfilm)):
    qextra, Iextra = extrapolatesaxs(qdry, radialmeancollectionthinfilm[i])
    PtCwater=auc(qextra, Iextra*(qextra**2))
    vf1,vf2,vf3 = volfraccalc(1.72522*(10**11),0,9.447*(10**10),volfrac_carbon,PtCwater-Pt_const,K_inv_PtC)
    if vf1 >0:
        thinfilm_satlevel_invariant.append(vf1/(volfrac_void))
    else:
        thinfilm_satlevel_invariant.append(0)

thinfilm_satlevel_invariant_percentage = [i*100 for i in thinfilm_satlevel_invariant ]

thinfilm_satlevel_voxelcount_percentage = [i*100 for i in thinfilm_satlevel_voxelcount ]

largepore_satlevel_invariant_percentage = [i*100 for i in largepore_satlevel_invariant ]

largepores_satlevel_voxelcount_percentage = [i*100 for i in largepores_satlevel_voxelcount ]

smallpores_satlevel_invariant_percentage = [i*100 for i in smallpores_satlevel_invariant ]

smallpores_satlevel_voxelcount_percentage = [i*100 for i in smallpores_satlevel_voxelcount ]

import matplotlib
matplotlib.rcParams.update(matplotlib.rcParamsDefault)



import matplotlib.pylab as pylab
params = {'legend.fontsize': 10,
          'figure.figsize': (5, 5),
         'axes.labelsize': 20,
         'axes.titlesize':20,
         'xtick.labelsize':18,
         'ytick.labelsize':18}

pylab.rcParams.update(params)
# mpl.rcParams.update(mpl.rcParamsDefault)
plt.figure()
plt.scatter(thinfilm_iter[0:], thinfilm_satlevel_invariant_percentage[1:],label='Invariant calculation from simulated SAXS profile')
plt.scatter(thinfilm_iter[0:],thinfilm_satlevel_voxelcount_percentage[1:],label='Voxel count calculation from 3D structure')
plt.ylim(-0.1,105)
plt.xlabel('Water thin film thickness [nm]')
plt.ylabel('Saturation Level [%]')
plt.xlim(-0.1,200)
plt.legend()

plt.savefig("Invariantversusvoxelcount1withoutPtC"+str(padding_or_not)+str(windowing_or_not)+".svg", dpi=300, transparent=False, bbox_inches='tight')


plt.figure()
plt.scatter(largepores_iter[0:], largepore_satlevel_invariant_percentage[1:],label='Invariant calculation from simulated SAXS profile')
plt.scatter(largepores_iter[0:],largepores_satlevel_voxelcount_percentage[1:],label='Voxel count calculation from 3D structure')
plt.ylim(-0.1,105)
plt.xlim(-0.1,200)
plt.xlabel('Pore size filled [nm]')
plt.ylabel('Saturation Level [%]')
plt.legend()

plt.savefig("Invariantversusvoxelcount2withoutPtC"+str(padding_or_not)+str(windowing_or_not)+".svg", dpi=300, transparent=False, bbox_inches='tight')

plt.figure()
plt.scatter(smallpores_iter[0:], smallpores_satlevel_invariant_percentage[1:],label='Invariant calculation from simulated SAXS profile')
plt.scatter(smallpores_iter[0:],smallpores_satlevel_voxelcount_percentage[1:],label='Voxel count calculation from 3D structure')
plt.ylim(-0.1,105)
plt.xlim(-0.1,200)
plt.xlabel('Pore size filled [nm]')
plt.ylabel('Saturation Level [%]')
plt.legend()


plt.savefig("Invariantversusvoxelcount3withoutPtC"+str(padding_or_not)+str(windowing_or_not)+".svg", dpi=300, transparent=False, bbox_inches='tight')

