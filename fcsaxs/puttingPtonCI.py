#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 14 19:48:29 2022

@author: kinanti_a
"""
import numpy as np
import scipy.ndimage as spim
import copy 
from skimage.morphology import disk, ball
import porespy as ps
import matplotlib.pyplot as plt
from functions import *
from hyperactive import Hyperactive
from hyperactive.optimizers import TreeStructuredParzenEstimators, BayesianOptimizer,EvolutionStrategyOptimizer
from porespy.filters import find_peaks, trim_saddle_points, trim_nearby_peaks, region_size
from skimage.segmentation import watershed
import skimage 
import functions

class puttingPt(object):
    def __init__(self, im_carbon,classB, boxsize, onevoxel, porosity, q_exp, I_exp, K_img2sas,SLD_solid):
        """
        Intialization of the IBM class
        """
        self.classB = classB
        self.boxsize = boxsize
        self.onevoxel = onevoxel
        self.porosity = porosity
        self.q_exp = q_exp
        self.I_exp = I_exp
        self.im_carbon = im_carbon
        self.K_img2sas=K_img2sas
        self.SLD_solid = SLD_solid
    # def makePtloadingregion(self,im,ionomerthickness,regions,poreradii,radiuspt1,vpt1,radiuspt2,vpt2,radiuspt3,vpt3,radiuspt4,vpt4):
    #         onevoxel=self.onevoxel
    #         im[im>0]=1
    #         c=im.shape[0]
    #         #ionomerthickness = 1
    #         if ionomerthickness ==0:
    #             im_closed = im
    #         else:
    #             im_closed = spim.morphology.binary_erosion(im, structure=ball(ionomerthickness)).astype('float32')
    #         #voxel with value 0 is the one to fill
    #         regions_ = copy.deepcopy(regions)
    #         poreradii = int(poreradii/onevoxel)
    #         clustersizethreshold = 4/3*np.pi*(poreradii**3)
    #         regionsmask = copy.deepcopy(regions)
    #         regions_[(regionsmask<clustersizethreshold)&(0<regionsmask)] = 1
    #         regions_[(regionsmask>=clustersizethreshold)&(0<regionsmask)] = 0
    #         if (vpt1 != 0):
                
    #             result = spim.morphology.binary_dilation(im_closed, structure=ball(2*radiuspt1+1)).astype('float32')
    #             result[result==0]=0
    #             solid = copy.deepcopy(im_closed)
    #             solid[solid>0]=1
    #             solid[solid==0]=0
    #             loading_region1 = result - solid
    #             loading_region1 = 1-loading_region1
                
    #         else:
    #             loading_region1 =np.ones([c,c,c])
                
    #         if (vpt2 != 0):
                
    #             result = spim.morphology.binary_dilation(im_closed, structure=ball(2*radiuspt2+1)).astype('float32')
    #             result[result==0]=0
    #             solid = copy.deepcopy(im_closed)
    #             solid[solid>0]=1
    #             solid[solid==0]=0
    #             loading_region2 = result - solid
    #             loading_region2 = 1-loading_region2 
                
    #         else:
    #             loading_region2 = np.ones([c,c,c])
                
    #         if (vpt3 != 0):
                
    #             result = spim.morphology.binary_dilation(im_closed, structure=ball(2*radiuspt3+1)).astype('float32')
    #             result[result==0]=0
    #             solid = copy.deepcopy(im_closed)
    #             solid[solid>0]=1
    #             solid[solid==0]=0
    #             loading_region3 = result - solid
    #             loading_region3 = 1-loading_region3

    #         else:
    #             loading_region3 = np.ones([c,c,c]) 
            
    #         if (vpt4 != 0):
               
    #             result = spim.morphology.binary_dilation(im_closed, structure=ball(2*radiuspt4+1)).astype(''float32'')
    #             result[result==0]=0
    #             solid = copy.deepcopy(im_closed)
    #             solid[solid>0]=1
    #             solid[solid==0]=0
    #             loading_region4 = result - solid
    #             loading_region4 = 1-loading_region4
               
    #         else:
    #             loading_region4 = np.ones([c,c,c])
                
    #         loading_region1[(loading_region1==0)&(regions_==1)]=1
    #         loading_region2[(loading_region2==0)&(regions_==1)]=1
    #         loading_region3[(loading_region3==0)&(regions_==1)]=1
    #         loading_region4[(loading_region4==0)&(regions_==1)]=1   
    #         return [loading_region1, loading_region2, loading_region3, loading_region4]
        
    def makePtloadingregion(self,im,ionomerthickness,regions,poreradii,radiuspt1,vpt1,radiuspt2,vpt2,radiuspt3,vpt3,radiuspt4,vpt4):
            onevoxel=self.onevoxel
            im[im>0]=1
            c=im.shape[0]
            #ionomerthickness = 1
            if ionomerthickness ==0:
                im_closed = im
            else:
                #im_closed = spim.morphology.binary_erosion(im, structure=ball(ionomerthickness)).astype('float32')
                im_closed = ps.filters.fftmorphology(im, strel=ball(ionomerthickness), mode='erosion').astype('float32')
            #voxel with value 0 is the one to fill
            regions_ = copy.deepcopy(regions)
            poreradii = int(poreradii/onevoxel)
            #clustersizethreshold = 2*poreradii
            clustersizethreshold = 4/3*np.pi*(poreradii**3)
            regionsmask = copy.deepcopy(regions)
            regions_[(regionsmask<clustersizethreshold)&(0<regionsmask)] = 1
            regions_[(regionsmask>=clustersizethreshold)&(0<regionsmask)] = 0
            if (vpt1 != 0):
                
                #result = spim.morphology.binary_dilation(im_closed, structure=ball(2*radiuspt1+1)).astype('float32')
                result = ps.filters.fftmorphology(im_closed, strel=ball(2*radiuspt1+1), mode='dilation').astype('float32')
                result[result==0]=0
                solid = copy.deepcopy(im_closed)
                solid[solid>0]=1
                solid[solid==0]=0
                loading_region1 = result - solid
                loading_region1 = 1-loading_region1
                
            else:
                loading_region1 =np.ones([c,c,c])
                
            if (vpt2 != 0):
                
                result = ps.filters.fftmorphology(im_closed, strel=ball(2*radiuspt2+1), mode='dilation').astype('float32')
                result[result==0]=0
                solid = copy.deepcopy(im_closed)
                solid[solid>0]=1
                solid[solid==0]=0
                loading_region2 = result - solid
                loading_region2 = 1-loading_region2 
                
            else:
                loading_region2 = np.ones([c,c,c])
                
            if (vpt3 != 0):
                
                #result = spim.morphology.binary_dilation(im_closed, structure=ball(2*radiuspt3+1)).astype('float32')
                result = ps.filters.fftmorphology(im_closed, strel=ball(2*radiuspt3+1), mode='dilation').astype('float32')
                result[result==0]=0
                solid = copy.deepcopy(im_closed)
                solid[solid>0]=1
                solid[solid==0]=0
                loading_region3 = result - solid
                loading_region3 = 1-loading_region3

            else:
                loading_region3 = np.ones([c,c,c]) 
            
            if (vpt4 != 0):
               
                result = ps.filters.fftmorphology(im_closed, strel=ball(2*radiuspt4+1), mode='dilation').astype('float32')
                result[result==0]=0
                solid = copy.deepcopy(im_closed)
                solid[solid>0]=1
                solid[solid==0]=0
                loading_region4 = result - solid
                loading_region4 = 1-loading_region4
               
            else:
                loading_region4 = np.ones([c,c,c])
                
            loading_region1[(loading_region1==0)&(regions_==1)]=1
            loading_region2[(loading_region2==0)&(regions_==1)]=1
            loading_region3[(loading_region3==0)&(regions_==1)]=1
            loading_region4[(loading_region4==0)&(regions_==1)]=1   
            return [loading_region1, loading_region2, loading_region3, loading_region4]
    
    
    
    
    
    
    
    
    
    
    
    
    
        
        
        
        
        
        
        
        
    def makePtloadingregion_v2(self,im,ionomerthickness,regions,poreradii,radiuspt1,vpt1,radiuspt2,vpt2,radiuspt3,vpt3,radiuspt4,vpt4):
            onevoxel=self.onevoxel
            im[im>0]=1
            c=im.shape[0]
            #ionomerthickness = 1
            if ionomerthickness ==0:
                im_closed = im
            else:
                im_closed = spim.morphology.binary_erosion(im, structure=ball(ionomerthickness)).astype('float32')
            #voxel with value 0 is the one to fill
            regions_ = copy.deepcopy(regions)
            poreradii = int(poreradii/onevoxel)
            clustersizethreshold = 4/3*np.pi*(poreradii**3)
            regionsmask = copy.deepcopy(regions)
            regions_[(regionsmask<clustersizethreshold)&(0<regionsmask)] = 1
            regions_[(regionsmask>=clustersizethreshold)&(0<regionsmask)] = 0
            if (vpt1 != 0):
                
                result = spim.morphology.binary_dilation(im_closed, structure=ball(2*radiuspt1+1)).astype('float32')
                result[result==0]=0
                solid = copy.deepcopy(im_closed)
                solid[solid>0]=1
                solid[solid==0]=0
                loading_region1 = result - solid
                loading_region1 = 1-loading_region1
                
            else:
                loading_region1 =np.ones([c,c,c])
                
            if (vpt2 != 0):
                
                result = spim.morphology.binary_dilation(im_closed, structure=ball(2*radiuspt2+1)).astype('float32')
                result[result==0]=0
                solid = copy.deepcopy(im_closed)
                solid[solid>0]=1
                solid[solid==0]=0
                loading_region2 = result - solid
                loading_region2 = 1-loading_region2 
                
            else:
                loading_region2 = np.ones([c,c,c])
                
            if (vpt3 != 0):
                
                result = spim.morphology.binary_dilation(im_closed, structure=ball(2*radiuspt3+1)).astype('float32')
                result[result==0]=0
                solid = copy.deepcopy(im_closed)
                solid[solid>0]=1
                solid[solid==0]=0
                loading_region3 = result - solid
                loading_region3 = 1-loading_region3

            else:
                loading_region3 = np.ones([c,c,c]) 
            
            if (vpt4 != 0):
               
                result = spim.morphology.binary_dilation(im_closed, structure=ball(2*radiuspt4+1)).astype('float32')
                result[result==0]=0
                solid = copy.deepcopy(im_closed)
                solid[solid>0]=1
                solid[solid==0]=0
                loading_region4 = result - solid
                loading_region4 = 1-loading_region4
               
            else:
                loading_region4 = np.ones([c,c,c])
                
            loading_region1[(loading_region1==0)&(regions_==1)]=1
            loading_region2[(loading_region2==0)&(regions_==1)]=1
            loading_region3[(loading_region3==0)&(regions_==1)]=1
            loading_region4[(loading_region4==0)&(regions_==1)]=1   
            return [loading_region1, loading_region2, loading_region3, loading_region4]
            
    def decoratingPt(self,ionomerthickness,radiuspt1,vpt1,radiuspt2,vpt2,radiuspt3,vpt3,radiuspt4,vpt4,onevoxel,loading_region,SLD_solid):
            im = self.im_carbon
            SLD_solid = self.SLD_solid
            im_sld_nowater = copy.deepcopy(im)
             
            im_sld_nowater[im_sld_nowater>=1]=SLD_solid
            im_sld_nowater_binary = copy.deepcopy(im_sld_nowater)
            im_sld_nowater_binary[im_sld_nowater_binary > 0] = 1#/255 
            im_sld_nowater_binary[im_sld_nowater_binary == 0] = 0
            v_loadingregion = []
            
            for i in range(len(loading_region)):
                v_loadingregion.append(loading_region[i].sum()/loading_region[i].size)
            if ionomerthickness==0:
                im_sld_nowater_binary = im_sld_nowater_binary
            else:
                im_sld_nowater_binary= spim.morphology.binary_erosion(im_sld_nowater_binary, structure=ball(ionomerthickness)).astype('float32')
                im_sld_nowater_eroded = copy.deepcopy(im_sld_nowater_binary)
                im_sld_nowater_eroded[im_sld_nowater_eroded>=1]=SLD_solid
            loading_region[0][loading_region[0]>0]=1
            pt1= ps.generators.RSA(loading_region[0], radiuspt1, v_loadingregion[0]+vpt1, n_max=1000000)
            pt_only_1 = copy.deepcopy(pt1)
            pt_only_1 = pt_only_1 - (loading_region[0])
            pt_only_1[pt_only_1>0]=1
            
            load2 = loading_region[1]+pt_only_1
            load2[load2>0]=1
            pt2= ps.generators.RSA(load2, radiuspt2, v_loadingregion[1]+vpt1+vpt2, n_max=1000000)  
            pt_only_2 = copy.deepcopy(pt2)
            pt_only_2 = pt_only_2 - (load2)
            pt_only_2[pt_only_2>0]=1
            
            load3 = loading_region[2]+pt_only_1+pt_only_2
            load3[load3>0]=1
            pt3= ps.generators.RSA(load3, radiuspt3, v_loadingregion[2]+vpt1+vpt2+vpt3, n_max=1000000)  
            pt_only_3 = copy.deepcopy(pt3)
            pt_only_3 = pt_only_3 - (load3)
            pt_only_3[pt_only_3>0]=1

            load4=loading_region[3]+pt_only_1+pt_only_2+pt_only_3
            load4[load4>0]=1
            pt4= ps.generators.RSA(load4, radiuspt4, v_loadingregion[3]+vpt1+vpt2+vpt3+vpt4, n_max=1000000)  
            pt_only_4 = copy.deepcopy(pt4)
            pt_only_4 = pt_only_4 - load4
            pt_only_4[pt_only_4>0]=1
            boxsize=pt4.shape[0]
            pt_only=np.zeros([boxsize,boxsize,boxsize])
            
            if (vpt1!=0):
                pt_only = pt_only_1
                
            if (vpt2!=0):
                pt_only += pt_only_2

            if (vpt3!=0):
                pt_only += pt_only_3
                
            if (vpt4!=0):
                pt_only += pt_only_4
                
            print(np.count_nonzero(pt_only)/(pt_only.size))
            pt_only[pt_only>0]= 126*(10**10)
            # plt.figure()
            # plt.imshow(pt_only[:,:,500])
            
            #CL_carbon_Pt = pt_only + im_sld_nowater 
            
            if ionomerthickness==0:
                CL_carbon_Pt = pt_only + im_sld_nowater
            else:
                ErodedcarbonwithPt = im_sld_nowater_eroded + pt_only 
            
                ErodedcarbonwithPt_binary = copy.deepcopy(ErodedcarbonwithPt)
                ErodedcarbonwithPt_binary[ErodedcarbonwithPt_binary>0]=1
                ErodedcarbonwithPt_binary_copy=copy.deepcopy(ErodedcarbonwithPt_binary)
                ErodedcarbonwithPt_binary_thendilated = spim.morphology.binary_dilation(ErodedcarbonwithPt_binary_copy, structure=ball(ionomerthickness)).astype('float32') 
                ionomercover = ErodedcarbonwithPt_binary_thendilated - ErodedcarbonwithPt_binary
                # plt.figure()
                # plt.imshow(ionomercover[:,:,500])
                ionomercover[ionomercover>0]= SLD_solid
                CL_carbon_Pt = pt_only + im_sld_nowater
                # plt.figure()
                # plt.imshow(CL_carbon_Pt [:,:,500])
                
            CL_carbon_Pt[CL_carbon_Pt>=126*(10**10)] =126*(10**10)
            CL_carbon_Pt[(CL_carbon_Pt<125*(10**10))&(CL_carbon_Pt>=SLD_solid)] =SLD_solid
            # plt.figure()
            # plt.imshow(CL_carbon_Pt [:,:,500])
            img2sas_params = self.boxsize,self.onevoxel
            #I_carbon,q_carbon =img2sas(im_sld_nowater,self.boxsize,self.onevoxel)
            I_Pt, q_Pt = img2sas(CL_carbon_Pt,self.boxsize,self.onevoxel) 
            print("new")
            return I_Pt,q_Pt, CL_carbon_Pt
        
                 
    def decoratingPt_v2(self,ionomerthickness,radiuspt1,vpt1,radiuspt2,vpt2,radiuspt3,vpt3,radiuspt4,vpt4,onevoxel,loading_region,SLD_solid):
            im = self.im_carbon
            SLD_solid = self.SLD_solid
            im_sld_nowater = copy.deepcopy(im)
             
            im_sld_nowater[im_sld_nowater>=1]=SLD_solid
            im_sld_nowater_binary = copy.deepcopy(im_sld_nowater)
            im_sld_nowater_binary[im_sld_nowater_binary > 0] = 1#/255 
            im_sld_nowater_binary[im_sld_nowater_binary == 0] = 0
            v_loadingregion = []
            
            for i in range(len(loading_region)):
                v_loadingregion.append(loading_region[i].sum()/loading_region[i].size)
            if ionomerthickness==0:
                im_sld_nowater_binary = im_sld_nowater_binary
            else:
                im_sld_nowater_binary= spim.morphology.binary_erosion(im_sld_nowater_binary, structure=ball(ionomerthickness)).astype('float32')
                im_sld_nowater_eroded = copy.deepcopy(im_sld_nowater_binary)
                im_sld_nowater_eroded[im_sld_nowater_eroded>=1]=SLD_solid
            loading_region[0][loading_region[0]>0]=1
            pt1= ps.generators.RSA(loading_region[0], radiuspt1, v_loadingregion[0]+vpt1, n_max=1000000)
            pt_only_1 = copy.deepcopy(pt1)
            pt_only_1 = pt_only_1 - (loading_region[0])
            pt_only_1[pt_only_1>0]=1
            
            load2 = loading_region[1]+pt_only_1
            load2[load2>0]=1
            pt2= ps.generators.RSA(load2, radiuspt2, v_loadingregion[1]+vpt1+vpt2, n_max=1000000)  
            pt_only_2 = copy.deepcopy(pt2)
            pt_only_2 = pt_only_2 - (load2)
            pt_only_2[pt_only_2>0]=1
            
            load3 = loading_region[2]+pt_only_1+pt_only_2
            load3[load3>0]=1
            pt3= ps.generators.RSA(load3, radiuspt3, v_loadingregion[2]+vpt1+vpt2+vpt3, n_max=1000000)  
            pt_only_3 = copy.deepcopy(pt3)
            pt_only_3 = pt_only_3 - (load3)
            pt_only_3[pt_only_3>0]=1

            load4=loading_region[3]+pt_only_1+pt_only_2+pt_only_3
            load4[load4>0]=1
            pt4= ps.generators.RSA(load4, radiuspt4, v_loadingregion[3]+vpt1+vpt2+vpt3+vpt4, n_max=1000000)  
            pt_only_4 = copy.deepcopy(pt4)
            pt_only_4 = pt_only_4 - load4
            pt_only_4[pt_only_4>0]=1
            boxsize=pt4.shape[0]
            pt_only=np.zeros([boxsize,boxsize,boxsize])
            
            if (vpt1!=0):
                pt_only = pt_only_1
                
            if (vpt2!=0):
                pt_only += pt_only_2

            if (vpt3!=0):
                pt_only += pt_only_3
                
            if (vpt4!=0):
                pt_only += pt_only_4
                
            print(np.count_nonzero(pt_only)/(pt_only.size))
            pt_only[pt_only>0]= 126*(10**10)
            # plt.figure()
            # plt.imshow(pt_only[:,:,500])
            
            #CL_carbon_Pt = pt_only + im_sld_nowater 
            
            
            CL_carbon_Pt = pt_only + im_sld_nowater
                # plt.figure()
                # plt.imshow(CL_carbon_Pt [:,:,500])
                
            CL_carbon_Pt[CL_carbon_Pt>=126*(10**10)] =126*(10**10)
            CL_carbon_Pt[(CL_carbon_Pt<125*(10**10))&(CL_carbon_Pt>=SLD_solid)] =SLD_solid
            # plt.figure()
            # plt.imshow(CL_carbon_Pt [:,:,500])
            img2sas_params = self.boxsize,self.onevoxel
            I_carbon,q_carbon =img2sas(im_sld_nowater,self.boxsize,self.onevoxel)
            I_Pt, q_Pt = img2sas(CL_carbon_Pt,self.boxsize,self.onevoxel) 
            return I_Pt,q_Pt, I_carbon, q_carbon, CL_carbon_Pt
        
    def trim(self,im):
            im_forwatershed = copy.deepcopy(im)
            im_forwatershed[im_forwatershed>0]=1
            sigma = 0.2
            dt = spim.distance_transform_edt(input=1-im_forwatershed)
            dt = spim.gaussian_filter(input=dt, sigma=sigma)
            peaks = find_peaks(dt=dt)
            
            print('Initial number of peaks: ', spim.label(peaks)[1])
            peaks = trim_saddle_points(peaks=peaks, dt=dt)
            print('Peaks after trimming saddle points: ', spim.label(peaks)[1])
            peaks = trim_nearby_peaks(peaks=peaks, dt=dt)
            peaks, N = spim.label(peaks)
            print('Peaks after trimming nearby peaks: ', N)
            regions = skimage.segmentation.watershed(image=-dt, markers=peaks, connectivity=1, offset=None, mask=dt > 0, compactness=0, watershed_line=False)
            regions2 = region_size(regions)
            self.watershed = regions2
            
   
    def minimizeobjective_puttingPt(self,params):
            qexp=self.q_exp
            Iexp=self.I_exp
            im=self.im_carbon
            SLD_solid = self.SLD_solid
            if 'watershed' in self.__dict__:
                regions3=self.watershed

            else:    
                self.trim(self.im_carbon)
                regions3=self.watershed
                print('no initial watershed')
                
            onevoxel=self.onevoxel
            radiuspt_1 = int(params['radiuspt_1']/self.onevoxel)
            radiuspt_2 = int(params['radiuspt_2']/self.onevoxel)
            radiuspt_3 = int(params['radiuspt_3']/self.onevoxel)
            radiuspt_4 = int(params['radiuspt_4']/self.onevoxel)
            vpt_1 = params['vpt_1']
            vpt_2 = params['vpt_2']
            vpt_3 = params['vpt_3']
            vpt_4 = params['vpt_4']
            ionomerthickness=int(params['ionomerthickness']/self.onevoxel)
            poreradii=int(params['threshold']/self.onevoxel)
            loading_region = self.makePtloadingregion(im,ionomerthickness,regions3,poreradii,radiuspt_1,vpt_1,radiuspt_2,vpt_2,radiuspt_3,vpt_3,radiuspt_4,vpt_4)
            I_img2sas, q_img2sas, I_carbon,q_carbon, im_Pt = self.decoratingPt(ionomerthickness,radiuspt_1,vpt_1,radiuspt_2,vpt_2,radiuspt_3,vpt_3,radiuspt_4,vpt_4,onevoxel,loading_region,SLD_solid)
            q_img2sas_filter,ys,I_img2sas_filter=binningq(qexp,Iexp,q_img2sas,I_img2sas)              
            #K_img2sas=find_scalingfactor(q_img2sas_filter,ys,q_carbon,I_carbon,0.2)
            K_img2sas = self.K_img2sas
            residual = 0
            for j in range(len(q_img2sas_filter)):
                residual += ((((I_img2sas_filter[j])*K_img2sas)/ys[j])-1)**2
                #residual /= len(q_img2sas_filter)
            residual = residual**(1/2)
            intensity_data = {'q_img2sas_Pt': q_img2sas_filter,
                              'I_exp_Pt' : ys,
                              'I_img2sas_Pt': I_img2sas_filter,
                              'K_img2sas':K_img2sas}
                              #,'im_Pt':im_Pt}
            #return q_carbon,I_carbon, q_img2sas_filter, ys, I_img2sas_filter, K_img2sas, im_Pt
            return -residual, intensity_data
        
      
    def minimizeobjective_puttingPt_v2(self,params):
            qexp=self.q_exp
            Iexp=self.I_exp
            im=self.im_carbon
            SLD_solid = self.SLD_solid
            if 'watershed' in self.__dict__:
                regions3=self.watershed

            else:    
                self.trim(self.im_carbon)
                regions3=self.watershed
                print('no initial watershed')
                

            onevoxel=self.onevoxel
            radiuspt_1 = int(params['radiuspt_1']/self.onevoxel)
            radiuspt_2 = int(params['radiuspt_2']/self.onevoxel)
            radiuspt_3 = int(params['radiuspt_3']/self.onevoxel)
            radiuspt_4 = int(params['radiuspt_4']/self.onevoxel)
            vpt_1 = params['vpt_1']
            vpt_2 = params['vpt_2']
            vpt_3 = params['vpt_3']
            vpt_4 = params['vpt_4']
            ionomerthickness=int(params['ionomerthickness']/self.onevoxel)
            poreradii=int(params['threshold']/self.onevoxel)
            loading_region = self.makePtloadingregion_v2(im,ionomerthickness,regions3,poreradii,radiuspt_1,vpt_1,radiuspt_2,vpt_2,radiuspt_3,vpt_3,radiuspt_4,vpt_4)
            I_img2sas, q_img2sas, I_carbon,q_carbon, im_Pt = self.decoratingPt_v2(ionomerthickness,radiuspt_1,vpt_1,radiuspt_2,vpt_2,radiuspt_3,vpt_3,radiuspt_4,vpt_4,onevoxel,loading_region,SLD_solid)
            q_img2sas_filter,ys,I_img2sas_filter=binningq(qexp,Iexp,q_img2sas,I_img2sas)              
            #K_img2sas=find_scalingfactor(q_img2sas_filter,ys,q_carbon,I_carbon,0.2)
            K_img2sas = self.K_img2sas
            residual = 0
            for j in range(len(q_img2sas_filter)):
                residual += ((((I_img2sas_filter[j])*K_img2sas)/ys[j])-1)**2
                #residual /= len(q_img2sas_filter)
            residual = residual**(1/2)
            intensity_data = {'q_img2sas_Pt': q_img2sas_filter,
                              'I_exp_Pt' : ys,
                              'I_img2sas_Pt': I_img2sas_filter,
                              'K_img2sas':K_img2sas}
                              #,'im_Pt':im_Pt}
            #return q_carbon,I_carbon, q_img2sas_filter, ys, I_img2sas_filter, K_img2sas, im_Pt
            return -residual, intensity_data
   
    def storeintensity_puttingPt(self,params):
            qexp=self.q_exp
            Iexp=self.I_exp
            im=self.im_carbon
            SLD_solid = self.SLD_solid
            if 'watershed' in self.__dict__:
                regions3=self.watershed

            else:    
                self.trim(self.im_carbon)
                regions3=self.watershed
                print('no initial watershed')
                
            onevoxel=self.onevoxel
            radiuspt_1 = int(params['radiuspt_1']/self.onevoxel)
            radiuspt_2 = int(params['radiuspt_2']/self.onevoxel)
            radiuspt_3 = int(params['radiuspt_3']/self.onevoxel)
            radiuspt_4 = int(params['radiuspt_4']/self.onevoxel)
            vpt_1 = params['vpt_1']
            vpt_2 = params['vpt_2']
            vpt_3 = params['vpt_3']
            vpt_4 = params['vpt_4']
            ionomerthickness=int(params['ionomerthickness']/self.onevoxel)
            poreradii=int(params['threshold']/self.onevoxel)
            loading_region = self.makePtloadingregion(im,ionomerthickness,regions3,poreradii,radiuspt_1,vpt_1,radiuspt_2,vpt_2,radiuspt_3,vpt_3,radiuspt_4,vpt_4)
            I_img2sas, q_img2sas,im_Pt = self.decoratingPt(ionomerthickness,radiuspt_1,vpt_1,radiuspt_2,vpt_2,radiuspt_3,vpt_3,radiuspt_4,vpt_4,onevoxel,loading_region,SLD_solid)
            q_img2sas_filter,ys,I_img2sas_filter=binningq(qexp,Iexp,q_img2sas,I_img2sas)              
            #K_img2sas=find_scalingfactor(q_img2sas_filter,ys,q_carbon,I_carbon,0.2)
            K_img2sas = self.K_img2sas
            residual = 0
            for j in range(len(q_img2sas_filter)):
                residual += ((((I_img2sas_filter[j])*K_img2sas)/ys[j])-1)**2
                #residual /= len(q_img2sas_filter)
            
            return q_img2sas_filter, ys, I_img2sas_filter, K_img2sas, im_Pt
        
    def storeintensity_puttingPt_v2(self,params):
            qexp=self.q_exp
            Iexp=self.I_exp
            im=self.im_carbon
            SLD_solid = self.SLD_solid
            if 'watershed' in self.__dict__:
                regions3=self.watershed

            else:    
                self.trim(self.im_carbon)
                regions3=self.watershed
                print('no initial watershed')
                
            onevoxel=self.onevoxel
            radiuspt_1 = int(params['radiuspt_1']/self.onevoxel)
            radiuspt_2 = int(params['radiuspt_2']/self.onevoxel)
            radiuspt_3 = int(params['radiuspt_3']/self.onevoxel)
            radiuspt_4 = int(params['radiuspt_4']/self.onevoxel)
            vpt_1 = params['vpt_1']
            vpt_2 = params['vpt_2']
            vpt_3 = params['vpt_3']
            vpt_4 = params['vpt_4']
            ionomerthickness=int(params['ionomerthickness']/self.onevoxel)
            poreradii=int(params['threshold']/self.onevoxel)
            loading_region = self.makePtloadingregion_v2(im,ionomerthickness,regions3,poreradii,radiuspt_1,vpt_1,radiuspt_2,vpt_2,radiuspt_3,vpt_3,radiuspt_4,vpt_4)
            I_img2sas, q_img2sas, I_carbon,q_carbon, im_Pt = self.decoratingPt_v2(ionomerthickness,radiuspt_1,vpt_1,radiuspt_2,vpt_2,radiuspt_3,vpt_3,radiuspt_4,vpt_4,onevoxel,loading_region,SLD_solid)
            q_img2sas_filter,ys,I_img2sas_filter=binningq(qexp,Iexp,q_img2sas,I_img2sas)              
            #K_img2sas=find_scalingfactor(q_img2sas_filter,ys,q_carbon,I_carbon,0.2)
            K_img2sas = self.K_img2sas
            residual = 0
            for j in range(len(q_img2sas_filter)):
                residual += ((((I_img2sas_filter[j])*K_img2sas)/ys[j])-1)**2
                #residual /= len(q_img2sas_filter)
            
            return q_carbon,I_carbon, q_img2sas_filter, ys, I_img2sas_filter, K_img2sas, im_Pt
                     
    def optimization_puttingPt(self, opt_space, n_iter=100,**previousfit): 
            hyper = Hyperactive(verbosity=["progress_bar", "print_results", "print_times"])
            optimizer = TreeStructuredParzenEstimators()
            early_stopping = {
                    "n_iter_no_change": 300,
                    "tol_abs":None,
                   "tol_rel": None
                   }
            
            #optimizer = EvolutionStrategyOptimizer()
            if ('search_data' in previousfit)&('previousbestpara' in previousfit):
                search_data= previousfit['search_data']
                previousbestpara= previousfit['previousbestpara']
                initialize = {'grid':4,'vertices':4,'random':6,'warm_start':[previousbestpara]}
                hyper.add_search(self.minimizeobjective_puttingPt, opt_space, optimizer=optimizer, n_iter=n_iter,early_stopping=early_stopping, memory="share",memory_warm_start=search_data,initialize=initialize)
            elif ('previousbestpara' in previousfit):   
                previousbestpara= previousfit['previousbestpara']
                initialize = {'grid':4,'vertices':4,'random':6,'warm_start':[previousbestpara]}
                hyper.add_search(self.minimizeobjective_puttingPt, opt_space, optimizer=optimizer, n_iter=n_iter,early_stopping=early_stopping, memory="share",initialize=initialize)
            else:
                initialize = {'grid':4,'vertices':4,'random':6}
                hyper.add_search(self.minimizeobjective_puttingPt, opt_space, optimizer=optimizer, n_iter=n_iter,early_stopping=early_stopping, memory="share",initialize=initialize)
            hyper.run()
            self.opt_para = hyper.best_para(self.minimizeobjective_puttingPt)
            self.search_data = hyper.search_data(self.minimizeobjective_puttingPt)
            
            
    def optimization_puttingPt_v2(self, opt_space, n_iter=100,**previousfit): 
            hyper = Hyperactive(verbosity=["progress_bar", "print_results", "print_times"])
            optimizer = TreeStructuredParzenEstimators()
            early_stopping = {
                    "n_iter_no_change": 300,
                    "tol_abs":None,
                   "tol_rel": None
                   }
            
            #optimizer = EvolutionStrategyOptimizer()
            if ('search_data' in previousfit)&('previousbestpara' in previousfit):
                search_data= previousfit['search_data']
                previousbestpara= previousfit['previousbestpara']
                initialize = {'grid':4,'vertices':4,'random':6,'warm_start':[previousbestpara]}
                hyper.add_search(self.minimizeobjective_puttingPt_v2, opt_space, optimizer=optimizer, n_iter=n_iter,early_stopping=early_stopping, memory="share",memory_warm_start=search_data,initialize=initialize)
            elif ('previousbestpara' in previousfit):   
                previousbestpara= previousfit['previousbestpara']
                initialize = {'grid':4,'vertices':4,'random':6,'warm_start':[previousbestpara]}
                hyper.add_search(self.minimizeobjective_puttingPt_v2, opt_space, optimizer=optimizer, n_iter=n_iter,early_stopping=early_stopping, memory="share",initialize=initialize)
            else:
                initialize = {'grid':4,'vertices':4,'random':6}
                hyper.add_search(self.minimizeobjective_puttingPt_v2, opt_space, optimizer=optimizer, n_iter=n_iter,early_stopping=early_stopping, memory="share",initialize=initialize)
            hyper.run()
            self.opt_para = hyper.best_para(self.minimizeobjective_puttingPt_v2)
            self.search_data = hyper.search_data(self.minimizeobjective_puttingPt_v2)
            
    def minimizeobjective_puttingPt_decane(self,params):
            qexpwet=self.qwet
            Iexpwet=self.Iwet
            qexp=self.q_exp
            Iexp=self.I_exp
            im=self.im_carbon
            SLD_solid = self.SLD_solid
            if 'watershed' in self.__dict__:
                regions3=self.watershed

            else:    
                self.trim(self.im_carbon)
                regions3=self.watershed
                print('no initial watershed')
                
            onevoxel=self.onevoxel
            radiuspt_1 = int(params['radiuspt_1']/self.onevoxel)
            radiuspt_2 = int(params['radiuspt_2']/self.onevoxel)
            radiuspt_3 = int(params['radiuspt_3']/self.onevoxel)
            radiuspt_4 = int(params['radiuspt_4']/self.onevoxel)
            vpt_1 = params['vpt_1']
            vpt_2 = params['vpt_2']
            vpt_3 = params['vpt_3']
            vpt_4 = params['vpt_4']
            ionomerthickness=int(params['ionomerthickness']/self.onevoxel)
            poreradii=int(params['threshold']/self.onevoxel)
            loading_region = self.makePtloadingregion(im,ionomerthickness,regions3,poreradii,radiuspt_1,vpt_1,radiuspt_2,vpt_2,radiuspt_3,vpt_3,radiuspt_4,vpt_4)
            I_img2sas, q_img2sas,im_Pt = self.decoratingPt(ionomerthickness,radiuspt_1,vpt_1,radiuspt_2,vpt_2,radiuspt_3,vpt_3,radiuspt_4,vpt_4,onevoxel,loading_region,SLD_solid)
            q_img2sas_filter,ys,I_img2sas_filter=binningq(qexp,Iexp,q_img2sas,I_img2sas)
           
            im_Pt_decane = copy.deepcopy(im_Pt)
            im_Pt_decane[im_Pt_decane ==0]=7.156*(10**10)
            I_decane, q_decane  = functions.img2sas(im_Pt_decane,self.boxsize,self.onevoxel)
            q_img2sas_filter_decane,ys_decane,I_img2sas_filter_decane=binningq(qexpwet,Iexpwet,q_decane,I_decane)
            #K_img2sas=find_scalingfactor(q_img2sas_filter,ys,q_carbon,I_carbon,0.2)
            K_img2sas = self.K_img2sas
            residual = 0
            for j in range(len(q_img2sas_filter)):
                residual += (((((I_img2sas_filter[j])*K_img2sas)/ys[j])-1)**2)+ (((((I_img2sas_filter_decane[j])*K_img2sas)/ys_decane[j])-1)**2)
                #residual /= len(q_img2sas_filter)
            residual = residual**(1/2)
            intensity_data = {'q_img2sas_Pt': q_img2sas_filter,
                              'I_exp_Pt' : ys,
                              'I_img2sas_Pt': I_img2sas_filter,
                              'I_exp_Pt_decane' : ys_decane,
                              'I_img2sas_Pt_decane': I_img2sas_filter_decane,
                              'K_img2sas':K_img2sas}
                              #,'im_Pt':im_Pt}
            #return q_carbon,I_carbon, q_img2sas_filter, ys, I_img2sas_filter, K_img2sas, im_Pt
            return -residual, intensity_data
                
    def minimizeobjective_puttingPt_decane_v2(self,params):
            qexpwet=self.qwet
            Iexpwet=self.Iwet
            qexp=self.q_exp
            Iexp=self.I_exp
            im=self.im_carbon
            SLD_solid = self.SLD_solid
            if 'watershed' in self.__dict__:
                regions3=self.watershed

            else:    
                self.trim(self.im_carbon)
                regions3=self.watershed
                print('no initial watershed')
                
            onevoxel=self.onevoxel
            radiuspt_1 = int(params['radiuspt_1']/self.onevoxel)
            radiuspt_2 = int(params['radiuspt_2']/self.onevoxel)
            radiuspt_3 = int(params['radiuspt_3']/self.onevoxel)
            radiuspt_4 = int(params['radiuspt_4']/self.onevoxel)
            vpt_1 = params['vpt_1']
            vpt_2 = params['vpt_2']
            vpt_3 = params['vpt_3']
            vpt_4 = params['vpt_4']
            ionomerthickness=int(params['ionomerthickness']/self.onevoxel)
            poreradii=int(params['threshold']/self.onevoxel)
            loading_region = self.makePtloadingregion_v2(im,ionomerthickness,regions3,poreradii,radiuspt_1,vpt_1,radiuspt_2,vpt_2,radiuspt_3,vpt_3,radiuspt_4,vpt_4)
            I_img2sas, q_img2sas, I_carbon,q_carbon, im_Pt = self.decoratingPt_v2(ionomerthickness,radiuspt_1,vpt_1,radiuspt_2,vpt_2,radiuspt_3,vpt_3,radiuspt_4,vpt_4,onevoxel,loading_region,SLD_solid)
            q_img2sas_filter,ys,I_img2sas_filter=binningq(qexp,Iexp,q_img2sas,I_img2sas)
           
            im_Pt_decane = copy.deepcopy(im_Pt)
            im_Pt_decane[im_Pt_decane ==0]=7.156*(10**10)
            I_decane, q_decane  = functions.img2sas(im_Pt_decane,self.boxsize,self.onevoxel)
            q_img2sas_filter_decane,ys_decane,I_img2sas_filter_decane=binningq(qexpwet,Iexpwet,q_decane,I_decane)
            #K_img2sas=find_scalingfactor(q_img2sas_filter,ys,q_carbon,I_carbon,0.2)
            K_img2sas = self.K_img2sas
            residual = 0
            for j in range(len(q_img2sas_filter)):
                residual += (((((I_img2sas_filter[j])*K_img2sas)/ys[j])-1)**2)+ (((((I_img2sas_filter_decane[j])*K_img2sas)/ys_decane[j])-1)**2)
                #residual /= len(q_img2sas_filter)
            residual = residual**(1/2)
            intensity_data = {'q_img2sas_Pt': q_img2sas_filter,
                              'I_exp_Pt' : ys,
                              'I_img2sas_Pt': I_img2sas_filter,
                              'I_exp_Pt_decane' : ys_decane,
                              'I_img2sas_Pt_decane': I_img2sas_filter_decane,
                              'K_img2sas':K_img2sas}
                              #,'im_Pt':im_Pt}
            #return q_carbon,I_carbon, q_img2sas_filter, ys, I_img2sas_filter, K_img2sas, im_Pt
            return -residual, intensity_data
    
    def minimizeobjective_puttingPt_nodecane(self,params):

            qexp=self.q_exp
            Iexp=self.I_exp
            im=self.im_carbon
            SLD_solid = self.SLD_solid
            if 'watershed' in self.__dict__:
                regions3=self.watershed

            else:    
                self.trim(self.im_carbon)
                regions3=self.watershed
                print('no initial watershed')
                
            onevoxel=self.onevoxel
            radiuspt_1 = int(params['radiuspt_1']/self.onevoxel)
            radiuspt_2 = int(params['radiuspt_2']/self.onevoxel)
            radiuspt_3 = int(params['radiuspt_3']/self.onevoxel)
            radiuspt_4 = int(params['radiuspt_4']/self.onevoxel)
            vpt_1 = params['vpt_1']
            vpt_2 = params['vpt_2']
            vpt_3 = params['vpt_3']
            vpt_4 = params['vpt_4']
            ionomerthickness=int(params['ionomerthickness']/self.onevoxel)
            poreradii=int(params['threshold']/self.onevoxel)
            loading_region = self.makePtloadingregion(im,ionomerthickness,regions3,poreradii,radiuspt_1,vpt_1,radiuspt_2,vpt_2,radiuspt_3,vpt_3,radiuspt_4,vpt_4)
            I_img2sas, q_img2sas,im_Pt = self.decoratingPt(ionomerthickness,radiuspt_1,vpt_1,radiuspt_2,vpt_2,radiuspt_3,vpt_3,radiuspt_4,vpt_4,onevoxel,loading_region,SLD_solid)
            q_img2sas_filter,ys,I_img2sas_filter=binningq(qexp,Iexp,q_img2sas,I_img2sas)
           
            # im_Pt_decane = copy.deepcopy(im_Pt)
            # im_Pt_decane[im_Pt_decane ==0]=7.156*(10**10)
            # I_decane, q_decane  = functions.img2sas(im_Pt_decane,self.boxsize,self.onevoxel)
            # q_img2sas_filter_decane,ys_decane,I_img2sas_filter_decane=binningq(qexpwet,Iexpwet,q_decane,I_decane)
            #K_img2sas=find_scalingfactor(q_img2sas_filter,ys,q_carbon,I_carbon,0.2)
            K_img2sas = self.K_img2sas
            residual = 0
            for j in range(len(q_img2sas_filter)):
                residual += (((((I_img2sas_filter[j])*K_img2sas)/ys[j])-1)**2)
                #residual /= len(q_img2sas_filter)
            residual = residual**(1/2)
            intensity_data = {'q_img2sas_Pt': q_img2sas_filter,
                              'I_exp_Pt' : ys,
                              'I_img2sas_Pt': I_img2sas_filter,
                              
                              'K_img2sas':K_img2sas}
                              #,'im_Pt':im_Pt}
            #return q_carbon,I_carbon, q_img2sas_filter, ys, I_img2sas_filter, K_img2sas, im_Pt
            return -residual, intensity_data
                
        
    def optimization_puttingPt_decane(self, qwet,Iwet, opt_space, n_iter=100,**previousfit): 
        self.qwet=qwet
        self.Iwet=Iwet
        hyper = Hyperactive(verbosity=["progress_bar", "print_results", "print_times"])
        optimizer = TreeStructuredParzenEstimators()
        early_stopping = {
                "n_iter_no_change": 300,
                "tol_abs":None,
               "tol_rel": None
               }
        
        #optimizer = EvolutionStrategyOptimizer()
        if ('search_data' in previousfit)&('previousbestpara' in previousfit):
            search_data= previousfit['search_data']
            previousbestpara= previousfit['previousbestpara']
            initialize = {'grid':4,'vertices':4,'random':6,'warm_start':[previousbestpara]}
            hyper.add_search(self.minimizeobjective_puttingPt_decane, opt_space, optimizer=optimizer, n_iter=n_iter,early_stopping=early_stopping, memory="share",memory_warm_start=search_data,initialize=initialize)
        elif ('previousbestpara' in previousfit):   
            previousbestpara= previousfit['previousbestpara']
            initialize = {'grid':4,'vertices':4,'random':6,'warm_start':[previousbestpara]}
            hyper.add_search(self.minimizeobjective_puttingPt_decane, opt_space, optimizer=optimizer, n_iter=n_iter,early_stopping=early_stopping, memory="share",initialize=initialize)
        else:
            initialize = {'grid':4,'vertices':4,'random':6}
            hyper.add_search(self.minimizeobjective_puttingPt_decane, opt_space, optimizer=optimizer, n_iter=n_iter,early_stopping=early_stopping, memory="share",initialize=initialize)
        hyper.run()
        self.opt_para = hyper.best_para(self.minimizeobjective_puttingPt_decane)
        self.search_data = hyper.search_data(self.minimizeobjective_puttingPt_decane)
        
    def optimization_puttingPt_nodecane(self, opt_space, n_iter=100,**previousfit): 

        hyper = Hyperactive(verbosity=["progress_bar", "print_results", "print_times"])
        optimizer = TreeStructuredParzenEstimators()
        early_stopping = {
                "n_iter_no_change": 300,
                "tol_abs":None,
               "tol_rel": None
               }
        
        #optimizer = EvolutionStrategyOptimizer()
        if ('search_data' in previousfit)&('previousbestpara' in previousfit):
            search_data= previousfit['search_data']
            previousbestpara= previousfit['previousbestpara']
            initialize = {'grid':4,'vertices':4,'random':6,'warm_start':[previousbestpara]}
            hyper.add_search(self.minimizeobjective_puttingPt_nodecane, opt_space, optimizer=optimizer, n_iter=n_iter,early_stopping=early_stopping, memory="share",memory_warm_start=search_data,initialize=initialize)
        elif ('previousbestpara' in previousfit):   
            previousbestpara= previousfit['previousbestpara']
            initialize = {'grid':4,'vertices':4,'random':6,'warm_start':[previousbestpara]}
            hyper.add_search(self.minimizeobjective_puttingPt_nodecane, opt_space, optimizer=optimizer, n_iter=n_iter,early_stopping=early_stopping, memory="share",initialize=initialize)
        else:
            initialize = {'grid':4,'vertices':4,'random':6}
            hyper.add_search(self.minimizeobjective_puttingPt_nodecane, opt_space, optimizer=optimizer, n_iter=n_iter,early_stopping=early_stopping, memory="share",initialize=initialize)
        hyper.run()
        self.opt_para = hyper.best_para(self.minimizeobjective_puttingPt_nodecane)
        self.search_data = hyper.search_data(self.minimizeobjective_puttingPt_nodecane)
        
    def optimization_puttingPt_decane_v2(self, qwet,Iwet, opt_space, n_iter=100,**previousfit): 
        self.qwet=qwet
        self.Iwet=Iwet
        hyper = Hyperactive(verbosity=["progress_bar", "print_results", "print_times"])
        optimizer = TreeStructuredParzenEstimators()
        early_stopping = {
                "n_iter_no_change": 300,
                "tol_abs":None,
               "tol_rel": None
               }
        
        #optimizer = EvolutionStrategyOptimizer()
        if ('search_data' in previousfit)&('previousbestpara' in previousfit):
            search_data= previousfit['search_data']
            previousbestpara= previousfit['previousbestpara']
            initialize = {'grid':4,'vertices':4,'random':6,'warm_start':[previousbestpara]}
            hyper.add_search(self.minimizeobjective_puttingPt_decane_v2, opt_space, optimizer=optimizer, n_iter=n_iter,early_stopping=early_stopping, memory="share",memory_warm_start=search_data,initialize=initialize)
        elif ('previousbestpara' in previousfit):   
            previousbestpara= previousfit['previousbestpara']
            initialize = {'grid':4,'vertices':4,'random':6,'warm_start':[previousbestpara]}
            hyper.add_search(self.minimizeobjective_puttingPt_decane_v2, opt_space, optimizer=optimizer, n_iter=n_iter,early_stopping=early_stopping, memory="share",initialize=initialize)
        else:
            initialize = {'grid':4,'vertices':4,'random':6}
            hyper.add_search(self.minimizeobjective_puttingPt_decane_v2, opt_space, optimizer=optimizer, n_iter=n_iter,early_stopping=early_stopping, memory="share",initialize=initialize)
        hyper.run()
        self.opt_para = hyper.best_para(self.minimizeobjective_puttingPt_decane_v2)
        self.search_data = hyper.search_data(self.minimizeobjective_puttingPt_decane_v2)
           
    def optimization_puttingPt_decane_es(self, qwet,Iwet, opt_space, n_iter=100,**previousfit): 
        self.qwet=qwet
        self.Iwet=Iwet
        hyper = Hyperactive(verbosity=["progress_bar", "print_results", "print_times"])
        optimizer =EvolutionStrategyOptimizer()
        early_stopping = {
                "n_iter_no_change": 300,
                "tol_abs":None,
               "tol_rel": None
               }
        
        #optimizer = EvolutionStrategyOptimizer()
        if ('search_data' in previousfit)&('previousbestpara' in previousfit):
            search_data= previousfit['search_data']
            previousbestpara= previousfit['previousbestpara']
            initialize = {'grid':4,'vertices':4,'random':6,'warm_start':[previousbestpara]}
            hyper.add_search(self.minimizeobjective_puttingPt_decane, opt_space, optimizer=optimizer, n_iter=n_iter,early_stopping=early_stopping, memory="share",memory_warm_start=search_data,initialize=initialize)
        elif ('previousbestpara' in previousfit):   
            previousbestpara= previousfit['previousbestpara']
            initialize = {'grid':4,'vertices':4,'random':6,'warm_start':[previousbestpara]}
            hyper.add_search(self.minimizeobjective_puttingPt_decane, opt_space, optimizer=optimizer, n_iter=n_iter,early_stopping=early_stopping, memory="share",initialize=initialize)
        else:
            initialize = {'grid':4,'vertices':4,'random':6}
            hyper.add_search(self.minimizeobjective_puttingPt_decane, opt_space, optimizer=optimizer, n_iter=n_iter,early_stopping=early_stopping, memory="share",initialize=initialize)
        hyper.run()
        self.opt_para = hyper.best_para(self.minimizeobjective_puttingPt_decane)
        self.search_data = hyper.search_data(self.minimizeobjective_puttingPt_decane)
            