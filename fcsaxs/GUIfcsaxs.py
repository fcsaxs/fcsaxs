import sys
import numpy as np
import pickle
from PyQt5 import QtWidgets, QtCore
from PyQt5.QtWidgets import QFileDialog, QVBoxLayout, QHBoxLayout, QPushButton, QProgressBar, QTextEdit
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from scipy.optimize import curve_fit
import time
import numpy as np 
import itertools
import matplotlib.pyplot as plt
import matplotlib
matplotlib.rcParams.update({'font.size':30})
import matplotlib.pyplot as plt
from skimage.data import astronaut
import skimage
import numpy as np
import scipy
from scipy import ndimage
import matplotlib.pyplot as plt
import matplotlib.pyplot as plt
import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import matplotlib.pyplot as plt
import time
import tifffile as tif
import math
import raster_geometry as ras
import porespy as ps
from scipy.signal import general_gaussian, tukey
import jscatter as js 
import itertools
from scipy.stats import binned_statistic
from lmfit.models import SkewedGaussianModel, LinearModel, Model, PowerLawModel
from scipy.optimize import curve_fit
from lmfit.models import ExponentialModel, GaussianModel, PolynomialModel
import functions
import IBM_Pt_simultaneous
import os
import multiprocessing

class FittingThread(QtCore.QThread):
    fittingProgress = QtCore.pyqtSignal(int)

    def __init__(self, parent=None):
        super().__init__(parent)
        self.q = None
        self.I = None
        self.params = None

    def set_data(self, q, I):
        self.q = q
        self.I = I

    def set_params(self, params):
        self.params = params
        self.classnum=params['classnum']
        self.boxsize=params['boxsize']
        self.voxelsize=params['voxelsize']
        self.porosity=params['porosity']
        self.search_space=params['search_space']
        self.timestr= time.strftime("%Y%m%d")
        self.samplename=params['samplename']
        self.SLD_solid=params['SLD_solid']
        if params['K_img2sas']:
            self.K_img2sas = params['K_img2sas']

    def run(self):
     
        functions.padding_or_not(0)
        functions.cpu_or_not(0)
        
        #make instance of IBM structure (class, simulation box size, voxel size, desired porosity, q exp, Int exp, scattering length density of solid)
        self.IBM_1 = IBM_Pt_simultaneous.IBM_simultaneous(self.classnum, self.boxsize, self.voxelsize, self.porosity, self.q[self.q<3], self.I[self.q<3],self.SLD_solid)
        search_space=self.search_space
        if self.K_img2sas:
            self.IBM_1.K_img2sas = self.K_img2sas
            
        
        
        #codes to make the search space in a suitable format for optimizer to work
        aaa=list(search_space.values())
        bbb=list(search_space.keys())
        i=0
        for key in bbb:
            if key =="scalingfactor":
                search_space[key]=[aaa[i]]
            else:
                search_space[key]=list(aaa[i])
            i+=1
            
        #do you have previous fit?
        previousfit = {}
        self.IBM_1.numiter=200
        #optimization occurs
        self.IBM_1.optimization_puttingPt_nodecane(search_space,n_iter=self.IBM_1.numiter,**previousfit)
        
        active = multiprocessing.active_children()
        for child in active:
            child.terminate()
        #save as variable the search data
        self.search_data_fix = self.IBM_1.search_data
        
        self.IBM_1.seed=np.random.randint(1,100)
        
        self.search_data_best= self.search_data_fix[self.search_data_fix.score==max(self.search_data_fix.score)]
        self.fittingProgress.emit(95)
        # error, intensity_data,im_Pt = self.IBM_1.afterminimizationPt_nodecane(
        #     dict(self.search_data_best.iloc[0, :11+10]))
        
        # # while ( (1 - (np.count_nonzero(im_Pt)/(self.IBM_1.boxsize**3))) <( 0.95*self.IBM_1.porosity)) | ( (1 - (np.count_nonzero(im_Pt)/(self.IBM_1.boxsize**3))) >( 1.05*self.IBM_1.porosity)):
        # #     # IBM_1.porosity=0.69  
        # self.IBM_1.seed = np.random.randint(1,100)    
        # error, intensity_data,im_Pt = self.IBM_1.afterminimizationPt_nodecane(
        # dict(self.search_data_best.iloc[0, :11+10]))

        I_img2sas,q_img2sas = functions.img2sas(self.IBM_1.im_Pt,self.boxsize,self.voxelsize)
        
        self.error=self.IBM_1.error
        self.intensity_data=self.IBM_1.intensity_data
        self.im_Pt=self.IBM_1.im_Pt
        self.I_img2sas,self.q_img2sas=self.IBM_1.intensity_data['I_img2sas_Pt'],self.IBM_1.intensity_data['q_img2sas_Pt']
        # Emit the fitting progress signal
        self.fittingProgress.emit(100)



class MainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()

        # Initialize the window properties
        self.setWindowTitle("FCSAXS - Representative Structure Modelling for PEFC Catalyst Layer from SAXS Data")
        self.setGeometry(100, 100, 1500, 800)
        # self.showMaximized()

        # Create the central widget
        self.central_widget = QtWidgets.QWidget()
        self.setCentralWidget(self.central_widget)

        # Create layout for the central widget
        self.layout = QtWidgets.QHBoxLayout(self.central_widget)

        # Create a vertical layout for the left side (parameter printing area)
        self.left_layout = QtWidgets.QVBoxLayout()

        # Create a text edit widget for parameter printing
        self.parameter_textedit = QTextEdit(self)
        self.parameter_textedit.setReadOnly(True)
 
        self.parameter_textedit.setMaximumHeight(1000)
        self.parameter_textedit.setMaximumWidth(200)
        self.layout.addWidget(self.parameter_textedit)

        # Create a vertical layout for the right side (plotting area)
        self.right_layout = QtWidgets.QVBoxLayout()

        # Create a button for loading data
        self.load_button = QtWidgets.QPushButton("Load Dry Data", self)
        self.load_button.clicked.connect(self.load_data)
        self.right_layout.addWidget(self.load_button)

         # Create a button for loading initial parameters
        self.load_params_button = QtWidgets.QPushButton("Load Initial Parameters", self)
        self.load_params_button.clicked.connect(self.load_parameters)
        self.right_layout.addWidget(self.load_params_button)

      # Create a button for fitting
        self.fit_button = QtWidgets.QPushButton("Fit Data", self)
        self.fit_button.clicked.connect(self.fit_data)
        self.right_layout.addWidget(self.fit_button)

        # Create a button for storing parameters
        self.store_button = QtWidgets.QPushButton("Store Parameters", self)
        self.store_button.clicked.connect(self.store_parameters)
        self.right_layout.addWidget(self.store_button)

        # Create a button for resetting
        self.reset_button = QtWidgets.QPushButton("Reset", self)
        self.reset_button.clicked.connect(self.reset_program)
        self.right_layout.addWidget(self.reset_button)

        # Create a progress bar
        self.progress_bar = QProgressBar(self)
        self.right_layout.addWidget(self.progress_bar)
        self.progress_bar.hide()

        # Create a plot widget
        self.plot_widget = Figure()
        self.plot_canvas = FigureCanvas(self.plot_widget)
        self.plot_canvas.setMaximumHeight(2000)
        self.plot_canvas.setMaximumWidth(2000)
        self.right_layout.addWidget(self.plot_canvas)



        # Add the right layout to the main layout
        self.layout.addLayout(self.right_layout)

        # Initialize parameters
        self.params = {}

        # Create fitting thread
        self.fitting_thread = FittingThread(self)
        self.fitting_thread.fittingProgress.connect(self.update_progress)

    def load_parameters(self):
        # Open a file dialog to select a file
        file_dialog = QFileDialog()
        file_dialog.setNameFilter("Pickle (*.pkl)")
        file_path, _ = file_dialog.getOpenFileName(self, "Load Initial Parameters", "", "Pickle files (*.pkl)")

        if file_path:
            # Load the parameters from the pickle file
            with open(file_path, "rb") as f:
                loaded_params = pickle.load(f)

            # Update the parameters dictionary
            self.params.update(loaded_params)

            # Print the loaded parameters
            self.print_parameters()

    def load_data(self):
        # Open a file dialog to select a file
        file_dialog = QFileDialog()
        file_dialog.setNameFilter("txt (*.txt)")
        file_path, _ = file_dialog.getOpenFileName(self, "Load Dry Data", "", "txt files (*.txt)")

        if file_path:
            # Load the data from the file (example assumes data is in TXT format)
            q, I, E = self.load_txt_data(file_path)

            # Clear the plot widget
            self.plot_widget.clear()

            # Plot the data
            ax = self.plot_widget.add_subplot(121)
            ax.loglog(q, I,label='experimental')
            #ax.errorbar(q, I, E, linestyle='None', marker='o')
            
            ax.set_xlabel("q [1/nm]",fontsize='small')
            ax.set_ylabel("Intensity [a.u.]",fontsize='small')
            ax.set_title("Data Plot",pad=20,fontsize='small')
            ax.legend(fontsize='small')
            ax.tick_params(axis='x', labelsize='small')
            ax.tick_params(axis='y', labelsize='small')
            ax.set_box_aspect(1)
            # Redraw the plot
            self.plot_canvas.draw()

    def load_txt_data(self, file_path):
        # TODO: Implement TXT data loading logic
        # Example implementation assuming data is in a specific format

        data = np.loadtxt(file_path)    
        data = data[~np.isnan(data).any(axis=1)]


        def removebackground(qexp, Iexp):
            """
            A function to remove background at high-q 

            Parameters
            ----------
            qexp : array
                q data of a SAXS profile to be extrapolated.
            Iexp : array
                Intensity data of a SAXS profile to be extrapolated.

            Returns
            -------
            qremove: array
                Extrapolated q, both high-q and low-q.
            Iremove: array
                Extrapolated intensity, both high-q and low-q.

            """
            setvaluea,setvalueb= np.polyfit((qexp[qexp>3]**-4),(Iexp[qexp>3]),1)
            # #porod function
            def porod(x,a,c):
                return a*(x**-4)+c
            #fitting the porod
            mod = Model(porod, prefix='porod_')
            pars= mod.make_params()
            pars['porod_a'].set(value=setvaluea, min=0)
            pars['porod_c'].set(value=setvalueb, min=0)
            out = mod.fit(Iexp[qexp>3], pars, x=qexp[qexp>3])
            params_dict=out.params.valuesdict()
            C = params_dict['porod_c']

            qremove =  qexp
            Iremove= Iexp-C
            return qremove,Iremove
        #data cleaning
        qexp = data[:, 0]
        Iexp = data[:, 1]
        Eexp = data[:, 2]

        # qexp = qexp[Iexp > 0.1]

        # Eexp = Eexp[Iexp > 0.1]

        # Iexp = Iexp[Iexp > 0.1]


        # qexp,Iexp=removebackground(qexp,Iexp)

        import functions

        qexp_,Iexp=functions.binningq_forfitting(qexp, Iexp)
        qexp,Eexp=functions.binningq_forfitting(qexp,Eexp)
        return qexp, Iexp, Eexp

    def fit_data(self):
        # Retrieve the data from the plot
        ax = self.plot_widget.get_axes()[0]
        q = ax.lines[0].get_xdata()
        I = ax.lines[0].get_ydata()

        # Update the fitting thread with data and parameters
        self.fitting_thread.set_data(q, I)
        self.fitting_thread.set_params(self.params)

        # Start the fitting thread
        self.fitting_thread.start()

        # Show the progress bar
        self.progress_bar.show()
        self.progress_bar.setValue(0)

    def update_progress(self, value):
        # Update the progress bar value
        # value= self.fitting_thread.IBM_1.iter / self.fitting_thread.IBM_1.numiter * 100
        self.progress_bar.setValue(value)

        # If the fitting is complete, hide the progress bar
        if value == 100:
            # self.progress_bar.hide()



            # Plot the fitted curve
            ax = self.plot_widget.get_axes()[0]
            ax.loglog(self.fitting_thread.q_img2sas, self.fitting_thread.search_data_best['K_img2sas'].iloc[0]*self.fitting_thread.
                      I_img2sas, 'ro', fillstyle='none',label='img2sas fit')
            ax.legend(fontsize='small')
            ax.set_box_aspect(1)
            
            ax = self.plot_widget.add_subplot(122)
            from matplotlib import colors


            
            uniquevalues= np.unique(self.fitting_thread.im_Pt)
            uniquevalues=np.sort(uniquevalues)
            
            cmap = colors.ListedColormap(['white','black','red'])
            boundaries = [uniquevalues[0]-1,uniquevalues[0]+1,uniquevalues[1]+1,uniquevalues[2]+1]
            norm = colors.BoundaryNorm(boundaries, cmap.N, clip=True)
            #ax.imshow(self.fitting_thread.im_Pt[:,:,int(self.fitting_thread.boxsize/2)])
            ax.imshow(self.fitting_thread.im_Pt[:,:,int(self.fitting_thread.boxsize/2)],cmap=cmap,norm=norm,interpolation='nearest',extent=[0,self.fitting_thread.voxelsize*self.fitting_thread.boxsize,self.fitting_thread.voxelsize*self.fitting_thread.boxsize,0])
            ax.set_xlabel('[nm]',fontsize='small')
            ax.set_ylabel('[nm]',fontsize='small')
            ax.tick_params(axis='x', labelsize='small')
            ax.tick_params(axis='y', labelsize='small')
            ax.set_box_aspect(1)
            self.plot_widget.savefig("guifcsaxs_output.svg",dpi=300)

            # Redraw the plot
            self.plot_canvas.draw()

            #Print the fitting parameters
            self.print_bestfit()

    def linear_func(self, x, slope, intercept):
        return slope * x + intercept

    def print_parameters(self):
        # self.classnum=params['classnum']
        # self.boxsize=params['boxsize']
        # self.voxelsize=params['voxelsize']
        # self.porosity=params['porosity']
        # self.search_space=params['search_space']
        # self.timestr= time.strftime("%Y%m%d")
        # self.samplename=params['samplename']
        # Clear the parameter text edit
        self.parameter_textedit.clear()

        # Print the fitting parameters
        self.parameter_textedit.append("Fitting Parameters:")
        self.parameter_textedit.append(f"Class num: {self.params['classnum']}")
        self.parameter_textedit.append(f"Box size: {self.params['boxsize']}")
        self.parameter_textedit.append(f"Voxel size: {self.params['voxelsize']}")
        self.parameter_textedit.append(f"Porosity: {self.params['porosity']}")
        self.parameter_textedit.append(f"Search space: {self.params['search_space']}")  
        
    def print_bestfit(self):
        # self.classnum=params['classnum']
        # self.boxsize=params['boxsize']
        # self.voxelsize=params['voxelsize']
        # self.porosity=params['porosity']
        # self.search_space=params['search_space']
        # self.timestr= time.strftime("%Y%m%d")
        # self.samplename=params['samplename']
        # Clear the parameter text edit
        # self.parameter_textedit.clear()

        # Print the fitting parameters
        self.parameter_textedit.append("Best Fit:")
        self.parameter_textedit.append(f"{ self.fitting_thread.search_data_best.iloc[0, :2*self.fitting_thread.IBM_1.classB+1+10]}")
                         

    def store_parameters(self):
        # Store the parameters in a pickle file
        file_dialog = QFileDialog()
        file_dialog.setDefaultSuffix(".pkl")
        file_path, _ = file_dialog.getSaveFileName(self, "Save Fitting Parameters", "", "Pickle files (*.pkl)")
        
        legitpara_IBM=self.fitting_thread.search_data_fix[self.fitting_thread.search_data_fix.score==max(self.fitting_thread.search_data_fix.score)]
        best_score_IBM=max(self.fitting_thread.search_data_fix.score)
        best_para_IBM =   self.fitting_thread.search_data_best.iloc[0, :2*self.fitting_thread.IBM_1.classB+1+10]
        #best_para_IBM=fittingIBM['best_para']
        metadata ={}
        metadata['date']=self.fitting_thread.timestr
        metadata['im_Pt']=self.fitting_thread.im_Pt
        metadata['int_img2sas']=self.fitting_thread.I_img2sas
        metadata['q_img2sas']=self.fitting_thread.q_img2sas
        metadata['K_img2sas']=self.fitting_thread.search_data_best['K_img2sas'].iloc[0]
        metadata['I_exp']=self.fitting_thread.I
        metadata['q_exp']=self.fitting_thread.q
        metadata['best_score']=best_score_IBM
        metadata['best_para']=best_para_IBM
        metadata['voxelsize']=self.fitting_thread.IBM_1.onevoxel
        metadata['boxsize']=self.fitting_thread.IBM_1.boxsize
        metadata['optimizer'] = 'Evolution Strategy +TPE'
        metadata['comment'] = 'simulatenous'
        metadata['searchdata'] = self.fitting_thread.search_data_fix
        metadata['searchspace'] = self.fitting_thread.search_space
        metadata['samplename'] = self.fitting_thread.samplename
        if file_path:
            with open(file_path, "wb") as f:
                pickle.dump(metadata, f)

    def reset_program(self):
        # Clear the plot
        self.plot_widget.clear()
        self.plot_canvas.draw()

        # Clear the fitting parameters
        self.params.clear()

        # Clear the parameter text edit
        self.parameter_textedit.clear()

        # Hide the progress bar
        self.progress_bar.hide()

    def closeEvent(self, event):
        # Close the application and clear stored parameters
        self.params.clear()
        event.accept()


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    window = MainWindow()
    window.show()
    sys.exit(app.exec_())
