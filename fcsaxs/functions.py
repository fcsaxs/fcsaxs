#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 29 10:34:03 2021

@author: xu_l
"""

import numpy as np
import tifffile as tif
from scipy import ndimage
from lmfit.models import SkewedGaussianModel , LinearModel, Model, PowerLawModel
from scipy.optimize import curve_fit
from lmfit.models import ExponentialModel, GaussianModel, PolynomialModel
from scipy.stats import binned_statistic
import gc 
import scipy as sp
import time
import skimage

from scipy.interpolate import UnivariateSpline

import cupy as cp
import cupyx as cpx
from cupyx.scipy import ndimage as ndicu

def padding_or_not(pad_or_not):
    """

    Parameters
    ----------
    pad_or_not : 0 for not padding with mirror image of original image, 
    1 for padding, None for clearing the previously specified. 

    Returns
    -------
    will modify the img2sas padding setting accordingly to the whole code

    """
    if pad_or_not == None:
        if 'pad' in globals():
            del globals()['pad']
    else:
        global pad
        pad = pad_or_not
        
def windowing_or_not(window_or_not):
    """

    Parameters
    ----------
    window_or_not : 0 for not windowing, 
    1 for padding, None for clearing the previously specified. 

    Returns
    -------
    will modify the img2sas windowing setting accordingly to the whole code

    """
    if window_or_not == None:
        if 'window' in globals():
            del globals()['window']
    else:
        global window
        window = window_or_not
        
def cpu_or_not(cpu_or_gpu):
    """

    Parameters
    ----------
    cpu_or_gpu : 1 for cpu, 
    0 for gpu, None for clearing the previously specified. 

    Returns
    -------
    will modify the img2sas cpu/gpu setting accordingly to the whole code

    """
    if cpu_or_gpu == None:
        if 'cpugpu' in globals():
            del globals()['cpugpu']
    else:
        global cpugpu
        cpugpu=cpu_or_gpu

def img2sas(im_win, boxsize, onevoxel,paddefault=1,windowdefault=0,cpudefault=1,returnim=0):
    """
    Numerical scattering of a 3D-image (in cubic shape)

    Parameters
    ----------
    im_win : array-like
        The input image
    boxsize : int
        Box length of the 3D-image
    onevoxel : int or float32
        Size of each single voxel

    Returns
    -------
    Int: array-like
        The numerical scattering intensity array
    x: array-like
        The scattering vector array
    """
    #fourier transform the 3d spectra
    listofglobals = globals()
    
    if 'pad' in listofglobals.keys()  :
        pad = listofglobals['pad']
    else: 
        pad = paddefault
        
    if pad ==  1 :
        
        im_win = np.pad(im_win, int(boxsize/2), mode='reflect')
        boxsize = boxsize*2
    else :
        im_win = im_win
        
        
        
    if 'window' in listofglobals.keys()  :
        window = listofglobals['window']
    else: 
        window = windowdefault
        
    if window ==  1 :
        
        im_win *= window3d(sp.signal.windows.tukey(boxsize, alpha=0.05, sym=True))
    else :
        im_win = im_win
        
    print('shape of image = '+ str(im_win.shape[0])+', window_or_not '+str(window))
    
    if 'cpugpu' in listofglobals.keys()  :
        cpu_or_gpu = listofglobals['cpugpu']
    else: 
        cpu_or_gpu = cpudefault
    
    if cpu_or_gpu==1:
    
        # start = time.time()
        spectrum_3d = sp.fft.fftn(im_win,workers=15)
        # print(time.time()-start)
        # start = time.time()
        #shift the zero frequency to the center
        spectrum_3d= sp.fft.fftshift(spectrum_3d)
        # print(time.time()-start)
        #fourier space intensity 
        spectrum_3d = (spectrum_3d.real)**2 + (spectrum_3d.imag)**2
        
        #taking into account the shape of intensity 
        sx, sy, sz = spectrum_3d.shape
        
        #creating a grid based on that
        X, Y, Z = np.ogrid[0:sx, 0:sy, 0:sz]
        
        #converting X Y Z to r in polar coordinates
        r = (np.sqrt(((X - sx/2)**2)+ ((Y - sy/2)**2)+ ((Z - sz/2)**2)))
        
        s=boxsize/2
        
        rbin = np.around(s*np.sqrt(3)*(r)/(r.max()))
        rbin = rbin.astype(int)
        # start = time.time()
        #perform an average of intensity inside one bin and do it for all the bins
        radial_mean = ndimage.mean(spectrum_3d, labels=rbin, index=np.arange(1, s+1))
        
        #making x axis corresponding to the reciprocal of real space
        x=np.arange(2*np.pi/(onevoxel*boxsize),(np.pi/onevoxel)+(2*np.pi/(onevoxel*boxsize)),2*np.pi/(onevoxel*boxsize))
        
        radial_mean *=((np.sin(x*onevoxel/2)/(x*onevoxel/2))**2)
        
        Int = radial_mean*(onevoxel**6)
        
        gc.collect(generation=2)
        # print(time.time()-start)
        if pad ==  1 :
            Int/=8
        else :
            Int = Int
    else:
        im_win = cp.asarray(im_win)
        #fourier transform the 3d spectra
        im_win = cpx.scipy.fft.fftn(im_win)
     
        #shift the zero frequency to the center
        im_win= cpx.scipy.fft.fftshift(im_win) 
     
        #fourier space intensity 
        im_win = (im_win.real)**2 + (im_win.imag)**2
     
        #taking into account the shape of intensity 
        sx, sy, sz = im_win.shape
     
        #creating a grid based on that
        X, Y, Z = cp.ogrid[0:sx, 0:sy, 0:sz]
     
        #converting X Y Z to r in polar coordinates
        r = (cp.sqrt(((X - sx/2)**2)+ ((Y - sy/2)**2)+ ((Z - sz/2)**2)))
     
        s=boxsize/2
     
        rbin = cp.around(s*cp.sqrt(3)*(r)/(r.max()))
        rbin=rbin.astype(int)

        #perform an average of intensity inside one bin and do it for all the bins
        radial_mean = ndicu.mean(im_win, labels=rbin, index=cp.arange(1, s+1))
     
        #making x axis corresponding to the reciprocal of real space
        x=cp.arange(2*cp.pi/(onevoxel*boxsize),(cp.pi/onevoxel)+(2*cp.pi/(onevoxel*boxsize)),2*cp.pi/(onevoxel*boxsize))
     
        radial_mean *=((cp.sin(x*onevoxel/2)/(x*onevoxel/2))**2)
            
        Int = radial_mean*(onevoxel**6)
     
        Int = cp.asnumpy(Int)
        x = cp.asnumpy(x)
        # cache = cp.fft.config.get_plan_cache()
        # cache.clear()
        
    if returnim ==0 :
        return [Int,x]
    else: 
        return [Int,x,im_win]



def img2sas_oldfft(im_win, boxsize, onevoxel,paddefault=1):
    """
    Numerical scattering of a 3D-image (in cubic shape)

    Parameters
    ----------
    im_win : array-like
        The input image
    boxsize : int
        Box length of the 3D-image
    onevoxel : int or float32
        Size of each single voxel

    Returns
    -------
    Int: array-like
        The numerical scattering intensity array
    x: array-like
        The scattering vector array
    """
    #fourier transform the 3d spectra
    listofglobals = globals()
    
    if 'pad' in listofglobals.keys()  :
        pad = listofglobals['pad']
    else: 
        pad = paddefault
        
    if pad ==  1 :
        
        im_win = np.pad(im_win, int(boxsize/2), mode='reflect')
        boxsize = boxsize*2
    else :
        im_win = im_win
        
    print(im_win.shape[0])
    spectrum_3d = np.fft.fftn(im_win)

    #shift the zero frequency to the center
    spectrum_3d= np.fft.fftshift(spectrum_3d) 

    #fourier space intensity 
    spectrum_3d = (spectrum_3d.real)**2 + (spectrum_3d.imag)**2

    #taking into account the shape of intensity 
    sx, sy, sz = spectrum_3d.shape

    #creating a grid based on that
    X, Y, Z = np.ogrid[0:sx, 0:sy, 0:sz]

    #converting X Y Z to r in polar coordinates
    r = (np.sqrt(((X - sx/2)**2)+ ((Y - sy/2)**2)+ ((Z - sz/2)**2)))

    s=boxsize/2

    rbin = np.around(s*np.sqrt(3)*(r)/(r.max()))
    rbin = rbin.astype(int)

    #perform an average of intensity inside one bin and do it for all the bins
    radial_mean = ndimage.mean(spectrum_3d, labels=rbin, index=np.arange(1, s+1))

    #making x axis corresponding to the reciprocal of real space
    x=np.arange(2*np.pi/(onevoxel*boxsize),(np.pi/onevoxel)+(2*np.pi/(onevoxel*boxsize)),2*np.pi/(onevoxel*boxsize))
    
    radial_mean *=((np.sin(x*onevoxel/2)/(x*onevoxel/2))**2)
        
    Int = radial_mean*(onevoxel**6)
    gc.collect(generation=2)
    
    return [Int,x]



def img2sas_speedndimage(im_win, boxsize, onevoxel,paddefault=1):
    """
    Numerical scattering of a 3D-image (in cubic shape)

    Parameters
    ----------
    im_win : array-like
        The input image
    boxsize : int
        Box length of the 3D-image
    onevoxel : int or float32
        Size of each single voxel

    Returns
    -------
    Int: array-like
        The numerical scattering intensity array
    x: array-like
        The scattering vector array
    """
    #fourier transform the 3d spectra
    listofglobals = globals()
    
    if 'pad' in listofglobals.keys()  :
        pad = listofglobals['pad']
    else: 
        pad = paddefault
        
    if pad ==  1 :
        
        im_win = np.pad(im_win, int(boxsize/2), mode='reflect')
        boxsize = boxsize*2
    else :
        im_win = im_win
        
    print(im_win.shape[0])
    spectrum_3d = sp.fft.fftn(im_win,workers=10)

    #shift the zero frequency to the center
    spectrum_3d= np.fft.fftshift(spectrum_3d) 

    #fourier space intensity 
    spectrum_3d = (spectrum_3d.real)**2 + (spectrum_3d.imag)**2

    #taking into account the shape of intensity 
    sx, sy, sz = spectrum_3d.shape

    #creating a grid based on that
    X, Y, Z = np.ogrid[0:sx, 0:sy, 0:sz]

    #converting X Y Z to r in polar coordinates
    r = (np.sqrt(((X - sx/2)**2)+ ((Y - sy/2)**2)+ ((Z - sz/2)**2)))

    s=boxsize/2

    rbin = np.around(s*np.sqrt(3)*(r)/(r.max()))
    rbin=rbin.astype(int)

    #perform an average of intensity inside one bin and do it for all the bins
    
    radial_mean = ndimage.mean(spectrum_3d, labels=rbin, index=np.arange(1, s+1))
    
    # radial_mean=[]
    # props=skimage.measure.regionprops_table(rbin,spectrum_3d,properties=['label', 'intensity_mean'])
    # print(props['intensity_mean'])
    # # for i in np.arange(1, s+1):
    # #     i=i.astype(int)
    # #     radial_mean.append(props[i].intensity_mean)
        

    #making x axis corresponding to the reciprocal of real space
    x=np.arange(2*np.pi/(onevoxel*boxsize),(np.pi/onevoxel)+(2*np.pi/(onevoxel*boxsize)),2*np.pi/(onevoxel*boxsize))
    
    radial_mean *=((np.sin(x*onevoxel/2)/(x*onevoxel/2))**2)
        
    Int = radial_mean*(onevoxel**6)
    gc.collect(generation=2)
    if pad ==  1 :
        Int/=8
    else :
        Int = Int
    
    return [Int,x]





import cupy as cp
import cupyx as cpx
from cupyx.scipy import ndimage as ndicu
def img2sas_gpu(im_win, boxsize, onevoxel):
    """
    Numerical scattering of a 3D-image (in cubic shape)

    Parameters
    ----------
    im_win : array-like
        The input image
    boxsize : int
        Box length of the 3D-image
    onevoxel : int or float32
        Size of each single voxel

    Returns
    -------
    Int: array-like
        The numerical scattering intensity array
    x: array-like
        The scattering vector array
    """
 
    im_win = cp.asarray(im_win)
    #fourier transform the 3d spectra
    im_win = cpx.scipy.fft.fftn(im_win)
 
    #shift the zero frequency to the center
    im_win= cpx.scipy.fft.fftshift(im_win) 
 
    #fourier space intensity 
    im_win = (im_win.real)**2 + (im_win.imag)**2
 
    #taking into account the shape of intensity 
    sx, sy, sz = im_win.shape
 
    #creating a grid based on that
    X, Y, Z = cp.ogrid[0:sx, 0:sy, 0:sz]
 
    #converting X Y Z to r in polar coordinates
    r = (cp.sqrt(((X - sx/2)**2)+ ((Y - sy/2)**2)+ ((Z - sz/2)**2)))
 
    s=boxsize/2
 
    rbin = cp.around(s*cp.sqrt(3)*(r)/(r.max()))
    rbin=rbin.astype(int)
    onevoxel = 1
    #perform an average of intensity inside one bin and do it for all the bins
    radial_mean = ndicu.mean(im_win, labels=rbin, index=cp.arange(1, s+1))
 
    #making x axis corresponding to the reciprocal of real space
    x=cp.arange(2*cp.pi/(onevoxel*boxsize),(cp.pi/onevoxel)+(2*cp.pi/(onevoxel*boxsize)),2*cp.pi/(onevoxel*boxsize))
 
    radial_mean *=((cp.sin(x*onevoxel/2)/(x*onevoxel/2))**2)
        
    Int = radial_mean*(onevoxel**6)
 
    Int = cp.asnumpy(Int)
    x = cp.asnumpy(x)
    # cache = cp.fft.config.get_plan_cache()
    # cache.clear()
    return [Int,x]







def find_scalingfactor(expq,expI,analq,analI, qval=0.2):
    """
    Proportionally scale a scattering curve (analytical) to another scattering curve (experimental)

    Parameters
    ----------
    expq : 1D-array
        The scattering vector array of the experimental SAXS curve
    expI : 1D-array
        The scattering intensity array of the experimental SAXS curve
    analq : 1D-array
        The scattering vector array of the analytical SAXS curve.
    analI : 1D-array
        The scattering intensity array of the analytical SAXS curve.
    qval : float32
        The q-value at which the 2 scattering curves are scaled. The default is 0.2.

    Returns
    -------
    K1/K2 : float32
        The scaling factor by which the analytical intensity should be multiplied to fit the experimental curve

    """
    idx = (np.abs(expq - qval)).argmin()
    K1=expI[idx]
    
    idx = (np.abs(analq - qval)).argmin()
    K2=analI[idx]
    
    return K1/K2

def Iq_logslope(q, I):
    """
    Log-linear fitting of a SAXS curve (or a part of the curve)

    Parameters
    ----------
    q : 1D-array
        The scattering vector array of the SAXS curve
    I : 1D-array
        The scattering intensity array of the SAXS curve

    Returns
    -------
    logslope : float32
        The slope of the log-linear fitting
    intersection : float32
        The intersection of the log-linear fitting
    y_fit : 1D-array
        The intensity array by log-linear fitting

    """
    q_tofit = np.log(q)
    Int_tofit = np.log(I)
    
    # fit log(y) = m*log(x) + c
    logslope, intersection = np.polyfit(q_tofit, Int_tofit,1)   
    y_fit = np.exp(logslope*q_tofit + intersection)
    
    return logslope, intersection, y_fit
    
    
def export_tiff(filepath, im, **kwargs):
    im_tiff = im.astype('float32')
    imshape = np.shape(im_tiff)
    im_tiff.shape = 1,imshape[0],1,imshape[1],imshape[2],1
    
    tif.imwrite(filepath,im_tiff,imagej=True, resolution=(1, 1),
      metadata={'spacing': 1, 'unit': 'nm'})
    
def extrapolatesaxs(qexp,Iexp,guinier_towhich=5,porod_towhich=10,extraq_lowerbound=0.00001,extraq_upperbound=100):
    # Iexp -= Iexp[-1]
    qlowq= np.linspace(extraq_lowerbound,qexp[0],10000)
    qhighq= np.linspace(qexp[-1],extraq_upperbound,10000)
    setvaluea,setvalueb= np.polyfit((qexp[:guinier_towhich]**2),np.log(Iexp[:guinier_towhich]),1)
    def guinier(x,a,b):
        return a*np.exp(-1*(b**2)*(x**2)/3)
    mod = Model(guinier, prefix='guinier_')
    pars= mod.make_params()
    pars['guinier_a'].set(value=Iexp[0], min=0)
    pars['guinier_b'].set(value=np.sqrt(-3*setvaluea), min=0)
    out = mod.fit(Iexp[:guinier_towhich], pars, x=qexp[:guinier_towhich])
    params_dict=out.params.valuesdict()
    alowq = params_dict['guinier_a']
    blowq = params_dict['guinier_b']
    ylowq= alowq*np.exp(-1*(blowq**2)*(qlowq**2)/3)
    setvaluea,setvalueb= np.polyfit((qexp[-porod_towhich:-1]**-4),(Iexp[-porod_towhich:-1]),1)
    def porod(x,a):
        return a*(x**-4)
    mod = Model(porod, prefix='porod_')
    pars= mod.make_params()
    pars['porod_a'].set(value=setvaluea, min=0)
    out = mod.fit(Iexp[-porod_towhich:-1], pars, x=qexp[-porod_towhich:-1])
    params_dict=out.params.valuesdict()
    mhighq = params_dict['porod_a']
    yhighq = mhighq*(qhighq**-4)
    qextra = np.concatenate((qlowq,qexp,qhighq))
    Iextra = np.concatenate((ylowq,Iexp,yhighq))
    gc.collect(generation=2)
    return qextra, Iextra
    
   
def extrapolatesaxs_constantc(qexp,Iexp,guinier_towhich=5,porod_towhich=10,extraq_lowerbound=0.00001,extraq_upperbound=100):
    # Iexp -= Iexp[-1]
    qlowq= np.linspace(extraq_lowerbound,qexp[0],10000)
    qhighq= np.linspace(qexp[-1],extraq_upperbound,10000)

    setvaluea,setvalueb= np.polyfit((qexp[-porod_towhich:-1]**-4),(Iexp[-porod_towhich:-1]),1)
    def porod(x,a,c):
        return (a*(x**-4))+c
    mod = Model(porod, prefix='porod_')
    pars= mod.make_params()
    pars['porod_a'].set(value=setvaluea, min=0)
    pars['porod_c'].set(value=setvalueb, min=0)
    out = mod.fit(Iexp[-porod_towhich:-1], pars, x=qexp[-porod_towhich:-1])
    params_dict=out.params.valuesdict()
    mhighq = params_dict['porod_a']
    yhighq = mhighq*(qhighq**-4)
    setvaluea,setvalueb= np.polyfit((qexp[:guinier_towhich]**2),np.log(Iexp[:guinier_towhich]),1)
    def guinier(x,a,b):
        return a*np.exp(-1*(b**2)*(x**2)/3)
    mod = Model(guinier, prefix='guinier_')
    pars= mod.make_params()
    pars['guinier_a'].set(value=Iexp[0], min=0)
    pars['guinier_b'].set(value=np.sqrt(-3*setvaluea), min=0)
    I_subs = Iexp-params_dict['porod_c']
    out = mod.fit(I_subs[:guinier_towhich], pars, x=qexp[:guinier_towhich])
    params_dict=out.params.valuesdict()
    alowq = params_dict['guinier_a']
    blowq = params_dict['guinier_b']
    ylowq= alowq*np.exp(-1*(blowq**2)*(qlowq**2)/3)
    qextra = np.concatenate((qlowq,qexp,qhighq))
    Iextra = np.concatenate((ylowq,I_subs,yhighq))
    gc.collect(generation=2)
    return qextra, Iextra
   # imagej=True, resolution=(1, 1),    metadata={'spacing': 1, 'unit': 'nm'}
    
def minusbackground(qexp,Iexp,guinier_towhich=5,porod_towhich=10,extraq_lowerbound=0.00001,extraq_upperbound=100):
    # Iexp -= Iexp[-1]
    qlowq= np.linspace(extraq_lowerbound,qexp[0],10000)
    qhighq= np.linspace(qexp[-1],extraq_upperbound,10000)

    setvaluea,setvalueb= np.polyfit((qexp[-porod_towhich:-1]**-4),(Iexp[-porod_towhich:-1]),1)
    def porod(x,a,c):
        return (a*(x**-4))+c
    mod = Model(porod, prefix='porod_')
    pars= mod.make_params()
    pars['porod_a'].set(value=setvaluea, min=0)
    pars['porod_c'].set(value=setvalueb, min=0)
    out = mod.fit(Iexp[-porod_towhich:-1], pars, x=qexp[-porod_towhich:-1])
    params_dict=out.params.valuesdict()
    mhighq = params_dict['porod_a']
    yhighq = mhighq*(qhighq**-4)
    setvaluea,setvalueb= np.polyfit((qexp[:guinier_towhich]**2),np.log(Iexp[:guinier_towhich]),1)
    def guinier(x,a,b):
        return a*np.exp(-1*(b**2)*(x**2)/3)
    mod = Model(guinier, prefix='guinier_')
    pars= mod.make_params()
    pars['guinier_a'].set(value=Iexp[0], min=0)
    pars['guinier_b'].set(value=np.sqrt(-3*setvaluea), min=0)
    I_subs = Iexp-params_dict['porod_c']
    out = mod.fit(I_subs[:guinier_towhich], pars, x=qexp[:guinier_towhich])
    params_dict=out.params.valuesdict()
    alowq = params_dict['guinier_a']
    blowq = params_dict['guinier_b']
    ylowq= alowq*np.exp(-1*(blowq**2)*(qlowq**2)/3)
    qextra = np.concatenate((qlowq,qexp,qhighq))
    Iextra = np.concatenate((ylowq,I_subs,yhighq))
    gc.collect(generation=2)
    return qexp,I_subs

def window3d(w):
    L=w.shape[0]
    m1=np.outer(np.ravel(w), np.ravel(w))
    win1=np.tile(m1,np.hstack([L,1,1]))
    m2=np.outer(np.ravel(w),np.ones([1,L]))
    win2=np.tile(m2,np.hstack([L,1,1]))
    win2=np.transpose(win2,np.hstack([1,2,0]))
    win=np.multiply(win1,win2)
    return win


def binningq(qexp,Iexp,q_img2sas,I_img2sas):
    # # q_img2sas = np.where((q_img2sas<qexp[-1])&(q_img2sas>qexp[0]),q_img2sas,-1)
    # # I_img2sas = np.where(q_img2sas>0,I_img2sas,-1)
    # q_img2sas_filter = q_img2sas[(I_img2sas>0)&(q_img2sas>0)]
    # I_img2sas_filter = I_img2sas[(I_img2sas>0)&(q_img2sas>0)]

    
    # if qexp[-1] <= q_img2sas_filter[-1] :
    #     print(qexp[-1],q_img2sas_filter[-1])
    #     maxq= qexp[-1]
    # else:
    #     maxq= q_img2sas_filter[-1]
        
    # if qexp[0] >= q_img2sas_filter[0]:
    #     print(qexp[0],q_img2sas_filter[0])
    #     minq=qexp[0]
    # else:
    #     minq=q_img2sas_filter[0]
        
    
    # s=qexp
    # ys=Iexp
    # so, edges, _ = binned_statistic(s,ys, statistic='median', bins=np.linspace(0.01,maxq+1,1000))
    # d=edges[:-1]+np.diff(edges)/2

    # q_img2sas_filter 
    # ys = so
    # #so, edges, _ = binned_statistic(q_img2sas_filter,I_img2sas_filter, statistic='median', bins=q_img2sas_filter)
    # so, edges, _ = binned_statistic(q_img2sas_filter,I_img2sas_filter, statistic='median', bins=np.linspace(0.01,maxq+1,1000))
    # d=edges[:-1]+np.diff(edges)/2

    # q_img2sas_filter = d
    # I_img2sas_filter = so
    
    
    # mask = np.logical_and(~np.isnan(I_img2sas_filter), ~np.isnan(ys))
    
    # q_img2sas_filter_ = q_img2sas_filter[mask]
    # ys_ = ys[mask]
    # I_img2sas_filter_  = I_img2sas_filter[mask]
    # gc.collect(generation=2)
    

    # return q_img2sas_filter_,ys_,I_img2sas_filter_


    # q_img2sas = np.where((q_img2sas<qexp[-1])&(q_img2sas>qexp[0]),q_img2sas,-1)
    # I_img2sas = np.where(q_img2sas>0,I_img2sas,-1)
    q_img2sas_filter = q_img2sas[(I_img2sas>0)&(q_img2sas>0)]
    I_img2sas_filter = I_img2sas[(I_img2sas>0)&(q_img2sas>0)]
    
    if qexp[-1] <= q_img2sas_filter[-1] :
        print(qexp[-1],q_img2sas_filter[-1])
        maxq= qexp[-1]
    else:
        maxq= q_img2sas_filter[-1]
        
    if qexp[0] >= q_img2sas_filter[0]:
        print(qexp[0],q_img2sas_filter[0])
        minq=qexp[0]
    else:
        minq=q_img2sas_filter[0]
        
    
    
    
    s = UnivariateSpline(qexp, Iexp, s=0)
    ys = s(q_img2sas_filter[(q_img2sas_filter<=maxq)&(q_img2sas_filter>=minq)])
    
    s = UnivariateSpline(q_img2sas_filter, I_img2sas_filter , s=0)
    I_img2sas_filter= s(q_img2sas_filter[(q_img2sas_filter<=maxq)&(q_img2sas_filter>=minq)])
    
    # s=qexp
    # ys=Iexp
    # so, edges, _ = binned_statistic(s,ys, statistic='median', bins=np.logspace(np.log10(np.min(q_img2sas_filter)),np.log10(np.max(q_img2sas_filter)),200))
    # d=edges[:-1]+np.diff(edges)/2

    # q_img2sas_filter 
    # ys = so

    # so, edges, _ = binned_statistic(q_img2sas_filter,I_img2sas_filter, statistic='median', bins=np.logspace(np.log10(np.min(q_img2sas_filter)),np.log10(np.max(q_img2sas_filter)),200))
    # d=edges[:-1]+np.diff(edges)/2


    q_img2sas_filter=q_img2sas_filter[(q_img2sas_filter<=maxq)&(q_img2sas_filter>=minq)]
    
    mask = np.logical_and(~np.isnan(I_img2sas_filter), ~np.isnan(ys))
    
    q_img2sas_filter_ = q_img2sas_filter[mask]
    ys_ = ys[mask]
    I_img2sas_filter_  = I_img2sas_filter[mask]
    gc.collect(generation=2)
    
    # s = UnivariateSpline(qexp, Iexp, s=0)
    # ys_ = s(q_img2sas_filter_)
    
    # s= UnivariateSpline(q_img2sas,I_img2sas, s=0)
    # I_img2sas_filter_ = s(q_img2sas_filter_)
    
    
    

    return q_img2sas_filter_,ys_,I_img2sas_filter_


def binningq_forfitting(qexp,Iexp):

    s=qexp
    ys=Iexp
    so, edges, _ = binned_statistic(s,ys, statistic='mean', bins=500)
    d=edges[:-1]+np.diff(edges)/2


    ys = so
    gc.collect(generation=2)

    return d,ys


def spline(qexp,Iexp,q_img2sas,I_img2sas):

    # q_img2sas = np.where((q_img2sas<qexp[-1])&(q_img2sas>qexp[0]),q_img2sas,-1)
    # I_img2sas = np.where(q_img2sas>0,I_img2sas,-1)
    q_img2sas_filter = q_img2sas[(I_img2sas>0)&(q_img2sas>0)]
    I_img2sas_filter = I_img2sas[(I_img2sas>0)&(q_img2sas>0)]
    
    if qexp[-1] <= q_img2sas_filter[-1] :
        print(qexp[-1],q_img2sas_filter[-1])
        maxq= qexp[-1]
    else:
        maxq= q_img2sas_filter[-1]
        
    if qexp[0] >= q_img2sas_filter[0]:
        print(qexp[0],q_img2sas_filter[0])
        minq=qexp[0]
    else:
        minq=q_img2sas_filter[0]
        
    
    
    
    s = UnivariateSpline(qexp, Iexp, s=0)
    ys = s(q_img2sas_filter[(q_img2sas_filter<=maxq)&(q_img2sas_filter>=minq)])
    
    s = UnivariateSpline(q_img2sas_filter, I_img2sas_filter , s=0)
    I_img2sas_filter= s(q_img2sas_filter[(q_img2sas_filter<=maxq)&(q_img2sas_filter>=minq)])
    
    # s=qexp
    # ys=Iexp
    # so, edges, _ = binned_statistic(s,ys, statistic='median', bins=np.logspace(np.log10(np.min(q_img2sas_filter)),np.log10(np.max(q_img2sas_filter)),200))
    # d=edges[:-1]+np.diff(edges)/2

    # q_img2sas_filter 
    # ys = so

    # so, edges, _ = binned_statistic(q_img2sas_filter,I_img2sas_filter, statistic='median', bins=np.logspace(np.log10(np.min(q_img2sas_filter)),np.log10(np.max(q_img2sas_filter)),200))
    # d=edges[:-1]+np.diff(edges)/2


    q_img2sas_filter=q_img2sas_filter[(q_img2sas_filter<=maxq)&(q_img2sas_filter>=minq)]
    
    mask = np.logical_and(~np.isnan(I_img2sas_filter), ~np.isnan(ys))
    
    q_img2sas_filter_ = q_img2sas_filter[mask]
    ys_ = ys[mask]
    I_img2sas_filter_  = I_img2sas_filter[mask]
    gc.collect(generation=2)
    
    # s = UnivariateSpline(qexp, Iexp, s=0)
    # ys_ = s(q_img2sas_filter_)
    
    # s= UnivariateSpline(q_img2sas,I_img2sas, s=0)
    # I_img2sas_filter_ = s(q_img2sas_filter_)
    
    
    

    return q_img2sas_filter_,ys_,I_img2sas_filter_

def spline_more_points(qexp,Iexp,q_img2sas,I_img2sas):
    # q_img2sas = np.where((q_img2sas<qexp[-1])&(q_img2sas>qexp[0]),q_img2sas,-1)
    # I_img2sas = np.where(q_img2sas>0,I_img2sas,-1)
    q_img2sas_filter = q_img2sas[(I_img2sas>0)&(q_img2sas>0)]
    I_img2sas_filter = I_img2sas[(I_img2sas>0)&(q_img2sas>0)]
    s = UnivariateSpline(q_img2sas_filter , I_img2sas_filter, s=0)
    ys = s(qexp)
    
    # s=qexp
    # ys=Iexp
    # so, edges, _ = binned_statistic(s,ys, statistic='median', bins=np.logspace(np.log10(np.min(q_img2sas_filter)),np.log10(np.max(q_img2sas_filter)),200))
    # d=edges[:-1]+np.diff(edges)/2

    # q_img2sas_filter 
    # ys = so

    # so, edges, _ = binned_statistic(q_img2sas_filter,I_img2sas_filter, statistic='median', bins=np.logspace(np.log10(np.min(q_img2sas_filter)),np.log10(np.max(q_img2sas_filter)),200))
    # d=edges[:-1]+np.diff(edges)/2

    
    
    mask = np.logical_and(~np.isnan(Iexp), ~np.isnan(ys))
    
    q_img2sas_filter_ = qexp[mask]
    I_img2sas_filter_ = ys[mask]
    ys_ = Iexp[mask]
    gc.collect(generation=2)

    return q_img2sas_filter_,ys_,I_img2sas_filter_

def spline2(qexp,Iexp,q_img2sas,I_img2sas):
    # q_img2sas = np.where((q_img2sas<qexp[-1])&(q_img2sas>qexp[0]),q_img2sas,-1)
    # I_img2sas = np.where(q_img2sas>0,I_img2sas,-1)
    q_img2sas_filter = q_img2sas[(I_img2sas>0)&(q_img2sas>0)]
    I_img2sas_filter = I_img2sas[(I_img2sas>0)&(q_img2sas>0)]
    s = UnivariateSpline(  q_img2sas_filter , I_img2sas_filter , s=0)
    ys = s(qexp)
    
    # s=qexp
    # ys=Iexp
    # so, edges, _ = binned_statistic(s,ys, statistic='median', bins=np.logspace(np.log10(np.min(q_img2sas_filter)),np.log10(np.max(q_img2sas_filter)),200))
    # d=edges[:-1]+np.diff(edges)/2

    # q_img2sas_filter 
    # ys = so

    # so, edges, _ = binned_statistic(q_img2sas_filter,I_img2sas_filter, statistic='median', bins=np.logspace(np.log10(np.min(q_img2sas_filter)),np.log10(np.max(q_img2sas_filter)),200))
    # d=edges[:-1]+np.diff(edges)/2
    I_img2sas_filter = ys
    q_img2sas_filter = qexp

    
    
    mask = np.logical_and(~np.isnan(I_img2sas_filter), ~np.isnan(ys))
    
    q_img2sas_filter_ = q_img2sas_filter[mask]
    ys_ = Iexp[mask]
    I_img2sas_filter_  = I_img2sas_filter[mask]
    gc.collect(generation=2)

    return q_img2sas_filter_,ys_,I_img2sas_filter_

from porespy.filters import find_peaks, trim_saddle_points, trim_nearby_peaks, region_size
from skimage.segmentation import watershed
import copy
import scipy.ndimage as spim
import skimage 
import porespy as ps
from itertools import chain
def trim(im):
        im_forwatershed = copy.deepcopy(im)
        im_forwatershed[im_forwatershed>0]=1
        sigma = 0.4
        dt = spim.distance_transform_edt(input=1-im_forwatershed)
        dt = spim.gaussian_filter(input=dt, sigma=sigma)
        peaks = find_peaks(dt=dt)
        
        print('Initial number of peaks: ', spim.label(peaks)[1])
        peaks = trim_saddle_points(peaks=peaks, dt=dt)
        print('Peaks after trimming saddle points: ', spim.label(peaks)[1])
        peaks = trim_nearby_peaks(peaks=peaks, dt=dt)
        peaks, N = spim.label(peaks)
        print('Peaks after trimming nearby peaks: ', N)
        regions = skimage.segmentation.watershed(image=-dt, markers=peaks, connectivity=1, offset=None, mask=dt > 0, compactness=0, watershed_line=False)
        regions2 = region_size(regions)
        return regions2
    

def trim_localthickness(im):
    im_forwatershed = copy.deepcopy(im)
    im_forwatershed[im_forwatershed>0]=1
    # cc = chain(np.arange(0,5,0.1), np.arange(5,100,0.3),np.arange(100,300,0.5))
    cc = chain(np.linspace(0.5,10,19,endpoint=False),np.arange(10,50,5),np.arange(50,200,5),np.arange(200,400,50))
    cc=list(cc)
    print(cc)
    regions2=ps.filters.local_thickness(1-im_forwatershed, sizes=cc, mode='hybrid', divs=1)
    return regions2