#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug  9 09:40:18 2021

@author: kinanti_a
"""

import numpy as np 
import itertools
import matplotlib.pyplot as plt
import matplotlib
matplotlib.rcParams.update({'font.size':30})
import matplotlib.pyplot as plt
from skimage.data import astronaut
import skimage
import numpy as np
import scipy
from scipy import ndimage
import matplotlib.pyplot as plt
import matplotlib.pyplot as plt
import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import matplotlib.pyplot as plt
import time
import tifffile as tif
import math
import raster_geometry as ras
import porespy as ps
from scipy.signal import general_gaussian, tukey
import jscatter as js 
import itertools
from scipy.stats import binned_statistic
from lmfit.models import SkewedGaussianModel, LinearModel, Model, PowerLawModel
from scipy.optimize import curve_fit
from lmfit.models import ExponentialModel, GaussianModel, PolynomialModel

#experiment Pt/C

data = np.loadtxt('SAXSdataandpicklefiles/PtCdecane_dry_2.txt')
data = data[~np.isnan(data).any(axis=1)]
datawet = np.loadtxt('SAXSdataandpicklefiles/PtCdecane_wet_2.txt')
datawet = datawet[~np.isnan(datawet).any(axis=1)]


def removebackground(qexp, Iexp):
    """
    A function to remove background at high-q 

    Parameters
    ----------
    qexp : array
        q data of a SAXS profile to be extrapolated.
    Iexp : array
        Intensity data of a SAXS profile to be extrapolated.

    Returns
    -------
    qremove: array
        Extrapolated q, both high-q and low-q.
    Iremove: array
        Extrapolated intensity, both high-q and low-q.

    """
    setvaluea,setvalueb= np.polyfit((qexp[-10:-1]**-4),(Iexp[-10:-1]),1)
    # #porod function
    def porod(x,a,c):
        return a*(x**-4)+c
    #fitting the porod
    mod = Model(porod, prefix='porod_')
    pars= mod.make_params()
    pars['porod_a'].set(value=setvaluea, min=0)
    pars['porod_c'].set(value=setvalueb, min=0)
    out = mod.fit(Iexp[-10:-1], pars, x=qexp[-10:-1])
    params_dict=out.params.valuesdict()
    C = params_dict['porod_c']

    qremove =  qexp
    Iremove= Iexp-C
    return qremove,Iremove
#data cleaning
qexp = data[:-50, 0]
Iexp = data[:-50, 1]
Eexp = data[:-50, 2]
qexpwet = datawet[:-50, 0]
Iexpwet = datawet[:-50, 1]
Eexpwet = datawet[:-50, 2]
qexp = qexp[Iexp > 0.1]
qexpwet = qexpwet[Iexpwet > 0.1]

Eexp = Eexp[Iexp > 0.1]
Eexpwet = Eexpwet[Iexpwet > 0.1]
Iexp = Iexp[Iexp > 0.1]
Iexpwet = Iexpwet[Iexpwet > 0.1]

#remove constant background deviation from porod law a high-q
qexpwet,Iexpwet=removebackground(qexpwet,Iexpwet)
qexp,Iexp=removebackground(qexp,Iexp)

#%%
import intersected_Boolean_model 
import os

classnum = 8
boxsize = 512
voxelsize = 1 
porosity = 0.54

#make instance of IBM structure (class, simulation box size, voxel size, desired porosity, q exp, Int exp, scattering length density of solid)
IBM_1 = intersected_Boolean_model.IBM(classnum, boxsize, voxelsize, porosity, qexp, Iexp,SLD_solid=1.72252*10**(11))

#make a folder to store your result
timestr = time.strftime("%Y%m%d")
parentdir = 'SAXSdataandpicklefiles/'+timestr
directory = parentdir+'goodanalyticalfit/'
if os.path.isdir(directory)==False:
    os.mkdir(directory)
    
#make instance of parameters to search for IBM
search_space = {"radius1" : np.arange(1, 25, 1),
                "radius2" : np.arange(10, 50, 1),
                "radius3" : np.arange(25, 75, 1),
                "radius4" : np.arange(30, 100, 1),
                "radius5" : np.arange(100, 125, 1),
                "radius6" : np.arange(110, 150, 1),
                "radius7" : np.arange(140, 175, 1),
                "radius8" : np.arange(150, 200, 1),
                "volfrac1" : np.arange(0, 0.1, 0.001),
                "volfrac2": np.arange(0, 0.1, 0.001),
                "volfrac3": np.arange(0, 0.1, 0.001),
                "volfrac4": np.arange(0, 0.1, 0.001),
                "volfrac5" : np.arange(0, 0.1, 0.001),
                "volfrac6": np.arange(0, 0.1, 0.001),
                "volfrac7": np.arange(0, 0.1, 0.001),
                "volfrac8": np.arange(0, 0.1, 0.001),
                "scalingfactor": np.logspace(23.5,24,1000)[415]}

#codes to make the search space in a suitable format for optimizer to work
aaa=list(search_space.values())
bbb=list(search_space.keys())
i=0
for key in bbb:
    if key =="scalingfactor":
        search_space[key]=[aaa[i]]
    else:
        search_space[key]=list(aaa[i])
    i+=1
    
#do you have previous fit?
previousfit = {}

#optimization occurs
IBM_1.optimization_forPtC(search_space,n_iter=2000,**previousfit)

#save as variable the search data
search_data_fix = IBM_1.search_data

#%%
SLD_solid=1.72252*10**(11)

##to search what is available after fitting IBM_1.__dict__
q_anl = IBM_1.q_anal
I_anl = IBM_1.I_anal

## make structure with periodic boundary condition
im = IBM_1.makeimg_2phase_periodic(IBM_1.opt_para)

##make structure stochastic without periodicity over the boundary
#im2 = IBM_1.makeimg_2phase(IBM_1.opt_para)

im_carbon =IBM_1.image_3d
im_carbon[im_carbon==1]=SLD_solid

plt.figure(figsize=[5,5],dpi=300)
plt.imshow(im_carbon[:,:,200])
plt.show()

#%%
import functions

I_img2sas,q_img2sas=functions.img2sas(im_carbon, IBM_1.boxsize, IBM_1.onevoxel ,paddefault=0)
q_binned, I_exp_binned, I_img2sas_binned=functions.binningq(qexp, Iexp, q_img2sas, I_img2sas)
q_binned, I_anal_binned, I_img2sas_binned=functions.binningq(IBM_1.q_anal,IBM_1.I_anal, q_img2sas, I_img2sas)
K_img2sas = (np.sum(I_anal_binned)/len(I_anal_binned))/(np.sum( I_img2sas_binned)/len( I_img2sas_binned))
#%%

plt.figure(figsize=[5,5],dpi=300)
plt.loglog(q_img2sas,I_img2sas*K_img2sas,'bo',fillstyle='none')
plt.loglog(IBM_1.q_anal,IBM_1.I_anal,'-')
#plt.xlim([10**-1.8,4])
plt.xlabel('q [1/nm]')
plt.ylabel('Intensity [-]')
plt.show()

#%%
import pickle
import matplotlib as mpl
mpl.rcParams.update(mpl.rcParamsDefault)

K_img2sas = sum(I_anal_binned[:])/sum(I_img2sas_binned[:])

    
def realspace(q):
    return 2*np.pi/q

def reciprocal(q):
    return 2*np.pi/q
    
fig=plt.figure(figsize=[5,5],dpi=300)
ax1 = fig.add_subplot(111)
ax2 = ax1.secondary_xaxis(location='top',functions= (reciprocal,realspace))    
ax1.loglog(q_binned,I_img2sas_binned*K_img2sas/I_exp_binned,'o',label='img2sas')
ax1.loglog(q_binned, I_anal_binned/I_exp_binned,'o',label='analytical')
ax1.loglog(q_binned, I_exp_binned/I_exp_binned,'-',label='exp')
ax1.set_yscale('linear')
ax1.set_xlabel('q [1/nm]')
ax1.set_ylabel('Relative Intensity')
ax2.set_xlabel('Dimension [nm]')
ax1.set_ylim([0,2])
plt.legend()
plt.savefig(directory+'/fittingIBMresult_relintprofile.svg',dpi=300,transparent=False)

fig=plt.figure(figsize=[5,5],dpi=300)
ax1 = fig.add_subplot(111)
ax2 = ax1.secondary_xaxis(location='top',functions= (reciprocal,realspace))   
ax1.loglog(q_binned,I_img2sas_binned*K_img2sas,'o',label='img2sas')
ax1.loglog(q_binned, I_anal_binned,'o',label='analytical')
ax1.loglog(q_binned, I_exp_binned,'-',label='exp')
#ax1.set_ylim([10**2,10**9])
ax1.set_xlabel('q [1/nm]')
ax1.set_ylabel('Intensity [arb. u.]')
ax2.set_xlabel('Dimension [nm]')
plt.legend()
plt.savefig(directory+'/fittingIBMresult_Iqprofile.svg',dpi=300,transparent=False)


legitpara_IBM=search_data_fix[search_data_fix.score==max(search_data_fix.score)]
best_score_IBM=max(search_data_fix.score)
best_para_IBM = IBM_1.opt_para   
#best_para_IBM=fittingIBM['best_para']
metadata ={}
metadata['date']=timestr
metadata['im_carbon']=im_carbon
metadata['int_img2sas_carbon']=I_img2sas_binned
metadata['int_analytical_carbon']=I_anal_binned
metadata['int_exp_carbon']=I_exp_binned
metadata['K_img2sas']=K_img2sas
metadata['q_carbon']=q_binned
metadata['best_score']=best_score_IBM
metadata['best_para']=best_para_IBM
metadata['voxelsize']=IBM_1.onevoxel
metadata['boxsize']=IBM_1.boxsize
metadata['optimizer'] = 'Evolution Strategy'
metadata['comment'] = 'looong carbon fitting iterations'
metadata['searchdata'] = search_data_fix

with open(directory+'fittingIBM_512_periodic.pkl', 'wb+') as f:
    pickle.dump(metadata, f)
plt.show()

#%%

from puttingPtonCI import puttingPt
import pickle
import functions
directory='/mnt/nas_Uwrite/AK54/KinanSAXSfiles/Kinantifffromporespy/fcsaxs_versioncontrol/fcsaxs-master/fcsaxs/SAXSdataandpicklefiles/20230103goodanalyticalfit/'
with open(directory+'fittingIBM_512_periodic.pkl', 'rb') as f:
    fittingIBM= pickle.load(f)
im_carbon  = fittingIBM['im_carbon']
K_img2sas = fittingIBM['K_img2sas']
SLD_solid = 1.72522*(10**11)

functions.padding_or_not(0)

IBM_Pt= puttingPt(im_carbon,8,512,1,0.54, qexp, Iexp, K_img2sas,SLD_solid)


search_space = {"radiuspt_1" : [2],
                "vpt_1" : np.linspace(0.0,0.020,num=51),
                "radiuspt_2" : [3],
                "vpt_2" : np.linspace(0.0,0.002,num=20),
                "radiuspt_3" : [4],
                "vpt_3" : np.linspace(0.0,0.002,num=20),
                "radiuspt_4" : [2],
                "vpt_4" : np.linspace(0.0,0.02,num=51),
                "threshold" : np.arange(0,40,4),
                "ionomerthickness": [0]
                }

aaa=list(search_space.values())

bbb=list(search_space.keys())

i=0
for key in bbb:
    search_space[key]=list(aaa[i])
    i+=1
    
search_data_IBMPt_fix=[]
best_para_Pt_=[]  




previousfit = {}
IBM_Pt.optimization_puttingPt_decane(qexpwet,Iexpwet,search_space,n_iter=600,**previousfit)

import pandas as pd
#%%


search_data_IBMPt_fix.append(IBM_Pt.search_data)
best_para_Pt_.append(IBM_Pt.opt_para)

q_img2sas_filter, ys, I_img2sas_filter, K_img2sas, im_Pt = IBM_Pt.storeintensity_puttingPt(best_para_Pt_[-1])
plt.loglog(q_img2sas_filter, ys/ys,q_img2sas_filter,I_img2sas_filter*K_img2sas/ys)

plt.yscale('linear')
plt.ylim([0,2])

#search_data_IBMPt_fix= pd.concat([search_data_IBMPt_fix, IBM_Pt.search_data], ignore_index=True, sort=False)

legitpara=search_data_IBMPt_fix[-1][search_data_IBMPt_fix[-1].score==max(search_data_IBMPt_fix[-1].score)]

#%%%
directoryfittingPt= directory

if os.path.isdir(directoryfittingPt)==False:
    os.mkdir(directoryfittingPt)
fig=plt.figure(figsize=[5,5],dpi=300)
ax1 = fig.add_subplot(111)
ax2 = ax1.secondary_xaxis(location='top',functions= (reciprocal,realspace))  
ax1.loglog(legitpara.iloc[0,-2],legitpara.iloc[0,-4]*legitpara.iloc[0,-3]/legitpara.iloc[0,-6],'o',markersize= 3,label= 'img2sas/exp decane')
ax1.loglog(legitpara.iloc[0,-2],legitpara.iloc[0,-5]/legitpara.iloc[0,-5],'red',linewidth=10,label= 'exp/exp')
ax1.loglog(legitpara.iloc[0,-2],legitpara.iloc[0,-5]*legitpara.iloc[0,-3]/legitpara.iloc[0,-7],'o',markersize= 3,label= 'img2sas/exp dry')
ax1.set_yscale('linear')
ax1.set_xlabel('q [1/nm]')
ax1.set_ylabel('Relative Intensity')
ax2.set_xlabel('Dimension [nm]')
ax1.set_ylim([0,2])
plt.legend()
plt.savefig(directoryfittingPt+'/fittingPtresult_relintprofile_2_nopad512.svg',dpi=300,transparent=False)


fig=plt.figure(figsize=[5,5],dpi=300)
ax1.set_title('Best IBM')
ax1 = fig.add_subplot(111)
ax2 = ax1.secondary_xaxis(location='top',functions= (reciprocal,realspace)) 
ax1.loglog(legitpara.iloc[0,-2],legitpara.iloc[0,-4]*legitpara.iloc[0,-3],'o',markersize= 3,label= 'img2sas decane')
ax1.loglog(legitpara.iloc[0,-2],legitpara.iloc[0,-5]*legitpara.iloc[0,-3],'o',markersize= 3,label= 'img2sas dry')
ax1.loglog(legitpara.iloc[0,-2],legitpara.iloc[0,-6],'red',linewidth=1,label= 'exp decane')
ax1.loglog(legitpara.iloc[0,-2],legitpara.iloc[0,-7],'black',linewidth=1,label= 'exp dry')
ax1.set_ylim([10**2,10**8])
ax1.set_xlabel('q [1/nm]')
ax1.set_ylabel('Intensity [arb. u.]')
ax2.set_xlabel('Dimension [nm]')
plt.legend()
plt.savefig(directoryfittingPt+'/fittingPtresult_Iqprofile_2_nopad512.svg',dpi=300,transparent=False)
   
best_score_Pt=max(search_data_IBMPt_fix[-1].score)
best_para_Pt = legitpara.iloc[0,:10]
metadata ={}
metadata['date']=timestr
metadata['im_Pt']=im_Pt
metadata['int_img2sas_Pt']=legitpara.iloc[0,-5]
metadata['int_exp_Pt']=legitpara.iloc[0,-6]
metadata['K_img2sas']=K_img2sas
metadata['q_Pt']=legitpara.iloc[0,-2]
metadata['best_score']=best_score_Pt
metadata['best_para']=best_para_Pt[-1]
metadata['voxelsize']=IBM_Pt.onevoxel
metadata['boxsize']=IBM_Pt.boxsize
metadata['optimizer'] = 'ES'
metadata['comment'] = 'Best IBM'
metadata['searchdata'] = search_data_IBMPt_fix[-1]

with open(directoryfittingPt+'fittingPt_nopadding_verygoodanalytical_periodic_512.pkl', 'wb+') as f:
    pickle.dump(metadata, f)

   
    

