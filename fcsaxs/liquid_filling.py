#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 29 15:50:00 2021

@author: xu_l
"""

import numpy as np
import matplotlib.pyplot as plt
import porespy as ps
from matplotlib import pylab
from sklearn import metrics
from porespy.filters import find_peaks, trim_saddle_points, trim_nearby_peaks, region_size
from skimage.segmentation import watershed
from porespy.tools import randomize_colors
import matplotlib.colors as cl
import scipy.ndimage as spim
import copy 
from functions import img2sas
import tifffile as tiff
from skimage.morphology import disk, ball
import functions
from hyperactive import Hyperactive
from natsort import natsorted, ns
from scipy.stats import binned_statistic
import skimage
import natsort
from functions import export_tiff
import gc
from itertools import chain
from scipy.interpolate import UnivariateSpline
from scipy.interpolate import UnivariateSpline
from scipy.stats import binned_statistic
from scipy.signal import savgol_filter
import gc
from scipy.interpolate import make_interp_spline
from statsmodels.nonparametric.kernel_regression import KernelReg
from hyperactive.optimizers import EvolutionStrategyOptimizer, TreeStructuredParzenEstimators
import scipy 
import skfmm
import datetime

def invariant(q,Int):
    """
    Calculating invariant of a given scattering curve

    Parameters
    ----------
    q : 1D-array
        The scattering vector array of the SAXS curve
    Int : 1D-array
        The scattering intensity array of the SAXS curve

    Returns
    -------
    Inv : float32
        The invariant value of the scattering curve

    """
    Inv = metrics.auc(q, Int*(q**2))
    return Inv


def liquid_vfraction(SLD_A,SLD_B,SLD_C,volfrac_A,dryinv, wetinv):
    """
    Calculating liquid volume fraction in a 3-phase structure containing solid, pores(void), and liquid, using invariants of the dry and wet structure

    Parameters
    ----------
    SLD_A : float32
        Scattering length density of the solid phase
    SLD_B : float32
        Scattering length density of the void phase (usually taken as 0)
    SLD_C : float32
        Scattering length density of the liquid phase
    volfrac_A : float32
        The solid phase volume fraction
    dryinv : float32
        The invariant of the dry structure (only solid and void phases, no liquid)
    wetinv : float32
        The invariant of the wet structure (3-phase structure with liquid)

    Returns
    -------
    Volfrac1 & Volfrac2: float32
        Two possible solution of the quadratic equation. The more reasonable one of them should be taken.

    Volfrac3 : float32
        Porosity of the dry structure

    """
    shiftingfactor = (dryinv)/(2*(np.pi**2))/((SLD_A**2)*volfrac_A*(1-volfrac_A))
    K=shiftingfactor
    Inv_Wet = wetinv
    A = (SLD_A-SLD_B)*(SLD_A-SLD_C)
    B = (SLD_B-SLD_A)*(SLD_B-SLD_C)
    C = (SLD_C-SLD_A)*(SLD_C-SLD_B)
    
    a = -C-B
    b = (1-2*volfrac_A)*B+C
    c = (A+B)*(volfrac_A-volfrac_A**2) - Inv_Wet/((2*(np.pi**2)*K))
    
    Volfrac1 = ((-b-(((b**2)-(4*a*c))**(1/2)))/(2*a))
    Volfrac2 = ((-b+(((b**2)-(4*a*c))**(1/2)))/(2*a))
    Volfrac3 = 1-volfrac_A
    return Volfrac1, Volfrac2, Volfrac3

def filling_degree(I_dry, I_wet, SLD_s, SLD_l):
    """
    Calculating q-dependent filling degree from scattering curve of the dry and wet structures

    Parameters
    ----------
    I_dry : 1D-array
        The scattering intensity array of the scattering curve of the dry structure.
    I_wet : 1D-array
        The scattering intensity array of the scattering curve of the wet structure.
    SLD_s : float32
        Scattering length density of the solid phase.
    SLD_l : float32
        Scattering length density of the liquid phase.

    Returns
    -------
    pq : TYPE
        DESCRIPTION.

    """
    pq = SLD_s/SLD_l*(1-(I_wet/I_dry)**(1/2))
    return pq

class filling():
    def __init__(self, im, boxsize, onevoxel):
            self.im = im
            self.boxsize = boxsize
            self.onevoxel = onevoxel
    

    
    def trim(self,im):
        im_forwatershed = copy.deepcopy(im)
        im_forwatershed[im_forwatershed>0]=1
        sigma = 0.2
        dt = spim.distance_transform_edt(input=1-im_forwatershed)
        dt = spim.gaussian_filter(input=dt, sigma=sigma)
        peaks = find_peaks(dt=dt)
        
        print('Initial number of peaks: ', spim.label(peaks)[1])
        peaks = trim_saddle_points(peaks=peaks, dt=dt)
        print('Peaks after trimming saddle points: ', spim.label(peaks)[1])
        peaks = trim_nearby_peaks(peaks=peaks, dt=dt)
        peaks, N = spim.label(peaks)
        print('Peaks after trimming nearby peaks: ', N)
        regions = skimage.segmentation.watershed(image=-dt, markers=peaks, connectivity=1, offset=None, mask=dt > 0, compactness=0, watershed_line=False)
        regions2 = region_size(regions)
        self.watershed = regions2
        
    def trim_localthickness(self,im):
        im_forwatershed = copy.deepcopy(im)
        im_forwatershed[im_forwatershed>0]=1
        cc = chain(np.linspace(0,5,20), np.linspace(5,100,200),np.linspace(100,300,400))
        cc=list(cc)
        print(cc)
        regions2=ps.filters.local_thickness(1-im_forwatershed, sizes=cc, mode='hybrid', divs=1)
        self.watershed = regions2
        
    def coloringpartialwater_largeporesfirst(self,array,boxsize,clustersizethreshold): 
        regions2 = array
        #regions2 = region_size(array)
        regions3 = copy.deepcopy(regions2)

        regions4 =  copy.deepcopy(regions2)
        regions4[(regions2>=clustersizethreshold)&(0<regions2)] = 5
        regions4[(regions2<clustersizethreshold)&(0<regions2)] = 15

        return regions4, regions3 
    
    def largeporefirst_fill(self, filling_poreradii, SLD_liquid=9.447*(10**10),SLD_solid=17.2522*(10**10),exporttiff=False,**kwargs):
        im_dry = copy.deepcopy(self.im)
        Idry, qdry=img2sas(im_dry,self.boxsize,self.onevoxel)
        radialmean_collection_largepores=[Idry]
        im_collection=[self.im]
        saturationlevel=[0]
        
        for pore_radii in filling_poreradii:  
            #vol_pore = 4/3*np.pi*((pore_radii/self.onevoxel)**3)
            vol_pore = (pore_radii/self.onevoxel)
            
            im2, im3 =self.coloringpartialwater_largeporesfirst(self.watershed, self.boxsize, vol_pore)
             
            im_sld_nowater = copy.deepcopy(self.im)
            
            
            im_sld_withwater= copy.deepcopy(im2)
            
            im_sld_withwater = im_sld_withwater.astype('float32')

            im_sld_withwater[im_sld_withwater==0]= 0

            im_sld_withwater[im_sld_withwater==5]= SLD_liquid

            im_sld_withwater[im_sld_withwater==15] = 0 

            sat = np.count_nonzero(im_sld_withwater)/(im_sld_withwater.shape[0]**3)
            por = 1-(np.count_nonzero(im_sld_nowater)/(im_sld_nowater.shape[0]**3))
            im_sld_withwater= im_sld_withwater + im_sld_nowater
            im_sld_withwater[(im_sld_withwater > SLD_solid)&(im_sld_withwater < 125*(10**10))] = SLD_solid
            im_sld_withwater[im_sld_withwater >= 126*(10**10)] = 126*(10**10)
        
            Ilarge,qlarge=img2sas(im_sld_withwater,self.boxsize,self.onevoxel)

            radialmean_collection_largepores.append(Ilarge)
            # im_collection.append(im_sld_withwater)
            print("poreradii is "+ str(pore_radii) + " saturation level is "+ str(sat/por))
            saturationlevel.append(sat/por)
            dateTime=datetime.datetime.today()
            ts =dateTime.isoformat()
            if exporttiff == True:
                export_tiff(str(ts)+"largeporesfirst_"+"radii"+str(pore_radii)+".tif", im_sld_withwater)
                
        self.qdry = qdry
        self.Int_collection_largeporesfirst = radialmean_collection_largepores
        # self.im_collection_largeporesfirst = im_collection
        self.saturationlevel = saturationlevel
        
        
    def coloringpartialwater_smallporesfirst(self,array,boxsize,clustersizethreshold): 
        regions2 = array
        #regions2 = region_size(array)
        regions3 = copy.deepcopy(regions2)

        regions4 =  copy.deepcopy(regions2)
        regions4[(regions2<=clustersizethreshold)&(0<regions2)] = 5
        regions4[(regions2>clustersizethreshold)&(0<regions2)] = 15
        return regions4, regions3
    
    def smallporefirst_fill(self, filling_poreradii, SLD_liquid=9.447*(10**10),SLD_solid=17.2522*(10**10),exporttiff=False,**kwargs):
        im_dry = copy.deepcopy(self.im)
        Idry, qdry=img2sas(im_dry,self.boxsize,self.onevoxel)
        radialmean_collection_smallpores=[Idry]
        im_collection=[self.im]
        saturationlevel=[0]
        
        for pore_radii in filling_poreradii:  
            #vol_pore = 4/3*np.pi*((pore_radii/self.onevoxel)**3)
            vol_pore = (pore_radii/self.onevoxel)
            print(vol_pore)
            im2, im3 =self.coloringpartialwater_smallporesfirst(self.watershed, self.boxsize, vol_pore)
             
            im_sld_nowater = copy.deepcopy(self.im)
            
            
            im_sld_withwater= copy.deepcopy(im2)
            
            im_sld_withwater = im_sld_withwater.astype('float32')

            im_sld_withwater[im_sld_withwater==0]= 0

            im_sld_withwater[im_sld_withwater==5]= SLD_liquid

            im_sld_withwater[im_sld_withwater==15] = 0 

            sat = np.count_nonzero(im_sld_withwater)/(im_sld_withwater.shape[0]**3)
            im_sld_withwater= im_sld_withwater + im_sld_nowater
            im_sld_withwater[(im_sld_withwater > SLD_solid)&(im_sld_withwater < 125*(10**10))] = SLD_solid
            im_sld_withwater[im_sld_withwater >= 126*(10**10)] = 126*(10**10)
        
            Ismall,qsmall=img2sas(im_sld_withwater,self.boxsize,self.onevoxel)
            por = 1-(np.count_nonzero(im_sld_nowater)/(im_sld_nowater.shape[0]**3))
            radialmean_collection_smallpores.append(Ismall)
            # im_collection.append(im_sld_withwater)
            print("poreradii is "+ str(pore_radii) + " saturation level is "+ str(sat/por))
            saturationlevel.append(sat/por)
            dateTime=datetime.datetime.today()
            ts =dateTime.isoformat()
            if exporttiff == True:
                export_tiff(str(ts)+"smallporesfirst_"+"radii"+str(pore_radii)+".tif", im_sld_withwater)
                
        self.qdry = qdry
        self.Int_collection_smallporesfirst = radialmean_collection_smallpores
        # self.im_collection_smallporesfirst = im_collection
        self.saturationlevel_smallporesfirst = saturationlevel
    
    def thinfilm_fill(self, film_thickness, SLD_liquid=9.447*(10**10),SLD_solid=17.2522*(10**10),exporttiff=False,**kwargs):
        im_dry = copy.deepcopy(self.im)
        Idry, qdry=img2sas(im_dry,self.boxsize,self.onevoxel)
        radialmean_collection_thinfilm = [Idry]
        im_collection=[self.im]
        base_for_dilation = copy.deepcopy(self.im)
        base_for_dilation[base_for_dilation>0]=1
        
        im_sld_nowater=copy.deepcopy(self.im)
        saturationlevel=[0]
        for n in film_thickness:
            dilated = spim.morphology.binary_dilation(base_for_dilation, iterations=n).astype('float32')
            waterthinfilm = dilated - base_for_dilation  
            waterthinfilm[waterthinfilm > 0]= SLD_liquid
            CL_carbon_Pt_thinfilmcondensate = im_sld_nowater + waterthinfilm
            CL_carbon_Pt_thinfilmcondensate[CL_carbon_Pt_thinfilmcondensate > 126*(10**10)] = 126*(10**10)
            CL_carbon_Pt_thinfilmcondensate[(CL_carbon_Pt_thinfilmcondensate < 125*(10**10))&(CL_carbon_Pt_thinfilmcondensate > SLD_solid)] = SLD_solid
            #ax1.loglog(x[x<=1], radial_mean[x<=1]/(10**27),x2[x2<=1], radial_mean2[x2<=1]/(10**27), 'r-')
            radial_mean2, q = img2sas(CL_carbon_Pt_thinfilmcondensate, self.boxsize, self.onevoxel )
            radialmean_collection_thinfilm.append(radial_mean2)           
            # im_collection.append(CL_carbon_Pt_thinfilmcondensate)
            saturationlevel.append(waterthinfilm[waterthinfilm==SLD_liquid].shape[0]/(base_for_dilation[base_for_dilation==0].shape[0]))
            print("thin film thickness is "+ str(n) + " saturation level is "+ str(waterthinfilm[waterthinfilm==SLD_liquid].shape[0]/(base_for_dilation[base_for_dilation==0].shape[0])))
            dateTime=datetime.datetime.today()
            ts =dateTime.isoformat()
            if exporttiff == True:
                export_tiff(str(ts)+"thinfilm_"+"thickness"+str(n)+".tif", CL_carbon_Pt_thinfilmcondensate)
          
        self.qdry = qdry
        self.Int_collection_thinfilm = radialmean_collection_thinfilm
        # self.im_collection_thinfilm = im_collection
        self.saturationlevelthinfilm = saturationlevel
        
class fillingoperandofitting(object):
    
    def __init__(self, im_Pt,boxsize, onevoxel, qdry_exp,Idry_exp,qwet_exp,Iwet_exp,K_img2sas,SLD_solid):
            self.im_Pt = im_Pt
            self.boxsize = boxsize
            self.onevoxel = onevoxel
            CL_carbon_Pt = im_Pt
            im_sld_nowater= CL_carbon_Pt
            I_img2sas_dry,q_img2sas_dry=img2sas(im_sld_nowater,self.boxsize,self.onevoxel)
            self.K_img2sas = K_img2sas
            q_img2sas_filterdry, Idryspline,I_img2sas_dryfilter=functions.spline(qdry_exp,Idry_exp,q_img2sas_dry,I_img2sas_dry)
            self.I_img2sas_drynotbinned = I_img2sas_dry
            self.q = q_img2sas_filterdry
            self.I_img2sas_dry = I_img2sas_dryfilter
            self.I_exp_dry = Idryspline
            self.q_exp_wet = qwet_exp
            self.I_exp_wet = Iwet_exp
            self.SLD_solid = SLD_solid
            gc.collect(generation=2)
            
    def trim(self,im):
            im_forwatershed = copy.deepcopy(im)
            im_forwatershed[im_forwatershed>0]=1
            sigma = 0.2
            dt = spim.distance_transform_edt(input=1-im_forwatershed)
            dt = spim.gaussian_filter(input=dt, sigma=sigma)
            peaks = find_peaks(dt=dt)
            
            print('Initial number of peaks: ', spim.label(peaks)[1])
            peaks = trim_saddle_points(peaks=peaks, dt=dt)
            print('Peaks after trimming saddle points: ', spim.label(peaks)[1])
            peaks = trim_nearby_peaks(peaks=peaks, dt=dt)
            peaks, N = spim.label(peaks)
            print('Peaks after trimming nearby peaks: ', N)
            regions = skimage.segmentation.watershed(image=-dt, markers=peaks, connectivity=1, offset=None, mask=dt > 0, compactness=0, watershed_line=False)
            regions2 = region_size(regions)
            self.watershed = regions2
            
            
    def trim_localthickness(self,im):
        im_forwatershed = copy.deepcopy(im)
        im_forwatershed[im_forwatershed>0]=1
        cc = chain(np.linspace(0,5,20), np.linspace(5,100,200),np.linspace(100,300,400))
        cc=chain(np.arange(70,50,-1),np.arange(50,10,-5),np.linspace(10,0,21,endpoint=True))
        cc=list(cc)
       
        regions2=ps.filters.local_thickness(1-im_forwatershed, sizes=cc, mode='hybrid', divs=1)
        self.watershed = regions2

# def splining(self,qexp,Iexp,q_img2sas,I_img2sas):
#     q_img2sas = np.where((q_img2sas<qexp[-1])&(q_img2sas>qexp[0]),q_img2sas,-1)
#     I_img2sas = np.where(q_img2sas>0,I_img2sas,-1)
#     q_img2sas_filter = q_img2sas[(I_img2sas>0)&(q_img2sas>0)]
#     I_img2sas_filter = I_img2sas[(I_img2sas>0)&(q_img2sas>0)]
#     # s = UnivariateSpline(qexp, Iexp, s=0)
#     # ys = s(q_img2sas_filter)
    
#     s=qexp
#     ys=Iexp
#     so, edges, _ = binned_statistic(s,ys, statistic='mean', bins=np.logspace(np.log10(np.min(q_img2sas_filter)),np.log10(np.max(q_img2sas_filter)),100))
#     d=edges[:-1]+np.diff(edges)/2

#     q_img2sas_filter 
#     ys = so

#     so, edges, _ = binned_statistic(q_img2sas_filter,I_img2sas_filter, statistic='mean', bins=np.logspace(np.log10(np.min(q_img2sas_filter)),np.log10(np.max(q_img2sas_filter)),100))
#     d=edges[:-1]+np.diff(edges)/2

#     q_img2sas_filter = d
#     I_img2sas_filter = so
    
    
#     mask = np.logical_and(~np.isnan(I_img2sas_filter), ~np.isnan(ys))
    
#     q_img2sas_filter_ = q_img2sas_filter[mask]
#     ys_ = ys[mask]
#     I_img2sas_filter_  = I_img2sas_filter[mask]
#     return q_img2sas_filter_,ys_,I_img2sas_filter_

                        
    
    # def splining(self,qexp,Iexp,q_img2sas,I_img2sas):
    #     q_img2sas = np.where((q_img2sas<qexp[-1])&(q_img2sas>qexp[0]),q_img2sas,-1)
    #     I_img2sas = np.where(q_img2sas>0,I_img2sas,-1)
    #     q_img2sas_filter = q_img2sas[(I_img2sas>0)&(q_img2sas>0)]
    #     I_img2sas_filter = I_img2sas[(I_img2sas>0)&(q_img2sas>0)]

    #     s=qexp
    #     ys=Iexp
    #     so, edges, _ = binned_statistic(s,ys, statistic='mean', bins=np.logspace(np.log10(np.min(q_img2sas_filter)),np.log10(np.max(q_img2sas_filter)),100))
    #     d=edges[:-1]+np.diff(edges)/2

    #     q_img2sas_filter 
    #     ys = so

    #     so, edges, _ = binned_statistic(q_img2sas_filter,I_img2sas_filter, statistic='mean', bins=np.logspace(np.log10(np.min(q_img2sas_filter)),np.log10(np.max(q_img2sas_filter)),100))
    #     d=edges[:-1]+np.diff(edges)/2

    #     q_img2sas_filter = d
    #     I_img2sas_filter = so
        
        
    #     mask = np.logical_and(~np.isnan(I_img2sas_filter), ~np.isnan(ys))
        
    #     q_img2sas_filter_ = q_img2sas_filter[mask]
    #     ys_ = ys[mask]
    #     I_img2sas_filter_  = I_img2sas_filter[mask]
        
    #     artificialq=np.logspace(np.log10(np.min(q_img2sas_filter)),np.log10(np.max(q_img2sas_filter)),100)
        
    #     s = UnivariateSpline(q_img2sas_filter_, I_img2sas_filter_, s=2)
    #     I_img2sas_filter_ = s(artificialq)
    #     s = UnivariateSpline(q_img2sas_filter_,ys_, s=2)
    #     ys_ = s(artificialq)
        
        
    #     gc.collect(generation=2)
    #     return artificialq,ys_,I_img2sas_filter_
    #     #return q_img2sas_filter_,ys_,I_img2sas_filter_

    def moving_average(self,x, w):
        return np.convolve(x, np.ones(w), 'valid') / w

    def splining(self,qexp,Iexp,q_img2sas,I_img2sas):
        q_img2sas_ori = q_img2sas
        I_img2sas_ori = I_img2sas
        
        # artificialq=np.logspace(np.log10(q_img2sas_ori[0]),np.log10(np.max(qexp)),500)
        
        # s = make_interp_spline(qexp, Iexp)
        # #s = UnivariateSpline(qexp, Iexp,s=0)
        # Iexp = s(artificialq)
        # qexp = artificialq
        
        # s = make_interp_spline(q_img2sas, I_img2sas)
        # #s = UnivariateSpline(qexp, Iexp,s=0)
        # I_img2sas = s(artificialq)
        # q_img2sas = artificialq
        
        # I_img2sas = moving_average(I_img2sas, 5)
        # q_img2sas = moving_average(q_img2sas, 5)
        
        
        
        # q_img2sas = np.where((q_img2sas<=qexp[-1])&(q_img2sas>=qexp[0]),q_img2sas,-1)
        # I_img2sas = np.where(q_img2sas>0,I_img2sas,-1)
        # q_img2sas_filter = q_img2sas[(I_img2sas>0)&(q_img2sas>0)]
        # I_img2sas_filter = I_img2sas[(I_img2sas>0)&(q_img2sas>0)]

        s=qexp
        ys=Iexp
        so, edges, _ = binned_statistic(s,ys, statistic='mean', bins=np.logspace(np.log10(0.001),np.log10(np.max(qexp)),200))
        d=edges[:-1]+np.diff(edges)/2

      
        ys = so

        so, edges, _ = binned_statistic(q_img2sas,I_img2sas, statistic='mean', bins=np.logspace(np.log10(0.001),np.log10(np.max(qexp)),200))
        d=edges[:-1]+np.diff(edges)/2

        q_img2sas = d
        I_img2sas = so
        
        
        mask = np.logical_and(~np.isnan(I_img2sas), ~np.isnan(ys))
        
        q_img2sas_filter_ = q_img2sas[mask]
        ys_ = ys[mask]
        I_img2sas_filter_  = I_img2sas[mask]
        
        
        
        # #I_img2sas_filter_ = savgol_filter(I_img2sas_filter_, 61, 4)
        # kr = KernelReg(I_img2sas_filter_,q_img2sas_filter_,'c')

        # I_img2sas_filter_, y_std = kr.fit(q_img2sas_filter_)
        
        gc.collect(generation=2)
        #return artificialq,ys_,I_img2sas_filter_
        return q_img2sas_filter_,ys_,I_img2sas_filter_



    def residual(self,q_spline, Iexp_spline,I_img2sas, scalingfactor_spline):
        residual=0
        # I_img2sas[I_img2sas>1]=1
        
        # Iexp_spline[Iexp_spline>1]=1
        for j in range(len(q_spline)-1):

            residual += ((((I_img2sas[j]*scalingfactor_spline)-(Iexp_spline[j])))**2)#/(q_spline[j] **2)

        residual = (residual/len(q_spline))**(1/2)
        gc.collect(generation=2)
        return residual
    
    def residual_withqweight(self,q_spline, Iexp_spline,I_img2sas, scalingfactor_spline):
        residual=0
        for j in range(len(q_spline)-1):

            residual += ((((I_img2sas[j]*scalingfactor_spline)-(Iexp_spline[j])))/(q_spline[j]) )**2

        residual = (residual/len(q_spline))**(1/2)
        gc.collect(generation=2)
        return residual
    
    def coloringpartialwater_classeswatersaturation(self,params1,params2): 
        regions2 = self.watershed
        regions3 = self.edt
        params1 = np.array(params1)
        params2 = np.array(params2)
        poresize=np.linspace(-10,(np.max(regions2))+10,self.classnum*10+1,endpoint=True)
        if self.modewetting=="discrete":
            parasat=[]
            parasat2=[]
            j=0
            k=0
            im_all = np.zeros(regions2.shape)
            
            for i in range(len(params2)):
                regions4 =  copy.deepcopy(regions2)
                cond= ((regions2>0)&(regions2>params1[j])&(regions2<params1[j+1]))
                im_edit = np.where(cond,regions3,0) 
                thres= params2[i]*np.max(im_edit)
                
                cond2= ((regions3>0)&(regions3<thres))
                
                # plt.figure()
                # plt.imshow(im_edit[:,:,200])
                # plt.title(np.max(im_edit))
                # plt.show()
                j+=2
                
                regions4=np.where(cond&cond2,5,0)  
                im_all+=regions4
                satlevel=np.count_nonzero(regions4)/(regions4.shape[0]**3)
                
                cond3=((regions2>0)&(regions2>poresize[k])&(regions2<poresize[k+10]))
                regions5=np.where(cond3,5,0)  
                satlevelallwet=np.count_nonzero(regions5)/(regions5.shape[0]**3)
                parasat.append([params1[j],params1[j+1],satlevel])
                parasat2.append([poresize[k],poresize[k+10],satlevelallwet])
                im_all+=regions4
                k+=10
        if self.modewetting=="continous":
            parasat=[]
            parasat2=[]
            j=0
            k=0
            im_all = np.zeros(regions2.shape)
            for i in range(len(params2)):
                regions4 =  copy.deepcopy(regions2)
                cond= ((regions2>0)&(regions2>params1[j])&(regions2<params1[j+1]))
                im_edit = np.where(cond,regions3,0) 
                thres= params2[i]*np.max(im_edit)
                cond2= ((regions3>0)&(regions3<thres))
                

                
                
                regions4=np.where(cond&cond2,5,0)  
                satlevel=np.count_nonzero(regions4)/(regions4.shape[0]**3)
                cond3=((regions2>0)&(regions2>poresize[k])&(regions2<poresize[k+10]))
                regions5=np.where(cond3,5,0)  

                satlevelallwet=np.count_nonzero(regions5)/(regions5.shape[0]**3)
                parasat.append([params1[j],params1[j+1],satlevel])
                parasat2.append([poresize[k],poresize[k+10],satlevelallwet])
                k+=10
                
                im_all+=regions4
                j+=1
            
        im_all[im_all>5]=5

        gc.collect(generation=2)
        return im_all,parasat,parasat2
    
    def intensitywaterout_classeswatersaturation(self,niceparams,mode="discrete"):
        CL_carbon_Pt=self.im_Pt
        classnum=self.classnum
        
        if mode =="discrete":
            params1=list(niceparams.values())[:2*classnum]
            params2=list(niceparams.values())[2*classnum:]
        if mode=="continous":
            params1=list(niceparams.values())[:classnum+1]
            params2=list(niceparams.values())[classnum+1:]
        
        self.modewetting = mode

        im_sld_nowater= CL_carbon_Pt
        waterclasses,parasat,parasat2 = self.coloringpartialwater_classeswatersaturation(params1, params2)
        
        im_sld_withwater = waterclasses
        warning=np.count_nonzero(im_sld_withwater)/im_sld_withwater.size
        
        im_sld_withwater[im_sld_withwater>0]=9.447*(10**10)#9.447*(10**10)
        im_sld_withwater = im_sld_withwater + im_sld_nowater
        im_sld_withwater[(im_sld_withwater > self.SLD_solid)&(im_sld_withwater < 125*(10**10))] = self.SLD_solid
        im_sld_withwater[im_sld_withwater >= 126*(10**10)] = 126*(10**10)
        
        
        I_img2sas_water,q_img2sas_water=img2sas(im_sld_withwater,self.boxsize,self.onevoxel)
        qtarget = self.q_exp_wet
        Itarget = self.I_exp_wet
        q_img2sas_filter, Itargetspline,I_img2sas_waterfilter= functions.spline(qtarget,Itarget,q_img2sas_water,I_img2sas_water)

            #error = residual(q_img2sas_filter,  np.log10(I_img2sas_dryfilter*K_img2sasdry*100)/np.log10(I_img2sas_waterfilter*K_img2sasdry*100), np.log10(Idryspline*100)/np.log10(Itargetspline*100), 1)
        error = self.residual(q_img2sas_filter, (I_img2sas_waterfilter)/ (self.I_img2sas_dry),(Itargetspline)/ (self.I_exp_dry), 1)
        
        intensityandq = {'intensitywet_img2sas' : I_img2sas_water*self.K_img2sas,
                    'intensitywet_exp' : Itargetspline,
                    'q' : q_img2sas_water,
                    'img_water': im_sld_withwater,'score':-error*100
                    ,'parasat':parasat,'parasat2':parasat2
                    }

        return intensityandq
      
            
    def optimization_puttingwater_classeswatersaturation(self,classnum=10,mode="continous",max_score=None, n_iter=100,**previousfit): 
        self.mode=mode
        if 'watershed' in self.__dict__:
            regions2=self.watershed
    
        else:    
            self.trim_localthickness(self.im_Pt)
            regions2=self.watershed
            print('no initial watershed')
        regions2=self.watershed
       
        
        hyper = Hyperactive()
        optimizer = EvolutionStrategyOptimizer()
        early_stopping = {
                "n_iter_no_change": 100000,
                "tol_abs": None,
                "tol_rel": None}
                
        self.classnum = classnum
        
        rangeforwatersaturation = np.unique(self.watershed)
        rangeforwatersaturation = np.asarray(natsort.natsorted(rangeforwatersaturation,reverse=False))

        if mode=="continous":
            j=0
            
            opt_space={}
            
            # poresize=np.logspace(0,np.log10(max(rangeforwatersaturation)),IBM_water[ID].classnum*10)
            
            poresize=np.linspace(-10,(max(rangeforwatersaturation))+10,self.classnum*10+1,endpoint=True)
    
            for  i in range(0,self.classnum+1):
                # if i == 0 :
                #     opt_space['{}'.format(i)]=0
                # else:
                    opt_space['{}'.format(i)]=list(poresize[j:j+10])
                    j+=10
                
            for  i in range(int(self.classnum)+1,int((self.classnum)*2)+1,1):
                opt_space['{}'.format(i)]=list(np.linspace(0,1,11))
        else:
            j=0
    
            opt_space={}
    
            poresize=np.linspace(-10,(max(rangeforwatersaturation))+10,self.classnum*10+1,endpoint=True)
   
            
            for  i in range(0,2*self.classnum,2):
                opt_space['{}'.format(i)]=list(poresize[j:j+10])
    
                opt_space['{}'.format(i+1)]=list(poresize[j:j+10])
                j+=10
                
            for  i in range(int(self.classnum*2),int((self.classnum)*3),1):
                opt_space['{}'.format(i)]=list(np.linspace(0,1,11))
    
                    
        #optimizer = EvolutionStrategyOptimizer()
        if ('search_data' in previousfit)&('previousbestpara' in previousfit):
            search_data= previousfit['search_data']
            previousbestpara= previousfit['previousbestpara']
            initialize = {'grid':8,'vertices':8,'random':6,'warm_start':[previousbestpara]}
            hyper.add_search(self.minimizeobjective_puttingwater_classeswatersaturation, opt_space, optimizer=optimizer, n_iter=n_iter,early_stopping=early_stopping, memory_warm_start=search_data,initialize=initialize,max_score=max_score)
        elif ('previousbestpara' in previousfit):   
            previousbestpara= previousfit['previousbestpara']
            initialize = {'grid':8,'vertices':8,'random':6,'warm_start':[previousbestpara]}
            hyper.add_search(self.minimizeobjective_puttingwater_classeswatersaturation, opt_space, optimizer=optimizer, n_iter=n_iter,early_stopping=early_stopping, initialize=initialize,max_score=max_score)
        else:
            initialize = {'grid':8,'vertices':8,'random':6}
            hyper.add_search(self.minimizeobjective_puttingwater_classeswatersaturation, opt_space, optimizer=optimizer, n_iter=n_iter,early_stopping=early_stopping, initialize=initialize,max_score=max_score)
        hyper.run()
        gc.collect(generation=2)
        self.opt_para = hyper.best_para(self.minimizeobjective_puttingwater_classeswatersaturation)
        self.search_data = hyper.search_data(self.minimizeobjective_puttingwater_classeswatersaturation)
        # self.search_data['I_img2sas_dry'] = self.I_img2sas_dry*self.K_img2sas
        # self.search_data['I_exp_dry'] = self.I_exp_dry
        # self.search_data['q_exp_wet']=self.q_exp_wet
        # self.search_data['I_exp_wet']=self.I_exp_wet
        # self.search_data['SLD_solid']=self.SLD_solid
        self.intensityandq = self.intensitywaterout_classeswatersaturation(self.opt_para,mode=self.mode)
        gc.collect()   
        
    def minimizeobjective_puttingwater_classeswatersaturation(self,niceparams):
        CL_carbon_Pt=self.im_Pt
        classnum=self.classnum
        mode=self.mode
        
        if mode =="discrete":
            params1=list(niceparams.values())[:2*classnum]
            params2=list(niceparams.values())[2*classnum:]
        if mode=="continous":
            params1=list(niceparams.values())[:classnum+1]
            params2=list(niceparams.values())[classnum+1:]
        
        self.modewetting = mode
        print(params1,params2,mode)

        im_sld_nowater= CL_carbon_Pt
        waterclasses,parasat,parasat2 = self.coloringpartialwater_classeswatersaturation(params1, params2)
        im_sld_withwater = waterclasses
        warning=np.count_nonzero(im_sld_withwater)/im_sld_withwater.size
        
        im_sld_withwater[im_sld_withwater>0]=9.447*(10**10)#9.447*(10**10)
        im_sld_withwater = im_sld_withwater + im_sld_nowater
        im_sld_withwater[(im_sld_withwater > self.SLD_solid)&(im_sld_withwater < 125*(10**10))] = self.SLD_solid
        im_sld_withwater[im_sld_withwater >= 126*(10**10)] = 126*(10**10)
        
        
        I_img2sas_water,q_img2sas_water=img2sas(im_sld_withwater,self.boxsize,self.onevoxel)
        qtarget = self.q_exp_wet
        Itarget = self.I_exp_wet
        q_img2sas_filter, Itargetspline,I_img2sas_waterfilter= functions.spline(qtarget,Itarget,q_img2sas_water,I_img2sas_water)

            #error = residual(q_img2sas_filter,  np.log10(I_img2sas_dryfilter*K_img2sasdry*100)/np.log10(I_img2sas_waterfilter*K_img2sasdry*100), np.log10(Idryspline*100)/np.log10(Itargetspline*100), 1)
        error = self.residual(q_img2sas_filter, (I_img2sas_waterfilter)/ (self.I_img2sas_dry),(Itargetspline)/ (self.I_exp_dry), 1)
        
        intensityandq = {'intensitywet_img2sas' : I_img2sas_water*self.K_img2sas,
                    'intensitywet_exp' : Itargetspline,
                    'q' : q_img2sas_water,
                    # 'img_water': im_sld_withwater,
                    'score':-error*100
                    ,'parasat':parasat,'parasat2':parasat2
                    }

        return -error*100,intensityandq
            
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
        
    def coloringpartialwater_largeporesfirst(self,clustersizethreshold,anotherthreshold): 
        regions2 = self.watershed
        if anotherthreshold < clustersizethreshold:
            regions4 =  copy.deepcopy(regions2)
            regions4=np.where((regions2>=clustersizethreshold)&(0<regions2),5,0)    
        else:
            regions4=np.where((regions2>=clustersizethreshold)&(0<regions2)&(regions2<=anotherthreshold),5,0)
            # regions4[(regions2>=clustersizethreshold)&(0<regions2)&(regions2<=anotherthreshold)] = 5
            # regions4[(regions2<clustersizethreshold)&(0<regions2)&(regions2>anotherthreshold)]= 0    
        gc.collect(generation=2)
        return regions4
     
        
    def coloringpartialwater_smallporesfirst(self,clustersizethreshold,anotherthreshold): 
        regions2 = self.watershed
        if anotherthreshold > clustersizethreshold:
            regions4 =  copy.deepcopy(regions2)
            regions4=np.where((regions2<=clustersizethreshold)&(0<regions2),5,0)
        else: 
            regions4 =  copy.deepcopy(regions2)               
            regions4=np.where((regions2<=clustersizethreshold)&(0<regions2)&(regions2>=anotherthreshold),5,0)
        gc.collect(generation=2)
        return regions4

    def coloringpartialwater_thinfilm(self,iteration):
        regions2 = self.watershed
        CL_carbon_Pt=self.im_Pt
        if iteration == 0:
            regions4 =  copy.deepcopy(regions2)
            regions4[regions4>0]=0
        else:
            iteration=int(iteration)
            base_for_dilation = copy.deepcopy(CL_carbon_Pt)
            base_for_dilation[base_for_dilation>0]=1
            dilated = spim.morphology.binary_dilation(base_for_dilation, iterations=iteration).astype('float32')
            waterthinfilm = dilated - base_for_dilation 
            regions4 = waterthinfilm
            regions4[regions4>0]=5
        gc.collect(generation=2)
        return regions4 
    
        
    def minimizeobjective_puttingwater(self,niceparams):
        CL_carbon_Pt=self.im_Pt
        iteration_thinfilm=list(niceparams.values())[0]
        clusterthreshold_smallpores=list(niceparams.values())[1]
        clusterthreshold_largepores=list(niceparams.values())[2]
        anotherthreshold_smallpores=list(niceparams.values())[3]
        anotherthreshold_largepores=list(niceparams.values())[4]
        if (clusterthreshold_largepores < clusterthreshold_smallpores) :
            error = 1000
            #print('error a')

        elif (clusterthreshold_largepores >= clusterthreshold_smallpores) &(anotherthreshold_largepores<clusterthreshold_largepores) :
            error = 1000
            #print('error b')
        elif (clusterthreshold_largepores >= clusterthreshold_smallpores) &(anotherthreshold_smallpores >clusterthreshold_smallpores):
            error = 1000
            #print('error c')
        else:
            
            im_sld_nowater= CL_carbon_Pt
            water_thinfilm=self.coloringpartialwater_thinfilm(iteration_thinfilm)
            water_smallpores=self.coloringpartialwater_smallporesfirst(clusterthreshold_smallpores,anotherthreshold_smallpores)
            water_largepores=self.coloringpartialwater_largeporesfirst(clusterthreshold_largepores,anotherthreshold_largepores)
            im_sld_withwater = water_thinfilm + water_smallpores +  water_largepores
            warning=np.count_nonzero(im_sld_withwater)/im_sld_withwater.size
            
            im_sld_withwater[im_sld_withwater>0]=9.447*(10**10)#9.447*(10**10)
            im_sld_withwater = im_sld_withwater + im_sld_nowater
            im_sld_withwater[(im_sld_withwater > self.SLD_solid)&(im_sld_withwater < 125*(10**10))] = self.SLD_solid
            im_sld_withwater[im_sld_withwater >= 126*(10**10)] = 126*(10**10)
            
            if warning > 0.68:
                error = 1000000
                #print('error f')
            else:
                
                I_img2sas_water,q_img2sas_water=img2sas(im_sld_withwater,self.boxsize,self.onevoxel)
                qtarget = self.q_exp_wet
                Itarget = self.I_exp_wet
                q_img2sas_filter, Itargetspline,I_img2sas_waterfilter= self.splining(qtarget,Itarget,q_img2sas_water,I_img2sas_water)
    
                    #error = residual(q_img2sas_filter,  np.log10(I_img2sas_dryfilter*K_img2sasdry*100)/np.log10(I_img2sas_waterfilter*K_img2sasdry*100), np.log10(Idryspline*100)/np.log10(Itargetspline*100), 1)
                error = self.residual(q_img2sas_filter, (I_img2sas_waterfilter)/ (self.I_img2sas_dry),(Itargetspline)/ (self.I_exp_dry), 1)
        score = -error*100
        # if score > -15:
        #     intensityandq = {'intensitywet_img2sas' : I_img2sas_waterfilter*self.K_img2sas,
        #                 'intensitywet_exp' : Itargetspline,
        #                 'q' : q_img2sas_filter,
        #                 'img_water': im_sld_withwater[:,:,250]
        #                 }
        # else:
            
        #     intensityandq = {}
        #     gc.collect(generation=2)
        #     gc.collect()     
        #return score, intensityandq
        return score
    
    def minimizeobjective_puttingwater_withqweight(self,niceparams):
        CL_carbon_Pt=self.im_Pt
        iteration_thinfilm=list(niceparams.values())[0]
        clusterthreshold_smallpores=list(niceparams.values())[1]
        clusterthreshold_largepores=list(niceparams.values())[2]
        anotherthreshold_smallpores=list(niceparams.values())[3]
        anotherthreshold_largepores=list(niceparams.values())[4]
        if (clusterthreshold_largepores < clusterthreshold_smallpores) :
            error = 1000
            #print('error a')

        elif (clusterthreshold_largepores >= clusterthreshold_smallpores) &(anotherthreshold_largepores<clusterthreshold_largepores) :
            error = 1000
            #print('error b')
        elif (clusterthreshold_largepores >=clusterthreshold_smallpores) &(anotherthreshold_smallpores >clusterthreshold_smallpores):
            error = 1000
            #print('error c')
        else:
            
            im_sld_nowater= CL_carbon_Pt
            water_thinfilm=self.coloringpartialwater_thinfilm(iteration_thinfilm)
            water_smallpores=self.coloringpartialwater_smallporesfirst(clusterthreshold_smallpores,anotherthreshold_smallpores)
            water_largepores=self.coloringpartialwater_largeporesfirst(clusterthreshold_largepores,anotherthreshold_largepores)
            im_sld_withwater = water_thinfilm + water_smallpores +  water_largepores
            warning=np.count_nonzero(im_sld_withwater)/im_sld_withwater.size
            
            im_sld_withwater[im_sld_withwater>0]=9.447*(10**10)#9.447*(10**10)
            im_sld_withwater = im_sld_withwater + im_sld_nowater
            im_sld_withwater[(im_sld_withwater > self.SLD_solid)&(im_sld_withwater < 125*(10**10))] = self.SLD_solid
            im_sld_withwater[im_sld_withwater >= 126*(10**10)] = 126*(10**10)
            
            if warning > 0.68:
                error = 1000000
                #print('error f')
            else:
                
                I_img2sas_water,q_img2sas_water=img2sas(im_sld_withwater,self.boxsize,self.onevoxel)
                qtarget = self.q_exp_wet
                Itarget = self.I_exp_wet
                q_img2sas_filter, Itargetspline,I_img2sas_waterfilter= self.splining(qtarget,Itarget,q_img2sas_water,I_img2sas_water)
    
                    #error = residual(q_img2sas_filter,  np.log10(I_img2sas_dryfilter*K_img2sasdry*100)/np.log10(I_img2sas_waterfilter*K_img2sasdry*100), np.log10(Idryspline*100)/np.log10(Itargetspline*100), 1)
                error = self.residual_withqweight(q_img2sas_filter,  (I_img2sas_waterfilter)/ (self.I_img2sas_dry),(Itargetspline)/ (self.I_exp_dry), 1)
        score = -error*100
        # if score > -15:
        #     intensityandq = {'intensitywet_img2sas' : I_img2sas_waterfilter*self.K_img2sas,
        #                 'intensitywet_exp' : Itargetspline,
        #                 'q' : q_img2sas_filter,
        #                 'img_water': im_sld_withwater[:,:,250]
        #                 }
        # else:
            
        #     intensityandq = {}
        #     gc.collect(generation=2)
        #     gc.collect()     
        return score
    
    
    def optimization_puttingwater(self,max_score=None, n_iter=100,**previousfit): 
        if 'watershed' in self.__dict__:
            regions2=self.watershed
    
        else:    
            self.trim(self.im_Pt)
            regions2=self.watershed
            print('no initial watershed')
        regions2=self.watershed
       
        
        cc = chain(np.arange(0,5,0.1), np.arange(5,100,0.3),np.arange(100,300,0.5))
        cc=list(cc)
        vv=[(4/3)*np.pi*((2*item)**3 )for item in cc]
        print("binning success ")
        histogramofporesize = np.histogram(np.ndarray.flatten(regions2),bins=vv)
        
        binnedporesize = histogramofporesize[1][1:]
        counts = histogramofporesize[0][0:]
        rangeforwatersaturation = binnedporesize[counts!=0]
        rangeforwatersaturation=np.append(rangeforwatersaturation,0)
        rangeforwatersaturation = np.asarray(natsort.natsorted(rangeforwatersaturation,reverse=False))
        descendrangeforwatersaturation = np.asarray(natsort.natsorted(rangeforwatersaturation,reverse=True))

        opt_space = {'thinfilm' : [0,1,2,3],
        
        'smallpores' : list(rangeforwatersaturation),
        
        'largepores' : list(descendrangeforwatersaturation),
                        'anothersmallpores' : list(rangeforwatersaturation),
        
        'anotherlargepores' : list(descendrangeforwatersaturation),
        
        }
        
        
        hyper = Hyperactive()
        optimizer = TreeStructuredParzenEstimators()
        early_stopping = {
                "n_iter_no_change": 300,
                "tol_abs": None,
                "tol_rel": None}
        
        #optimizer = EvolutionStrategyOptimizer()
        if ('search_data' in previousfit)&('previousbestpara' in previousfit):
            search_data= previousfit['search_data']
            previousbestpara= previousfit['previousbestpara']
            initialize = {'grid':4,'vertices':4,'random':6,'warm_start':[previousbestpara]}
            hyper.add_search(self.minimizeobjective_puttingwater, opt_space, optimizer=optimizer, n_iter=n_iter,early_stopping=early_stopping, memory="share",memory_warm_start=search_data,initialize=initialize,max_score=max_score)
        elif ('previousbestpara' in previousfit):   
            previousbestpara= previousfit['previousbestpara']
            initialize = {'grid':4,'vertices':4,'random':6,'warm_start':[previousbestpara]}
            hyper.add_search(self.minimizeobjective_puttingwater, opt_space, optimizer=optimizer, n_iter=n_iter,early_stopping=early_stopping, memory="share",initialize=initialize,max_score=max_score)
        else:
            initialize = {'grid':4,'vertices':4,'random':6}
            hyper.add_search(self.minimizeobjective_puttingwater, opt_space, optimizer=optimizer, n_iter=n_iter,early_stopping=early_stopping, memory="share",initialize=initialize,max_score=max_score)
        hyper.run()
        gc.collect(generation=2)
        self.opt_para = hyper.best_para(self.minimizeobjective_puttingwater)
        self.search_data = hyper.search_data(self.minimizeobjective_puttingwater)
        # self.search_data['I_img2sas_dry'] = self.I_img2sas_dry*self.K_img2sas
        # self.search_data['I_exp_dry'] = self.I_exp_dry
        # self.search_data['q_exp_wet']=self.q_exp_wet
        # self.search_data['I_exp_wet']=self.I_exp_wet
        # self.search_data['SLD_solid']=self.SLD_solid

        gc.collect()
        
    def optimization_puttingwater_localthickness(self,max_score=None, n_iter=100,**previousfit): 
        if 'watershed' in self.__dict__:
            regions2=self.watershed
    
        else:    
            self.trim_localthickness(self.im_Pt)
            regions2=self.watershed
            print('no initial watershed')
        regions2=self.watershed
       
        
        # cc = chain(np.linspace(0,5,20,endpoint=False), np.arange(5,100,1),np.arange(100,300,1))
        # cc=list(cc)
        # #vv=[(4/3)*np.pi*((2*item)**3 )for item in cc]
        # print("binning success ")
        # histogramofporesize = np.histogram(np.ndarray.flatten(regions2),bins=cc)
        
        # binnedporesize = histogramofporesize[1][1:]
        # counts = histogramofporesize[0][0:]
        # rangeforwatersaturation = binnedporesize[counts!=0]
        # rangeforwatersaturation=np.append(rangeforwatersaturation,0)
        rangeforwatersaturation = np.unique(regions2)
        rangeforwatersaturation=np.append(rangeforwatersaturation,0)
        rangeforwatersaturation = np.asarray(natsort.natsorted(rangeforwatersaturation,reverse=False))
        descendrangeforwatersaturation = np.asarray(natsort.natsorted(rangeforwatersaturation,reverse=True))

        opt_space = {'thinfilm' : [0,1,2,3],
        
        'smallpores' : list(rangeforwatersaturation),
        
        'largepores' : list(descendrangeforwatersaturation),
                        'anothersmallpores' : list(rangeforwatersaturation),
        
        'anotherlargepores' : list(descendrangeforwatersaturation),
        
        }
        
        
        hyper = Hyperactive()
        optimizer = TreeStructuredParzenEstimators()
        early_stopping = {
                "n_iter_no_change": 300,
                "tol_abs": None,
                "tol_rel": None}
        
        #optimizer = EvolutionStrategyOptimizer()
        if ('search_data' in previousfit)&('previousbestpara' in previousfit):
            search_data= previousfit['search_data']
            previousbestpara= previousfit['previousbestpara']
            initialize = {'grid':8,'vertices':8,'random':6,'warm_start':[previousbestpara]}
            hyper.add_search(self.minimizeobjective_puttingwater, opt_space, optimizer=optimizer, n_iter=n_iter,early_stopping=early_stopping, memory="share",memory_warm_start=search_data,initialize=initialize,max_score=max_score)
        elif ('previousbestpara' in previousfit):   
            previousbestpara= previousfit['previousbestpara']
            initialize = {'grid':8,'vertices':8,'random':6,'warm_start':[previousbestpara]}
            hyper.add_search(self.minimizeobjective_puttingwater, opt_space, optimizer=optimizer, n_iter=n_iter,early_stopping=early_stopping, memory="share",initialize=initialize,max_score=max_score)
        else:
            initialize = {'grid':8,'vertices':8,'random':6}
            hyper.add_search(self.minimizeobjective_puttingwater, opt_space, optimizer=optimizer, n_iter=n_iter,early_stopping=early_stopping, memory="share",initialize=initialize,max_score=max_score)
        hyper.run()
        gc.collect(generation=2)
        self.opt_para = hyper.best_para(self.minimizeobjective_puttingwater)
        self.search_data = hyper.search_data(self.minimizeobjective_puttingwater)
        # self.search_data['I_img2sas_dry'] = self.I_img2sas_dry*self.K_img2sas
        # self.search_data['I_exp_dry'] = self.I_exp_dry
        # self.search_data['q_exp_wet']=self.q_exp_wet
        # self.search_data['I_exp_wet']=self.I_exp_wet
        # self.search_data['SLD_solid']=self.SLD_solid
        self.intensityandq = self.intensitywaterout(self.opt_para)
        gc.collect()
        
        
        
    def optimization_puttingwater_localthickness_evolutionstrategy(self,max_score=None, n_iter=100,**previousfit): 
        if 'watershed' in self.__dict__:
            regions2=self.watershed
    
        else:    
            self.trim_localthickness(self.im_Pt)
            regions2=self.watershed
            print('no initial watershed')
        regions2=self.watershed
       
        
        # cc = chain(np.linspace(0,5,20,endpoint=False), np.arange(5,100,1),np.arange(100,300,1))
        # cc=list(cc)
        # #vv=[(4/3)*np.pi*((2*item)**3 )for item in cc]
        # print("binning success ")
        # histogramofporesize = np.histogram(np.ndarray.flatten(regions2),bins=cc)
        
        # binnedporesize = histogramofporesize[1][1:]
        # counts = histogramofporesize[0][0:]
        # rangeforwatersaturation = binnedporesize[counts!=0]
        rangeforwatersaturation = np.unique(regions2)
        rangeforwatersaturation=np.append(rangeforwatersaturation,0)
        rangeforwatersaturation = np.asarray(natsort.natsorted(rangeforwatersaturation,reverse=False))
        descendrangeforwatersaturation = np.asarray(natsort.natsorted(rangeforwatersaturation,reverse=True))

        opt_space = {'thinfilm' : [0,1,2,3],
        
        'smallpores' : list(rangeforwatersaturation),
        
        'largepores' : list(descendrangeforwatersaturation),
                        'anothersmallpores' : list(rangeforwatersaturation),
        
        'anotherlargepores' : list(descendrangeforwatersaturation),
        
        }
        
        
        hyper = Hyperactive()
        optimizer = EvolutionStrategyOptimizer()
        early_stopping = {
                "n_iter_no_change": 300,
                "tol_abs": None,
                "tol_rel": None}
        
        #optimizer = EvolutionStrategyOptimizer()
        if ('search_data' in previousfit)&('previousbestpara' in previousfit):
            search_data= previousfit['search_data']
            previousbestpara= previousfit['previousbestpara']
            initialize = {'grid':8,'vertices':8,'random':6,'warm_start':[previousbestpara]}
            hyper.add_search(self.minimizeobjective_puttingwater, opt_space, optimizer=optimizer, n_iter=n_iter,early_stopping=early_stopping, memory="share",memory_warm_start=search_data,initialize=initialize,max_score=max_score)
        elif ('previousbestpara' in previousfit):   
            previousbestpara= previousfit['previousbestpara']
            initialize = {'grid':8,'vertices':8,'random':6,'warm_start':[previousbestpara]}
            hyper.add_search(self.minimizeobjective_puttingwater, opt_space, optimizer=optimizer, n_iter=n_iter,early_stopping=early_stopping, memory="share",initialize=initialize,max_score=max_score)
        else:
            initialize = {'grid':8,'vertices':8,'random':6}
            hyper.add_search(self.minimizeobjective_puttingwater, opt_space, optimizer=optimizer, n_iter=n_iter,early_stopping=early_stopping, memory="share",initialize=initialize,max_score=max_score)
        hyper.run()
        gc.collect(generation=2)
        self.opt_para = hyper.best_para(self.minimizeobjective_puttingwater)
        self.search_data = hyper.search_data(self.minimizeobjective_puttingwater)
        # self.search_data['I_img2sas_dry'] = self.I_img2sas_dry*self.K_img2sas
        # self.search_data['I_exp_dry'] = self.I_exp_dry
        # self.search_data['q_exp_wet']=self.q_exp_wet
        # self.search_data['I_exp_wet']=self.I_exp_wet
        # self.search_data['SLD_solid']=self.SLD_solid
        self.intensityandq = self.intensitywaterout(self.opt_para)
        gc.collect()
        
    def intensitywaterout(self,niceparams):
        CL_carbon_Pt=self.im_Pt
        iteration_thinfilm=list(niceparams.values())[0]
        clusterthreshold_smallpores=list(niceparams.values())[1]
        clusterthreshold_largepores=list(niceparams.values())[2]
        anotherthreshold_smallpores=list(niceparams.values())[3]
        anotherthreshold_largepores=list(niceparams.values())[4]

        im_sld_nowater= CL_carbon_Pt
        water_thinfilm=self.coloringpartialwater_thinfilm(iteration_thinfilm)
        water_smallpores=self.coloringpartialwater_smallporesfirst(clusterthreshold_smallpores,anotherthreshold_smallpores)
        water_largepores=self.coloringpartialwater_largeporesfirst(clusterthreshold_largepores,anotherthreshold_largepores)
        im_sld_withwater = water_thinfilm + water_smallpores +  water_largepores
        warning=np.count_nonzero(im_sld_withwater)/im_sld_withwater.size
        
        im_sld_withwater[im_sld_withwater>0]=9.447*(10**10)#9.447*(10**10)
        im_sld_withwater = im_sld_withwater + im_sld_nowater
        im_sld_withwater[(im_sld_withwater > self.SLD_solid)&(im_sld_withwater < 125*(10**10))] = self.SLD_solid
        im_sld_withwater[im_sld_withwater >= 126*(10**10)] = 126*(10**10)
        
        
        I_img2sas_water,q_img2sas_water=img2sas(im_sld_withwater,self.boxsize,self.onevoxel)
        qtarget = self.q_exp_wet
        Itarget = self.I_exp_wet
        q_img2sas_filter, Itargetspline,I_img2sas_waterfilter= functions.spline(qtarget,Itarget,q_img2sas_water,I_img2sas_water)
        print(len(q_img2sas_filter),len(self.I_img2sas_dry),len(I_img2sas_waterfilter),len(self.I_exp_dry),len(Itargetspline))
            #error = residual(q_img2sas_filter,  np.log10(I_img2sas_dryfilter*K_img2sasdry*100)/np.log10(I_img2sas_waterfilter*K_img2sasdry*100), np.log10(Idryspline*100)/np.log10(Itargetspline*100), 1)
        error = self.residual(q_img2sas_filter, (I_img2sas_waterfilter)/ (self.I_img2sas_dry),(Itargetspline)/ (self.I_exp_dry), 1)
        
        intensityandq = {'intensitywet_img2sas' : I_img2sas_water*self.K_img2sas,
                    'intensitywet_exp' : Itargetspline,
                    'q' : q_img2sas_water,
                    'img_water': im_sld_withwater,'score':-error*100
                    }

        return intensityandq
    
   
    def optimization_puttingwater_localthickness_withqweight(self,max_score=None, n_iter=100,**previousfit): 
        if 'watershed' in self.__dict__:
            regions2=self.watershed
    
        else:    
            self.trim_localthickness(self.im_Pt)
            regions2=self.watershed
            print('no initial watershed')
        regions2=self.watershed
       
        
        cc = chain(np.linspace(0,5,20,endpoint=False), np.arange(5,100,1),np.arange(100,300,1))
        cc=list(cc)
        #vv=[(4/3)*np.pi*((2*item)**3 )for item in cc]
        print("binning success ")
        histogramofporesize = np.histogram(np.ndarray.flatten(regions2),bins=cc)
        
        binnedporesize = histogramofporesize[1][1:]
        counts = histogramofporesize[0][0:]
        rangeforwatersaturation = binnedporesize[counts!=0]
        rangeforwatersaturation=np.append(rangeforwatersaturation,0)
        rangeforwatersaturation = np.asarray(natsort.natsorted(rangeforwatersaturation,reverse=False))
        descendrangeforwatersaturation = np.asarray(natsort.natsorted(rangeforwatersaturation,reverse=True))
        print(rangeforwatersaturation)
        opt_space = {'thinfilm' : [0,1,2,3],
        
        'smallpores' : list(rangeforwatersaturation),
        
        'largepores' : list(descendrangeforwatersaturation),
                        'anothersmallpores' : list(rangeforwatersaturation),
        
        'anotherlargepores' : list(descendrangeforwatersaturation),
        
        }
        
        
        hyper = Hyperactive()
        optimizer = TreeStructuredParzenEstimators()
        early_stopping = {
                "n_iter_no_change": 300,
                "tol_abs": None,
                "tol_rel": None}
        
        #optimizer = EvolutionStrategyOptimizer()
        if ('search_data' in previousfit)&('previousbestpara' in previousfit):
            search_data= previousfit['search_data']
            previousbestpara= previousfit['previousbestpara']
            initialize = {'grid':8,'vertices':8,'random':6,'warm_start':[previousbestpara]}
            hyper.add_search(self.minimizeobjective_puttingwater_withqweight, opt_space, optimizer=optimizer, n_iter=n_iter,early_stopping=early_stopping, memory="share",memory_warm_start=search_data,initialize=initialize,max_score=max_score)
        elif ('previousbestpara' in previousfit):   
            previousbestpara= previousfit['previousbestpara']
            initialize = {'grid':8,'vertices':8,'random':6,'warm_start':[previousbestpara]}
            hyper.add_search(self.minimizeobjective_puttingwater_withqweight, opt_space, optimizer=optimizer, n_iter=n_iter,early_stopping=early_stopping, memory="share",initialize=initialize,max_score=max_score)
        else:
            initialize = {'grid':8,'vertices':8,'random':6}
            hyper.add_search(self.minimizeobjective_puttingwater_withqweight, opt_space, optimizer=optimizer, n_iter=n_iter,early_stopping=early_stopping, memory="share",initialize=initialize,max_score=max_score)
        hyper.run()
        gc.collect(generation=2)
        self.opt_para = hyper.best_para(self.minimizeobjective_puttingwater_withqweight)
        self.search_data = hyper.search_data(self.minimizeobjective_puttingwater_withqweight)
        # self.search_data['I_img2sas_dry'] = self.I_img2sas_dry*self.K_img2sas
        # self.search_data['I_exp_dry'] = self.I_exp_dry
        # self.search_data['q_exp_wet']=self.q_exp_wet
        # self.search_data['I_exp_wet']=self.I_exp_wet
        # self.search_data['SLD_solid']=self.SLD_solid

        gc.collect()
        
  
            
    def makeimg_puttingwater(self,niceparams):
        CL_carbon_Pt=self.im_Pt
        iteration_thinfilm=list(niceparams.values())[0]
        clusterthreshold_smallpores=list(niceparams.values())[1]
        clusterthreshold_largepores=list(niceparams.values())[2]
        anotherthreshold_smallpores=list(niceparams.values())[3]
        anotherthreshold_largepores=list(niceparams.values())[4]
        if (clusterthreshold_largepores <= clusterthreshold_smallpores) &(anotherthreshold_largepores<clusterthreshold_largepores) :
            error = 1000
            print('error a')
        elif (clusterthreshold_largepores <= clusterthreshold_smallpores) &(anotherthreshold_smallpores >clusterthreshold_smallpores):
            error = 1000
            print('error a')
        elif (clusterthreshold_largepores > clusterthreshold_smallpores) &(anotherthreshold_largepores<clusterthreshold_largepores) :
            error = 1000
            print('error b')
        elif (clusterthreshold_largepores > clusterthreshold_smallpores) &(anotherthreshold_smallpores >clusterthreshold_smallpores):
            error = 1000
            print('error c')
        else:
            
            im_sld_nowater= CL_carbon_Pt
            water_thinfilm=self.coloringpartialwater_thinfilm(iteration_thinfilm)
            water_smallpores=self.coloringpartialwater_smallporesfirst(clusterthreshold_smallpores,anotherthreshold_smallpores)
            water_largepores=self.coloringpartialwater_largeporesfirst(clusterthreshold_largepores,anotherthreshold_largepores)
            im_sld_withwater = water_thinfilm + water_smallpores +  water_largepores
            warning=np.count_nonzero(im_sld_withwater)/im_sld_withwater.size
            
            im_sld_withwater[im_sld_withwater>0]=9.447*(10**10)#9.447*(10**10)
            im_sld_withwater = im_sld_withwater + im_sld_nowater
            im_sld_withwater[(im_sld_withwater > self.SLD_solid)&(im_sld_withwater < 125*(10**10))] = self.SLD_solid
            im_sld_withwater[im_sld_withwater >= 126*(10**10)] = 126*(10**10)
        gc.collect(generation=2)
        return im_sld_withwater      
    
    
    ##########################
    
    def makeimg_puttingwater_noanotherthreshold(self,niceparams):
        CL_carbon_Pt=self.im_Pt
        iteration_thinfilm=list(niceparams.values())[0]
        clusterthreshold_smallpores=list(niceparams.values())[1]
        clusterthreshold_largepores=list(niceparams.values())[2]
        
            
        im_sld_nowater= CL_carbon_Pt
        water_thinfilm=self.coloringpartialwater_thinfilm(iteration_thinfilm)
        water_smallpores=self.coloringpartialwater_smallporesfirst_noanotherthreshold(clusterthreshold_smallpores)
        water_largepores=self.coloringpartialwater_largeporesfirst_noanotherthreshold(clusterthreshold_largepores)
        im_sld_withwater = water_thinfilm + water_smallpores +  water_largepores
        warning=np.count_nonzero(im_sld_withwater)/im_sld_withwater.size
        
        im_sld_withwater[im_sld_withwater>0]=9.447*(10**10)#9.447*(10**10)
        im_sld_withwater = im_sld_withwater + im_sld_nowater
        im_sld_withwater[(im_sld_withwater > self.SLD_solid)&(im_sld_withwater < 125*(10**10))] = self.SLD_solid
        im_sld_withwater[im_sld_withwater >= 126*(10**10)] = 126*(10**10)
        gc.collect(generation=2)
        return im_sld_withwater      
    
        
    def coloringpartialwater_largeporesfirst_noanotherthreshold(self,clustersizethreshold): 
        regions2 = self.watershed
        regions4 =  copy.deepcopy(regions2)     
        regions4=np.where((regions2>=clustersizethreshold)&(0<regions2),5,0)
        gc.collect(generation=2)
        return regions4
     
        
    def coloringpartialwater_smallporesfirst_noanotherthreshold(self,clustersizethreshold): 
        regions2 = self.watershed

        regions4 =  copy.deepcopy(regions2)               
        regions4=np.where((regions2<=clustersizethreshold)&(0<regions2),5,0)
        gc.collect(generation=2)
        return regions4

    def coloringpartialwater_thinfilm_noanotherthreshold(self,iteration):
        regions2 = self.watershed
        CL_carbon_Pt=self.im_Pt
        if iteration == 0:
            regions4 =  copy.deepcopy(regions2)
            regions4[regions4>0]=0
        else:
            iteration=int(iteration)
            base_for_dilation = copy.deepcopy(CL_carbon_Pt)
            base_for_dilation[base_for_dilation>0]=1
            dilated = spim.morphology.binary_dilation(base_for_dilation, iterations=iteration).astype('float32')
            waterthinfilm = dilated - base_for_dilation 
            regions4 = waterthinfilm
            regions4[regions4>0]=5
        gc.collect(generation=2)
        return regions4 
    
    def minimizeobjective_puttingwater_noanotherthreshold(self,niceparams):
        CL_carbon_Pt=self.im_Pt
        iteration_thinfilm=list(niceparams.values())[0]
        clusterthreshold_smallpores=list(niceparams.values())[1]
        clusterthreshold_largepores=list(niceparams.values())[2]
        if (clusterthreshold_largepores < clusterthreshold_smallpores) :
            error = 1000

        else:    
            im_sld_nowater= CL_carbon_Pt
            water_thinfilm=self.coloringpartialwater_thinfilm_noanotherthreshold(iteration_thinfilm)
            water_smallpores=self.coloringpartialwater_smallporesfirst_noanotherthreshold(clusterthreshold_smallpores)
            water_largepores=self.coloringpartialwater_largeporesfirst_noanotherthreshold(clusterthreshold_largepores)
            im_sld_withwater = water_thinfilm + water_smallpores +  water_largepores
            warning=np.count_nonzero(im_sld_withwater)/im_sld_withwater.size
            
            im_sld_withwater[im_sld_withwater>0]=9.447*(10**10)#9.447*(10**10)
            im_sld_withwater = im_sld_withwater + im_sld_nowater
            im_sld_withwater[(im_sld_withwater > self.SLD_solid)&(im_sld_withwater < 125*(10**10))] = self.SLD_solid
            im_sld_withwater[im_sld_withwater >= 126*(10**10)] = 126*(10**10)
            
            if warning > 0.68:
                error = 1000000
                #print('error f')
            else:
                
                I_img2sas_water,q_img2sas_water=img2sas(im_sld_withwater,self.boxsize,self.onevoxel)
                qtarget = self.q_exp_wet
                Itarget = self.I_exp_wet
                q_img2sas_filter, Itargetspline,I_img2sas_waterfilter= self.splining(qtarget,Itarget,q_img2sas_water,I_img2sas_water)
    
                    #error = residual(q_img2sas_filter,  np.log10(I_img2sas_dryfilter*K_img2sasdry*100)/np.log10(I_img2sas_waterfilter*K_img2sasdry*100), np.log10(Idryspline*100)/np.log10(Itargetspline*100), 1)
                error = self.residual(q_img2sas_filter,  1/((self.I_img2sas_dry)/(I_img2sas_waterfilter)), 1/((self.I_exp_dry)/(Itargetspline)), 1)
        score = -error*100
        gc.collect(generation=2)
        gc.collect()     
        return score
    
   
    
            
    def optimization_puttingwater_localthickness_evolutionstrategy_noanotherthreshold(self,max_score=None, n_iter=100,**previousfit): 
        if 'watershed' in self.__dict__:
            regions2=self.watershed
    
        else:    
            self.trim_localthickness(self.im_Pt)
            regions2=self.watershed
            print('no initial watershed')
        regions2=self.watershed
       
        rangeforwatersaturation = np.unique(regions2)
        rangeforwatersaturation=np.append(rangeforwatersaturation,0)
        rangeforwatersaturation = np.asarray(natsort.natsorted(rangeforwatersaturation,reverse=False))
        descendrangeforwatersaturation = np.asarray(natsort.natsorted(rangeforwatersaturation,reverse=True))

        opt_space = {'thinfilm' : [0,1,2,3],
        
        'smallpores' : list(rangeforwatersaturation),
        
        'largepores' : list(descendrangeforwatersaturation)}
        
        hyper = Hyperactive()
        optimizer = EvolutionStrategyOptimizer()
        early_stopping = {
                "n_iter_no_change": 300,
                "tol_abs": None,
                "tol_rel": None}
        
        #optimizer = EvolutionStrategyOptimizer()
        if ('search_data' in previousfit)&('previousbestpara' in previousfit):
            search_data= previousfit['search_data']
            previousbestpara= previousfit['previousbestpara']
            initialize = {'grid':8,'vertices':8,'random':6,'warm_start':[previousbestpara]}
            hyper.add_search(self.minimizeobjective_puttingwater_noanotherthreshold, opt_space, optimizer=optimizer, n_iter=n_iter,early_stopping=early_stopping, memory="share",memory_warm_start=search_data,initialize=initialize,max_score=max_score)
        elif ('previousbestpara' in previousfit):   
            previousbestpara= previousfit['previousbestpara']
            initialize = {'grid':8,'vertices':8,'random':6,'warm_start':[previousbestpara]}
            hyper.add_search(self.minimizeobjective_puttingwater_noanotherthreshold, opt_space, optimizer=optimizer, n_iter=n_iter,early_stopping=early_stopping, memory="share",initialize=initialize,max_score=max_score)
        else:
            initialize = {'grid':8,'vertices':8,'random':6}
            hyper.add_search(self.minimizeobjective_puttingwater_noanotherthreshold, opt_space, optimizer=optimizer, n_iter=n_iter,early_stopping=early_stopping, memory="share",initialize=initialize,max_score=max_score)
        hyper.run()
        gc.collect(generation=2)
        self.opt_para = hyper.best_para(self.minimizeobjective_puttingwater_noanotherthreshold)
        self.search_data = hyper.search_data(self.minimizeobjective_puttingwater_noanotherthreshold)
        self.intensityandq = self.intensitywaterout_noanotherthreshold(self.opt_para)
        gc.collect()
        
        
    def optimization_puttingwater_localthickness_noanotherthreshold(self,max_score=None, n_iter=100,**previousfit): 
        if 'watershed' in self.__dict__:
            regions2=self.watershed
    
        else:    
            self.trim_localthickness(self.im_Pt)
            regions2=self.watershed
            print('no initial watershed')
        regions2=self.watershed
       
        rangeforwatersaturation = np.unique(regions2)
        rangeforwatersaturation=np.append(rangeforwatersaturation,0)
        rangeforwatersaturation = np.asarray(natsort.natsorted(rangeforwatersaturation,reverse=False))
        descendrangeforwatersaturation = np.asarray(natsort.natsorted(rangeforwatersaturation,reverse=True))

        opt_space = {'thinfilm' : list(np.arange(0,100)),
        
        'smallpores' : list(rangeforwatersaturation),
        
        'largepores' : list(descendrangeforwatersaturation)}
        
        hyper = Hyperactive()
        optimizer = TreeStructuredParzenEstimators()
        early_stopping = {
                "n_iter_no_change": 300,
                "tol_abs": None,
                "tol_rel": None}
        
        #optimizer = EvolutionStrategyOptimizer()
        if ('search_data' in previousfit)&('previousbestpara' in previousfit):
            search_data= previousfit['search_data']
            previousbestpara= previousfit['previousbestpara']
            initialize = {'grid':8,'vertices':8,'random':6,'warm_start':[previousbestpara]}
            hyper.add_search(self.minimizeobjective_puttingwater_noanotherthreshold, opt_space, optimizer=optimizer, n_iter=n_iter,early_stopping=early_stopping, memory="share",memory_warm_start=search_data,initialize=initialize,max_score=max_score)
        elif ('previousbestpara' in previousfit):   
            previousbestpara= previousfit['previousbestpara']
            initialize = {'grid':8,'vertices':8,'random':6,'warm_start':[previousbestpara]}
            hyper.add_search(self.minimizeobjective_puttingwater_noanotherthreshold, opt_space, optimizer=optimizer, n_iter=n_iter,early_stopping=early_stopping, memory="share",initialize=initialize,max_score=max_score)
        else:
            initialize = {'grid':8,'vertices':8,'random':6}
            hyper.add_search(self.minimizeobjective_puttingwater_noanotherthreshold, opt_space, optimizer=optimizer, n_iter=n_iter,early_stopping=early_stopping, memory="share",initialize=initialize,max_score=max_score)
        hyper.run()
        gc.collect(generation=2)
        self.opt_para = hyper.best_para(self.minimizeobjective_puttingwater_noanotherthreshold)
        self.search_data = hyper.search_data(self.minimizeobjective_puttingwater_noanotherthreshold)
        self.intensityandq = self.intensitywaterout_noanotherthreshold(self.opt_para)
        gc.collect()
    
    def intensitywaterout_noanotherthreshold(self,niceparams):
        CL_carbon_Pt=self.im_Pt
        iteration_thinfilm=list(niceparams.values())[0]
        clusterthreshold_smallpores=list(niceparams.values())[1]
        clusterthreshold_largepores=list(niceparams.values())[2]
      
        im_sld_nowater= CL_carbon_Pt
        water_thinfilm=self.coloringpartialwater_thinfilm_noanotherthreshold(iteration_thinfilm)
        water_smallpores=self.coloringpartialwater_smallporesfirst_noanotherthreshold(clusterthreshold_smallpores)
        water_largepores=self.coloringpartialwater_largeporesfirst_noanotherthreshold(clusterthreshold_largepores)
        im_sld_withwater = water_thinfilm + water_smallpores +  water_largepores
        warning=np.count_nonzero(im_sld_withwater)/im_sld_withwater.size
        
        im_sld_withwater[im_sld_withwater>0]=9.447*(10**10)#9.447*(10**10)
        im_sld_withwater = im_sld_withwater + im_sld_nowater
        im_sld_withwater[(im_sld_withwater > self.SLD_solid)&(im_sld_withwater < 125*(10**10))] = self.SLD_solid
        im_sld_withwater[im_sld_withwater >= 126*(10**10)] = 126*(10**10)
        
        
        I_img2sas_water,q_img2sas_water=img2sas(im_sld_withwater,self.boxsize,self.onevoxel)
        qtarget = self.q_exp_wet
        Itarget = self.I_exp_wet
        q_img2sas_filter, Itargetspline,I_img2sas_waterfilter= self.splining(qtarget,Itarget,q_img2sas_water,I_img2sas_water)

            #error = residual(q_img2sas_filter,  np.log10(I_img2sas_dryfilter*K_img2sasdry*100)/np.log10(I_img2sas_waterfilter*K_img2sasdry*100), np.log10(Idryspline*100)/np.log10(Itargetspline*100), 1)
        error = self.residual(q_img2sas_filter, (I_img2sas_waterfilter)/ (self.I_img2sas_dry),(Itargetspline)/ (self.I_exp_dry), 1)
        
        intensityandq = {'intensitywet_img2sas' : I_img2sas_waterfilter*self.K_img2sas,
                    'intensitywet_exp' : Itargetspline,
                    'q' : q_img2sas_filter,
                    'img_water': im_sld_withwater[:,:,250],'score':-error*100
                    }

        return intensityandq
    
    ####
    
    def makeimg_puttingwater_noanotherthreshold_nucleation(self,niceparams):
        CL_carbon_Pt=self.im_Pt
        iteration_thinfilm=list(niceparams.values())[0]
        clusterthreshold_smallpores=list(niceparams.values())[1]
        clusterthreshold_largepores=list(niceparams.values())[2]
        
            
        im_sld_nowater= CL_carbon_Pt
        water_thinfilm=self.coloringpartialwater_thinfilm(iteration_thinfilm)
        water_smallpores=self.coloringpartialwater_nucleation(clusterthreshold_smallpores)
        water_largepores=self.coloringpartialwater_largeporesfirst_noanotherthreshold(clusterthreshold_largepores)
        im_sld_withwater = water_thinfilm + water_smallpores +  water_largepores
        warning=np.count_nonzero(im_sld_withwater)/im_sld_withwater.size
        
        im_sld_withwater[im_sld_withwater>0]=9.447*(10**10)#9.447*(10**10)
        im_sld_withwater = im_sld_withwater + im_sld_nowater
        im_sld_withwater[(im_sld_withwater > self.SLD_solid)&(im_sld_withwater < 125*(10**10))] = self.SLD_solid
        im_sld_withwater[im_sld_withwater >= 126*(10**10)] = 126*(10**10)
        gc.collect(generation=2)
        return im_sld_withwater      
    
        

     
        
    def coloringpartialwater_nucleation(self,clustersizethreshold): 
        regions2 = self.geodist
        regions4 =  copy.deepcopy(regions2)               
        regions4=np.where((regions2<=clustersizethreshold)&(0<regions2),5,0)
        gc.collect(generation=2)
        return regions4
    
    def minimizeobjective_puttingwater_noanotherthreshold_nucleation(self,niceparams):
        CL_carbon_Pt=self.im_Pt
        iteration_thinfilm=list(niceparams.values())[0]
        clusterthreshold_smallpores=list(niceparams.values())[1]
        clusterthreshold_largepores=list(niceparams.values())[2]
        if (clusterthreshold_largepores < clusterthreshold_smallpores) :
            error = 1000

        else:    
            im_sld_nowater= CL_carbon_Pt
            water_thinfilm=self.coloringpartialwater_thinfilm_noanotherthreshold(iteration_thinfilm)
            water_smallpores=self.coloringpartialwater_nucleation(clusterthreshold_smallpores)
            water_largepores=self.coloringpartialwater_largeporesfirst_noanotherthreshold(clusterthreshold_largepores)
            im_sld_withwater = water_thinfilm + water_smallpores +  water_largepores
            warning=np.count_nonzero(im_sld_withwater)/im_sld_withwater.size
            
            im_sld_withwater[im_sld_withwater>0]=9.447*(10**10)#9.447*(10**10)
            im_sld_withwater = im_sld_withwater + im_sld_nowater
            im_sld_withwater[(im_sld_withwater > self.SLD_solid)&(im_sld_withwater < 125*(10**10))] = self.SLD_solid
            im_sld_withwater[im_sld_withwater >= 126*(10**10)] = 126*(10**10)
            
            if warning > 0.68:
                error = 1000000
                #print('error f')
            else:
                
                I_img2sas_water,q_img2sas_water=img2sas(im_sld_withwater,self.boxsize,self.onevoxel)
                qtarget = self.q_exp_wet
                Itarget = self.I_exp_wet
                q_img2sas_filter, Itargetspline,I_img2sas_waterfilter= self.splining(qtarget,Itarget,q_img2sas_water,I_img2sas_water)
    
                    #error = residual(q_img2sas_filter,  np.log10(I_img2sas_dryfilter*K_img2sasdry*100)/np.log10(I_img2sas_waterfilter*K_img2sasdry*100), np.log10(Idryspline*100)/np.log10(Itargetspline*100), 1)
                error = self.residual(q_img2sas_filter, (I_img2sas_waterfilter)/(self.I_img2sas_dry), (Itargetspline)/(self.I_exp_dry), 1)
        score = -error*100
        gc.collect(generation=2)
        gc.collect()     
        return score
    
   
    
            
    def optimization_puttingwater_localthickness_evolutionstrategy_noanotherthreshold_nucleation(self,max_score=None, n_iter=100,**previousfit): 
        if 'watershed' in self.__dict__:
            regions2=self.watershed
    
        else:    
            self.trim_localthickness(self.im_Pt)
            regions2=self.watershed
            print('no initial watershed')
        
        if 'geodist' in self.__dict__:
            regions3=self.geodist
    
        else:    
            self.trim_localthickness(self.im_Pt)
            regions3=self.geodist
            print('no initial Pt geodist')
                
            
        regions2=self.watershed
        rangeforwatersaturation = np.unique(regions2)
        rangeforwatersaturation=np.append(rangeforwatersaturation,0)
        rangeforwatersaturation = np.asarray(natsort.natsorted(rangeforwatersaturation,reverse=False))
        descendrangeforwatersaturation = np.asarray(natsort.natsorted(rangeforwatersaturation,reverse=True))
        
        regions3=self.geodist
        
        cc = chain(np.linspace(0,10,101,endpoint=False), np.arange(15,100,1),np.arange(100,300,1))
        cc=list(cc)
        #vv=[(4/3)*np.pi*((2*item)**3 )for item in cc]
        print("binning success ")
        histogramofporesize = np.histogram(np.ndarray.flatten(regions3),bins=cc)
        
        binnedporesize = histogramofporesize[1][1:]
        counts = histogramofporesize[0][0:]
        rangeforwatersaturation = binnedporesize[counts!=0]
        rangeforwatersaturation=np.append(rangeforwatersaturation,0)



        opt_space = {'thinfilm' : [0,1,2,3],
        
        'smallpores' : list(rangeforwatersaturation),
        
        'largepores' : list(descendrangeforwatersaturation)}
        
        hyper = Hyperactive()
        optimizer = EvolutionStrategyOptimizer()
        early_stopping = {
                "n_iter_no_change": 300,
                "tol_abs": None,
                "tol_rel": None}
        
        #optimizer = EvolutionStrategyOptimizer()
        if ('search_data' in previousfit)&('previousbestpara' in previousfit):
            search_data= previousfit['search_data']
            previousbestpara= previousfit['previousbestpara']
            initialize = {'grid':8,'vertices':8,'random':6,'warm_start':[previousbestpara]}
            hyper.add_search(self.minimizeobjective_puttingwater_noanotherthreshold_nucleation, opt_space, optimizer=optimizer, n_iter=n_iter,early_stopping=early_stopping, memory="share",memory_warm_start=search_data,initialize=initialize,max_score=max_score)
        elif ('previousbestpara' in previousfit):   
            previousbestpara= previousfit['previousbestpara']
            initialize = {'grid':8,'vertices':8,'random':6,'warm_start':[previousbestpara]}
            hyper.add_search(self.minimizeobjective_puttingwater_noanotherthreshold_nucleation, opt_space, optimizer=optimizer, n_iter=n_iter,early_stopping=early_stopping, memory="share",initialize=initialize,max_score=max_score)
        else:
            initialize = {'grid':8,'vertices':8,'random':6}
            hyper.add_search(self.minimizeobjective_puttingwater_noanotherthreshold_nucleation, opt_space, optimizer=optimizer, n_iter=n_iter,early_stopping=early_stopping, memory="share",initialize=initialize,max_score=max_score)
        hyper.run()
        gc.collect(generation=2)
        self.opt_para = hyper.best_para(self.minimizeobjective_puttingwater_noanotherthreshold_nucleation)
        self.search_data = hyper.search_data(self.minimizeobjective_puttingwater_noanotherthreshold_nucleation)
        self.intensityandq = self.intensitywaterout_noanotherthreshold_nucleation(self.opt_para)
        gc.collect()
    

    
    def outgeodist(self,im):
        im_carbon=copy.deepcopy(im)
        im_carbon = np.where((im_carbon<125*10**10)&(im_carbon>0),1,0)
        im_carbon = scipy.ndimage.binary_erosion(im_carbon,iterations=3)    
        nucleationpoints = copy.deepcopy(im)
        nucleationpoints[nucleationpoints==0]=1
        nucleationpoints[nucleationpoints>124*10**10]=-1
        mask = np.where(im_carbon>1, True,False )
        nucleationpoints = np.ma.MaskedArray(nucleationpoints, mask)
        im_distance = skfmm.distance(nucleationpoints)
        im_distance_backgroundsubs = np.where((im>17*10**10),0,im_distance)

        self.geodist= im_distance_backgroundsubs
            
    def optimization_puttingwater_localthickness_noanotherthreshold_nucleation(self,max_score=None, n_iter=100,**previousfit): 
        if 'watershed' in self.__dict__:
            regions2=self.watershed
    
        else:    
            self.trim_localthickness(self.im_Pt)
            regions2=self.watershed
            print('no initial watershed')
        
        if 'geodist' in self.__dict__:
            regions3=self.geodist
    
        else:    
            self.outgeodist(self.im_Pt)
            regions3=self.geodist
            print('no initial Pt geodist')
                
            
        regions2=self.watershed
        rangeforwatersaturation = np.unique(regions2)
        rangeforwatersaturation=np.append(rangeforwatersaturation,0)
        rangeforwatersaturation = np.asarray(natsort.natsorted(rangeforwatersaturation,reverse=False))
        descendrangeforwatersaturation = np.asarray(natsort.natsorted(rangeforwatersaturation,reverse=True))
        
        regions3=self.geodist
        
        cc = chain(np.linspace(0,10,101,endpoint=False), np.arange(15,100,1),np.arange(100,300,1))
        cc=list(cc)
        #vv=[(4/3)*np.pi*((2*item)**3 )for item in cc]
        print("binning success ")
        histogramofporesize = np.histogram(np.ndarray.flatten(regions3),bins=cc)
        
        binnedporesize = histogramofporesize[1][1:]
        counts = histogramofporesize[0][0:]
        rangeforwatersaturation = binnedporesize[counts!=0]
        rangeforwatersaturation=np.append(rangeforwatersaturation,0)





        opt_space = {'thinfilm' : [0,1,2,3],
        
        'smallpores' : list(rangeforwatersaturation),
        
        'largepores' : list(descendrangeforwatersaturation)}
        
        hyper = Hyperactive()
        optimizer = TreeStructuredParzenEstimators()
        early_stopping = {
                "n_iter_no_change": 300,
                "tol_abs": None,
                "tol_rel": None}
        
        #optimizer = EvolutionStrategyOptimizer()
        if ('search_data' in previousfit)&('previousbestpara' in previousfit):
            search_data= previousfit['search_data']
            previousbestpara= previousfit['previousbestpara']
            initialize = {'grid':8,'vertices':8,'random':6,'warm_start':[previousbestpara]}
            hyper.add_search(self.minimizeobjective_puttingwater_noanotherthreshold_nucleation, opt_space, optimizer=optimizer, n_iter=n_iter,early_stopping=early_stopping, memory="share",memory_warm_start=search_data,initialize=initialize,max_score=max_score)
        elif ('previousbestpara' in previousfit):   
            previousbestpara= previousfit['previousbestpara']
            initialize = {'grid':8,'vertices':8,'random':6,'warm_start':[previousbestpara]}
            hyper.add_search(self.minimizeobjective_puttingwater_noanotherthreshold_nucleation, opt_space, optimizer=optimizer, n_iter=n_iter,early_stopping=early_stopping, memory="share",initialize=initialize,max_score=max_score)
        else:
            initialize = {'grid':8,'vertices':8,'random':6}
            hyper.add_search(self.minimizeobjective_puttingwater_noanotherthreshold_nucleation, opt_space, optimizer=optimizer, n_iter=n_iter,early_stopping=early_stopping, memory="share",initialize=initialize,max_score=max_score)
        hyper.run()
        gc.collect(generation=2)
        self.opt_para = hyper.best_para(self.minimizeobjective_puttingwater_noanotherthreshold_nucleation)
        self.search_data = hyper.search_data(self.minimizeobjective_puttingwater_noanotherthreshold_nucleation)
        self.intensityandq = self.intensitywaterout_noanotherthreshold_nucleation(self.opt_para)
        gc.collect()
        
        
    def intensitywaterout_noanotherthreshold_nucleation(self,niceparams):
        CL_carbon_Pt=self.im_Pt
        iteration_thinfilm=list(niceparams.values())[0]
        clusterthreshold_smallpores=list(niceparams.values())[1]
        clusterthreshold_largepores=list(niceparams.values())[2]
      
        im_sld_nowater= CL_carbon_Pt
        water_thinfilm=self.coloringpartialwater_thinfilm_noanotherthreshold(iteration_thinfilm)
        water_smallpores=self.coloringpartialwater_nucleation(clusterthreshold_smallpores)
        water_largepores=self.coloringpartialwater_largeporesfirst_noanotherthreshold(clusterthreshold_largepores)
        im_sld_withwater = water_thinfilm + water_smallpores +  water_largepores
        warning=np.count_nonzero(im_sld_withwater)/im_sld_withwater.size
        
        im_sld_withwater[im_sld_withwater>0]=9.447*(10**10)#9.447*(10**10)
        im_sld_withwater = im_sld_withwater + im_sld_nowater
        im_sld_withwater[(im_sld_withwater > self.SLD_solid)&(im_sld_withwater < 125*(10**10))] = self.SLD_solid
        im_sld_withwater[im_sld_withwater >= 126*(10**10)] = 126*(10**10)
        
        
        I_img2sas_water,q_img2sas_water=img2sas(im_sld_withwater,self.boxsize,self.onevoxel)
        qtarget = self.q_exp_wet
        Itarget = self.I_exp_wet
        q_img2sas_filter, Itargetspline,I_img2sas_waterfilter= self.splining(qtarget,Itarget,q_img2sas_water,I_img2sas_water)

            #error = residual(q_img2sas_filter,  np.log10(I_img2sas_dryfilter*K_img2sasdry*100)/np.log10(I_img2sas_waterfilter*K_img2sasdry*100), np.log10(Idryspline*100)/np.log10(Itargetspline*100), 1)
        error = self.residual(q_img2sas_filter, (I_img2sas_waterfilter)/ (self.I_img2sas_dry),(Itargetspline)/ (self.I_exp_dry), 1)
        
        intensityandq = {'intensitywet_img2sas' : I_img2sas_waterfilter*self.K_img2sas,
                    'intensitywet_exp' : Itargetspline,
                    'q' : q_img2sas_filter,
                    'img_water': im_sld_withwater[:,:,250],'score':-error*100
                    }

        return intensityandq

