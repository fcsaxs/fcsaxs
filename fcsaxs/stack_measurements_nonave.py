#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 20 07:55:13 2021

@author: xu_l
"""

from hyperactive.optimizers import TreeStructuredParzenEstimators, BayesianOptimizer, EvolutionStrategyOptimizer
from scipy.signal import savgol_filter
from hyperactive import Hyperactive
from porespy.tools import ps_ball
from matplotlib_scalebar.scalebar import ScaleBar
from matplotlib import colors
from matplotlib import cm
import random
import tifffile
from skimage.measure import label
from scipy import signal
from skimage.morphology import disk, dilation, rectangle
import numpy as np
import pandas as pd
import porespy as ps
import matplotlib.pyplot as plt
from matplotlib import pylab
from skimage.morphology import disk, ball

import tifffile as tif
import copy
from scipy import ndimage

from skimage.transform import rescale, resize, downscale_local_mean
import scipy.ndimage as spim

from functions import img2sas
import functions
data = np.loadtxt(
    '/mnt/nas_Uwrite/AK54/KinanSAXSfiles/Kinantifffromporespy/fcsaxs_versioncontrol/fcsaxs-master/fcsaxs/nonmodif_scaled.txt')
data = data[~np.isnan(data).any(axis=1)]
qexp = data[:, 0]
Iexpdry = data[:, 1]

data = np.loadtxt(
    '/mnt/nas_Uwrite/AK54/KinanSAXSfiles/Kinantifffromporespy/fcsaxs_versioncontrol/fcsaxs-master/fcsaxs/modif_scaled.txt')
data = data[~np.isnan(data).any(axis=1)]
qexpmodif = data[:, 0]
Iexpdrymodif = data[:, 1]
plt.loglog(qexp, Iexpdrymodif, qexp, Iexpdry)


def window3d(w):
    # Convert a 1D filtering kernel to 3D
    # eg, window3D(numpy.hanning(5))
    L = w.shape[0]
    m1 = np.outer(np.ravel(w), np.ravel(w))
    win1 = np.tile(m1, np.hstack([L, 1, 1]))
    m2 = np.outer(np.ravel(w), np.ones([1, L]))
    win2 = np.tile(m2, np.hstack([L, 1, 1]))
    win2 = np.transpose(win2, np.hstack([1, 2, 0]))
    win = np.multiply(win1, win2)
    return win


img_PtKB = tif.imread('/mnt/nas_Uwrite/XL54/TEM_PtKB/stack_measurements.tif')
#img_sec = img_NNMC[:,313:547, 362:596]
#img_sec = img_NNMC[0:200,230:430, 230:430]
img_PtKB = img_PtKB.astype('int')
win = window3d(signal.tukey(753, alpha=0.02))
win = resize(win, [234, 640, 753])
img_PtKB[img_PtKB == 217] = 12.6 * (10**11)
img_PtKB[img_PtKB == 65] = 1.756 * (10**11)
img_PtKB[img_PtKB == 43] = 1.630 * (10**11)
img_PtKB_win = img_PtKB
img_tile = np.tile(img_PtKB, (round(753/234), round(753/640), 1))
img_tile_win = np.tile(img_PtKB_win, (round(753/234), round(753/640), 1))
img_tile_win_sec = img_tile_win[:640, :640, :640]
img_tile_sec = img_tile[:640, :640, :640]


img_PtKB_ionomerwet = tif.imread(
    '/mnt/nas_Uwrite/XL54/TEM_PtKB/stack_measurements.tif')
#img_sec = img_NNMC[:,313:547, 362:596]
#img_sec = img_NNMC[0:200,230:430, 230:430]
img_PtKB_ionomerwet = img_PtKB_ionomerwet.astype('int')
win = window3d(signal.tukey(753, alpha=0.02))
win = resize(win, [234, 640, 753])
img_PtKB_ionomerwet[img_PtKB_ionomerwet == 217] = 12.6 * (10**11)
img_PtKB_ionomerwet[img_PtKB_ionomerwet == 65] = 1.756 * (10**11)
img_PtKB_ionomerwet[img_PtKB_ionomerwet == 43] = 1.630 * (10**11)
# img_PtKB_ionomerwet[img_PtKB_ionomerwet ==0] =9.447* (10**10)
img_PtKB_ionomerwet_win = img_PtKB_ionomerwet
img_tile_ionomerwet = np.tile(
    img_PtKB_ionomerwet, (round(753/234), round(753/640), 1))
img_tile_win_ionomerwet = np.tile(
    img_PtKB_ionomerwet_win, (round(753/234), round(753/640), 1))
img_tile_win_sec_ionomerwet = img_tile_win_ionomerwet[:640, :640, :640]
img_tile_sec_ionomerwet = img_tile_ionomerwet[:640, :640, :640]


functions.padding_or_not(0)
functions.cpu_or_not(0)
params_img2sas = 640, 0.4
I_ionomerdry, q_ionomerdry = img2sas(img_tile_sec, 640, 0.4)

params_img2sas = 640, 0.4
I_ionomerwet, q_ionomerwet = img2sas(img_tile_sec_ionomerwet, 640, 0.4)

plt.loglog(q_ionomerwet, I_ionomerwet/I_ionomerdry, c='blue')
plt.loglog(q_ionomerdry, I_ionomerdry/I_ionomerdry, c='red')
plt.yscale('linear')
plt.ylim([0, 2])
plt.legend()
plt.show()

# import porespy as ps

# im = ps.generators.blobs([512,512,512],0.5)

# I,q = img2sas(im,512,0.5)
# plt.loglog(q,I)


# %%


def find_scalingfactor(expq, expI, analq, analI, qval=0.8):
    idx = (np.abs(expq - qval)).argmin()
    K1 = expI[idx]
    idx = (np.abs(analq - qval)).argmin()
    K2 = analI[idx]
    return K1/K2


whichlocation = []
q_img2sas_collection = []
radialmean_img2sas_collection = []
scalingfactor_collection = []


img_tile = np.tile(img_PtKB, (round(753/234), round(753/640), 1))

img_tile_win = np.tile(img_PtKB_win, (round(753/234), round(753/640), 1))
plt.figure()
plt.imshow(img_tile_win[:, :, 5])

plt.figure()
plt.imshow(img_tile[:, :, 5])

img_tile_win_sec = img_tile_win[:640, :640, :640]
img_tile_sec = img_tile[:640, :640, :640]

img_sec_resized = resize(img_tile_win_sec, [64, 64, 64])
img_sec_resized[img_sec_resized > 0] = 1
img_sec_resized = np.tile(img_sec_resized, [10, 10, 10])
mask = (img_sec_resized == 0) & (img_tile_win_sec == 1.756 * (10**11))
imask = np.logical_not(mask)
img_tile_win_secwithmesoporestile = np.where(imask, img_tile_win_sec, 0)


# %%
onevoxel = 0.4
img_tile_win_sec_Ptonly = copy.deepcopy(img_PtKB)
img_tile_win_sec_Ptonly[img_tile_win_sec_Ptonly <= 12.3 * (10**11)] = 0
# noPtatall = img_tile_win_sec-img_tile_win_sec_Ptonly


# img_tile = np.tile(img_PtKB,(round(753/234),round(753/640),1))
# img_tile_win = np.tile(img_PtKB_win,(round(753/234),round(753/640),1))
# img_tile_win_sec = img_tile_win[:640,:640,:640]
# img_tile_sec = img_tile[:640,:640,:640]


Pt_only = label(img_tile_win_sec_Ptonly)
label = np.ndarray.flatten(Pt_only)
label = np.unique(label)

vol_Pt_array = copy.deepcopy(Pt_only.astype(float))

vols = ps.metrics.region_volumes(regions=Pt_only, mode='voxel')
regions_vol = np.copy(Pt_only)
for i in range(1, len(vols)+1):
    mask = np.array(np.where(regions_vol == i, regions_vol, 0), dtype=bool)
    regions_vol[mask] = vols[i-1]


regions_vol[regions_vol == 3] = 0
vol_Pt_array = regions_vol.copy()

vol_Pt_array = np.tile(vol_Pt_array, (round(753/234), round(753/640), 1))
vol_Pt_array = vol_Pt_array[:640, :640, :640]

vol_Pt_array = vol_Pt_array.astype('float')
vol_Pt_array *= 0.4**3

functions.export_tiff('tem_Pt_vol.tif', vol_Pt_array)
# %%
vol_Pt_array = tifffile.imread(
    '/mnt/nas_Uwrite/AK54/KinanSAXSfiles/Kinantifffromporespy/fcsaxs_versioncontrol/fcsaxs-master/fcsaxs/tem_Pt_vol.tif')
volume_Pt_sorted = np.sort(np.unique(vol_Pt_array))


# %%%
Idry, q = img2sas(img_tile_win_sec, 640, 0.4)


img_tile_win_sec_Ptonly = copy.deepcopy(img_tile_win_sec)
img_tile_win_sec_Ptonly[img_tile_win_sec_Ptonly <= 12.3 * (10**11)] = 0
# %%


def mesoporesonly(n):
    vol_Pt_forPtripe = copy.deepcopy(vol_Pt_array)
    vol_Pt_forPtgone = copy.deepcopy(vol_Pt_array)
    vol_Pt_forPtburn = copy.deepcopy(vol_Pt_array)

    # thresholdvolumePt_burn = volume_Pt_sorted[4000]
    # thresholdvolumePt_gone = volume_Pt_sorted[3500]
    # thresholdvolumePt_ripe = volume_Pt_sorted[5420]

    thresholdvolumePt_burn = volume_Pt_sorted[50]
    thresholdvolumePt_gone = volume_Pt_sorted[200]
    thresholdvolumePt_ripe = volume_Pt_sorted[600]

    basefordilation = copy.deepcopy(img_tile_win_sec_Ptonly)
    basefordilation[basefordilation > 0] = 1

    # rectanglestrel_v = np.zeros([3, 3, 3])
    # rectanglestrel_v[ 1:2,:, 1] = 1
    # rectanglestrel_v[ :,:, 1] = 1
    # structure=rectanglestrel_v,
    # allplatinumdilated = np.zeros([640,640,640])
    # allplatinum = np.zeros([640,640,640])

    temp = copy.deepcopy(vol_Pt_forPtburn)
    #thresholdvolumePt_burn = volume_Pt_sorted[3700]

    #radiusthresholdvolumePt = (thresholdvolumePt*3/(np.pi*4))**(1/3)

    temp = np.where((temp > 0) & (temp < thresholdvolumePt_burn), 1000000, 0)
    allplatinum = np.where(temp == 1000000, 1, 0)
    dilated = ps.filters.fftmorphology(
        allplatinum, ps.tools.ps_ball(n), mode='dilation').astype(float)
    allplatinumdilated = dilated - allplatinum

    allplatinumdilated[allplatinumdilated > 1] = 1

    mesopores = allplatinumdilated
    mask = (mesopores == 1) & (img_tile_win_sec == 1.756 * (10**11))
    imask = np.logical_not(mask)

    img_tile_win_secwithmesopores_oxi = np.where(imask, img_tile_win_sec, 0)
    Imodif, q = img2sas(img_tile_win_secwithmesopores_oxi, 640, 0.4)
    return Imodif, q, img_tile_win_secwithmesopores_oxi[:, 200, :]


n = np.arange(0, 15)
resultmesoporesonly = []

for i in n:
    resultmesoporesonly.append(mesoporesonly(i))
# %%


def realspace(q):
    return 2*np.pi/q


def reciprocal(q):
    return 2*np.pi/q


cmap = cm.get_cmap('cool', len(n))


fig, ax = plt.subplots(1, 2, figsize=[10, 10], dpi=300)

ax2a = ax[0].secondary_xaxis(
    location='top', functions=(reciprocal, realspace))

K = ((sum(Iexpdry[qexp < 4])/len(Iexpdry[qexp < 4])) /
     (sum(Idry[q < 4])/len(Idry[q < 4])))/0.5
ax[0].loglog(q, Idry*K, label="Initial TEM")

for i in n:
    ax[0].loglog(resultmesoporesonly[i][1], resultmesoporesonly[i][0]
                 * K, label='' + str("{:.2f}".format(i*0.4)) + ' nm', color=cmap(i))
ax[0].loglog(qexp, Iexpdry, label="Exp. Non-modif", color='C1')
ax[0].loglog(qexp, Iexpdrymodif, label="Exp. Modif", color='C2')
ax[0].set_ylabel('Intensity [a.u.]')
ax[0].set_xlabel('q [1/nm]')
ax2a.set_xlabel('Dimension (2$\pi$/q) [nm]')
ax[0].set_box_aspect(1)
ax[0].set_xlim([qexp[0], 4])
cmap = cm.get_cmap('cool', len(n))

for i in n:
    ax[1].loglog(resultmesoporesonly[i][1], resultmesoporesonly[i][0] /
                 Idry, label='' + str("{:.2f}".format(i*0.4)) + ' nm', color=cmap(i))
ax[1].loglog(qexp, Iexpdry/Iexpdry, label="Exp. Non-modif", color='C1')
ax[1].loglog(qexp, Iexpdrymodif/Iexpdry, label="Exp. Modif", color='C2')
ax2a = ax[1].secondary_xaxis(
    location='top', functions=(reciprocal, realspace))
ax2a.set_xlabel('Dimension (2$\pi$/q) [nm]')
ax[1].set_ylabel('Relative Intensity')
ax[1].set_xlabel('q [1/nm]')
ax[1].set_yscale('linear')
ax[1].set_ylim([0, 2])
ax[1].set_xlim([qexp[0], 4])
plt.legend(ncol=3, fontsize='small')
ax[1].set_box_aspect(1)
plt.savefig("Figmesopores_int.svg")
plt.savefig("Figmesopores_int.png")
# %%
uniquevalues = np.unique(resultmesoporesonly[i][2])
uniquevalues = np.sort(uniquevalues)

cmap = colors.ListedColormap(['white', 'green', 'black', 'red'])
boundaries = [uniquevalues[0]-1, uniquevalues[0]+1,
              uniquevalues[1]+1, uniquevalues[2]+1, uniquevalues[3]+1]
norm = colors.BoundaryNorm(boundaries, cmap.N, clip=True)

fig, ax = plt.subplots(1, 5, figsize=[15, 15], dpi=300)
j = 0
for i in range(0, len(n), 3):
    ax[j].imshow(resultmesoporesonly[i][2], cmap=cmap,
                 norm=norm, interpolation='nearest')
    ax[j].set_title('' + str("{:.2f}".format(i*0.4)) + ' nm')
    ax[j].axis('off')
    scalebar = ScaleBar(0.4, "nm", length_fraction=0.25)
    ax[j].add_artist(scalebar)
    j += 1
plt.tight_layout()
plt.savefig("Figmesopores_img.svg")
plt.savefig("Figmesopores_img.png")
# %%


def Ptonly(n):
    vol_Pt_forPtripe = copy.deepcopy(vol_Pt_array)
    vol_Pt_forPtgone = copy.deepcopy(vol_Pt_array)
    vol_Pt_forPtburn = copy.deepcopy(vol_Pt_array)

    # thresholdvolumePt_burn = volume_Pt_sorted[4000]
    # thresholdvolumePt_gone = volume_Pt_sorted[3500]
    # thresholdvolumePt_ripe = volume_Pt_sorted[5420]

    thresholdvolumePt_burn = volume_Pt_sorted[200]
    thresholdvolumePt_gone = volume_Pt_sorted[200]
    thresholdvolumePt_ripe = volume_Pt_sorted[400]

    basefordilation = copy.deepcopy(img_tile_win_sec_Ptonly)
    basefordilation[basefordilation > 0] = 1

    # rectanglestrel_v = np.zeros([3, 3, 3])
    # rectanglestrel_v[ 1:2,:, 1] = 1
    # rectanglestrel_v[ :,:, 1] = 1
    # structure=rectanglestrel_v,
    # allplatinumdilated = np.zeros([640,640,640])
    # allplatinum = np.zeros([640,640,640])

    temp = copy.deepcopy(vol_Pt_forPtburn)
    #thresholdvolumePt_burn = volume_Pt_sorted[3700]

    #radiusthresholdvolumePt = (thresholdvolumePt*3/(np.pi*4))**(1/3)

    temp = np.where((temp > 0) & (temp > thresholdvolumePt_ripe), 1000000, 0)
    allplatinum = np.where(temp == 1000000, 1, 0)
    dilated = ps.filters.fftmorphology(
        allplatinum, ps.tools.ps_ball(n), mode='dilation').astype(float)
    allplatinumdilated = dilated - allplatinum

    allplatinumdilated[allplatinumdilated > 1] = 1

    ptripe = allplatinumdilated
    mask = (ptripe == 1) & (img_tile_win_sec == 1.756 * (10**11))
    imask = np.logical_not(mask)

    img_tile_win_secptripe = np.where(mask, 126*10**10, img_tile_win_sec)
    Imodif, q = img2sas(img_tile_win_secptripe, 640, 0.4)
    return Imodif, q, img_tile_win_secptripe[:, 200, :]


n = np.arange(1, 6)
resultptonly = []
for i in n:
    resultptonly.append(Ptonly(i))
# %%


def realspace(q):
    return 2*np.pi/q


def reciprocal(q):
    return 2*np.pi/q


cmap = cm.get_cmap('cool', len(n))


fig, ax = plt.subplots(1, 2, figsize=[10, 10], dpi=300)

ax2a = ax[0].secondary_xaxis(
    location='top', functions=(reciprocal, realspace))

K = ((sum(Iexpdry[qexp < 4])/len(Iexpdry[qexp < 4])) /
     (sum(Idry[q < 4])/len(Idry[q < 4])))/0.5
ax[0].loglog(q, Idry*K, label="Initial TEM")

for i in range(len(n)):
    ax[0].loglog(resultptonly[i][1], resultptonly[i][0]*K, label='' +
                 str("{:.2f}".format(n[i]*0.4)) + ' nm', color=cmap(i))
ax[0].loglog(qexp, Iexpdry, label="Exp. Non-modif", color='C1')
ax[0].loglog(qexp, Iexpdrymodif, label="Exp. Modif", color='C2')
ax[0].set_ylabel('Intensity [a.u.]')
ax[0].set_xlabel('q [1/nm]')
ax2a.set_xlabel('Dimension (2$\pi$/q) [nm]')
ax[0].set_box_aspect(1)
ax[0].set_xlim([qexp[0], 4])
cmap = cm.get_cmap('cool', len(n))

for i in range(len(n)):
    ax[1].loglog(resultptonly[i][1], resultptonly[i][0]/Idry,
                 label='' + str("{:.2f}".format(n[i]*0.4)) + ' nm', color=cmap(i))
ax[1].loglog(qexp, Iexpdry/Iexpdry, label="Exp. Non-modif", color='C1')
ax[1].loglog(qexp, Iexpdrymodif/Iexpdry, label="Exp. Modif", color='C2')
ax2a = ax[1].secondary_xaxis(
    location='top', functions=(reciprocal, realspace))
ax2a.set_xlabel('Dimension (2$\pi$/q) [nm]')
ax[1].set_ylabel('Relative Intensity')
ax[1].set_xlabel('q [1/nm]')
ax[1].set_yscale('linear')
ax[1].set_ylim([0, 2])
ax[1].set_xlim([qexp[0], 4])
plt.legend(ncol=3, fontsize='small')
ax[1].set_box_aspect(1)
plt.savefig("Figpt_int.svg")
plt.savefig("Figpt_int.png")
# %%
uniquevalues = np.unique(resultptonly[i][2])
uniquevalues = np.sort(uniquevalues)

cmap = colors.ListedColormap(['white', 'green', 'black', 'red'])
boundaries = [uniquevalues[0]-1, uniquevalues[0]+1,
              uniquevalues[1]+1, uniquevalues[2]+1, uniquevalues[3]+1]
norm = colors.BoundaryNorm(boundaries, cmap.N, clip=True)

fig, ax = plt.subplots(1, 5, figsize=[15, 15], dpi=300)
j = 0
for i in range(0, len(n)):
    ax[j].imshow(resultptonly[i][2], cmap=cmap,
                 norm=norm, interpolation='nearest')
    ax[j].set_title('' + str("{:.2f}".format(n[i]*0.4)) + ' nm')
    ax[j].axis('off')
    scalebar = ScaleBar(0.4, "nm", length_fraction=0.25)
    ax[j].add_artist(scalebar)
    j += 1
plt.tight_layout()
plt.savefig("Figpt_img.svg")
plt.savefig("Figpt_img.png")

# %%


Idry, q = img2sas(img_tile_win_sec, 640, 0.4)
# %%


def Pterodeonly(n):
    vol_Pt_forPtripe = copy.deepcopy(vol_Pt_array)
    vol_Pt_forPtgone = copy.deepcopy(vol_Pt_array)
    vol_Pt_forPtburn = copy.deepcopy(vol_Pt_array)

    # thresholdvolumePt_burn = volume_Pt_sorted[4000]
    # thresholdvolumePt_gone = volume_Pt_sorted[3500]
    # thresholdvolumePt_ripe = volume_Pt_sorted[5420]

    thresholdvolumePt_burn = volume_Pt_sorted[200]
    thresholdvolumePt_gone = volume_Pt_sorted[200]
    thresholdvolumePt_ripe = volume_Pt_sorted[600]

    basefordilation = copy.deepcopy(img_tile_win_sec_Ptonly)
    basefordilation[basefordilation > 0] = 1

    # rectanglestrel_v = np.zeros([3, 3, 3])
    # rectanglestrel_v[ 1:2,:, 1] = 1
    # rectanglestrel_v[ :,:, 1] = 1
    # structure=rectanglestrel_v,
    # allplatinumdilated = np.zeros([640,640,640])
    # allplatinum = np.zeros([640,640,640])

    temp = copy.deepcopy(vol_Pt_forPtburn)
    #thresholdvolumePt_burn = volume_Pt_sorted[3700]

    #radiusthresholdvolumePt = (thresholdvolumePt*3/(np.pi*4))**(1/3)

    temp = np.where((temp > 0) & (temp < thresholdvolumePt_gone), 1000000, 0)
    allplatinum = np.where(temp == 1000000, 1, 0)
    eroded = ps.filters.fftmorphology(
        allplatinum, ps.tools.ps_ball(n), mode='erosion').astype(float)
    allplatinumeroded = allplatinum-eroded

    allplatinumeroded[allplatinumeroded > 1] = 1

    ptgone = allplatinumeroded
    mask = (ptgone == 1) & (img_tile_win_sec == 12.6 * (10**11))
    imask = np.logical_not(mask)

    img_tile_win_secptgone = np.where(mask, 0, img_tile_win_sec)
    Imodif, q = img2sas(img_tile_win_secptgone, 640, 0.4)
    return Imodif, q, img_tile_win_secptgone[:, 200, :]


n = np.arange(1, 6)
resultptonly = []

for i in n:
    resultptonly.append(Pterodeonly(i))

# %%Q


def realspace(q):
    return 2*np.pi/q


def reciprocal(q):
    return 2*np.pi/q


cmap = cm.get_cmap('cool', len(n))


fig, ax = plt.subplots(1, 2, figsize=[10, 10], dpi=300)

ax2a = ax[0].secondary_xaxis(
    location='top', functions=(reciprocal, realspace))

K = ((sum(Iexpdry[qexp < 4])/len(Iexpdry[qexp < 4])) /
     (sum(Idry[q < 4])/len(Idry[q < 4])))/0.5
ax[0].loglog(q, Idry*K, label="Initial TEM")

for i in range(len(n)):
    ax[0].loglog(resultptonly[i][1], resultptonly[i][0]*K, label='' +
                 str("{:.2f}".format(n[i]*0.4)) + ' nm', color=cmap(i))
ax[0].loglog(qexp, Iexpdry, label="Exp. Non-modif", color='C1')
ax[0].loglog(qexp, Iexpdrymodif, label="Exp. Modif", color='C2')
ax[0].set_ylabel('Intensity [a.u.]')
ax[0].set_xlabel('q [1/nm]')
ax2a.set_xlabel('Dimension (2$\pi$/q) [nm]')
ax[0].set_box_aspect(1)
ax[0].set_xlim([qexp[0], 4])
cmap = cm.get_cmap('cool', len(n))

for i in range(len(n)):
    ax[1].loglog(resultptonly[i][1], resultptonly[i][0]/Idry,
                 label='' + str("{:.2f}".format(n[i]*0.4)) + ' nm', color=cmap(i))
ax[1].loglog(qexp, Iexpdry/Iexpdry, label="Exp. Non-modif", color='C1')
ax[1].loglog(qexp, Iexpdrymodif/Iexpdry, label="Exp. Modif", color='C2')
ax2a = ax[1].secondary_xaxis(
    location='top', functions=(reciprocal, realspace))
ax2a.set_xlabel('Dimension (2$\pi$/q) [nm]')
ax[1].set_ylabel('Relative Intensity')
ax[1].set_xlabel('q [1/nm]')
ax[1].set_yscale('linear')
ax[1].set_ylim([0, 2])
ax[1].set_xlim([qexp[0], 4])
plt.legend(ncol=3, fontsize='small')
ax[1].set_box_aspect(1)
plt.savefig("Figpterode_int.svg")
plt.savefig("Figpterode_int.png")
# %%
uniquevalues = np.unique(resultptonly[i][2])
uniquevalues = np.sort(uniquevalues)

cmap = colors.ListedColormap(['white', 'green', 'black', 'red'])
boundaries = [uniquevalues[0]-1, uniquevalues[0]+1,
              uniquevalues[1]+1, uniquevalues[2]+1, uniquevalues[3]+1]
norm = colors.BoundaryNorm(boundaries, cmap.N, clip=True)

fig, ax = plt.subplots(1, 5, figsize=[15, 15], dpi=300)
j = 0
for i in range(0, len(n)):
    ax[j].imshow(resultptonly[i][2], cmap=cmap,
                 norm=norm, interpolation='nearest')
    ax[j].set_title('' + str("{:.2f}".format(n[i]*0.4)) + ' nm')
    ax[j].axis('off')
    scalebar = ScaleBar(0.4, "nm", length_fraction=0.25)
    ax[j].add_artist(scalebar)
    j += 1
plt.tight_layout()
plt.savefig("Figpterode_img.svg")
plt.savefig("Figpterode_img.png")

# %%


def combineburngoneripe(params):

    vol_burn, vol_gone, vol_ripe, burn_param, gone_param, ripe_param = params['vol_burn'], params[
        'vol_gone'], params['vol_ripe'], params['burn_param'], params['gone_param'], params['ripe_param']
    # print(gone_param)
    if vol_gone <= vol_ripe:
        vol_Pt_forPtripe = copy.deepcopy(vol_Pt_array)
        vol_Pt_forPtgone = copy.deepcopy(vol_Pt_array)
        vol_Pt_forPtburn = copy.deepcopy(vol_Pt_array)

        thresholdvolumePt_burn = volume_Pt_sorted[vol_burn]
        thresholdvolumePt_gone = volume_Pt_sorted[vol_gone]
        thresholdvolumePt_ripe = volume_Pt_sorted[vol_ripe]

        # basefordilation = copy.deepcopy(img_tile_win_sec_Ptonly)
        # basefordilation[basefordilation > 0] = 1

        temp = copy.deepcopy(vol_Pt_forPtburn)

        temp = np.where((temp > 0) & (
            temp < thresholdvolumePt_burn), 1000000, 0)
        allplatinum = np.where(temp == 1000000, 1, 0)

        if (burn_param == [0]) | (burn_param == 0):
            img3 = img_tile_win_sec.copy()

        elif (burn_param != 0):

            dilated = ps.filters.fftmorphology(allplatinum, ps.tools.ps_ball(
                burn_param), mode='dilation').astype(float)
            allplatinumdilated = dilated - allplatinum
            allplatinumdilated[allplatinumdilated > 1] = 1

            mesopores = allplatinumdilated
            mask = (mesopores == 1) & (img_tile_win_sec == 1.756 * (10**11))
            imask = np.logical_not(mask)
            img3 = np.where(imask, img_tile_win_sec, 0)

        # plt.figure()
        # plt.imshow(img3[:,200,:])

        poreopenedwithoutPt = copy.deepcopy(img3)
        poreopenedwithoutPt[poreopenedwithoutPt == 12.6*(10**11)] = 0
        # plt.figure()
        # plt.imshow(poreopenedwithoutPt[:,200,:])
        allplatinum = np.zeros([640, 640, 640])

        temp = copy.deepcopy(vol_Pt_forPtgone)

        temp = np.where((temp > 0) & (
            temp < thresholdvolumePt_gone), 5000000, 0)
        allplatinum = np.where(temp == 5000000, 1, 0)

        if (gone_param == [0]) | (gone_param == 0):
            img4 = np.zeros(allplatinum.shape)
            img4 = allplatinum.astype('float')
            img4[img4 > 0] = 12.6*(10**11)
            # print(str('no gone param'))

        elif (gone_param != 0):
            # allplatinumdil = ps.filters.fftmorphology(allplatinum, ps_ball(gone_param),mode='dilation').astype('float')
            # pttoc = allplatinumdil-allplatinum
            # pttoc[pttoc >0]= 1.756 * (10**11)
            # img4=allplatinum.astype('float')
            # img4[img4>0]=12.6*(10**11)
            # img4+=pttoc
            allplatinum = ps.filters.fftmorphology(
                allplatinum, ps_ball(gone_param), mode='erosion').astype('float')
            # pttoc = allplatinumdil-allplatinum
            # pttoc[pttoc >0]= 1.756 * (10**11)
            img4 = allplatinum.astype('float')
            img4[img4 > 0] = 12.6*(10**11)
            # img4+=pttoc

        # plt.figure()
        # plt.imshow(img4[:,200,:])
        # print(np.unique(img4))

        temp = copy.deepcopy(vol_Pt_forPtripe)

        temp = np.where((temp > 0) & (
            temp > thresholdvolumePt_ripe)& (
                temp != np.max(volume_Pt_sorted)), 5000000, 0)
        allplatinum = np.where(temp == 5000000, 1, 0)
        if (ripe_param == [0]) | (ripe_param == 0):
            img2 = allplatinum
            img2[img2 > 0] = 12.6*(10**11)

        else:
            allplatinumdilated = ps.filters.fftmorphology(
                allplatinum, ps_ball(ripe_param), mode='dilation').astype('float')
            img2 = allplatinumdilated
            img2[img2 > 0] = 12.6*(10**11)

        temp = copy.deepcopy(vol_Pt_forPtripe)

        unaffected = np.where(((temp > 0) &( temp <= thresholdvolumePt_ripe) & (
            temp >= thresholdvolumePt_gone))|((temp > 0) &(
                temp == np.max(volume_Pt_sorted))), 1, 0)
        unaffected[unaffected > 0] = 12.6*(10**11)
        img5 = unaffected
        # plt.figure()
        # plt.imshow(img5[:,200,:])

        img_all = poreopenedwithoutPt + img4 + img5 + img2

        img_all[img_all >= 12.6*(10**11)] = 12.6*(10**11)
        # plt.figure()
        # plt.imshow(img_all[:,200,:])
        I, q = img2sas(img_all, 640, 0.4)
        # print(np.unique(img5))
        # plt.figure()
        # plt.loglog(qexp[qexp<3.1],Iexpdry[qexp<3.1])
        # plt.loglog(q[q<3.1],Idry[q<3.1])

        qbin, Iexpbin, Iimg2sasbin = functions.binningq(
            qexp[qexp < 3.5], Iexpdry[qexp < 3.5], q[q < 3.5], Idry[q < 3.5])
        qbin, Iexpmodifbin, Iimg2sasmodifbin = functions.binningq(
            qexp[qexp < 3.5], Iexpdrymodif[qexp < 3.5], q[q < 3.5], I[q < 3.5])
        intensity_data = {"intensitymodif": Iimg2sasmodifbin,
                          "intensity": Iimg2sasbin, "q": qbin, "img": img_all[:, 200, :]}
        error = 0
        for i in range(len(qbin)):
            error += (((Iimg2sasmodifbin[i]/Iimg2sasbin[i]
                        )-(Iexpmodifbin[i]/Iexpbin[i])))**2
        error = error**(1/2)
        # plt.figure()
        # plt.loglog(qbin,Iimg2sasmodifbin/Iimg2sasbin)
        # plt.loglog(qbin,Iexpmodifbin/Iexpbin)
        # plt.yscale('linear')
        # plt.ylim([0,2])
        if -error < -1.5:
            return -error
        if -error >-1.5:
            return -error, intensity_data
    else:
        return -999
# %%

import numpy as np
search_space = {'vol_burn': np.arange(0, len(volume_Pt_sorted), 5),
                'vol_gone': np.arange(0, len(volume_Pt_sorted), 5),
                'vol_ripe': np.arange(0, len(volume_Pt_sorted), 5),
                'burn_param': np.arange(0, 12),
                'ripe_param': np.arange(0, 5),
                'gone_param': np.arange(0, 5)
                }

aaa = list(search_space.values())
bbb = list(search_space.keys())
i = 0
for key in bbb:
    if key == "scalingfactor":
        search_space[key] = [aaa[i]]
    else:
        search_space[key] = list(aaa[i])
    i += 1


def optimization(opt_space, n_iter=100, **previousfit):
    iter = 0
    previousresidual = -999999999
    hyper = Hyperactive(
        verbosity=["progress_bar", "print_results", "print_times"])
    # optimizer = TreeStructuredParzenEstimators()
    optimizer = EvolutionStrategyOptimizer()
    # optimizer=BayesianOptimizer()
    early_stopping = {
        "n_iter_no_change": 10000000,
        "tol_abs": None,
        "tol_rel": None
    }

    #optimizer = EvolutionStrategyOptimizer()
    if ('search_data' in previousfit) & ('previousbestpara' in previousfit):
        search_data = previousfit['search_data']
        previousbestpara = previousfit['previousbestpara']
        initialize = {'grid': 4, 'vertices': 4,
                      'random': 6, 'warm_start': [previousbestpara]}
        hyper.add_search(combineburngoneripe, opt_space, optimizer=optimizer, n_iter=n_iter,
                         early_stopping=early_stopping, memory_warm_start=search_data, initialize=initialize, memory="share")
    elif ('previousbestpara' in previousfit):
        previousbestpara = previousfit['previousbestpara']
        initialize = {'grid': 4, 'vertices': 4,
                      'random': 6, 'warm_start': [previousbestpara]}
        hyper.add_search(combineburngoneripe, opt_space, optimizer=optimizer, n_iter=n_iter,
                         early_stopping=early_stopping, initialize=initialize, memory="share")
    else:
        initialize = {'grid': 4, 'vertices': 4, 'random': 6}
        hyper.add_search(combineburngoneripe, opt_space, optimizer=optimizer, n_iter=n_iter, early_stopping=early_stopping, initialize=initialize, memory="share"
                         )
    hyper.run()
    opt_para = hyper.best_para(combineburngoneripe)
    search_data = hyper.search_data(combineburngoneripe)
    return opt_para, search_data


best_param = {'vol_burn': [400],  # 400 old #370new
              'vol_gone': [60],  # 60 old #59 new
              'vol_ripe': [632],  # 600old #487 new
              'burn_param': [6],
              'ripe_param': [3],
              'gone_param': [3]
              }
aaa = list(best_param.values())
bbb = list(best_param.keys())
i = 0
for key in bbb:
    if key == "scalingfactor":
        best_param[key] = [aaa[i]]
    else:
        best_param[key] = list(aaa[i])
    i += 1
previousfit = {}
previousfit['previousbestpara']=best_param
opt1, opt2 = optimization(search_space, n_iter=500, **previousfit)


# %%
best_param = {'vol_burn': [400],  # 400 old #370new
              'vol_gone': [100],  # 60 old #59 new
              'vol_ripe': [630],  # 600old #487 new
              'burn_param': [6],
              'ripe_param': [4],
              'gone_param': [3]
              }
# combineburngoneripe:  34%|███▎      | 67/200 [32:00<1:23:28, 37.66s/it, best_iter=24, best_pos=[469  67 653   6   4   3], best_score=-0.9453875755407695]
aaa = list(best_param.values())
bbb = list(best_param.keys())
i = 0
for key in bbb:
    if key == "scalingfactor":
        best_param[key] = [aaa[i]]
    else:
        best_param[key] = list(aaa[i])
    i += 1

# combineburngoneripe:  89%|████████▉ | 444/500 [3:43:52<37:32, 40.22s/it, best_iter=407, best_pos=[690  87 587   6   3   4], best_score=-0.8951829085691643]
#combineburngoneripe:  59%|█████▉    | 235/400 [3:28:26<27:41:20, 604.13s/it, best_iter=217, best_pos=[440  86 657   6   4   3], best_score=-1.5334664577728412]3#
error, intensitydata = combineburngoneripe(best_param)

# plt.figure()
# plt.loglog(intensitydata['q'],intensitydata['intensitymodif']/intensitydata['intensity'])
# plt.loglog(qexp[qexp<4],Iexpdrymodif[qexp<4]/Iexpdry[qexp<4])
# plt.yscale('linear')
# plt.ylim([0,2])

# plt.figure()
# plt.imshow(intensitydata['img'])

# opt_para=opt1

#%%
def realspace(q):
    return 2*np.pi/q


def reciprocal(q):
    return 2*np.pi/q


cmap = cm.get_cmap('cool', 2)


fig, ax = plt.subplots(1, 3, figsize=[16, 10], dpi=300)
# plt.title(str(best_param))
ax2a = ax[0].secondary_xaxis(
    location='top', functions=(reciprocal, realspace))

K = ((sum(Iexpdry[qexp < 4])/len(Iexpdry[qexp < 4])) /
     (sum(Idry[q < 4])/len(Idry[q < 4])))/0.5
ax[0].loglog(q, Idry*K, label="Initial TEM", color=cmap(0))
ax[0].loglog(intensitydata['q'],
             intensitydata['intensitymodif']*K, color=cmap(1))
ax[0].loglog(qexp, Iexpdry, label="Exp. Non-modif", color='C1')
ax[0].loglog(qexp, Iexpdrymodif, label="Exp. Modif", color='C2')
ax[0].set_ylabel('Intensity [a.u.]')
ax[0].set_xlabel('q [1/nm]')
ax2a.set_xlabel('Dimension (2$\pi$/q) [nm]')
ax[0].set_box_aspect(1)
ax[0].set_xlim([qexp[0], 3.1])


ax[1].loglog(intensitydata['q'], intensitydata['intensitymodif'] /
             intensitydata['intensity'], color=cmap(0), label="TEM img2sas Non-modif")
ax[1].loglog(intensitydata['q'], intensitydata['intensitymodif'] /
             intensitydata['intensity'], color=cmap(1), label="TEM img2sas Modif")
ax[1].loglog(qexp, Iexpdry/Iexpdry, label="Exp. Non-modif", color='C1')
ax[1].loglog(qexp, Iexpdrymodif/Iexpdry, label="Exp. Modif", color='C2')
ax2a = ax[1].secondary_xaxis(
    location='top', functions=(reciprocal, realspace))
ax2a.set_xlabel('Dimension (2$\pi$/q) [nm]')
ax[1].set_ylabel('Relative Intensity')
ax[1].set_xlabel('q [1/nm]')
ax[1].set_yscale('linear')
ax[1].set_ylim([0, 2])
ax[1].set_xlim([qexp[0], 3.1])
plt.legend(ncol=2, fontsize='small')
ax[1].set_box_aspect(1)

ax[2].hist(volume_Pt_sorted,bins=300)
ax[2].axvline(volume_Pt_sorted[best_param['vol_burn']],color='yellow',label="Pore opening by "+str(round(best_param['burn_param'][0]*0.4,2))+' nm')
ax[2].axvline(volume_Pt_sorted[best_param['vol_ripe']],color='green',label="Pt ripening by "+str(round(best_param['ripe_param'][0]*0.4,2))+' nm')
ax[2].axvline(volume_Pt_sorted[best_param['vol_gone']],color='red',label="Pt gone by "+str(round(best_param['gone_param'][0]*0.4,2))+' nm')
ax[2].set_box_aspect(1)
ax[2].set_ylabel("Frequency")
ax[2].set_xlabel("Pt volume [nm$^3$]")
ax[2].legend()

plt.savefig("Figptfit_int.svg")
plt.savefig("Figptfit_int.png")

uniquevalues = np.unique(intensitydata['img'])
uniquevalues = np.sort(uniquevalues)

cmap = colors.ListedColormap(['white', 'green', 'black', 'red'])
boundaries = [uniquevalues[0]-1, uniquevalues[0]+1,
              uniquevalues[1]+1, uniquevalues[2]+1, uniquevalues[3]+1]
norm = colors.BoundaryNorm(boundaries, cmap.N, clip=True)

fig, ax = plt.subplots(1, 2, figsize=[5, 10], dpi=300)

ax[0].imshow(img_tile_win_sec[:, 200, :], cmap=cmap,
             norm=norm, interpolation='nearest')
ax[0].axis('off')
scalebar = ScaleBar(0.4, "nm", length_fraction=0.25)
ax[0].add_artist(scalebar)
ax[0].set_title("Initial")

ax[1].imshow(intensitydata['img'], cmap=cmap,
             norm=norm, interpolation='nearest')
ax[1].axis('off')
scalebar = ScaleBar(0.4, "nm", length_fraction=0.25)
ax[1].add_artist(scalebar)
ax[1].set_title("Best Fit")








plt.tight_layout()
plt.savefig("Figptfit_img.svg")
plt.savefig("Figptfit_img.png")


# %%
img3 = np.where(imask, img_tile_win_sec, 0)
img1 = img_tile_win_sec


poreopenedwithoutPt = copy.deepcopy(img3)
poreopenedwithoutPt[poreopenedwithoutPt == 12.6*(10**11)] = 0


allplatinum = np.zeros([640, 640, 640])


temp = copy.deepcopy(vol_Pt_forPtgone)
#thresholdvolumePt_gone = volume_Pt_sorted[3700]

#radiusthresholdvolumePt = (thresholdvolumePt*3/(np.pi*4))**(1/3)

temp = np.where((temp > 0) & (temp < thresholdvolumePt_gone), 5000000, 0)
allplatinum = np.where(temp == 5000000, 1, 0)

img4 = allplatinum
img4[img4 > 0] = 12.6*(10**11)


allplatinum = np.zeros([640, 640, 640])

n = 1
temp = copy.deepcopy(vol_Pt_forPtripe)
#thresholdvolumePt_ripe = volume_Pt_sorted[5200]

#radiusthresholdvolumePt = (thresholdvolumePt*3/(np.pi*4))**(1/3)

temp = np.where((temp > 0) & (temp > thresholdvolumePt_ripe), 5000000, 0)
allplatinum = np.where(temp == 5000000, 1, 0)
allplatinumdilated = spim.morphology.binary_dilation(
    allplatinum, iterations=n).astype(float)
img2 = allplatinumdilated
img2[img2 > 0] = 12.6*(10**11)


temp = copy.deepcopy(vol_Pt_forPtripe)

unaffected = np.where((temp > 0) & (temp <= thresholdvolumePt_ripe) & (
    temp >= thresholdvolumePt_gone), 1, 0)
unaffected[unaffected > 0] = 12.6*(10**11)
img5 = unaffected


img_all = img3 + img2 - img4
img_all[img_all >= 12.6*(10**11)] = 12.6*(10**11)

boxsize = 640
onevoxel = 0.4
params_img2sas = boxsize, onevoxel

I_img_all, q_img_all = img2sas(img_all, params_img2sas)
I_img1, q_img1 = img2sas(img1, params_img2sas)

plt.figure(figsize=[5, 5])
plt.loglog(q_img_all, I_img_all/I_img1)
plt.loglog(q_img_all, I_img1/I_img1)
plt.loglog(qexp, Iexpdrymodif/Iexpdry)
plt.loglog(qexp, Iexpdry/Iexpdry)
plt.yscale('linear')
plt.ylim([0.6, 1.5])
plt.xlim([0.04, 3])


# %%


onevoxel = 0.4
img_tile_sec_Ptonly = copy.deepcopy(img_tile_sec)
img_tile_sec_Ptonly[img_tile_sec_Ptonly <= 12.3 * (10**11)] = 0
noPtatall = img_tile_sec-img_tile_sec_Ptonly


Pt_only = label(img_tile_sec_Ptonly)
label = np.ndarray.flatten(Pt_only)
label = np.unique(label)

# %%

vol_Pt_forPtripe = copy.deepcopy(vol_Pt_array)
vol_Pt_forPtgone = copy.deepcopy(vol_Pt_array)
vol_Pt_forPtburn = copy.deepcopy(vol_Pt_array)


thresholdvolumePt_burn = volume_Pt_sorted[5240]
thresholdvolumePt_gone = volume_Pt_sorted[4300]
thresholdvolumePt_ripe = volume_Pt_sorted[5280]
n = 10

n2 = 1

basefordilation = copy.deepcopy(img_tile_sec_Ptonly)
basefordilation[basefordilation > 0] = 1

rectanglestrel_v = np.zeros([3, 3, 3])
rectanglestrel_v[1:2, :, 1] = 1
rectanglestrel_v[:, :, 1] = 1
# structure=rectanglestrel_v,
allplatinumdilated = np.zeros([640, 640, 640])
allplatinum = np.zeros([640, 640, 640])


temp = copy.deepcopy(vol_Pt_forPtburn)
#thresholdvolumePt_burn = volume_Pt_sorted[3700]

#radiusthresholdvolumePt = (thresholdvolumePt*3/(np.pi*4))**(1/3)

temp = np.where((temp > 0) & (temp <= thresholdvolumePt_burn), 1000000, 0)
allplatinum = np.where(temp == 1000000, 1, 0)
dilated = spim.morphology.binary_dilation(
    allplatinum, iterations=n).astype(float)
allplatinumdilated = dilated - allplatinum

allplatinumdilated[allplatinumdilated > 1] = 1
mesopores = allplatinumdilated
mask = (mesopores == 1) & (img_tile_sec == 1.756 * (10**11))
imask = np.logical_not(mask)

img3 = np.where(imask, img_tile_sec, 0)
img1 = img_tile_sec
img1 = img1.astype(float)
img3 = img3.astype(float)
poreopenedwithoutPt = copy.deepcopy(img3)
poreopenedwithoutPt[poreopenedwithoutPt == 12.6*(10**11)] = 0


allplatinum = np.zeros([640, 640, 640])


temp = copy.deepcopy(vol_Pt_forPtgone)
#thresholdvolumePt_gone = volume_Pt_sorted[3700]

#radiusthresholdvolumePt = (thresholdvolumePt*3/(np.pi*4))**(1/3)

temp = np.where((temp > 0) & (temp <= thresholdvolumePt_gone), 5000000, 0)
allplatinum = np.where(temp == 5000000, 1, 0)

img4 = allplatinum
img4 = img4.astype('float')
img4[img4 > 0] = 12.6*(10**11)


allplatinum = np.zeros([640, 640, 640])


temp = copy.deepcopy(vol_Pt_forPtripe)
#thresholdvolumePt_ripe = volume_Pt_sorted[5200]

#radiusthresholdvolumePt = (thresholdvolumePt*3/(np.pi*4))**(1/3)

temp = np.where((temp > 0) & (temp >= thresholdvolumePt_ripe), 5000000, 0)
allplatinum = np.where(temp == 5000000, 1, 0)
allplatinumdilated = spim.morphology.binary_dilation(
    allplatinum, iterations=n2).astype(float)
img2 = allplatinumdilated
img2[img2 > 0] = 12.6*(10**11)


temp = copy.deepcopy(vol_Pt_forPtripe)

unaffected = np.where((temp > 0) & (temp <= thresholdvolumePt_ripe) & (
    temp >= thresholdvolumePt_gone), 1, 0)
unaffected[unaffected > 0] = 12.6*(10**11)
img5 = unaffected


img_all = img3 + img2 - img4
img_all[img_all >= 12.6*(10**11)] = 12.6*(10**11)

boxsize = 640
onevoxel = 0.4
params_img2sas = boxsize, onevoxel

I_img_all, q_img_all = img2sas(img_all, params_img2sas)
I_img1, q_img1 = img2sas(img1, params_img2sas)

# %%


def realspace(q):
    return 2*np.pi/q


def reciprocal(q):
    return 2*np.pi/q


fig, ax = plt.subplots(constrained_layout=True, figsize=[5, 5])

plt.figure(figsize=[5, 5])
ax.loglog(q_img_all, I_img_all/I_img1, 'bo',
          label='Numerical SAXS from 3D TEM')
ax.loglog(q_img_all, I_img1/I_img1)
ax.loglog(qexp, Iexpdrymodif/Iexpdry, 'b-', label='Experimental SAXS')
ax.loglog(qexp, Iexpdry/Iexpdry)
ax.set_yscale('linear')
ax.set_ylim([0.5, 1.5])
ax.set_xlim([0.04, 2])
K = find_scalingfactor(qexp, Iexpdry, q_img_all, I_img1, qval=0.2)
ax2 = ax.secondary_xaxis(location='top', functions=(reciprocal, realspace))
ax2.set_xlabel('Dimension [nm]')
ax.set_ylabel('Intensity Pt/KB-280 / Pt/KB')
ax.set_xlabel('q [1/nm]')
ax.legend(fontsize=10, loc='lower left')

fig, ax = plt.subplots(constrained_layout=True, figsize=[5, 5])

plt.figure(figsize=[5, 5])
ax.loglog(q_img_all, I_img1*K, 'rs', label='Numerical SAXS from 3D TEM Pt/KB')
ax.loglog(q_img_all, I_img_all*K, 'bo',
          label='Numerical SAXS from 3D TEM Simulated Pt/KB-280')

ax.loglog(qexp, Iexpdry, 'r-', label='Experimental SAXS Pt/KB')
ax.loglog(qexp, Iexpdrymodif, 'b-', label='Experimental SAXS Pt/KB-280')

ax.set_xlim([0.04, 2])
ax.set_ylim([10**0, 10**6])
ax2 = ax.secondary_xaxis(location='top', functions=(reciprocal, realspace))
ax2.set_xlabel('Dimension [nm]')
ax.set_ylabel('Intensity [a.u.]')
ax.set_xlabel('q [1/nm]')
ax.legend(fontsize=10, loc='lower left')

##
fig, ax = plt.subplots(constrained_layout=True, figsize=[5, 5])

plt.figure(figsize=[5, 5])
# ax.loglog(q_img_all,I_img_all/I_img1,'bo',label='Numerical SAXS from 3D TEM')
# ax.loglog(q_img_all,I_img1/I_img1)
ax.loglog(qexp, Iexpdrymodif/Iexpdry, 'b-', label='Experimental SAXS')
ax.loglog(qexp, Iexpdry/Iexpdry)
ax.set_yscale('linear')
ax.set_ylim([0.5, 1.5])
ax.set_xlim([0.04, 2])
K = find_scalingfactor(qexp, Iexpdry, q_img_all, I_img1, qval=0.2)
ax2 = ax.secondary_xaxis(location='top', functions=(reciprocal, realspace))
ax2.set_xlabel('Dimension [nm]')
ax.set_ylabel('Intensity Pt/KB-280 / Pt/KB')
ax.set_xlabel('q [1/nm]')
ax.legend(fontsize=10, loc='lower left')

fig, ax = plt.subplots(constrained_layout=True, figsize=[5, 5])

plt.figure(figsize=[5, 5])
# ax.loglog(q_img_all,I_img1*K,'rs',label='Numerical SAXS from 3D TEM Pt/KB')
# ax.loglog(q_img_all,I_img_all*K,'bo',label='Numerical SAXS from 3D TEM Simulated Pt/KB-280')

ax.loglog(qexp, Iexpdry, 'r-', label='Experimental SAXS Pt/KB')
ax.loglog(qexp, Iexpdrymodif, 'b-', label='Experimental SAXS Pt/KB-280')

ax.set_xlim([0.04, 2])
ax.set_ylim([10**0, 10**6])
ax2 = ax.secondary_xaxis(location='top', functions=(reciprocal, realspace))
ax2.set_xlabel('Dimension [nm]')
ax.set_ylabel('Intensity [a.u.]')
ax.set_xlabel('q [1/nm]')
ax.legend(fontsize=10, loc='lower left')

plt.imshow(img_all[300, :, :])
plt.imshow(img1[300, :, :])

# %%


# a=640
# img_tile_win_secwithmesopores_oxi  = img_tile_win_secwithmesopores_oxi.astype('float32')
# img_tile_win_secwithmesopores_oxi.shape = 1, a, 1, a, a, 1

# tif.imwrite('/mnt/nas_Uwrite/AK54/TEM_PtKB_mesopores_oxi2.tif', img_tile_win_secwithmesopores_oxi)


# a=640
# img_tile_win_secwithmesoporestile  = img_tile_win_secwithmesoporestile.astype('float32')
# img_tile_win_secwithmesoporestile.shape = 1, a, 1, a, a, 1

# tif.imwrite('/mnt/nas_Uwrite/AK54/TEM_PtKB_mesoporestile2.tif', img_tile_win_secwithmesoporestile)


# %%
params = 640, 0.4
#
I_img_tile_win, q_img_tile_win = img2sas(img_tile_win_sec, params)
I_img_tile, q_img_tile = img2sas(img_tile_sec, params)
img_lossofPt = copy.deepcopy(img_tile_win_secwithmesoporestile)


i = 0
randint_Pt = np.argwhere(img_lossofPt == 12.6*(10**11))
while i < 100000:

    randint_Pt_draw = random.randint(0, len(randint_Pt))
    a, b, c = randint_Pt[randint_Pt_draw]
    img_lossofPt[a, b, c] = 0
    print(i)
    i += 1


I_img_Ptloss_oxi, q_img_Ptloss_oxi = img2sas(img_lossofPt, params)
I_img_tile_win_secwithmesoporestile, q_img_tile_win_secwithmesoporestile = img2sas(
    img_tile_win_secwithmesoporestile, params)
I_img_tile_win_secwithmesopores_oxi, q_img_tile_win_secwithmesopores_oxi = img2sas(
    img_tile_win_secwithmesopores_oxi, params)
s = savgol_filter(K*I_img_tile_win, 7, 6)
kernel_size = 1
kernel = np.hanning(1)
s = np.convolve(K*I_img_tile_win, kernel, mode='same')
plt.loglog(q_img_tile_win, I_img_tile_win, q_img_tile, I_img_tile,  q_img_tile_win_secwithmesoporestile,
           I_img_tile_win_secwithmesoporestile, q_img_tile_win_secwithmesopores_oxi, I_img_tile_win_secwithmesopores_oxi)
# %%

K = find_scalingfactor(
    qexp, Iexpdry, q_img_tile_win_secwithmesoporestile, I_img_tile_win, qval=0.2)
params_plot = {'legend.fontsize': 10,
               'figure.figsize': (5, 5),
               'axes.labelsize': 20,
               'axes.titlesize': 20,
               'xtick.labelsize': 20,
               'ytick.labelsize': 20,
               'legend.loc': 'lower left'}


pylab.rcParams.update(params_plot)

plt.figure(figsize=[5, 5])
plt.loglog(q_img_tile_win_secwithmesoporestile, K *
           I_img_tile_win, linewidth=0.8, label='Initial')
plt.loglog(q_img_tile_win_secwithmesopores_oxi, K *
           I_img_tile_win_secwithmesoporestile, linewidth=0.8, label='After oxidation')
#plt.loglog(q_img_tile_win_secwithmesopores_oxi,K*I_img_Ptloss_oxi,label='After oxidation with Pt loss')
plt.xlim([0.05, 3])
plt.ylim([10**0, 10**6])
plt.legend()
plt.figure(figsize=[5, 5])
plt.loglog(qexp, Iexpdry, label='Initial', linewidth=0.8)
plt.loglog(qexp, Iexpdrymodif, label='After oxidation', linewidth=0.8)
plt.xlim([0.05, 3])
plt.ylim([10**0, 10**6])
plt.legend()


plt.figure(figsize=[5, 5])
#plt.loglog(q_img_tile_win_secwithmesoporestile,I_img_tile_win_secwithmesoporestile/I_img_tile_win,label='Simulation - Intensity Modified / Unmodified')
#plt.loglog(q_img_tile_win_secwithmesopores_oxi,I_img_Ptloss_oxi/I_img_tile_win,label='After oxidation with Pt loss')
plt.loglog(qexp, Iexpdrymodif/Iexpdry,
           label='Experiment - Intensity Modified / Unmodified')
plt.axhline((1))
plt.ylim([6*10**-1, 1.7*10**0])
# plt.loglog(qexp,Iexpdrymodif)
plt.legend()

# %%


img_tile_win_secwithmesoporestile_decane = copy.deepcopy(
    img_tile_win_secwithmesoporestile)
img_tile_win_secwithmesoporestile_decane[img_tile_win_secwithmesoporestile_decane == 0] = 7.156*(
    10**10)
img_tile_win_secwithmesopores_oxi_decane = copy.deepcopy(
    img_tile_win_secwithmesopores_oxi)
img_tile_win_secwithmesopores_oxi_decane[img_tile_win_secwithmesopores_oxi_decane == 0] = 7.156*(
    10**10)

I_img_tile_win_secwithmesoporestile_decane, q_img_tile_win_secwithmesoporestile_decane = img2sas(
    img_tile_win_secwithmesoporestile_decane, params)
I_img_tile_win_secwithmesopores_oxi_decane, q_img_tile_win_secwithmesopores_oxi_decane = img2sas(
    img_tile_win_secwithmesopores_oxi_decane, params)

plt.figure(figsize=[5, 5])
plt.loglog(q_img_tile_win_secwithmesoporestile,
           I_img_tile_win_secwithmesoporestile/10**28, label='Initial')
plt.loglog(q_img_tile_win_secwithmesopores_oxi,
           I_img_tile_win_secwithmesopores_oxi/10**28, label='After oxidation')
plt.loglog(q_img_tile_win_secwithmesoporestile_decane,
           I_img_tile_win_secwithmesoporestile_decane/10**28, label='Initial Decane')
plt.loglog(q_img_tile_win_secwithmesopores_oxi_decane,
           I_img_tile_win_secwithmesopores_oxi_decane/10**28, label='After oxidation Decane')
plt.legend()
plt.xlim([10**-1, 3])
plt.ylim([1, 10**6])
# %%


def tem_saxs_ave(a, b, c):

    d = 216
    img_sec = img_PtKB[a:a+d, b:b+d, c:c+d]
    img_sec = img_sec.astype('float32')
    # img_sec[img_sec == 217] = 12.6*(10**11)
    # img_sec[img_sec == 65] = 1.756 * (10**11)
    # img_sec[img_sec == 43] = 1.630 * (10**11)
    im_size = [d, 0.4]
    # 217 pt
    # 65 carbon
    # 43
    Int_TEM, q = img2sas(img_sec, im_size)
    whichlocation.append([a, b, c])
    q_img2sas_collection.append(q)
    radialmean_img2sas_collection.append(Int_TEM)
    K_dry = find_scalingfactor(qexp, Iexpdry, q, Int_TEM, 0.8)
    scalingfactor_collection.append(K_dry)
    print('done ' + str([a, b, c]))


i = 0

# %%
while i < 1000:
    a = random.randint(0, img_PtKB.shape[0]-200)
    b = random.randint(0, img_PtKB.shape[1]-200)
    c = random.randint(0, img_PtKB.shape[2]-200)
    tem_saxs_ave(a, b, c)
    i += 1

data = np.asarray(radialmean_img2sas_collection)
kin = np.average(data, axis=0)
data_q = np.asarray(q_img2sas_collection)
q_ave = np.average(data_q, axis=0)

data_q = np.asarray(scalingfactor_collection)
K_ave = np.average(data_q, axis=0)

# %%


#K_dry = find_scalingfactor(qexp, Iexpdry, q, Int_TEM, 0.8)


def realspace(q):
    return 2*np.pi/q


def reciprocal(q):
    return 2*np.pi/q


params_plot = {'legend.fontsize': 15,
               'figure.figsize': (5, 5),
               'axes.labelsize': 20,
               'axes.titlesize': 20,
               'xtick.labelsize': 20,
               'ytick.labelsize': 20,
               'legend.loc': 'lower left'}


pylab.rcParams.update(params_plot)

fig, ax = plt.subplots(constrained_layout=True)

ax.loglog(qexp, Iexpdry, 'go', markersize=3)

ax.loglog(q_ave, K_ave*kin, 'bo', markersize=3)

# for i in range(0,len(whichlocation)):
#     K_dry = find_scalingfactor(qexp, Iexpdry, q_img2sas_collection[i],radialmean_img2sas_collection[i], 0.8)
#     ax.loglog(q_img2sas_collection[i],radialmean_img2sas_collection[i])

# ax.loglog(data[:550,6],data[:550,0],'mo',markersize = 5, markevery=1, label='Experimental (wet)')

ax.set_xlim([10**-2, 10**1])
# ax.loglog(q[20:], y_fit,'b-',linewidth=4,label='Log-linear fit')

# ax.loglog(x2,Int_wet_img2sas*K_wet,'b-s',label='Simulation (wet)')
# ax.title('Wetting simulation (NNMC-decane)')
ax.set_xlabel('q [1/nm]')
ax.set_ylabel('Intensity [arb. u.]')
ax2 = ax.secondary_xaxis(location='top', functions=(reciprocal, realspace))
ax2.set_xlabel('Dimension [nm]')
plt.legend()

plt.show()


# %%

# # %%
# a = d
# img_sec = img_sec.astype('float32')
# img_sec.shape = 1, a, 1, a, a, 1

# tif.imwrite('/mnt/nas_Uwrite/AK54/TEM_PtKB_sec.tif', img_sec)

# from porespy.filters import find_peaks, trim_saddle_points, trim_nearby_peaks, region_size
# from skimage.morphology import watershed
# from porespy.tools import randomize_colors
# import matplotlib.colors as cl
# import numpy as np

# import scipy.ndimage as spim
# from porespy.filters import find_peaks, trim_saddle_points, trim_nearby_peaks, region_size
# from skimage.segmentation import watershed
# from porespy.tools import randomize_colors
# import matplotlib.colors as cl
# import copy
# img_PtKB_ = copy.deepcopy(img_PtKB)
# img_PtKB_[img_PtKB_>0]=1
# img_forwatershed = 1- img_PtKB_
# sigma = 0.8
# dt = spim.distance_transform_edt(input=img_forwatershed)
# dt = spim.gaussian_filter(input=dt, sigma=sigma)
# peaks = find_peaks(dt=dt)

# print('Initial number of peaks: ', spim.label(peaks)[1])
# peaks = trim_saddle_points(peaks=peaks, dt=dt, max_iters=500)
# print('Peaks after trimming saddle points: ', spim.label(peaks)[1])
# peaks = trim_nearby_peaks(peaks=peaks, dt=dt)
# peaks, N = spim.label(peaks)
# print('Peaks after trimming nearby peaks: ', N)
# regions = watershed(image=-dt, markers=peaks, mask=dt > 0)

# import scipy as sp

# def bbox_to_slices(bbox):
#     r"""
#     Given a tuple containing bounding box coordinates, return a tuple of slice
#     objects.

#     A bounding box in the form of a straight list is returned by several
#     functions in skimage, but these cannot be used to direct index into an
#     image.  This function returns a tuples of slices can be, such as:
#     ``im[bbox_to_slices([xmin, ymin, xmax, ymax])]``.

#     Parameters
#     ----------
#     bbox : tuple of ints
#         The bounding box indices in the form (``xmin``, ``ymin``, ``zmin``,
#         ``xmax``, ``ymax``, ``zmax``).  For a 2D image, simply omit the
#         ``zmin`` and ``zmax`` entries.

#     Returns
#     -------
#     slices : tuple
#         A tuple of slice objects that can be used to directly index into a
#         larger image.

#     Examples
#     --------
#     `Click here
#     <https://porespy.org/examples/tools/howtos/bbox_to_slices.html>`_
#     to view online example.

#     """
#     if len(bbox) == 4:
#         ret = (slice(bbox[0], bbox[2]),
#                slice(bbox[1], bbox[3]))
#     else:
#         ret = (slice(bbox[0], bbox[3]),
#                slice(bbox[1], bbox[4]),
#                slice(bbox[2], bbox[5]))
#     return ret


# def extract_regions(regions, image, labels: list, trim=True):
#     r"""
#     Combine given regions into a single boolean mask

#     Parameters
#     -----------
#     regions : ndarray
#         An image containing an arbitrary number of labeled regions
#     labels : array_like or scalar
#         A list of labels indicating which region or regions to extract
#     trim : bool
#         If ``True`` then image shape will trimmed to a bounding box around the
#         given regions.

#     Returns
#     -------
#     im : ndarray
#         A boolean mask with ``True`` values indicating where the given labels
#         exist

#     Examples
#     --------
#     `Click here
#     <https://porespy.org/examples/tools/howtos/extract_regions.html>`_
#     to view online example.

#     """
#     if type(labels) is int:
#         labels = [labels]
#     s = spim.find_objects(regions)
#     im_new = np.zeros_like(regions)
#     x_min, y_min, z_min = sp.inf, sp.inf, sp.inf
#     x_max, y_max, z_max = 0, 0, 0
#     for i in labels:
#         im_new[s[i - 1]] = regions[s[i - 1]] == i
#         x_min, x_max = min(s[i - 1][0].start, x_min), max(s[i - 1][0].stop, x_max)
#         y_min, y_max = min(s[i - 1][1].start, y_min), max(s[i - 1][1].stop, y_max)
#         if regions.ndim == 3:
#             z_min, z_max = min(s[i - 1][2].start, z_min), max(s[i - 1][2].stop, z_max)
#     if trim:
#         if regions.ndim == 3:
#             bbox = bbox_to_slices([x_min, y_min, z_min, x_max, y_max, z_max])
#         else:
#             bbox = bbox_to_slices([x_min, y_min, x_max, y_max])
#         im_new = im_new[bbox]
#         image = image[bbox]
#     return im_new, image


# boundingbox_mask = [[] for i in range(max(np.ndarray.flatten(regions)))]
# boundingbox_image= [[] for i in range(max(np.ndarray.flatten(regions)))]

# for i in range(max(np.ndarray.flatten(regions))):
#     boundingbox_mask[i], boundingbox_image[i] = extract_regions(regions,img_PtKB,labels=i)


# %%
